PROD_PATH=/srv/inv-creatives-sampling/
DEV_PATH=.

install:
	sudo rsync -rlptv --progress . $(PROD_PATH) --exclude-from exclude-list.txt
	sudo chmod 775 ./app/script/*

install_venv:
	sudo $(PROD_PATH)/app/script/build_venv.sh $(PROD_PATH) $(PROD_PATH)/requirements.txt


dev_install:
	rsync -rlptv --progress . $(DEV_PATH) --exclude-from exclude-list.txt
	chmod 775 ./app/script/*

dev_install_venv:
	sudo $(DEV_PATH)/app/script/build_venv.sh $(DEV_PATH) $(DEV_PATH)/requirements.txt