sql_template = """
with
r AS (SELECT UPPER(idfa) AS retargeting_auid, 
	cast(collector_tstamp_v2 as int) as collect_time, 
	cast(app_install_tstamp as int) as app_install_tstamp
FROM retargeting_streaming_{start_date}_{end_date}
WHERE site_id='{site_id}' AND length(idfa) > 0 AND idfa != '00000000-0000-0000-0000-000000000000'
AND action_id in ('install', 'Install') 
AND (reengagement_info.is_reengaged=False or reengagement_info.is_reengaged is NULL) and geo_country='{country}'
GROUP BY 1,2,3),
ub AS 
(SELECT 
app_bundle as bundle_id, 
app_id,
partner_id,
if(imp_is_instl=='TRUE', 1, 0) as imp_is_instl,
imp_type,
imp_height,
imp_width,
device_type,
app_type,
tagid,
if(is_rewarded=True, 1, 0) as is_rewarded,
UPPER(idfa) as idfa,
cast(time as int) as time
FROM unified_impbid_{country}_{start_date}_{end_date}
WHERE length(idfa) > 0 AND idfa != '00000000-0000-0000-0000-000000000000'),
j as (
select 
ub.bundle_id,
ub.app_id,
ub.partner_id,
ub.imp_is_instl,
ub.imp_type,
ub.imp_height,
ub.imp_width,
ub.device_type,
ub.app_type,
ub.tagid,
ub.is_rewarded,
ub.idfa,
ub.time,
r.retargeting_auid,
r.app_install_tstamp,
if((r.app_install_tstamp-ub.time) > 40 and (r.app_install_tstamp-ub.time) < 4000, r.retargeting_auid, NULL) as install_rt_idfa
from (
ub left outer join r
on ub.idfa=r.retargeting_auid
))
select
bundle_id,
app_id,
partner_id,
imp_is_instl,
imp_type,
imp_height,
imp_width,
device_type,
app_type,
tagid,
is_rewarded,
count(DISTINCT retargeting_auid) as rt_uu,
count(DISTINCT install_rt_idfa) as install_rt_uu,
count(DISTINCT idfa) as uu,
count(*) as bid_count
from
j
group by 1,2,3,4,5,6,7,8,9,10,11
having count(DISTINCT retargeting_auid) > 0
"""
