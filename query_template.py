sql_template = """
with
r AS (SELECT UPPER(idfa) AS retargeting_auid, cast(collector_tstamp_v2 as int) as collect_time, site_id
FROM retargeting_streaming_{start_date}_{end_date}
WHERE site_id='{site_id}' AND idfa IS NOT NULL AND action_id='{action_id}'
GROUP BY 1,2,3), 
e as (select
cid2oid(CMPID2STR(cmp_id)) as oid,
time2datestr(time) as date,
bytes2str(bundle_id) as bundle_id,
bytes2str(app_id) as app_id,  
partnerid2str(partner_id) as partner_id,
if(imp_is_instl, 1, 0) as imp_is_instl,
imp_type as imp_type,
imp_height as imp_height,
imp_width as imp_width,
device_type as device_type,
app_type as app_type,
bytes2str(tagid) as tagid,
if(isnotnull(bid_price_appier), 1, 0) as bid,
bytes2str(bid_appier_id) as bid_appier_id,
UPPER(idfa2str(idfa)) as idfa
FROM engaged_bidding 
where _dh >= TIMESTAMP '{start_date_v2} 00:00:00' and _dh < TIMESTAMP '{end_date_v2} 00:00:00' AND country2str(country)='{country}'
),
j as (select
click_time as click_time,
show_time as show_time,
rtb_cost(partner_id, imp_type, win_price_appier, show_time, win_time) AS cost,
if(ISNOTNULL(win_time), 1, 0) as win,
if(ISNOTNULL(show_time), 1, 0) as show, 
IF(ISNOTNULL(click_time) AND click_is_counted, 1, 0) AS click, 
size(filterActionsByType(cmp_id, actions, 'app_install')) as app_install,
bytes2str(bid_appier_id) as bid_appier_id
FROM imp_join_all2_{start_date}_{end_date}
where ISNOTNULL(country) AND 
ISNULL(is_external)
), 
imp as (
select
e.oid as oid,
e.date as date,
e.bundle_id as bundle_id,
e.app_id as app_id,
e.partner_id as partner_id,
e.idfa as idfa,
e.imp_is_instl as imp_is_instl,
e.imp_type as imp_type,
e.imp_height as imp_height,
e.imp_width as imp_width,
e.device_type as device_type,
e.app_type as app_type,
e.tagid as tagid,
j.click_time as click_time,
j.show_time as show_time,
j.cost as cost,
e.bid as bid,
j.win as win,
j.show as show,
j.click as click,
j.app_install as action
from e
left outer join j
on e.bid_appier_id = j.bid_appier_id
)
select
date,
bundle_id,
app_id,
partner_id,
imp_is_instl,
imp_type,
imp_height,
imp_width,
device_type,
app_type,
tagid,
sum(cost) as cost,
sum(bid) as bids,
sum(win) as wins, 
sum(show) as shows,
sum(click) as clicks,
sum(action) as actions,
count(DISTINCT idfa) as uu,
count(DISTINCT r.retargeting_auid) as rt_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.show_time<=r.collect_time and imp.show>0 then r.retargeting_auid end)) as shows_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.click_time<=r.collect_time and imp.click>0 then r.retargeting_auid end)) as clicks_uu
from imp
left outer join r
on imp.idfa = r.retargeting_auid
group by 1,2,3,4,5,6,7,8,9,10,11

"""

gs_sql_template = """
with
r AS (SELECT UPPER(idfa) AS retargeting_auid, cast(if(app_install_tstamp is not NULL, app_install_tstamp, 0) as int) as collect_time, site_id
FROM retargeting_streaming_{start_date}_{end_date}
WHERE site_id='{site_id}' AND idfa IS NOT NULL AND action_id='{action_id}'
GROUP BY 1,2,3), 
e as (select
cid2oid(CMPID2STR(cmp_id)) as oid,
time2datestr(time) as date,
bytes2str(bundle_id) as bundle_id,
bytes2str(app_id) as app_id,  
partnerid2str(partner_id) as partner_id, 
if(imp_is_instl, 1, 0) as imp_is_instl,
imp_type as imp_type,
imp_height as imp_height,
imp_width as imp_width,
device_type as device_type,
app_type as app_type,
bytes2str(tagid) as tagid,
if(isnotnull(bid_price_appier), 1, 0) as bid,
bytes2str(bid_appier_id) as bid_appier_id,
UPPER(idfa2str(idfa)) as idfa
FROM engaged_bidding 
where _dh >= TIMESTAMP '{start_date_v2} 00:00:00' and _dh < TIMESTAMP '{end_date_v2} 00:00:00' AND country2str(country)='{country}'
),
j as (select
click_time as click_time,
show_time as show_time,
rtb_cost(partner_id, imp_type, win_price_appier, show_time, win_time) AS cost,
if(ISNOTNULL(win_time), 1, 0) as win,
if(ISNOTNULL(show_time), 1, 0) as show, 
IF(ISNOTNULL(click_time) AND click_is_counted, 1, 0) AS click, 
size(filterActionsByType(cmp_id, actions, 'app_install')) as app_install,
bytes2str(bid_appier_id) as bid_appier_id
FROM imp_join_all2_{start_date}_{end_date}
where ISNOTNULL(country) AND 
ISNULL(is_external)
), 
imp as (
select
e.oid as oid,
e.date as date,
e.bundle_id as bundle_id,
e.app_id as app_id,
e.partner_id as partner_id,
e.idfa as idfa,
e.imp_is_instl as imp_is_instl,
e.imp_type as imp_type,
e.imp_height as imp_height,
e.imp_width as imp_width,
e.device_type as device_type,
e.app_type as app_type,
e.tagid as tagid,
j.click_time as click_time,
j.show_time as show_time,
j.cost as cost,
e.bid as bid,
j.win as win,
j.show as show,
j.click as click,
j.app_install as action
from e
left outer join j
on e.bid_appier_id = j.bid_appier_id
)
select
date,
bundle_id,
app_id,
partner_id,
imp_is_instl,
imp_type,
imp_height,
imp_width,
device_type,
app_type,
tagid,
sum(cost) as cost,
sum(bid) as bids,
sum(win) as wins, 
sum(show) as shows,
sum(click) as clicks,
sum(action) as actions,
count(DISTINCT idfa) as uu,
count(DISTINCT r.retargeting_auid) as rt_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.show_time<=r.collect_time and imp.show>0 then r.retargeting_auid end)) as shows_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.click_time<=r.collect_time and imp.click>0 then r.retargeting_auid end)) as clicks_uu
from imp
left outer join r
on imp.idfa = r.retargeting_auid
group by 1,2,3,4,5,6,7,8,9,10,11

"""

sql_template_v2 = """
with
r AS (SELECT UPPER(idfa) AS retargeting_auid, cast(collector_tstamp_v2 as int) as collect_time, site_id
FROM retargeting_streaming_{start_date}_{end_date}
WHERE site_id='{site_id}' AND idfa IS NOT NULL AND action_id in ('install', 'Install') AND 
(reengagement_info.is_reengaged=False or reengagement_info.is_reengaged is NULL)
GROUP BY 1,2,3), 
e as (select
cid2oid(CMPID2STR(cmp_id)) as oid,
time2datestr(time) as date,
bytes2str(bundle_id) as bundle_id,
bytes2str(app_id) as app_id,  
partnerid2str(partner_id) as partner_id,
if(imp_is_instl, 1, 0) as imp_is_instl,
imp_type as imp_type,
imp_height as imp_height,
imp_width as imp_width,
device_type as device_type,
app_type as app_type,
bytes2str(tagid) as tagid,
if(isnotnull(bid_price_appier), 1, 0) as bid,
bytes2str(bid_appier_id) as bid_appier_id,
UPPER(idfa2str(idfa)) as idfa
FROM engaged_bidding 
where _dh >= TIMESTAMP '{start_date_v2} 00:00:00' and _dh < TIMESTAMP '{end_date_v2} 00:00:00' AND LOWER(impreq_country)='{country}'
),
j as (select
click_time as click_time,
show_time as show_time,
rtb_cost(partner_id, imp_type, win_price_appier, show_time, win_time) AS cost,
if(ISNOTNULL(win_time), 1, 0) as win,
if(ISNOTNULL(show_time), 1, 0) as show, 
IF(ISNOTNULL(click_time) AND click_is_counted, 1, 0) AS click, 
size(filterActionsByType(cmp_id, actions, 'app_install')) as app_install,
bytes2str(bid_appier_id) as bid_appier_id
FROM imp_join_all2_{start_date}_{end_date}
where ISNOTNULL(country) AND 
ISNULL(is_external)
), 
imp as (
select
e.oid as oid,
e.date as date,
e.bundle_id as bundle_id,
e.app_id as app_id,
e.partner_id as partner_id,
e.idfa as idfa,
e.imp_is_instl as imp_is_instl,
e.imp_type as imp_type,
e.imp_height as imp_height,
e.imp_width as imp_width,
e.device_type as device_type,
e.app_type as app_type,
e.tagid as tagid,
j.click_time as click_time,
j.show_time as show_time,
j.cost as cost,
e.bid as bid,
j.win as win,
j.show as show,
j.click as click,
j.app_install as action
from e
left outer join j
on e.bid_appier_id = j.bid_appier_id
)
select
date,
bundle_id,
app_id,
partner_id,
imp_is_instl,
imp_type,
imp_height,
imp_width,
device_type,
app_type,
tagid,
sum(cost) as cost,
sum(bid) as bids,
sum(win) as wins, 
sum(show) as shows,
sum(click) as clicks,
sum(action) as actions,
sum(if(imp.oid='{oid}', cost, 0)) as _cost,
sum(if(imp.oid='{oid}', bid, 0)) as _bids,
sum(if(imp.oid='{oid}', win, 0)) as _wins, 
sum(if(imp.oid='{oid}', show, 0)) as _shows,
sum(if(imp.oid='{oid}', click, 0)) as _clicks,
sum(if(imp.oid='{oid}', action, 0)) as _actions,
count(DISTINCT idfa) as uu,
count(DISTINCT r.retargeting_auid) as rt_uu,
count(DISTINCT (case when imp.show_time<=r.collect_time and imp.show>0 then r.retargeting_auid end)) as country_shows_rt_uu,
count(DISTINCT (case when imp.click_time<=r.collect_time and imp.click>0 then r.retargeting_auid end)) as country_clicks_rt_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.show>0 then imp.idfa end)) as shows_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.click>0 then imp.idfa end)) as clicks_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.show_time<=r.collect_time and imp.show>0 then r.retargeting_auid end)) as shows_rt_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.click_time<=r.collect_time and imp.click>0 then r.retargeting_auid end)) as clicks_rt_uu
from imp
left outer join r
on imp.idfa = r.retargeting_auid
group by 1,2,3,4,5,6,7,8,9,10,11

"""

install_sql_template_v2 = """
with
r AS (SELECT UPPER(idfa) AS retargeting_auid, cast(if(app_install_tstamp is not NULL, app_install_tstamp, cast(collector_tstamp_v2 as int)) as int) as collect_time, site_id
FROM retargeting_streaming_{start_date}_{end_date}
WHERE site_id='{site_id}' AND idfa IS NOT NULL AND action_id in ('install', 'Install') AND 
(reengagement_info.is_reengaged=False or reengagement_info.is_reengaged is NULL)
GROUP BY 1,2,3), 
e as (select
cid2oid(CMPID2STR(cmp_id)) as oid,
time2datestr(time) as date,
bytes2str(bundle_id) as bundle_id,
bytes2str(app_id) as app_id,  
partnerid2str(partner_id) as partner_id, 
if(imp_is_instl, 1, 0) as imp_is_instl,
imp_type as imp_type,
imp_height as imp_height,
imp_width as imp_width,
device_type as device_type,
app_type as app_type,
bytes2str(tagid) as tagid,
if(isnotnull(bid_price_appier), 1, 0) as bid,
bytes2str(bid_appier_id) as bid_appier_id,
UPPER(idfa2str(idfa)) as idfa
FROM engaged_bidding 
where _dh >= TIMESTAMP '{start_date_v2} 00:00:00' and _dh < TIMESTAMP '{end_date_v2} 00:00:00' AND LOWER(impreq_country)='{country}'
),
j as (select
click_time as click_time,
show_time as show_time,
rtb_cost(partner_id, imp_type, win_price_appier, show_time, win_time) AS cost,
if(ISNOTNULL(win_time), 1, 0) as win,
if(ISNOTNULL(show_time), 1, 0) as show, 
IF(ISNOTNULL(click_time) AND click_is_counted, 1, 0) AS click, 
size(filter(actions, x -> actionType(cmpid2str(cmp_id), x.action_id) = 'app_install')) AS app_install,
bytes2str(bid_appier_id) as bid_appier_id
FROM imp_join_all2_{start_date}_{end_date}
where ISNOTNULL(country) AND 
ISNULL(is_external)
), 
imp as (
select
e.oid as oid,
e.date as date,
e.bundle_id as bundle_id,
e.app_id as app_id,
e.partner_id as partner_id,
e.idfa as idfa,
e.imp_is_instl as imp_is_instl,
e.imp_type as imp_type,
e.imp_height as imp_height,
e.imp_width as imp_width,
e.device_type as device_type,
e.app_type as app_type,
e.tagid as tagid,
j.click_time as click_time,
j.show_time as show_time,
j.cost as cost,
e.bid as bid,
j.win as win,
j.show as show,
j.click as click,
j.app_install as action
from e
left outer join j
on e.bid_appier_id = j.bid_appier_id
)
select
date,
bundle_id,
app_id,
partner_id,
imp_is_instl,
imp_type,
imp_height,
imp_width,
device_type,
app_type,
tagid,
sum(cost) as cost,
sum(bid) as bids,
sum(win) as wins, 
sum(show) as shows,
sum(click) as clicks,
sum(action) as actions,
sum(if(imp.oid='{oid}', cost, 0)) as _cost,
sum(if(imp.oid='{oid}', bid, 0)) as _bids,
sum(if(imp.oid='{oid}', win, 0)) as _wins, 
sum(if(imp.oid='{oid}', show, 0)) as _shows,
sum(if(imp.oid='{oid}', click, 0)) as _clicks,
sum(if(imp.oid='{oid}', action, 0)) as _actions,
count(DISTINCT idfa) as uu,
count(DISTINCT r.retargeting_auid) as rt_uu,
count(DISTINCT (case when imp.show_time<=r.collect_time and imp.show>0 then r.retargeting_auid end)) as country_shows_rt_uu,
count(DISTINCT (case when imp.click_time<=r.collect_time and imp.click>0 then r.retargeting_auid end)) as country_clicks_rt_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.show>0 then imp.idfa end)) as shows_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.click>0 then imp.idfa end)) as clicks_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.show_time<=r.collect_time and imp.show>0 then r.retargeting_auid end)) as shows_rt_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.click_time<=r.collect_time and imp.click>0 then r.retargeting_auid end)) as clicks_rt_uu
from imp
left outer join r
on imp.idfa = r.retargeting_auid
group by 1,2,3,4,5,6,7,8,9,10,11
"""

install_sql_template_jp = """
with
r AS (SELECT UPPER(idfa) AS retargeting_auid, cast(if(app_install_tstamp is not NULL, app_install_tstamp, cast(collector_tstamp_v2 as int)) as int) as collect_time, site_id
FROM retargeting_streaming_{start_date}_{end_date}
WHERE site_id='{site_id}' AND idfa IS NOT NULL AND action_id in ('install', 'Install') AND 
(reengagement_info.is_reengaged=False or reengagement_info.is_reengaged is NULL)
GROUP BY 1,2,3), 
e as (select
cid2oid(CMPID2STR(cmp_id)) as oid,
time2datestr(time) as date,
bytes2str(bundle_id) as bundle_id,
bytes2str(app_id) as app_id,  
partnerid2str(partner_id) as partner_id, 
if(imp_is_instl, 1, 0) as imp_is_instl,
imp_type as imp_type,
imp_height as imp_height,
imp_width as imp_width,
device_type as device_type,
app_type as app_type,
bytes2str(tagid) as tagid,
if(isnotnull(bid_price_appier), 1, 0) as bid,
bytes2str(bid_appier_id) as bid_appier_id,
UPPER(idfa2str(idfa)) as idfa
FROM engaged_bidding 
where _dh >= TIMESTAMP '{start_date_v2} 00:00:00' and _dh < TIMESTAMP '{end_date_v2} 00:00:00' AND LOWER(impreq_country)='{country}'
),
j as (select
click_time as click_time,
show_time as show_time,
rtb_cost(partner_id, imp_type, win_price_appier, show_time, win_time) AS cost,
if(ISNOTNULL(win_time), 1, 0) as win,
if(ISNOTNULL(show_time), 1, 0) as show, 
IF(ISNOTNULL(click_time) AND click_is_counted, 1, 0) AS click, 
size(filter(actions, x -> actionType(cmpid2str(cmp_id), x.action_id) = 'app_install')) AS app_install,
bytes2str(bid_appier_id) as bid_appier_id
FROM imp_join_all2_{start_date}_{end_date}
where ISNOTNULL(country) AND 
ISNULL(is_external)
), 
imp as (
select
e.oid as oid,
e.date as date,
e.bundle_id as bundle_id,
e.app_id as app_id,
e.partner_id as partner_id,
e.idfa as idfa,
e.imp_is_instl as imp_is_instl,
e.imp_type as imp_type,
e.imp_height as imp_height,
e.imp_width as imp_width,
e.device_type as device_type,
e.app_type as app_type,
e.tagid as tagid,
j.click_time as click_time,
j.show_time as show_time,
j.cost as cost,
e.bid as bid,
j.win as win,
j.show as show,
j.click as click,
j.app_install as action
from e
left outer join j
on e.bid_appier_id = j.bid_appier_id
),
imp_drop as (
select
* from imp tablesample (50 percent)
)
select
date,
bundle_id,
app_id,
partner_id,
imp_is_instl,
imp_type,
imp_height,
imp_width,
device_type,
app_type,
tagid,
sum(cost) as cost,
sum(bid) as bids,
sum(win) as wins, 
sum(show) as shows,
sum(click) as clicks,
sum(action) as actions,
sum(if(imp_drop.oid='{oid}', cost, 0)) as _cost,
sum(if(imp_drop.oid='{oid}', bid, 0)) as _bids,
sum(if(imp_drop.oid='{oid}', win, 0)) as _wins, 
sum(if(imp_drop.oid='{oid}', show, 0)) as _shows,
sum(if(imp_drop.oid='{oid}', click, 0)) as _clicks,
sum(if(imp_drop.oid='{oid}', action, 0)) as _actions,
count(DISTINCT idfa) as uu,
count(DISTINCT r.retargeting_auid) as rt_uu,
count(DISTINCT (case when imp_drop.show_time<=r.collect_time and imp_drop.show>0 then r.retargeting_auid end)) as country_shows_rt_uu,
count(DISTINCT (case when imp_drop.click_time<=r.collect_time and imp_drop.click>0 then r.retargeting_auid end)) as country_clicks_rt_uu,
count(DISTINCT (case when imp_drop.oid='{oid}' and imp_drop.show>0 then imp_drop.idfa end)) as shows_uu,
count(DISTINCT (case when imp_drop.oid='{oid}' and imp_drop.click>0 then imp_drop.idfa end)) as clicks_uu,
count(DISTINCT (case when imp_drop.oid='{oid}' and imp_drop.show_time<=r.collect_time and imp_drop.show>0 then r.retargeting_auid end)) as shows_rt_uu,
count(DISTINCT (case when imp_drop.oid='{oid}' and imp_drop.click_time<=r.collect_time and imp_drop.click>0 then r.retargeting_auid end)) as clicks_rt_uu
from imp_drop
left outer join r
on imp_drop.idfa = r.retargeting_auid
group by 1,2,3,4,5,6,7,8,9,10,11
"""

sql_template_size = """
with
r AS (SELECT UPPER(idfa) AS retargeting_auid, cast(if(app_install_tstamp is not NULL, app_install_tstamp, cast(collector_tstamp_v2 as int)) as int) as collect_time, site_id
FROM retargeting_streaming_{start_date}_{end_date}
WHERE site_id='{site_id}' AND idfa IS NOT NULL AND action_id in ('install', 'Install') AND 
(reengagement_info.is_reengaged=False or reengagement_info.is_reengaged is NULL)
GROUP BY 1,2,3), 
e as (select
cid2oid(CMPID2STR(cmp_id)) as oid,
time2datestr(time) as date,
bytes2str(bundle_id) as bundle_id,
bytes2str(app_id) as app_id,  
partnerid2str(partner_id) as partner_id, 
if(imp_is_instl, 1, 0) as imp_is_instl,
imp_type as imp_type,
imp_height as imp_height,
imp_width as imp_width,
device_type as device_type,
app_type as app_type,
bytes2str(tagid) as tagid,
if(isnotnull(bid_price_appier), 1, 0) as bid,
bytes2str(bid_appier_id) as bid_appier_id,
UPPER(idfa2str(idfa)) as idfa
FROM engaged_bidding 
where _dh >= TIMESTAMP '{start_date_v2} 00:00:00' and _dh < TIMESTAMP '{end_date_v2} 00:00:00' AND LOWER(impreq_country)='{country}'
AND (imp_width, imp_height) in ({size_tuple})
),
j as (select
click_time as click_time,
show_time as show_time,
rtb_cost(partner_id, imp_type, win_price_appier, show_time, win_time) AS cost,
if(ISNOTNULL(win_time), 1, 0) as win,
if(ISNOTNULL(show_time), 1, 0) as show, 
IF(ISNOTNULL(click_time) AND click_is_counted, 1, 0) AS click,
size(filter(actions, x -> actionType(cmpid2str(cmp_id), x.action_id) = 'app_install')) AS app_install,
bytes2str(bid_appier_id) as bid_appier_id
FROM imp_join_all2_{start_date}_{end_date}
where ISNOTNULL(country) AND 
ISNULL(is_external)
), 
imp as (
select
e.oid as oid,
e.date as date,
e.bundle_id as bundle_id,
e.app_id as app_id,
e.partner_id as partner_id,
e.idfa as idfa,
e.imp_is_instl as imp_is_instl,
e.imp_type as imp_type,
e.imp_height as imp_height,
e.imp_width as imp_width,
e.device_type as device_type,
e.app_type as app_type,
e.tagid as tagid,
j.click_time as click_time,
j.show_time as show_time,
j.cost as cost,
e.bid as bid,
j.win as win,
j.show as show,
j.click as click,
j.app_install as action
from e
left outer join j
on e.bid_appier_id = j.bid_appier_id
)
select
date,
bundle_id,
app_id,
partner_id,
imp_is_instl,
imp_type,
imp_height,
imp_width,
device_type,
app_type,
tagid,
sum(cost) as cost,
sum(bid) as bids,
sum(win) as wins, 
sum(show) as shows,
sum(click) as clicks,
sum(action) as actions,
sum(if(imp.oid='{oid}', cost, 0)) as _cost,
sum(if(imp.oid='{oid}', bid, 0)) as _bids,
sum(if(imp.oid='{oid}', win, 0)) as _wins, 
sum(if(imp.oid='{oid}', show, 0)) as _shows,
sum(if(imp.oid='{oid}', click, 0)) as _clicks,
sum(if(imp.oid='{oid}', action, 0)) as _actions,
count(DISTINCT idfa) as uu,
count(DISTINCT r.retargeting_auid) as rt_uu,
count(DISTINCT (case when imp.show_time<=r.collect_time and imp.show>0 then r.retargeting_auid end)) as country_shows_rt_uu,
count(DISTINCT (case when imp.click_time<=r.collect_time and imp.click>0 then r.retargeting_auid end)) as country_clicks_rt_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.show>0 then imp.idfa end)) as shows_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.click>0 then imp.idfa end)) as clicks_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.show_time<=r.collect_time and imp.show>0 then r.retargeting_auid end)) as shows_rt_uu,
count(DISTINCT (case when imp.oid='{oid}' and imp.click_time<=r.collect_time and imp.click>0 then r.retargeting_auid end)) as clicks_rt_uu
from imp
left outer join r
on imp.idfa = r.retargeting_auid
group by 1,2,3,4,5,6,7,8,9,10,11
"""