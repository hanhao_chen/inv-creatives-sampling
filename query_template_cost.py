imp_cost_template = """
select 
cid2oid(CMPID2STR(cmp_id)) as oid,
time2datestr(time) as date,
bytes2str(bundle_id) as bundle_id,
bytes2str(app_id) as app_id,  
partnerid2str(partner_id) as partner_id, 
if(imp_is_instl, 1, 0) as imp_is_instl,
imp_type as imp_type,
imp_height as imp_height,
imp_width as imp_width,
device_type as device_type,
app_type as app_type,
bytes2str(tagid) as tagid,
sum(rtb_cost(partner_id, imp_type, win_price_appier, show_time, win_time)) AS cost,
sum(if(ISNOTNULL(show_time), 1, 0)) as shows, 
sum(IF(ISNOTNULL(click_time) AND click_is_counted, 1, 0)) AS clicks,
sum(size(filterActionsByType(cmp_id, actions, 'app_install'))) as actions
FROM imp_join_all2_{start_date}_{end_date}
where cid2oid(CMPID2STR(cmp_id)) in ({oid}) AND ISNOTNULL(country) AND 
ISNULL(is_external)
group by 1,2,3,4,5,6,7,8,9,10,11,12
"""