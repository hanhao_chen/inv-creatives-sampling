import argparse
import pandas as pd
from app.utils import get_env
from app.config import V1Config, init_db_clients
from app.module.CQueryDB import CQueryDB
from app.module.CEndpointIdash import CEndpointIdash
from app.utils.init_logger import init_logger
from app.external_optimization.deep_funnel.utils.interact_mysql_tables import UserListTableCenter

logger = init_logger(__name__)

def update_userlist(inv_table, idash_endpoint):
    sample_list = inv_table.get_sample_list()
    sample_df = pd.DataFrame(sample_list)
    cids = set(sample_df['cid'].values)

    for cid in cids:
        s_df = sample_df[sample_df['cid'] == cid]
        s_list = s_df.to_dict('index').values()
        logger.info('update cid: {}, sample inv: {}'.format(cid, len(s_list)))
        enhance_rules = inv_table.gen_enhance_rules(cid, s_list)
        idash_endpoint.send_update_data_request(cid, enhance_rules)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))
    idash_endpoint = CEndpointIdash(cfg.idash_cfg)

    inv_table = UserListTableCenter(idash_endpoint, query_db, cfg.inv_list_control)
    update_userlist(inv_table, idash_endpoint)

if __name__ == '__main__':
    main()