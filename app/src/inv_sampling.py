import argparse
import pandas as pd
import numpy as np
import arrow
import json
import constants as c
from constants import IMP_SIZE_SET
from collections import defaultdict

from app.utils.db import MySQL_DB_AIBE_DEV
from app.utils import get_env
from app.utils.base_tool import get_inv_query_dict, get_adjust_dict, \
    format_inv_key, format_inv_key_wo_app_type, format_idash_inv, lookupImpSizeGroup, \
    convert_inv_val_mp, format_inv, format_inv_val, format_inv_beethoven, inv_adjust_hash, \
    convert_inv_info_mp
from app.config import V1Config, init_db_clients, close_db_clients
from app.module.CQueryDB import CQueryDB
from app.module.CEndpointIdash import CEndpointIdash
from app.external_optimization.deep_funnel.utils.init_mysql_tables import InvTables
from app.utils.init_logger import init_logger
from app.external_optimization.deep_funnel.utils.interact_mysql_tables import InvTableCenter
from app.src.update_status import update_complete, update_spending
from app.utils.base_tool import adjust_weight
from app.utils.test_inventory import test_inventory

logger = init_logger(__name__)


def adjust_goal2weight(cfg, inv_table, cid):
    ini_tables = InvTables(inv_table.query_db, inv_table.df_db_client)
    ini_tables.init_inv_goal_value_table(cfg.inv_list_control['goal_adjust_table'])
    result = inv_table.get_cid_goal_value_status(cid)

    oid = inv_table.get_oid(cid)
    goal_cpm = inv_table.get_cid_goal_cpm(cid)
    if len(result) == 0:
        inv_table.insert_cid_goal(cid, goal_cpm)
    else:
        inv_table.update_cid_goal(cid, goal_cpm)
        his_goal_cpm = result[0]['goal_cpm']
        scale = 1. / (goal_cpm / his_goal_cpm)
        logger.info('Goal CPM: %f -> %f', his_goal_cpm, goal_cpm)
        if scale == 1:
            return

        candidate_inv = inv_table.get_topn_inv(cid, oid)
        adjust_rules_scale = []

        # if scale != 1:
        for inv in candidate_inv:
            logger.info('exchange: %s, app_id: %s, adjust_val: %f -> %f',
                        inv['exchange'], inv['app_id'], inv['adjust_weight'], inv['adjust_weight']*scale)
            inv_table.update_adjust_weight(oid, cid, inv, inv['adjust_weight']*scale)

            adjust_dict = get_adjust_dict(cid, inv, inv['adjust_weight']*scale)
            adjust_rules_scale.append(adjust_dict)


        r = inv_table.adjust_weight(cid, adjust_rules_scale)
        logger.info('update scale weight, %s', r)


def adjust_inv_status(idash_endpoint, inv_table, cid, reset_weight=False, freeze_weight=False, hf_table=False):

    if hf_table:
        freeze_weight = True

    oid = inv_table.get_oid(cid)
    country = inv_table.get_country(cid)
    goal_cpc = inv_table.get_goal_cpc(cid)
    thres_cost = 5 * inv_table.get_goal_cpa(oid) / 10000000. if c.score_type_mp.get(cid) != 'cpi' else 4.
    if thres_cost > 15:
        thres_cost = 15

    invs_info = inv_table.gather_oid_cost(oid, hf_table)
    invs_val = inv_table.gather_cid_adjust_val(cid)
    val_mp = convert_inv_val_mp(invs_val)
    info_mp = convert_inv_info_mp(invs_info)

    logger.info('oid: %s, cost threshold: %f', oid, thres_cost)
    inv_status = inv_table.gather_cid_invs(cid)

    adjust_rules_reset = []
    adjust_rules_win = []
    adjust_rules_cost = []

    for inv in inv_status:
        """
        win < 10% increase weight
        """
        inv['imp_size_group'] = lookupImpSizeGroup(inv)
        inv['exchange'] = inv['partner_id'].split('_')[0]
        inv['win_rate'] = float(inv['shows']) / float(inv['bids'])

        key = format_inv(inv)
        val_key = format_inv_val(inv)
        inv['cost'] = info_mp['cost'].get(key, 0)
        inv['shows'] = info_mp['shows'].get(key, 0)
        inv['clicks'] = info_mp['clicks'].get(key, 0)
        inv['adjust_val'] = val_mp.get(val_key, 0)
        query_inv = get_inv_query_dict(inv, oid, cid)
        cpm = inv['cost'] / inv['shows'] * 1000 if inv['shows'] > 0 else 0
        cpc = inv['cost'] / inv['clicks'] if inv['clicks'] > 0 else 0

        if inv['adjust_val'] > 0:
            logger.info('exchange: %s, app_id: %s, win_rate: %f, cost: %f, adjust_val: %f',
                        inv['exchange'], inv['app_id'], inv['win_rate'], inv['cost'], inv['adjust_val'])

        if reset_weight:
            adjust_dict = get_adjust_dict(cid, inv, 0.)
            adjust_rules_reset.append(adjust_dict)
            if inv['adjust_val'] > 0 and not freeze_weight:
                inv_table.update_adjust_weight(oid, cid, query_inv, 0)

        elif inv['win_rate'] < 0.4:
            adjust_val = inv['adjust_val'] * 1.5
            adjust_val = adjust_val if adjust_val < 32. else 32.
            adjust_dict = get_adjust_dict(cid, inv, adjust_val)
            adjust_rules_win.append(adjust_dict)
            if inv['adjust_val'] > 0 and not freeze_weight:
                inv_table.update_adjust_weight(oid, cid, query_inv, adjust_val)

        elif inv['win_rate'] >= 0.6:
            adjust_val = inv['adjust_val'] * 0.8
            adjust_dict = get_adjust_dict(cid, inv, adjust_val)
            adjust_rules_win.append(adjust_dict)
            if inv['adjust_val'] > 0 and not freeze_weight:
                inv_table.update_adjust_weight(oid, cid, query_inv, adjust_val)

        """
        set weight to 0 for cost > thres, and set to complete in table
        """
        if inv['cost'] >= thres_cost:
            adjust_dict = get_adjust_dict(cid, inv, 0)
            adjust_rules_cost.append(adjust_dict)

            inv_table.update_adjust_weight(oid, cid, query_inv, 0)
            inv_table.update_complete_inv(oid, cid, query_inv)
            logger.info('Set to complete')

        if country in ("kr", "th") and cpm > 0 and test_inventory(cpm, inv['clicks'], inv['cost'], goal_cpc)['result']:
            adjust_dict = get_adjust_dict(cid, inv, 0)
            adjust_rules_cost.append(adjust_dict)

            inv_table.update_adjust_weight(oid, cid, query_inv, 0)
            inv_table.update_complete_inv(oid, cid, query_inv)
            logger.info('Early Stop, app_id: %s, tagid: %s, goal cpc: %f, cpm: %f, cpc: %f',
                        inv['app_id'], inv['tagid'], goal_cpc, cpm, cpc)

    """
    get candidate from table and set whitelist to adgroup
    """
    if reset_weight:
        _ = inv_table.adjust_weight(cid, adjust_rules_reset)
        logger.info('Reset weight to 1')

    if not reset_weight and not freeze_weight:
        r = inv_table.adjust_weight(cid, adjust_rules_win)
        logger.info('Adjust weight, increase 1.1, %s', r)

    if not reset_weight and not freeze_weight:
        r = inv_table.adjust_weight(cid, adjust_rules_cost)
        logger.info('Set weight to zero, %s', r)

    candidate_inv = inv_table.get_topn_inv(cid, oid)

    if len(candidate_inv) > 0:
        enhance_rules = inv_table.gen_enhance_rules(cid, candidate_inv)
        idash_endpoint.send_update_data_request(cid, enhance_rules)

        adjust_rules_init = inv_table.init_inv_weight(candidate_inv, oid, cid, reset_weight)

        r = inv_table.adjust_weight(cid, adjust_rules_init)
        logger.info('Init candidate weight, %s', r)
    else:
        config = idash_endpoint.read_campaign_fields(cid, ('campaign_other_type', 'daily_max_budget', 'ext_bid_price'))
        idash_endpoint.send_cid_enable_request(cid, config['campaign_other_type'], False)

    """
    TODO set not candidate creatives to zero
    note 1. this part should be frequently check, sinece we don't have all combination of creatives
            currently beethoven perday update twice per day.
    """
    adjust_rules_close = []
    cn_set = set()

    same_inv = inv_table.gather_same_inv(oid, candidate_inv)
    for cn_inv in candidate_inv:
        cn_set.add(inv_adjust_hash(cn_inv))
    for s_inv in same_inv:
        key = inv_adjust_hash(s_inv)
        if key not in cn_set:
            adjust_dict = get_adjust_dict(cid, s_inv, 0)
            adjust_rules_close.append(adjust_dict)

    r = inv_table.adjust_weight(cid, adjust_rules_close)
    logger.info('close not candidate creatives %s', r)

    adjust_rules_close = []
    cn_set = set()

    beethoven_inv = inv_table.gather_oid_inv_beethoven(oid)
    for cn_inv in candidate_inv:
        cn_set.add(inv_adjust_hash(cn_inv))
    for s_inv in beethoven_inv:
        key = inv_adjust_hash(s_inv)
        if key not in cn_set:
            adjust_dict = get_adjust_dict(cid, s_inv, 0)
            adjust_rules_close.append(adjust_dict)

    r = inv_table.adjust_weight(cid, adjust_rules_close)
    logger.info('close none candidate creatives (beethoven) %s', r)

    adjust_rules_close = []
    cn_set = set()
    for cn_inv in candidate_inv:
        cn_set.add(inv_adjust_hash(cn_inv))
    for s_inv in invs_val:
        key = inv_adjust_hash(s_inv)
        if key not in cn_set:
            adjust_dict = get_adjust_dict(cid, s_inv, 0)
            adjust_rules_close.append(adjust_dict)

    r = inv_table.adjust_weight(cid, adjust_rules_close)
    logger.info('close none candidate creatives (beethoven adjust) %s', r)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    parser.add_argument('--oid_config', type=str, required=False, default='oid_config.json')
    parser.add_argument('--oids', type=str, nargs='+', help='oids')
    parser.add_argument('--cids', type=str, nargs='+', help='cids')
    parser.add_argument('--country', type=str, help='country')
    parser.add_argument('--date', default=None, type=str, help='run on date YYYYmmdd. (default: None=now)')
    parser.add_argument('--reset_weight', action='store_true')
    parser.add_argument('--freeze_weight', action='store_true')
    parser.add_argument('--hf_table', action='store_true')
    parser.add_argument('--goal_update', action='store_true')
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))
    idash_endpoint = CEndpointIdash(cfg.idash_cfg)

    inv_table = InvTableCenter(idash_endpoint, query_db, cfg.inv_list_control)

    cids = args.cids
    for cid in cids:
        if args.goal_update:
            adjust_goal2weight(cfg, inv_table, cid)
        else:
            update_complete(idash_endpoint, inv_table, cid, args.hf_table)
            update_spending(inv_table, cid, args.hf_table)

            adjust_inv_status(idash_endpoint, inv_table, cid, args.reset_weight, args.freeze_weight, args.hf_table)
            adjust_goal2weight(cfg, inv_table, cid)


if __name__ == '__main__':
    main()