import argparse
import pandas as pd


from app.utils import get_env
from app.config import V1Config, init_db_clients
from app.module.CQueryDB import CQueryDB
from app.module.CEndpointIdash import CEndpointIdash
from app.utils.init_logger import init_logger
from app.external_optimization.deep_funnel.utils.interact_mysql_tables import InvDynamicTableCenter

logger = init_logger(__name__)

def upload_whitelist(cid, inv_table, idash_endpoint):
    df_invs = pd.read_csv('tmp/whitelist.csv',
                          dtype={'tagid': str, 'app_id': str, 'partner_id': str})
    df_invs.fillna({'app_id': "", 'tagid': "", 'partner_id': ""}, inplace=True)
    df_invs['dealid'] = ""
    df_invs['deal_count'] = 0
    candidate_inv = df_invs.to_dict(orient='records')

    enhance_rules = inv_table.gen_enhance_rules(cid, candidate_inv)
    idash_endpoint.send_update_data_request(cid, enhance_rules)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    parser.add_argument('--oid_config', type=str, required=False, default='oid_config.json')
    parser.add_argument('--oids', type=str, nargs='+', help='oids')
    parser.add_argument('--cid', type=str, help='cid')
    parser.add_argument('--country', type=str, help='country')
    parser.add_argument('--date', default=None, type=str, help='run on date YYYYmmdd. (default: None=now)')
    parser.add_argument('--reset_weight', action='store_true')
    parser.add_argument('--freeze_weight', action='store_true')
    parser.add_argument('--hf_table', action='store_true')
    parser.add_argument('--goal_update', action='store_true')
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))
    idash_endpoint = CEndpointIdash(cfg.idash_cfg)

    inv_table = InvDynamicTableCenter(idash_endpoint, query_db, cfg.inv_list_control)

    upload_whitelist(args.cid, inv_table, idash_endpoint)

if __name__ == '__main__':
    main()