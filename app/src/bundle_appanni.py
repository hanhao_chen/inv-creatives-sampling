import argparse
import pandas as pd

from collections import defaultdict
from app.utils.db import MySQL_DB_AIBE_DEV
from app.utils import get_env
from app.config import V1Config, init_db_clients
from app.module.CQueryDB import CQueryDB
from app.external_optimization.deep_funnel.utils.init_mysql_tables import InvTables
from app.module.CEndpointIdash import CEndpointIdash
from app.module.CEndpointLogService import CEndpointLogService
from app.external_optimization.deep_funnel.utils.interact_mysql_tables import InvDynamicTableCenter
from app.module.Appanni import get_usage_app_history, get_rating, get_appannie, get_app_details, get_product_info
from app.utils.init_logger import init_logger

logger = init_logger(__name__)


def bundle_filter(bundle_list, exclude_bundle, is_ios=False):
    results = []
    for row in bundle_list:
        if row['bundle_id'] != "" and row['bundle_id'] not in exclude_bundle:
            if is_ios:
                try:
                    row['bundle_id'] = str(int(row['bundle_id']))
                    results.append(row)
                except:
                    continue
            else:
                results.append(row)

    return results

def update_appanni_cate_info_and(country, inv_table, logservice_endpoint, path='./tmp/bundle_list.csv'):
    appanni_db = inv_table.collect_appanni_cate_bundle()
    exclude_bundle = set([str(row['bundle_id']) for row in appanni_db])

    bundle_list = pd.read_csv(path)
    bundle_list = [{'bundle_id': str(bundle)} for bundle in bundle_list['bundle_id'].values]
    bundle_list = bundle_filter(bundle_list, exclude_bundle)
    bundles = [row['bundle_id'] for row in bundle_list]

    for i in range(0, len(bundles), 45):
        tmp_bundles = bundles[i:i + 45]
        piddict = logservice_endpoint.get_prd_id("android", tmp_bundles)
        pids = [str(v) for k, v in piddict.items() if v is not None and v!=""]
        logger.info('bundle: {}, {}'.format(i, i + 45))

        pinfo = get_product_info('android', pids)
        for bundle in tmp_bundles:
            pid = str(piddict.get(bundle, ""))
            if pid == "":
                continue
            info = pinfo.get(pid)
            if info is not None:
                category = []
                if info['main_category']:
                    main_category = info['main_category']
                    category.append(main_category)
                if info['other_categories']:
                    other_categories = sorted([cat for cat in info['other_categories'].split(',') if cat.lower() != 'overall'])
                    categories = ','.join(other_categories)
                    if len(other_categories) > 0:
                        category.append(categories)
                bundle_dict = {'bundle_id': bundle,
                               'product_id': bundle,
                               'country': country.upper(),
                               'unified_category_path': ' > '.join(category)}
                inv_table.insert_appanni_cate([bundle_dict])

def update_appanni_cate_info_ios(country, inv_table, path='./tmp/bundle_list.csv'):
    appanni_db = inv_table.collect_appanni_cate_pid()
    exclude_bundle = set([str(row['product_id']) for row in appanni_db])


    bundle_list = pd.read_csv(path)
    bundle_list = [{'bundle_id': str(bundle)} for bundle in bundle_list['bundle_id'].values]
    bundle_list = bundle_filter(bundle_list, exclude_bundle, True)
    bundles = [row['bundle_id'] for row in bundle_list]

    for i in range(0, len(bundles), 45):
        tmp_bundles = bundles[i:i+45]
        logger.info('bundle: {}, {}'.format(i, i+45))

        pinfo = get_product_info('ios', tmp_bundles)
        for bundle in tmp_bundles:
            info = pinfo.get(bundle)
            if info is not None:
                category = []
                if info['main_category']:
                    main_category = info['main_category']
                    category.append(main_category)
                if info['other_categories']:
                    other_categories = sorted([cat for cat in info['other_categories'].split(',') if cat.lower() != 'overall'])
                    categories = ','.join(other_categories)
                    if len(other_categories) > 0:
                        category.append(categories)
                bundle_dict = {'bundle_id': bundle,
                               'product_id': bundle,
                               'country': country.upper(),
                               'unified_category_path': ' > '.join(category)}
                inv_table.insert_appanni_cate([bundle_dict])
        # import pdb; pdb.set_trace()

def update_appanni_cate_bundle_ios(country, inv_table, path='./tmp/bundle_list.csv'):
    appanni_db = inv_table.collect_appanni_cate_pid()
    exclude_bundle = set([row['product_id'] for row in appanni_db])

    # country = inv_table.get_country(cid)
    # oid = inv_table.get_oid(cid)

    bundle_list = pd.read_csv(path)
    bundle_list = [{'bundle_id': bundle} for bundle in bundle_list['bundle_id'].values]
    bundle_list = bundle_filter(bundle_list, exclude_bundle)

    bundles = [row['bundle_id'] for row in bundle_list]

    for idx, bundle in enumerate(bundles):
        prdid = bundle
        logger.info('progress: {}, bundle: {}, product_id: {}'.format(idx, bundle, prdid))
        bundle_dict = {'bundle_id': bundle,
                       'product_id': bundle,
                       'country': country.upper()}
        if prdid != "":
            cate_path = get_app_details("ios", [prdid], col="unified_category_path").get(prdid)
            bundle_dict.update({"unified_category_path": cate_path})

        inv_table.insert_appanni_cate([bundle_dict])

def update_appanni_cate_bundle(country, inv_table, logservice_endpoint):
    appanni_db = inv_table.collect_appanni_cate_bundle()
    exclude_bundle = set([row['bundle_id'] for row in appanni_db])

    # country = inv_table.get_country(cid)
    # oid = inv_table.get_oid(cid)

    bundle_list = pd.read_csv('./tmp/bundle_list.csv')
    bundle_list = [{'bundle_id': bundle} for bundle in bundle_list['bundle_id'].values]
    bundle_list = bundle_filter(bundle_list, exclude_bundle)

    bundles = [row['bundle_id'] for row in bundle_list]

    bundle2prdids = {}
    for i in range(0, len(bundles), 15):
        prds = logservice_endpoint.get_prd_id("android", bundles[i:i + 15])
        bundle2prdids.update(prds)
    import pdb;pdb.set_trace()
    for idx, bundle in enumerate(bundles):
        prdid = bundle2prdids.get(bundle, "")
        logger.info('progress: {}, bundle: {}, product_id: {}'.format(idx, bundle, prdid))
        bundle_dict = {'bundle_id': bundle,
                       'product_id': bundle2prdids.get(bundle, ''),
                       'country': country.upper()}
        if prdid != "":
            cate_path = get_app_details("android", [prdid], col="unified_category_path").get(prdid)
            bundle_dict.update({"unified_category_path": cate_path})

        inv_table.insert_appanni_cate([bundle_dict])

def update_appanni_cate(cid, inv_table, logservice_endpoint):
    appanni_db = inv_table.collect_appanni_cate_bundle()
    exclude_bundle = set([row['bundle_id'] for row in appanni_db])

    country = inv_table.get_country(cid)
    oid = inv_table.get_oid(cid)
    bundle_list = inv_table.get_ctcv_bundle_list(oid)
    bundle_list = bundle_filter(bundle_list, exclude_bundle)

    bundles = [row['bundle_id'] for row in bundle_list]
    bundle2prdids = {}
    for i in range(0, len(bundles), 15):
        prds = logservice_endpoint.get_prd_id("android", bundles[i:i + 15])
        bundle2prdids.update(prds)

    for idx, bundle in enumerate(bundles):
        prdid = bundle2prdids.get(bundle, "")
        logger.info('progress: {}, bundle: {}, product_id: {}'.format(idx, bundle, prdid))
        bundle_dict = {'bundle_id': bundle,
                       'product_id': bundle2prdids.get(bundle, ''),
                       'country': country.upper()}
        if prdid != "":
           cate_path = get_app_details("android", [prdid], col="unified_category_path").get(prdid)
           bundle_dict.update({"unified_category_path": cate_path})

        inv_table.insert_appanni_cate([bundle_dict])

def update_appanni_table(cid, inv_table, logservice_endpoint):

    appanni_db = inv_table.collect_appanni_bundle()
    exclude_bundle = set([row['bundle_id'] for row in appanni_db])

    country = inv_table.get_country(cid)
    oid = inv_table.get_oid(cid)
    bundle_list = inv_table.get_ctcv_bundle_list(oid)
    bundle_list = bundle_filter(bundle_list, exclude_bundle)

    bundles = [row['bundle_id'] for row in bundle_list]
    bundle2prdids = {}
    for i in range(0, len(bundles), 15):
        prds = logservice_endpoint.get_prd_id("android", bundles[i:i+15])
        bundle2prdids.update(prds)

    for idx, bundle in enumerate(bundles):
        prdid = bundle2prdids.get(bundle, "")
        logger.info('progress: {}, bundle: {}, product_id: {}'.format(idx, bundle, prdid))
        bundle_dict = {'bundle_id': bundle,
                       'product_id': bundle2prdids.get(bundle, ''),
                       'country': country.upper()}
        if prdid != "":
            usage_list = ['usage_penetration', 'install_penetration', 'open_rate', 'avg_sessions_per_user',
                          'avg_session_duration', 'avg_time_per_user']
            usage_value = defaultdict(float)
            usage = get_usage_app_history("android", [prdid], country.upper()).get(prdid)
            if usage:
                for device in usage:
                    for key in usage_list:
                        if device[key] != 'n/a':
                            usage_value[key] += device[key]

                usage_value = {k:v/len(usage) for k, v in usage_value.items()}
                bundle_dict.update(usage_value)

            rating_list = ['star_1_count', 'star_2_count', 'star_3_count', 'star_4_count', 'star_5_count',
                           'average']
            rating_value = defaultdict(float)
            rating = get_rating("android", [prdid]).get(prdid)
            if rating:
                count = 0
                for _country in rating:
                    all_ratings = _country.get('all_ratings')
                    if isinstance(all_ratings, dict):
                        count += 1
                        for key in rating_list:
                            rating_value[key] += all_ratings[key]
                rating_value = {k:v/count for k, v in rating_value.items()}
                bundle_dict.update(rating_value)

        # query_data.append(bundle_dict)

        inv_table.insert_appanni([bundle_dict])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--local', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    parser.add_argument('--oid_config', type=str, required=False, default='oid_config.json')
    parser.add_argument('--oids', type=str, nargs='+', help='oids')
    parser.add_argument('--cids', type=str, nargs='+', help='cids')
    parser.add_argument('--country', type=str, help='country')
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))
    idash_endpoint = CEndpointIdash(cfg.idash_cfg)
    logservice_endpoint = CEndpointLogService(cfg.logservice_cfg)

    df_db_client_dev = query_db.db_clients[MySQL_DB_AIBE_DEV]
    appanni_table = cfg.inv_list_control['appanni_info']
    appanni_cate = cfg.inv_list_control['appanni_cate']
    inv_tables = InvTables(query_db, df_db_client_dev)

    inv_tables.init_appanni_info_table(appanni_table)
    inv_tables.init_appanni_cate(appanni_cate)


    inv_table = InvDynamicTableCenter(idash_endpoint, query_db, cfg.inv_list_control)

    # update_appanni_cate_bundle("tw", inv_table, logservice_endpoint)
    # update_appanni_cate_info_ios('tw', inv_table)
    # update_appanni_cate_info_ios('tw', inv_table, './tmp/tw_top.csv')
    update_appanni_cate_info_ios('tw', inv_table, './tmp/OP top spending - bundles.csv')

    # update_appanni_cate_info_and('kr', inv_table, logservice_endpoint, './tmp/mopub_bundlelist.csv')


    # update_appanni_cate_bundle_ios("tw", inv_table)
    # update_appanni_cate_bundle_ios("hk", inv_table, './tmp/bundle_list2.csv')
    # for cid in args.cids:
        # update_appanni_table(cid, inv_table, logservice_endpoint)
        # update_appanni_cate(cid, inv_table, logservice_endpoint)