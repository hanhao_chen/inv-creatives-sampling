import argparse
import pandas as pd
import numpy as np
import arrow
import json
import constants as c
from constants import IMP_SIZE_SET
from collections import defaultdict

import hashlib

from app.utils.db import MySQL_DB_AIBE_DEV
from app.utils import get_env
from app.utils.base_tool import get_inv_query_dict, get_adjust_dict, convert_inv_info_mp, \
    format_inv_key_wo_app_type, format_inv, inv_adjust_hash, format_inv_key_raw
from app.config import V1Config, init_db_clients, close_db_clients
from app.module.CQueryDB import CQueryDB
from app.module.CEndpointIdash import CEndpointIdash
from app.external_optimization.deep_funnel.utils.init_mysql_tables import InvTables
from app.utils.init_logger import init_logger
from app.external_optimization.deep_funnel.utils.interact_mysql_tables import InvDynamicTableCenter
from app.utils.base_tool import adjust_weight
from app.utils.test_inventory import test_inventory

logger = init_logger(__name__)


def hash_df(row):
    hash_str = format_inv_key_raw(row)
    sha1 = hashlib.md5(hash_str.encode('utf-8'))
    return sha1.hexdigest()[:7]


def update_spending(inv_table, cid):

    oid = inv_table.get_oid(cid)
    thres_cost = 5 * inv_table.get_goal_cpa(oid) / 10000000. if c.score_type_mp.get(cid) != 'cpi' else 4.
    if thres_cost > 15:
        thres_cost = 15

    invs_cost = inv_table.gather_oid_cost(oid)
    cost_mp = convert_inv_info_mp(invs_cost)

    candidate_inv = inv_table.get_topn_inv(cid, oid, topn=-1, all=True)
    for inv in candidate_inv:
        key = format_inv(inv)
        inv_cost = cost_mp.get(key, 0)
        inv_table.update_spending(oid, cid, inv, inv_cost, thres_cost)


def update_status(inv_table, cid, hf_table, level=0, goal_type='price', enable_early_stop=True):

    oid = inv_table.get_oid(cid)
    # goal_cpc = inv_table.get_goal_cpc(cid) if inv_table.get_goal_cpc(cid) else 0.03
    if cid in ['shrk9SrQQ-i_XEtmeFT8Zg', 'lPqXHKqWTzmhZ_zyrgZi8w']:
        thres_cost = 2 * inv_table.get_goal_cpa(oid) / 1e7
    else:
        thres_cost = 5 * inv_table.get_goal_cpa(oid) / 10000000.
    if cid == 'jG_MepCUQfOxuUVt2kyA-w':
        thres_cost = 3
    if thres_cost > 15:
        thres_cost = 15

    country = inv_table.get_country(cid)
    invs_info = inv_table.gather_oid_info(oid, hf_table, level=level)
    info_mp = convert_inv_info_mp(invs_info)

    candidate_inv = inv_table.get_complete_inv(cid, oid)
    for inv in candidate_inv:
        key = format_inv(inv)
        inv_cost = info_mp['cost'].get(key)
        inv_show = info_mp['shows'].get(key, 0)
        inv_click = info_mp['clicks'].get(key, 0)
        inv_install = info_mp['installs'].get(key, 0)
        cpm = inv_cost / inv_show * 1000 if inv_show > 0 else 0
        if inv_cost is None:
            logger.info('exchange: %s, app_id: %s, cost: NOT FOUND', inv['exchange'], inv['app_id'])
            inv_cost = 0
        elif inv_show is None:
            logger.info('exchange: %s, app_id: %s, show: NOT FOUND', inv['exchange'], inv['app_id'])
            inv_show = 0
        else:
            logger.info('cost: %f', inv_cost)

        logger.info('exchange: %s, app_id: %s, cost: %f', inv['exchange'], inv['app_id'], inv_cost)
        if goal_type == 'price':
            inv_table.update_complete_status(oid, cid, inv, inv_cost, thres_cost, goal_type, inv_install)
        elif goal_type == 'shows':
            inv_table.update_complete_status(oid, cid, inv, inv_show, 1/(inv['score'] * 0.7), goal_type, inv_install)

    # update status for inv_ctcv_sampling
    candidate_inv = inv_table.get_topn_inv(cid, oid, topn=-1)
    for inv in candidate_inv:
        key = format_inv(inv)
        inv_cost = info_mp['cost'].get(key)
        inv_show = info_mp['shows'].get(key, 0)
        inv_click = info_mp['clicks'].get(key, 0)
        inv_install = info_mp['installs'].get(key, 0)
        cpm = inv_cost / inv_show * 1000 if inv_show > 0 else 0
        cpc = inv_cost / inv_click if inv_click > 0 else 0

        if inv_cost is None:
            logger.info('exchange: %s, app_id: %s, cost: NOT FOUND',
                        inv['exchange'], inv['app_id'])
            inv_cost = 0
        elif inv_show is None:
            logger.info('exchange: %s, app_id: %s, show: NOT FOUND',
                        inv['exchange'], inv['app_id'])
            inv_show = 0
        else:
            logger.info('cost: %f', inv_cost)
        # query_inv = get_inv_query_dict(inv, oid, cid)
        # inv_table.update_complete_inv(oid, cid, query_inv, inv_cost)
        logger.info('exchange: %s, app_id: %s, cost: %f',
                    inv['exchange'], inv['app_id'], inv_cost)
        if goal_type == 'price':
            inv_table.update_sampling_status(oid, cid, inv, inv_cost, thres_cost, goal_type, inv_install)
        elif goal_type == 'shows':
            inv_table.update_sampling_status(oid, cid, inv, inv_show, 1/(inv['score'] * 0.7), goal_type, inv_install)

        # if enable_early_stop and inv['sampling'] == 1 and cpm > 0 \
        #     and test_inventory(cpm, inv_click, inv_cost, goal_cpc)['result'] and inv['checked'] == 0:
        #     inv['status'] = inv_cost
        #     inv_table.delete_sampling_inv(oid, cid, inv)
        #     inv_table.insert_complete_inv(inv, 'early_stop')
        #     continue

        # if inv_cost >= goal_cpc * 10:
        #     inv_table.update_checked(oid, cid, inv)


def update_complete(inv_table, cid):
    oid = inv_table.get_oid(cid)
    thres_cpa = 1.5 * inv_table.get_goal_cpa(oid) / 1e7

    candidate_inv = inv_table.get_complete_inv(cid, oid)
    for inv in candidate_inv:
        cpa = inv['status'] / inv['installs'] if inv['installs'] > 0 else np.inf
        if inv['status'] < inv['goal']:
            logger.info("Delete from completion table")
            inv_table.delete_complete_inv(oid, cid, inv)
        elif cpa < thres_cpa:
            logger.info("Save back from completion table")
            inv_table.delete_complete_inv(oid, cid, inv)
            inv['sampling'] = 1
            inv_table.insert_sampling_inv([inv])

    candidate_inv = inv_table.get_topn_inv(cid, oid, topn=-1)
    for inv in candidate_inv:
        cpa = inv['status'] / inv['installs'] if inv['installs'] > 0 else np.inf
        if inv['status'] >= inv['goal'] and cpa >= thres_cpa:
            logger.info('reach goal delete')
            inv_table.delete_sampling_inv(oid, cid, inv)

            inv_table.insert_complete_inv(inv, 'reach_goal')
            # adjust_dict = get_adjust_dict(cid, inv, 0.)
            # adjust_rules_complete.append(adjust_dict)


def fill_ctr(df, mapping, indices=None, value=1e-3):
    if indices:
        mapping = mapping.groupby(indices).agg({"shows": "sum", "clicks": "sum"}).astype("float")
        mapping["ctr"] = mapping["clicks"] / mapping["shows"]
        df = df.set_index(indices)
        df["ctr"] = df["ctr"].fillna(mapping["ctr"])
        df = df.reset_index()
    else:
        df["ctr"] = df["ctr"].fillna(value)
    return df


def update_sample_list(idash_endpoint, inv_table, cid_dict, site_id):
    is_updated = False
    cid = cid_dict['cid']
    country = inv_table.get_country(cid)
    oid = inv_table.get_oid(cid)
    imp_type = cid_dict["imp_type"]
    is_rewarded = cid_dict['is_rewarded']
    ssp = cid_dict.get('ssp')
    ignore_ctr = cid_dict["is_vtcv_campaign"]

    try:
        creatives = inv_table.get_cid_creatives_idash(cid)
    except Exception:
        creatives = []
    new_sample_list_rt_uu = inv_table.get_site_id_country_inv_rt_uu(site_id, country, imp_type, is_rewarded, ssp=ssp)
    new_sample_list_rt_uu = pd.DataFrame(new_sample_list_rt_uu)

    if ignore_ctr:
        # ignore ctr if campaign is vtcv
        new_sample_list_ctr = pd.DataFrame()
    else:
        new_sample_list_ctr = inv_table.get_site_id_country_inv_ctr(cid, site_id, country, imp_type,
                                                                    is_rewarded, creatives)
        new_sample_list_ctr = pd.DataFrame(new_sample_list_ctr)

    if new_sample_list_rt_uu.empty:
        # skip generating sampling list while the rt_uu is empty
        logger.error("[Error] oid={}, sampling list is empty since there are {} ctr data and {} rt_uu data".format(
            oid, len(new_sample_list_ctr), len(new_sample_list_rt_uu)
        ))
        return is_updated

    if new_sample_list_ctr.empty:
        if ignore_ctr:
            logger.info("[Warning] oid={}, ignore ctr data since it is a vtcv campaign".format(oid))
        else:
            logger.error("[Error] oid={}, ctr data is empty, {} ctr data and {} rt_uu data".format(
                oid, len(new_sample_list_ctr), len(new_sample_list_rt_uu)
            ))
        new_sample_list = new_sample_list_rt_uu
        new_sample_list["ctr"] = 1.0
        indices = ["cost", "shows", "clicks", "cpm", "country_clicks_rt_uu", "country_shows_rt_uu"]
        new_sample_list[indices] = pd.DataFrame([np.zeros(len(indices))], index=new_sample_list.index)
    else:
        indices = ["partner_id", "bundle_id", "app_id", "tagid", "app_type", "device_type",
                   "imp_is_instl", "imp_height", "imp_width", "imp_type", "is_rewarded", "inv_hash"]
        new_sample_list = pd.merge(new_sample_list_rt_uu, new_sample_list_ctr, how="left", on=indices)

        # handle missing value for ctr
        new_sample_list = fill_ctr(new_sample_list,
                                   new_sample_list_ctr,
                                   indices=["partner_id", "bundle_id", "app_id", "imp_type"])
        new_sample_list = fill_ctr(new_sample_list,
                                   new_sample_list_ctr,
                                   indices=["partner_id", "bundle_id", "imp_type"])
        new_sample_list = fill_ctr(new_sample_list,
                                   new_sample_list_ctr,
                                   indices=["partner_id", "imp_type"])
        new_sample_list = fill_ctr(new_sample_list, new_sample_list_ctr)
        indices = ["cost", "shows", "clicks", "cpm", "country_clicks_rt_uu", "country_shows_rt_uu"]
        new_sample_list[indices] = new_sample_list[indices].fillna(0.0)

    new_sample_list["oid"] = oid

    # compute new score = ctr * rt_ratio * log(uu)
    new_sample_list["rt_ratio"] = new_sample_list["rt_uu"] / new_sample_list["uu"]
    new_sample_list["score"] = new_sample_list["score"] * new_sample_list["ctr"]
    new_sample_list.sort_values("score", ascending=False, inplace=True)
    new_sample_list.reset_index(drop=True, inplace=True)

    # re-hash inventory
    new_sample_list = new_sample_list.to_dict("record")
    for inv in new_sample_list:
        inv["inv_hash"] = hash_df(inv)

    inv_rank = {}
    for idx, inv in enumerate(new_sample_list):
        inv_rank[(inv['inv_hash'], inv['tagid'], inv['is_rewarded'])] = idx+1

    keep_sample_inv = []
    current_sample_list = inv_table.get_topn_inv(cid, oid, topn=-1)

    for inv in current_sample_list:
        if inv['sampling'] == 1:
            rank = inv_rank.get((inv['inv_hash'], inv['tagid'], inv['is_rewarded']), 999999)
            if rank <= 150:
                keep_sample_inv.append(inv)
            else:
                inv_table.insert_complete_inv(inv, 'out_of_rank:{}'.format(rank))

    keep_sample_hash = set([(inv['inv_hash'], inv['tagid'], inv['is_rewarded']) for inv in keep_sample_inv])

    selected_sample_inv = []
    complete_inv_list = inv_table.get_complete_inv(cid, oid, complete_reason='reach_goal')
    complete_inv_hash = set([(inv['inv_hash'], inv['tagid'], inv['is_rewarded']) for inv in complete_inv_list])
    exist_inv_hash = keep_sample_hash | complete_inv_hash
    for inv in new_sample_list:
        if (inv['inv_hash'], inv['tagid'], inv['is_rewarded']) not in exist_inv_hash:
            inv['cid'] = cid
            selected_sample_inv.append(inv)

    inv_table.delete_invs(cid, oid)

    inv_table.insert_sampling_inv(keep_sample_inv)
    inv_table.insert_sampling_inv(selected_sample_inv)

    sample_count = 3

    new_sample_count = sample_count - len(keep_sample_inv)
    candidate_inv = keep_sample_inv
    if new_sample_count > 0:
        topn_non_sample_inv = inv_table.get_topn_non_sampling_inv(cid, oid, topn=new_sample_count)
        topn_non_sample_inv = topn_non_sample_inv[:new_sample_count]

        candidate_inv = candidate_inv + list(topn_non_sample_inv)

    if len(candidate_inv) > 0:
        use_tag_id = False if imp_type == 2 else True
        df = pd.DataFrame(candidate_inv)

        sampling_info = inv_table.get_inv_sampling_info(oid=oid, cid=cid)
        has_info = len(sampling_info) > 0
        if has_info:
            previous_candidate_inv = json.loads(sampling_info[0]['sampling_list'])
            previous_df = pd.DataFrame(previous_candidate_inv)
            columns = ["partner_id", "bundle_id", "app_id", "tagid"]
            is_same = set(map(tuple, df[columns].values)) == set(map(tuple, previous_df[columns].values))
        else:
            is_same = False

        if not has_info or not is_same:
            enhance_rules = inv_table.gen_enhance_rules(cid, candidate_inv, use_tag_id=use_tag_id)
            idash_endpoint.send_update_data_request(cid, enhance_rules)
            inv_table.remove_out_of_rank(oid, cid, candidate_inv)

            if not has_info:
                # if there exist no information and campaign is disabled, start running this cid of inventory sampling
                config = idash_endpoint.read_campaign_fields(cid, ('campaign_other_type', 'enabled'))
                if config['enabled'].lower() == "false":
                    idash_endpoint.send_cid_enable_request(cid, config['campaign_other_type'], True)

            # snapshot of sampling list
            columns = ["partner_id", "bundle_id", "app_id", "tagid", "app_type", "device_type",
                       "imp_is_instl", "imp_height", "imp_width", "imp_type", "is_rewarded", "inv_hash"]
            records = df[columns].to_dict(orient="records")
            ssp = json.dumps(ssp) if ssp else ""
            sampling_list = json.dumps(records)
            inv_table.insert_inv_sampling_info(oid=oid,
                                               cid=cid,
                                               country=country,
                                               imp_type=imp_type,
                                               is_rewarded=is_rewarded,
                                               ssp=ssp,
                                               sampling_size=sample_count,
                                               sampling_list=sampling_list)
            is_updated = True
            # adjust_rules_init = inv_table.init_inv_weight(candidate_inv, oid, cid)
            # r = inv_table.adjust_weight(cid, adjust_rules_init)
            # logger.info('Init candidate weight, %s', r)
    else:
        config = idash_endpoint.read_campaign_fields(cid, ('campaign_other_type', 'daily_max_budget', 'ext_bid_price'))
        idash_endpoint.send_cid_enable_request(cid, config['campaign_other_type'], False)
    return is_updated


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    parser.add_argument('--oid_config', type=str, required=False, default='oid_config.json')
    parser.add_argument('--oids', type=str, nargs='+', help='oids')
    parser.add_argument('--cids', type=str, nargs='+', help='cids')
    parser.add_argument('--country', type=str, help='country')
    parser.add_argument('--date', default=None, type=str, help='run on date YYYYmmdd. (default: None=now)')
    parser.add_argument('--type', default='update_complete', type=str)
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))
    idash_endpoint = CEndpointIdash(cfg.idash_cfg)

    df_db_client_dev = query_db.db_clients[MySQL_DB_AIBE_DEV]
    c_table = cfg.inv_list_control['inv_complete_table']
    s_table = cfg.inv_list_control['inv_sampling_table']
    inv_tables = InvTables(query_db, df_db_client_dev)

    inv_tables.init_inv_complete_table_table(c_table)
    inv_tables.init_inv_sampling_table_table(s_table)

    inv_table = InvDynamicTableCenter(idash_endpoint, query_db, cfg.inv_list_control)

    cids = args.cids

    if args.type == 'update_complete':
        for cid in cids:
            update_complete(idash_endpoint, inv_table, cid)
    elif args.type == 'update_status':
        for cid in cids:
            update_status(inv_table, cid)


if __name__ == '__main__':
    main()
