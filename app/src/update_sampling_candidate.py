import argparse
import pandas as pd
import numpy as np
import hashlib
import json

from app.utils.db import MySQL_DB_AIBE_DEV
from app.utils import get_env
from app.config import V1Config, init_db_clients, close_db_clients
from app.module.CQueryDB import CQueryDB
from app.external_optimization.deep_funnel.utils.init_mysql_tables import InvTables
from app.utils.init_logger import init_logger
from app.utils.base_tool import lookupImpSizeGroup, convert_rt_ratio, \
    convert_cpa, convert_ctr, convert_exchange, convert_cpm, convert_tmp_app_type
from app.utils.base_tool import format_inv_key_raw

logger = init_logger(__name__)
dup_keys = ['start_date', 'end_date', 'oid']

def update_inv_table(query_db, df_db_client, table_name, df):
    print(df.columns)
    for _, row in df.iterrows():
        query = """INSERT INTO {} 
            ({}, `update_time`) 
            VALUES ({}, CURRENT_TIMESTAMP)""".format(
            table_name,
            ','.join(['`{}`'.format(k) for k in df.columns if row[k] is not np.nan]),
            ','.join(["'{}'".format(v) for v in row.values if v is not np.nan])
        )
        # logger.info(query)
        query_db.execute_query(query, db_client=df_db_client)


def hash_df(row):
    hash_str = format_inv_key_raw(row)
    sha1 = hashlib.md5(hash_str.encode('utf-8'))
    return sha1.hexdigest()[:7]


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    parser.add_argument('--exchange_file', type=str, required=False, default='partnerid_exchangeid.json')
    parser.add_argument('--oid_config', type=str, required=False, default='oid_config.json')
    parser.add_argument('--oids', type=str, nargs='+', help='oids')
    parser.add_argument('--country', type=str, help='country')
    parser.add_argument('--date', default=None, type=str, help='run on date YYYYmmdd. (default: None=now)')
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))
    df_db_client = query_db.db_clients[MySQL_DB_AIBE_DEV]

    inv_tables = InvTables(query_db, df_db_client)
    table_config = {
        'inv-creative-table': cfg.inv_list_control['inv_creative_table']
    }
    inv_tables.init_tables_from_config(table_config)

    with open(args.exchange_file, "r") as f:
        exchange_mp = json.loads(f.read())

    # df = pd.read_csv('tmp/864_ios_vp.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = 'WqvPQ27vQaK3LHzeBLNZeA'
    # df['cid'] = '_ZgdP39wRyqtr-kEqhMG_A'
    # df['score_type'] = 'vp_bundle'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)
    #
    # df = pd.read_csv('tmp/864_and_vp.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = '8UqNB8jgRyWTGRPrXDHGlA'
    # df['cid'] = 'dacwUmowQ8KcLgq8ID4seQ'
    # df['score_type'] = 'vp_bundle'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)
    #
    # df = pd.read_csv('tmp/gs_vp.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = '6ydXFLjFT9iwe9WKxL9GmA'
    # df['cid'] = '3Dbh8E12QHW2fEnetj3P9A'
    # df['score_type'] = 'vp_bundle'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)
    #
    # df = pd.read_csv('tmp/gs_ctcv.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = '6ydXFLjFT9iwe9WKxL9GmA'
    # df['cid'] = 'lYmKXlMdQjeCt8Iglc8Kkg'
    # df['score_type'] = 'ctcv'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)
    #
    # df = pd.read_csv('tmp/spoon_vp.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = 'CQG5GWkBQ2681wYr0erwtw'
    # df['cid'] = 'iaVBIZ-FQoOLBwVAPQ984Q'
    # df['score_type'] = 'vp_bundle'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)
    #
    # df = pd.read_csv('tmp/nex_v4_vp.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = '6h0kW2biT6ycJgy9d0E0xw'
    # df['cid'] = 'wibmRCDHRxyIJLIh21D9kA'
    # df['score_type'] = 'vp_bundle'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)
    #
    # df = pd.read_csv('tmp/nex_v4_ctcv.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = '6h0kW2biT6ycJgy9d0E0xw'
    # df['cid'] = 'NQK71_vlRfW83HGZskOVxw'
    # df['score_type'] = 'ctcv'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)

    # df = pd.read_csv('tmp/nex_ctcv.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = 'u1E3jdJeQeKyv6ccZQZYEg'
    # df['cid'] = 'pI8ujKGqR7a-Fm7v-xqs5g'
    # df['score_type'] = 'ctcv'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)
    #
    # df = pd.read_csv('tmp/nex_vp.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = 'u1E3jdJeQeKyv6ccZQZYEg'
    # df['cid'] = 'f2qmXpW9QvSlJmdthXaiEg'
    # df['score_type'] = 'vp_bundle'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)

    # df = pd.read_csv('tmp/gs_cpi.csv')
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = '6ydXFLjFT9iwe9WKxL9GmA'
    # df['cid'] = ''
    # df['score_type'] = 'cpi'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)

    # df = pd.read_csv('tmp/waug_ctcv.csv')
    # df['start_date'] = '2020-03-04'
    # df['end_date'] = '2020-03-11'
    # df['oid'] = 'GEDnvNzwQ0WQXLFn9DHgng'
    # df['cid'] = 'jq37inBqT8C-Z4W7HKNiXw'
    # df['score_type'] = 'ctcv'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)

    # df = pd.read_csv('tmp/gs_ctcv_gadex.csv')
    # df.fillna({'app_id': "", 'tagid': "", 'partner_id': "", "bundle_id": ""}, inplace=True)
    # df['start_date'] = '2020-03-02'
    # df['end_date'] = '2020-03-09'
    # df['oid'] = '6ydXFLjFT9iwe9WKxL9GmA'
    # df['cid'] = 'PzQQASIQR2eCMrb0mbQ5gQ'
    # df['score_type'] = 'ctcv:gadex'
    # df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    # # df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    # df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)
    #
    # update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)

    df = pd.read_csv('tmp/86400_and_ctcv_impbid.csv', dtype={'bundle_id': str, 'app_id': str, 'tagid': str})
    df['start_date'] = '2020-03-29'
    df['end_date'] = '2020-03-29'
    df['oid'] = '8UqNB8jgRyWTGRPrXDHGlA'
    df['cid'] = 'Xb98g_8wRci17y9IQbj4bQ'
    df['score_type'] = 'ctcv_impbid'
    df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)

    update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)

    df = pd.read_csv('tmp/86400_ios_ctcv_impbid.csv', dtype={'bundle_id': str, 'app_id': str, 'tagid': str})
    df['start_date'] = '2020-03-29'
    df['end_date'] = '2020-03-29'
    df['oid'] = 'WqvPQ27vQaK3LHzeBLNZeA'
    df['cid'] = 'LiMMH56wRJWN9QYfnr8Pzw'
    df['score_type'] = 'ctcv_impbid'
    df['exchange'] = df.apply(lambda row: convert_exchange(row, exchange_mp), axis=1)
    # df['cpm'] = df.apply(lambda row: convert_cpm(row), axis=1)
    # df['bid2show'] = df['shows'] / df['bids']
    df['inv_hash'] = df.apply(lambda row: hash_df(row), axis=1)
    df['imp_size_group'] = df.apply(lambda row: lookupImpSizeGroup(row), axis=1)

    update_inv_table(query_db, df_db_client, table_config['inv-creative-table'], df)


if __name__ == '__main__':
    main()