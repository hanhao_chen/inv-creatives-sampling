import argparse
import arrow
from schedulerclient.api import get_scheduler
import json
import os
import pandas as pd
import time
import traceback
import urllib
import hashlib
from urllib.parse import urlparse

from app.utils.s3_utils import s3_download_dir
from constants import S3_KEY_ID, S3_CREDENTIAL
from app.utils.db import MySQL_DB_AIBE_DEV, MySQL_DB_CAMPAIGN
from app.config import V1Config, init_db_clients, close_db_clients
from app.utils import get_env
from app.module.CQueryDB import CQueryDB
from app.utils.init_logger import init_logger
from constants import spark_sql_info
from app.external_optimization.deep_funnel.utils.init_mysql_tables import InvTables
from app.utils.base_tool import format_inv_key_raw
import constants as c
import query_template

logger = init_logger(__name__)
raw_data_dir = 'raw_data_v2'
data_dir = 'data_v2'


def make_query(start_date, end_date, start_date_v2, end_date_v2, country, site_id, oid, creative_size):
    if creative_size is not None:
        query = query_template.sql_template_size.format(
            country=country,
            start_date_v2=start_date_v2,
            end_date_v2=end_date_v2,
            start_date=start_date,
            end_date=end_date,
            site_id=site_id,
            oid=oid,
            size_tuple=','.join(["({}, {})".format(size[0], size[1])
                                for size in creative_size])
        )
    else:
        if country == 'jp':
            query = query_template.install_sql_template_jp.format(
                country=country,
                start_date_v2=start_date_v2,
                end_date_v2=end_date_v2,
                start_date=start_date,
                end_date=end_date,
                site_id=site_id,
                oid=oid
            )
        else:
            query = query_template.install_sql_template_v2.format(
                country=country,
                start_date_v2=start_date_v2,
                end_date_v2=end_date_v2,
                start_date=start_date,
                end_date=end_date,
                site_id=site_id,
                oid=oid
            )
    logger.info(query)
    return query


def submit_spark_sql(query, options):

    param_dict = {
        'sql_query': query,
        'cores': 64,
        'job_priority': 7,
        'job_timeout': 3600 * 2,
        'job_retry': 1,
        'drivermemory': '1G',
        'nomerge': 1,
        'group': options.get('group'),
        'master': spark_sql_info['master'],
        'username': spark_sql_info['user'],
        'password': spark_sql_info['password']
    }

    scheduler = get_scheduler(param_dict['master'])

    sql_params = dict([(k, v) for k, v in param_dict.items() if k in
                       ('sql_query', 'date_start', 'date_end', 'username',
                        'password', 'backend_ver', 'nomerge',
                        'parquet', 'output', 'preempt', 'job_priority',
                        'job_retry', 'job_timeout', 'pool', 'cores',
                        'drivermemory', 'group') and v])
    poll_params = dict([(k, v) for k, v in param_dict.items() if k in
                        ('sleep', 'verbose') and v])

    job = scheduler.sql(**sql_params)
    logger.info('job: %s', job)

    return scheduler, job, poll_params


def split_info(options):

    os.makedirs(options['result_dir'], exist_ok=True)
    result_path_template = options['result_dir'] + '/{date}.csv'

    with open(os.path.join(options['output_dir'], '_HEADER'), 'r') as f:
        header = f.read().strip().split(',')
    dfs = []
    for filename in os.listdir(options['output_dir']):
        if filename.endswith('.csv'):
            df = pd.read_csv(os.path.join(options['output_dir'], filename), header=None, names=header,
                             dtype={'bundle_id': str, 'tagid': str, 'app_id': str, 'partner_id': str})
            dfs.append(df)
    df = pd.concat(dfs).reset_index(drop=True)

    duration = (arrow.get(options['end_date'], 'YYYYMMDD') - arrow.get(options['start_date'], 'YYYYMMDD')).days

    for sft in range(duration+1):
        date = arrow.get(options['start_date'], 'YYYYMMDD').shift(days=sft).format('YYYYMMDD')
        date_str = arrow.get(options['start_date'], 'YYYYMMDD').shift(days=sft).format('YYYY-MM-DD')
        filepath = result_path_template.format(date=date)
        df_tmp = df[df['date'] == date_str].copy()
        logger.info("dump file to {}".format(filepath))
        df_tmp.to_csv(filepath, index=False)

    return df


def submit_spark_sql_and_download(query, options, force=False):
    output_dir = options['output_dir']
    os.makedirs(os.path.dirname(output_dir), exist_ok=True)
    for attempt in range(5):
        try:
            if not os.path.exists(output_dir) or force:
                scheduler, job, poll_params = submit_spark_sql(query, options)
                scheduler.poll(job, **poll_params)
                logger.info('Start download %s', output_dir)
                # scheduler.get_sql_result(job, output_dir)
                s3_output = job['outputs'][0]
                s3_url = urlparse(s3_output)
                s3_download_dir(s3_url.path[1:], output_dir, s3_url.netloc, S3_KEY_ID, S3_CREDENTIAL, True)

                logger.info('%s downloaded', output_dir)

                logger.info('%s downloaded', output_dir)

            else:
                logger.info('Spark already done. output: %s', output_dir)

            df = split_info(options)

            return df

        except urllib.error.URLError:
            logger.error('UrlError: %s', options)
            time.sleep(20)
            continue
        except AssertionError as e:
            logger.error('Exception: %s', options)
            traceback.print_exc()
        except Exception as e:
            logger.error('Exception: %s', options)
            traceback.print_exc()
            time.sleep(10)
            continue
        break

    return None


def submit_spark_job(oid, oid_config, date, force=False, days=2):
    oid_dict = {}
    with open(oid_config, "r") as f:
        oid_json = json.loads(f.read())

    try:
        oid_dict = oid_json[oid]
    except Exception as _:
        logger.error('Cannot find oid: %s info in config', oid)
    if oid_dict['country'] == 'jp':
        days = 1

    if date:
        today = date
    else:
        today = arrow.utcnow().shift(hours=6).format('YYYYMMDD')
    end_date = arrow.get(today, 'YYYYMMDD').shift(days=-1).format('YYYYMMDD')
    start_date = arrow.get(today, 'YYYYMMDD').shift(days=-days).format('YYYYMMDD')

    end_date_v2 = arrow.get(today, 'YYYYMMDD').shift(days=-0).format('YYYY-MM-DD')
    start_date_v2 = arrow.get(today, 'YYYYMMDD').shift(days=-days).format('YYYY-MM-DD')

    # output_path = os.path.join(raw_data_dir, oid, today) + '.csv'
    output_dir = os.path.join(raw_data_dir, oid, today)
    result_dir = os.path.join(data_dir, oid)
    logger.info("result dir exist: %s", os.path.exists(result_dir))

    spark_options = {'group': None,
                     'date': today,
                     'start_date': start_date,
                     'end_date': end_date,
                     'output_dir': output_dir,
                     'result_dir': result_dir}
    if not os.path.exists(output_dir) or force:
        query = make_query(start_date, end_date, start_date_v2, end_date_v2, oid_dict['country'].lower(), oid_dict['site_id'], oid, oid_dict.get("creative_size"))
        return submit_spark_sql_and_download(query, spark_options, force), spark_options
    else:
        return split_info(spark_options), spark_options


def update_table(df, query_db, df_db_client, table_name):

    values = []
    for _, row in df.iterrows():
        values.append(["{}".format(v) for v in row])

    query = """
        INSERT INTO `{}` 
        ({})
        VALUES ({})
        ON DUPLICATE KEY UPDATE
        `cost`=values(`cost`),
        `bids`=values(`bids`),
        `wins`=values(`wins`),
        `shows`=values(`shows`),
        `clicks`=values(`clicks`),
        `actions`=values(`actions`),
        `_cost`=values(`_cost`),
        `_bids`=values(`_bids`),
        `_wins`=values(`_wins`),
        `_shows`=values(`_shows`),
        `_clicks`=values(`_clicks`),
        `_actions`=values(`_actions`),
        `uu`=values(`uu`),
        `rt_uu`=values(`rt_uu`),
        `country_shows_rt_uu`=values(`country_shows_rt_uu`),
        `country_clicks_rt_uu`=values(`country_clicks_rt_uu`),
        `shows_uu`=values(`shows_uu`),
        `clicks_uu`=values(`clicks_uu`),
        `shows_rt_uu`=values(`shows_rt_uu`),
        `clicks_rt_uu`=values(`clicks_rt_uu`)
    """.format(
        table_name,
        ','.join(["`{}`".format(col) for col in df.columns]),
        ','.join(["%s" for _ in df.columns])
    )
    # logger.info(query)
    # query_db.execute_query(query, db_client=df_db_client)
    query_db.execute_query_many(query, values, db_client=df_db_client)


def hash_df(row):
    hash_str = format_inv_key_raw(row)
    sha1 = hashlib.md5(hash_str.encode('utf-8'))
    return sha1.hexdigest()[:7]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--local', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    parser.add_argument('--oid_config', type=str, required=False, default='oid_config.json')
    parser.add_argument('--oids', type=str, nargs='+', help='oids')
    parser.add_argument('--country', type=str, help='country')
    parser.add_argument('--limit', type=int, default=100)
    parser.add_argument('--force', action='store_true')
    parser.add_argument('--date', default=None, type=str, help='run on date YYYYmmdd. (default: None=now)')
    parser.add_argument("--days", default=2)
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))

    df_db_client_dev = query_db.db_clients[MySQL_DB_AIBE_DEV]
    uu_table = cfg.inv_list_control['country_oid_rt_uu_table']
    inv_tables = InvTables(query_db, df_db_client_dev)

    inv_tables.init_country_oid_uu_table(uu_table)

    oids = args.oids if args.oids else c.EXP_OIDS
    for oid in oids:
        try:
            df, spark_options = submit_spark_job(oid, args.oid_config, args.date, args.force, days=args.days)
            df.dropna(subset=['imp_type', 'device_type', 'imp_width', 'imp_height', 'app_type'], inplace=True)
        except:
            logger.info('oid: %s, use day 1', oid)
            df, spark_options = submit_spark_job(oid, args.oid_config, args.date, args.force, days=1)
            try:
                df.dropna(subset=['imp_type', 'device_type', 'imp_width', 'imp_height', 'app_type'], inplace=True)
            except:
                logger.info("oid fail to get uu data: {}".format(oid))
                continue

        df.fillna({'cost': 0, 'bids': 0, 'shows': 0, 'clicks': 0, 'actions': 0}, inplace=True)
        df.fillna({'app_id': "", 'tagid': "", 'partner_id': "", "bundle_id": ""}, inplace=True)
        df['oid'] = oid

        start_date = arrow.get(spark_options['start_date'], 'YYYYMMDD')
        end_date = arrow.get(spark_options['end_date'], 'YYYYMMDD')

        duration = (end_date - start_date).days

        for sft in range(duration + 1):
            date = arrow.get(spark_options['start_date'], 'YYYYMMDD').shift(days=sft)
            date_str = date.format('YYYY-MM-DD')
            df_tmp = df[df['date'] == date_str].copy()

            df_tmp = df_tmp[(df_tmp['shows']>10) | (df_tmp['rt_uu']>0)]
            if len(df_tmp) == 0:
                continue
            df_tmp['inv_hash'] = df_tmp.apply(lambda row: hash_df(row), axis=1)
            df_tmp['time'] = date.timestamp
            logger.info('update date: %s', date_str)
            update_table(df_tmp, query_db, df_db_client_dev, uu_table)

if __name__ == '__main__':
    main()