import argparse
import pandas as pd
import os
import hashlib
import numpy as np
import arrow
import json
import constants as c
from constants import IMP_SIZE_SET
from collections import defaultdict

from app.utils.db import MySQL_DB_AIBE_DEV
from app.utils import get_env
from app.config import V1Config, init_db_clients, close_db_clients
from app.module.CQueryDB import CQueryDB
from app.module.CEndpointIdash import CEndpointIdash
from app.external_optimization.deep_funnel.utils.init_mysql_tables import InvTables
from app.utils.init_logger import init_logger
from app.utils.base_tool import lookupImpSizeGroup, convert_exchange, format_inv_key, format_inv_key_raw, convert_is_instl
from app.external_optimization.deep_funnel.utils.interact_mysql_tables import InvTableCenter

logger = init_logger(__name__)


def get_date_info(query_db, df_db_client, table_name):
    query = """
            select
            date as time,
            oid
            from `cum_oid_performance`
            where date>'2020-01-08'
            group by 1,2
        """.format(table=table_name)
    logger.info(query)
    results = query_db.get_query_result(query, db_client=df_db_client)

    date_info = defaultdict(list)
    for row in results:
        date_info[row['oid']].append(arrow.get(row['time']))

    return date_info

def get_latest_date(query_db, df_db_client, table_name):
    query = """
            select max(date) as date from `{table}`
        """.format(table=table_name)
    logger.info(query)
    r = query_db.get_query_result(query, db_client=df_db_client)

    return r[0]['date']

def agg_oid_perf(query_db, df_db_client, table_name, oids, date):
    query = """
        select
        oid,
        app_id,
        partner_id,
        imp_is_instl,
        imp_type,
        imp_width,
        imp_height,
        app_type,
        device_type,
        tag_id as tagid,
        sum(cost) as cost,
        sum(bids) as bids,
        sum(shows) as shows,
        sum(clicks) as clicks,
        sum(actions) as actions
        from `{table}`
        where date='{date}' and oid in ({oids})
        group by 1,2,3,4,5,6,7,8,9,10
    """.format(table=table_name,
               date=date,
               oids=','.join(["'{}'".format(oid) for oid in oids]))
    logger.info(query)
    r = query_db.get_query_result(query, db_client=df_db_client)

    return pd.DataFrame(r)


def collect_daily_from_local(path, oid, latest_date):

    date_file = latest_date.format('YYYYMMDD') + '.csv'

    df = pd.DataFrame()
    logger.info('oid: %s, file: %s', oid, date_file)
    file_path = os.path.join(path, oid, date_file)
    try:
        df = pd.read_csv(file_path)
        df = trans_raw_data(df, oid)
        df = df.groupby(['oid', 'app_id', 'partner_id', 'bundle_id', 'imp_is_instl', 'imp_type', 'imp_width', 'imp_height',
                                 'app_type', 'device_type', 'tagid', 'imp_size_group']).agg('sum').reset_index()

    except:
        logger.info('oid: %s, daily file not found', oid)

    return df

def agg_daily_oid_perf(query_db, df_db_client, table_name, oids):
    query = """
        select
        date,
        oid,
        app_id,
        partner_id,
        imp_is_instl,
        imp_type,
        imp_width,
        imp_height,
        app_type,
        device_type,
        tag_id as tagid,
        sum(cost) as cost,
        sum(bids) as bids,
        sum(shows) as shows,
        sum(clicks) as clicks,
        sum(actions) as actions
        from `{table}`
        where date=(select max(date) from `{table}`)
        AND oid in ({oids})
        group by 1,2,3,4,5,6,7,8,9,10,11
    """.format(table=table_name,
               oids=','.join(["'{}'".format(oid) for oid in oids]))
    logger.info(query)
    r = query_db.get_query_result(query, db_client=df_db_client)
    df = pd.DataFrame(r)

    return df


def collect_agg_df(query_db, df_db_client, table_name, oid, date):

    query = """
    select
    oid, app_id, bundle_id, partner_id, imp_is_instl, imp_type,
    imp_width, imp_height, app_type,
    device_type, tagid, imp_size_group,
    cost, bids, shows, clicks, actions
    from `{table}`
    where date='{date}' AND
    oid='{oid}'
    """.format(table=table_name,
               date=date,
               oid=oid)
    logger.info(query)
    r = query_db.get_query_result(query, db_client=df_db_client)

    return pd.DataFrame(r)


def update_table_init(df, query_db, df_db_client, table_name):
    values = []
    for _, row in df.iterrows():
        values.append(["{}".format(v) for v in row])

    query = """
            INSERT INTO `{}`
            ({})
            VALUES ({})
        """.format(
        table_name,
        ','.join(["`{}`".format(col) for col in df.columns]),
        # ','.join(["({})".format(v) for v in values])
        ','.join(["%s" for _ in df.columns])
    )
    logger.info(query)
    # query_db.execute_query(query, db_client=df_db_client)
    query_db.execute_query_many(query, values, db_client=df_db_client)

def update_table(df, query_db, df_db_client, table_name):

    values = []
    for _, row in df.iterrows():
        values.append(["{}".format(v) for v in row])

    query = """
        INSERT IGNORE INTO `{}`
        ({})
        VALUES ({})
    """.format(
        table_name,
        ','.join(["`{}`".format(col) for col in df.columns]),
        # ','.join(["({})".format(v) for v in values])
        ','.join(["%s" for _ in df.columns])
    )
    # logger.info(query)
    # query_db.execute_query(query, db_client=df_db_client)
    query_db.execute_query_many(query, values, db_client=df_db_client)


def trans_raw_data(df, oid):

    df['imp_is_instl'] = df.apply(lambda row: convert_is_instl(row), axis=1)
    df['imp_size_group'] = df.apply(lambda row:lookupImpSizeGroup(row), axis=1)
    df['oid'] = oid
    df['tagid'] = df['tag_id']
    df = df[['oid', 'app_id', 'bundle_id', 'partner_id', 'imp_is_instl', 'imp_type', 'imp_width', 'imp_height', 'app_type', 'device_type', 'tagid', 'imp_size_group',
             'cost', 'bids', 'shows', 'clicks', 'actions']]
    df.fillna({'app_id': "", 'tagid': "", 'partner_id': "", "bundle_id": ""}, inplace=True)
    df.fillna({'cost': 0, 'bids': 0, 'shows': 0, 'clicks': 0, 'actions': 0}, inplace=True)

    return df


def hash_df(row):
    hash_str = format_inv_key_raw(row)
    sha1 = hashlib.md5(hash_str.encode('utf-8'))
    return sha1.hexdigest()[:7]


def collect_from_local(path, oids, query_db, df_db_client_dev, agg_table):

    for oid in oids:
        logger.info('oid: %s', oid)
        df_all = pd.DataFrame()
        oid_path = os.path.join(path, oid)
        for filename in sorted(os.listdir(oid_path)):
            if filename.endswith('.csv'):
                logger.info('date file: %s', filename)
                file_path = os.path.join(oid_path, filename)
                df = pd.read_csv(file_path)
                df = trans_raw_data(df, oid)
                df_all = pd.concat([df_all, df]).reset_index(drop=True)
                df_all = df_all.groupby(['oid', 'app_id', 'bundle_id', 'partner_id', 'imp_is_instl', 'imp_type', 'imp_width', 'imp_height',
                                         'app_type', 'device_type', 'tagid', 'imp_size_group']).agg('sum').reset_index()

                df_all['date'] = arrow.get(filename.split('.')[0]).format('YYYY-MM-DD')
                df_all['time'] = arrow.get(filename.split('.')[0]).timestamp
                df_all['inv_hash'] = df_all.apply(lambda row:hash_df(row), axis=1)
                logger.info('start update')
                update_table(df_all, query_db, df_db_client_dev, agg_table)


def update_daily_data(path, oid, df_all, oid_max_date, query_db, df_db_client_dev, agg_table):

    oid_path = os.path.join(path, oid)
    for filename in sorted(os.listdir(oid_path)):
        if filename.endswith('.csv') and arrow.get(filename.split('.')[0], 'YYYYMMDD') > oid_max_date:
            logger.info('date file: %s', filename)
            file_path = os.path.join(oid_path, filename)

            df = pd.read_csv(file_path, dtype=df_all.dtypes.to_dict())
            df = trans_raw_data(df, oid)
            df_all = pd.concat([df_all, df]).reset_index(drop=True)

            df_all = df_all.groupby(
                ['oid', 'app_id', 'bundle_id', 'partner_id', 'imp_is_instl', 'imp_type', 'imp_width', 'imp_height',
                 'app_type', 'device_type', 'tagid', 'imp_size_group']).agg('sum').reset_index()

            df_all['date'] = arrow.get(filename.split('.')[0]).format('YYYY-MM-DD')
            df_all['time'] = arrow.get(filename.split('.')[0]).timestamp
            df_all['inv_hash'] = df_all.apply(lambda row: hash_df(row), axis=1)
            logger.info('start update')

            update_table(df_all, query_db, df_db_client_dev, agg_table)


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    parser.add_argument('--exchange_file', type=str, required=False, default='partnerid_exchangeid.json')
    parser.add_argument('--oid_config', type=str, required=False, default='oid_config.json')
    parser.add_argument('--date', default=None, type=str, help='run on date YYYYmmdd. (default: None=now)')
    parser.add_argument('--init', action='store_true')
    parser.add_argument('--oids', type=str, nargs='+', help='oids')
    parser.add_argument('--init_daily', action='store_true')
    parser.add_argument('--data_path', default='/srv/inventory_sampling/data/sampling_oid_performance/')
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))
    df_db_client_dev = query_db.db_clients[MySQL_DB_AIBE_DEV]
    # df_db_client_prd = query_db.db_clients[MySQL_RTB_DB_PRD]

    inv_tables = InvTables(query_db, df_db_client_dev)
    cum_table = cfg.inv_list_control['oid_cum_performance_table']
    # sampling_table = cfg.inv_list_control['oid_daily_performance_table']

    oids = c.EXP_OIDS + c.EXP_OIDS_BID
    # oids = args.oids

    if args.init:
        # inv_tables.drop_table(cum_table)
        inv_tables.init_agg_oid_performance(cum_table)
        collect_from_local(args.data_path, oids, query_db, df_db_client_dev, cum_table)
    else:
        date_info = get_date_info(query_db, df_db_client_dev, cum_table)
        for oid in oids:
            if oid not in date_info:
                try:
                    collect_from_local(args.data_path, [oid], query_db, df_db_client_dev, cum_table)
                    date_info = get_date_info(query_db, df_db_client_dev, cum_table)
                except:
                    continue
            oid_max_date = max(date_info[oid]).format('YYYY-MM-DD')
            agg_df = collect_agg_df(query_db, df_db_client_dev, cum_table, oid, oid_max_date)

            update_daily_data(args.data_path, oid, agg_df, max(date_info[oid]), query_db, df_db_client_dev, cum_table)


if __name__ == '__main__':
    main()
