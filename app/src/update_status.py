import argparse
import pandas as pd
import numpy as np
import arrow
import json
import constants as c
from constants import IMP_SIZE_SET
from collections import defaultdict

from app.utils.db import MySQL_DB_AIBE_DEV
from app.utils import get_env
from app.utils.base_tool import get_inv_query_dict, get_adjust_dict, convert_inv_info_mp, \
    format_inv_key, format_inv_key_wo_app_type, format_idash_inv, format_inv, inv_adjust_hash
from app.config import V1Config, init_db_clients, close_db_clients
from app.module.CQueryDB import CQueryDB
from app.module.CEndpointIdash import CEndpointIdash
from app.external_optimization.deep_funnel.utils.init_mysql_tables import InvTables
from app.utils.init_logger import init_logger
from app.external_optimization.deep_funnel.utils.interact_mysql_tables import InvTableCenter
from app.utils.base_tool import adjust_weight

logger = init_logger(__name__)


def update_spending(inv_table, cid, hf_table):

    oid = inv_table.get_oid(cid)
    thres_cost = 5 * inv_table.get_goal_cpa(oid) / 10000000. if c.score_type_mp.get(cid) != 'cpi' else 4.
    if thres_cost > 15:
        thres_cost = 15

    invs_cost = inv_table.gather_oid_cost(oid, hf_table)
    cost_mp = convert_inv_info_mp(invs_cost)

    candidate_inv = inv_table.get_topn_inv(cid, oid, topn=-1, all=True)
    for inv in candidate_inv:
        key = format_inv(inv)
        inv_cost = cost_mp['cost'].get(key, 0)
        inv_table.update_spending(oid, cid, inv, inv_cost, thres_cost)


def update_complete(idash_endpoint, inv_table, cid, hf_table):

    oid = inv_table.get_oid(cid)
    thres_cost = 5 * inv_table.get_goal_cpa(oid) / 10000000. if c.score_type_mp.get(cid) != 'cpi' else 4.
    if thres_cost > 15:
        thres_cost = 15

    invs_cost = inv_table.gather_oid_cost(oid, hf_table)
    cost_mp = convert_inv_info_mp(invs_cost)

    candidate_inv = inv_table.get_topn_inv(cid, oid, topn=-1)
    adjust_rules_complete = []
    for inv in candidate_inv:
        key = format_inv(inv)
        # import pdb; pdb.set_trace()
        inv_cost = cost_mp['cost'].get(key)
        if inv_cost is None:

            logger.info('exchange: %s, app_id: %s, cost: NOT FOUND',
                        inv['exchange'], inv['app_id'])
            continue
        else:
            logger.info('cost: %f', inv_cost)
        query_inv = get_inv_query_dict(inv, oid, cid)
        if inv_cost >= thres_cost:
            inv_table.update_adjust_weight(oid, cid, query_inv, 0)
            inv_table.update_complete_inv(oid, cid, query_inv)
            logger.info('exchange: %s, app_id: %s, cost: %f, Set to complete',
                        inv['exchange'], inv['app_id'], inv_cost)

            adjust_dict = get_adjust_dict(cid, inv, 0.)
            adjust_rules_complete.append(adjust_dict)

    r = inv_table.adjust_weight(cid, adjust_rules_complete)
    logger.info('close complete inv %s', r)


    candidate_inv = inv_table.get_topn_inv(cid, oid)

    if len(candidate_inv) > 0:
        enhance_rules = inv_table.gen_enhance_rules(cid, candidate_inv)
        idash_endpoint.send_update_data_request(cid, enhance_rules)

        adjust_rules_init = inv_table.init_inv_weight(candidate_inv, oid, cid)

        r = inv_table.adjust_weight(cid, adjust_rules_init)
        logger.info('Init candidate weight, %s', r)
    else:
        config = idash_endpoint.read_campaign_fields(cid, ('campaign_other_type', 'daily_max_budget', 'ext_bid_price'))
        idash_endpoint.send_cid_enable_request(cid, config['campaign_other_type'], False)

    adjust_rules_close = []
    cn_set = set()

    same_inv = inv_table.gather_same_inv(oid, candidate_inv)
    for cn_inv in candidate_inv:
        cn_set.add(inv_adjust_hash(cn_inv))
    for s_inv in same_inv:
        key = inv_adjust_hash(s_inv)
        if key not in cn_set:
            adjust_dict = get_adjust_dict(cid, s_inv, 0)
            adjust_rules_close.append(adjust_dict)

    r = inv_table.adjust_weight(cid, adjust_rules_close)
    logger.info('close not candidate creatives %s', r)

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    parser.add_argument('--oid_config', type=str, required=False, default='oid_config.json')
    parser.add_argument('--oids', type=str, nargs='+', help='oids')
    parser.add_argument('--cids', type=str, nargs='+', help='cids')
    parser.add_argument('--country', type=str, help='country')
    parser.add_argument('--date', default=None, type=str, help='run on date YYYYmmdd. (default: None=now)')
    parser.add_argument('--type', default='update_complete', type=str)
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))
    idash_endpoint = CEndpointIdash(cfg.idash_cfg)

    inv_table = InvTableCenter(idash_endpoint, query_db, cfg.inv_list_control)

    cids = args.cids

    if args.type == 'update_complete':
        for cid in cids:
            update_complete(idash_endpoint, inv_table, cid)
    elif args.type == 'update_spending':
        for cid in cids:
            update_spending(inv_table, cid)


if __name__ == '__main__':
    main()