import argparse
import pandas as pd
import numpy as np
import arrow
import json
import constants as c
from constants import IMP_SIZE_SET
from collections import defaultdict

from app.utils.db import MySQL_DB_AIBE_DEV
from app.utils import get_env
from app.utils.base_tool import get_inv_query_dict, get_adjust_dict, \
    format_inv_key, format_inv_key_wo_app_type, format_idash_inv, lookupImpSizeGroup, \
    convert_inv_val_mp, format_inv, format_inv_val, format_inv_beethoven, inv_adjust_hash, \
    convert_inv_info_mp
from app.config import V1Config, init_db_clients, close_db_clients
from app.module.CQueryDB import CQueryDB
from app.module.CEndpointIdash import CEndpointIdash
from app.external_optimization.deep_funnel.utils.init_mysql_tables import InvTables
from app.utils.init_logger import init_logger
from app.external_optimization.deep_funnel.utils.interact_mysql_tables import InvDynamicTableCenter
from app.utils.base_tool import adjust_weight
from app.utils.test_inventory import test_inventory
from app.src.update_status_dynamic import update_status, update_complete, update_sample_list
from app.utils.idash_v3_utils import IDashV3Utils
from app.utils.campaign import CampaignCacheDB

logger = init_logger(__name__)


def adjust_goal2weight(cfg, inv_table, cid):
    ini_tables = InvTables(inv_table.query_db, inv_table.df_db_client)
    ini_tables.init_inv_goal_value_table(cfg.inv_list_control['goal_adjust_table'])
    result = inv_table.get_cid_goal_value_status(cid)

    oid = inv_table.get_oid(cid)
    goal_cpm = inv_table.get_cid_goal_cpm(cid)
    if len(result) == 0:
        inv_table.insert_cid_goal(cid, goal_cpm)
    else:
        inv_table.update_cid_goal(cid, goal_cpm)
        his_goal_cpm = result[0]['goal_cpm']
        scale = 1. / (goal_cpm / his_goal_cpm)
        logger.info('Goal CPM: %f -> %f', his_goal_cpm, goal_cpm)
        if scale == 1:
            return

        candidate_inv = inv_table.get_sampling_inv(cid, oid)
        adjust_rules_scale = []

        # if scale != 1:
        for inv in candidate_inv:
            logger.info('exchange: %s, app_id: %s, adjust_val: %f -> %f',
                        inv['exchange'], inv['app_id'], inv['adjust_weight'], inv['adjust_weight']*scale)
            inv_table.update_adjust_weight(oid, cid, inv, inv['adjust_weight']*scale)

            adjust_dict = get_adjust_dict(cid, inv, inv['adjust_weight']*scale)
            adjust_rules_scale.append(adjust_dict)


        r = inv_table.adjust_weight(cid, adjust_rules_scale)
        logger.info('update scale weight, %s', r)


def adjust_win_rate(idash_endpoint, inv_table, cid, reset_weight=False):

    oid = inv_table.get_oid(cid)
    model_type = inv_table.get_model_type(cid)
    if model_type == 'lobster':
        return
    # country = inv_table.get_country(cid)
    # goal_cpc = inv_table.get_goal_cpc(cid)
    thres_cost = 5 * inv_table.get_goal_cpa(oid) / 10000000.
    if thres_cost > 15:
        thres_cost = 15

    invs_val = inv_table.gather_cid_adjust_val(cid)
    val_mp = convert_inv_val_mp(invs_val)

    logger.info('oid: %s, cost threshold: %f', oid, thres_cost)
    inv_status = inv_table.gather_cid_invs(cid)

    adjust_rules_reset = []
    adjust_rules_win = []

    for inv in inv_status:
        """
        win < 10% increase weight
        """
        inv['imp_size_group'] = lookupImpSizeGroup(inv)
        inv['exchange'] = inv['partner_id'].split('_')[0]
        inv['win_rate'] = float(inv['shows']) / float(inv['bids'])

        val_key = format_inv_val(inv)
        inv['adjust_val'] = val_mp.get(val_key, 0)
        query_inv = get_inv_query_dict(inv, oid, cid)
        # cpm = inv['cost'] / inv['shows'] * 1000 if inv['shows'] > 0 else 0
        # cpc = inv['cost'] / inv['clicks'] if inv['clicks'] > 0 else 0

        if inv['adjust_val'] > 0:
            logger.info('exchange: %s, app_id: %s, win_rate: %f, adjust_val: %f, win_rate: %f',
                        inv['exchange'], inv['app_id'], inv['win_rate'], inv['adjust_val'], inv['win_rate'])

        if reset_weight:
            adjust_dict = get_adjust_dict(cid, inv, 0.)
            adjust_rules_reset.append(adjust_dict)
            if inv['adjust_val'] > 0:
                inv_table.update_adjust_weight(oid, cid, query_inv, 0)

        elif inv['win_rate'] < 0.4:
            adjust_val = inv['adjust_val'] * 1.5
            adjust_val = adjust_val if adjust_val < 32. else 32.
            adjust_dict = get_adjust_dict(cid, inv, adjust_val)
            adjust_rules_win.append(adjust_dict)
            if inv['adjust_val'] > 0:
                inv_table.update_adjust_weight(oid, cid, query_inv, adjust_val)

        elif inv['win_rate'] >= 0.6:
            adjust_val = inv['adjust_val'] * 0.8
            adjust_dict = get_adjust_dict(cid, inv, adjust_val)
            adjust_rules_win.append(adjust_dict)
            if inv['adjust_val'] > 0:
                inv_table.update_adjust_weight(oid, cid, query_inv, adjust_val)

    """
    get candidate from table and set whitelist to adgroup
    """
    if reset_weight:
        _ = inv_table.adjust_weight(cid, adjust_rules_reset)
        logger.info('Reset weight to 1')

    if not reset_weight:
        r = inv_table.adjust_weight(cid, adjust_rules_win)
        logger.info('Adjust weight, increase 1.1, %s', r)


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    parser.add_argument('--oid_config', type=str, required=False, default='oid_config.json')
    parser.add_argument('--oids', type=str, nargs='+', help='oids')
    parser.add_argument('--cids', type=str, nargs='+', help='cids')
    parser.add_argument('--country', type=str, help='country')
    parser.add_argument('--date', default=None, type=str, help='run on date YYYYmmdd. (default: None=now)')
    parser.add_argument('--reset_weight', action='store_true')
    parser.add_argument('--freeze_weight', action='store_true')
    parser.add_argument('--hf_table', action='store_true')
    parser.add_argument('--goal_update', action='store_true')
    args = parser.parse_args()

    credential = json.load(open(args.credential_file))
    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)
    query_db = CQueryDB(init_db_clients(cfg))
    idash_endpoint = CEndpointIdash(cfg.idash_cfg)

    df_db_client_dev = query_db.db_clients[MySQL_DB_AIBE_DEV]
    c_table = cfg.inv_list_control['inv_complete_table']
    s_table = cfg.inv_list_control['inv_sampling_table']
    inv_tables = InvTables(query_db, df_db_client_dev)

    inv_tables.init_inv_complete_table_table(c_table)
    inv_tables.init_inv_sampling_table_table(s_table)
    inv_tables.init_inv_sampling_info_table()

    inv_table = InvDynamicTableCenter(idash_endpoint, query_db, cfg.inv_list_control)
    idash_v3 = IDashV3Utils(credential["default"]["idash_v3"])
    campaign_db = CampaignCacheDB(credential["default"]["campaign_db"])

    oid_to_site_id = dict()
    oid_is_vtcv_campaign = dict()
    for result in campaign_db.get_available_oid_site_id():
        oid_to_site_id[result["oid"]] = result["retargeting_site_id"]
        development_customized_field = json.loads(result["development_customized_field"])
        oid_is_vtcv_campaign[result["oid"]] = development_customized_field.get("is_vtcv_campaign", False)

    cid_configs = []
    configs = idash_v3.get_rtb_app_install_config()
    for config in configs:
        oid = inv_table.get_oid(config['ad_group_id'])
        cid = config['ad_group_id']
        use_rtb_app_install_ai = config['config']['use_rtb_app_install_ai']
        sampling_types = config['config']['inventory_sampling_creative_types']

        # check oid has already enabled RTB App Install services and the
        # value of use_rtb_app_install_ai in the response is True
        if oid in oid_to_site_id and use_rtb_app_install_ai and len(sampling_types):
            site_id = oid_to_site_id[oid]
            is_vtcv_campaign = oid_is_vtcv_campaign[oid]
            imp_type = 2 if "Video" in sampling_types else 4
            is_rewarded = 1 if idash_endpoint.get_cid_status(cid).get('use_rewarded_traffic') == 'must' else 0
            if imp_type == 2:
                # ssp = ['mopub', 'google', 'adcolony', 'vungle', 'unity']
                cid_configs.append(dict(cid=cid, site_id=site_id, is_vtcv_campaign=is_vtcv_campaign, imp_type=imp_type,
                                        is_rewarded=is_rewarded))
            else:
                cid_configs.append(dict(cid=cid, site_id=site_id, is_vtcv_campaign=is_vtcv_campaign, imp_type=imp_type,
                                        is_rewarded=is_rewarded))

    logger.info("Get {} cid configs".format(len(cid_configs)))
    logger.info(cid_configs)
    update_list = []
    for cid_config in cid_configs:
        level = 0 if cid_config["imp_type"] != 2 else 1

        update_status(inv_table, cid_config['cid'], args.hf_table, level=level)
        update_complete(inv_table, cid_config['cid'])

        site_id = cid_config["site_id"]

        is_updated = update_sample_list(idash_endpoint, inv_table, cid_config, site_id)
        update_list.append(is_updated)
        update_status(inv_table, cid_config['cid'], False, enable_early_stop=False, level=level)
        update_complete(inv_table, cid_config['cid'])

    update_counts = np.sum(update_list)
    logger.info("Finish. {:.2%} ({}/{}) cids are updated".format(update_counts / len(update_list),
                                                                 update_counts,
                                                                 len(update_list)))



if __name__ == '__main__':
    main()
