# TODO: use orm

MySQL_HOST = 'performance-data.czyv6iwgcsxw.us-west-2.rds.amazonaws.com'  # for other .py to use
MySQL_PORT = 3306
MySQL_PASSWD = ''
MySQL_PASSWD_AI_FRAUD_DETECTION = ''

MySQL_DB = 'performance_data'
MySQL_DB_W = 'performance_data_write'

MySQL_DB_CAMPAIGN = 'campaign_cache'
MySQL_DB_CAMPAIGN_W = 'campaign_cache_write'

MySQL_DB_EXTERNAL = 'external_buying'
MySQL_DB_EXTERNAL_W = 'external_buying_write'

MySQL_DB_EVENT_METRIC_TRANSIENT = 'event_metric_transient'
MySQL_DB_EVENT_METRIC_TRANSIENT_W = 'event_metric_transient_write'

MySQL_DB_FRAUD = 'ai_fraud_detection'
MySQL_DB_FRAUD_W = 'ai_fraud_detection_write'

MySQL_DB_AI_RR_PERFORMANCE = 'ai_rr_performance'
MySQL_DB_AI_RR_PERFORMANCE_W = 'ai_rr_performance_write'

MySQL_DB_AI_RR_FRAM_RAW_ACTION = 'ai_rr_from_raw_action'
MySQL_DB_AI_RR_FRAM_RAW_ACTION_W = 'ai_rr_from_raw_action_write'

MySQL_DB_AI_DEEP_FUNNEL = 'ai_deep_funnel'
MySQL_DB_AI_DEEP_FUNNEL_W = 'ai_deep_funnel_write'

MySQL_DB_RULE = 'rule_data_write'
MySQL_DB_RULE_W = 'rule_data_write'

MySQL_DB_AI_OPTIMIZER = 'ai_optimier'
MySQL_DB_AI_OPTIMIZER_W = 'ai_optimier_write'

MySQL_DB_RTB_OPT_W = 'rtb_opt_write'
MySQL_DB_RTB_OPT = 'rtb_opt'

MySQL_DB_AI_HYDRA = 'ai_hydra'
MySQL_DB_AI_HYDRA_W = 'ai_hydra_write'

MySQL_DB_AIBE_DEV = 'rtb_db_prd'
# MySQL_DB_AIBE_DEV = 'aibe_db_dev'
# MySQL_RTB_DB_PRD = 'rtb_db_prd'

# seems duplicated function with execute_query
def execute_query_secure(db_client, query, param_tuples):
    db_client.execute_query(query, args=param_tuples)
    result = db_client.get_conn().commit()
    return result


# seems duplicated function with execute_query
def get_query_result_secure(db_client, query, param_tuples):
    cursor = db_client.execute_query(query, args=param_tuples)
    return [row for row in cursor.fetchall()]


def execute_query(db_client, query, args=None):
    db_client.execute_query(query, args=args)
    result = db_client.get_conn().commit()
    return result


def execute_query_many(db_client, query, data):
    db_client.execute_query_many(query, data)
    result = db_client.get_conn().commit()
    return result


# TODO: check if we can always use connector_type="python"
# def get_query_result(db_client, query, connector_type='python'):
def get_query_result(db_client, query):
    cursor = db_client.execute_query(query, None)
    return [row for row in cursor.fetchall()]
