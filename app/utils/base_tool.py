import constants as c
import json
from get_campaign_data_universal import get_all_campaign_data
from collections import defaultdict

r_device_mp = {v:k for k, v in c.device_type_mp.items()}
r_app_mp = {v:k for k, v in c.app_type_mp.items()}

def convert_is_instl(row):
    return 1 if row['imp_is_instl'] == True else 0

def convert_exchange(row, mp):
    return mp.get(row['partner_id'], '')

def convert_rt_ratio(row):
    return row['rt_uu'] / row['uu'] if row['uu'] > 0 else 0

def convert_tmp_app_type(row):
    return 1 if row['bundle_id'] != "" else 2

def convert_cpa(row):
    return row['cost']/10000000/row['actions'] if row['actions'] > 0 else 99999


def convert_ctr(row):
    return row['clicks']/row['shows'] if row['shows'] > 0 else 0


def convert_cpm(row):
    return row['cost']/10000000/row['shows']*1000


def format_inv_key_wo_app_type(inv):
    key = "{exchange},{device_type},{imp_size_group},{tagid},{app_id}".format(
        exchange=inv['exchange'],
        device_type=c.device_type_mp.get(inv['device_type']),
        imp_size_group=inv['imp_size_group'],
        tagid=inv['tagid'],
        app_id=inv['app_id'])
    return key

def format_idash_inv(inv):
    tmp_inv = inv.copy()
    tmp_inv['device_type'] = r_device_mp.get(tmp_inv['device_type'], "")
    tmp_inv['app_type'] = r_app_mp.get(tmp_inv['app_type'], "")

    return tmp_inv

def format_inv_key(inv, trans=True):
    key = "{partner},{device_type},{imp_size_group},{tagid},{app_type},{app_id}".format(
        exchange=inv['exchange'],
        device_type=c.device_type_mp.get(inv['device_type'].lower()) if trans else inv['device_type'],
        imp_size_group=inv['imp_size_group'],
        tagid=inv['tagid'],
        app_type=c.app_type_mp.get(inv['app_type'].lower()) if trans else inv['app_type'],
        app_id=inv['app_id'])
    return key


def format_inv_beethoven(inv):
    key = "{app_id},{exchange},{app_type}," \
          "{device_type},{tagid},{imp_size_group}".format(
        app_id=inv['app_id'],
        exchange=inv['exchange'],
        app_type=int(inv['app_type']),
        device_type=int(inv['device_type']),
        tagid=inv['tagid'],
        imp_size_group=inv['imp_size_group'],
    )
    return key


def format_inv(inv):
    key = "{app_id},{partner_id},{imp_is_instl}," \
          "{imp_type},{imp_width},{imp_height},{app_type},{device_type},{tagid}".format(
        app_id=inv['app_id'],
        partner_id=inv['partner_id'],
        imp_is_instl=int(inv['imp_is_instl']),
        imp_type=int(inv['imp_type']),
        imp_width=int(inv['imp_width']),
        imp_height=int(inv['imp_height']),
        app_type=int(inv['app_type']),
        device_type=int(inv['device_type']),
        tagid=inv['tagid']
    )
    return key

def format_inv_key_raw(inv):

    key = "{app_id},{bundle_id},{partner_id},{imp_is_instl},{imp_type}," \
          "{imp_width},{imp_height},{app_type},{device_type},{tagid}".format(
        app_id=inv['app_id'],
        bundle_id=inv['bundle_id'],
        partner_id=inv['partner_id'],
        imp_is_instl=int(inv['imp_is_instl']),
        imp_type=int(inv['imp_type']),
        imp_width=int(inv['imp_width']),
        imp_height=int(inv['imp_height']),
        app_type=int(inv['app_type']),
        device_type=int(inv['device_type']),
        tagid=inv['tagid']
    )
    return key

def format_inv_val(inv):
    key = "{app_id},{tagid},{exchange},{app_type},{device_type},{imp_size_group}".format(
        app_id=inv['app_id'],
        tagid=inv['tagid'],
        exchange=inv['exchange'],
        app_type=int(inv['app_type']),
        device_type=int(inv['device_type']),
        imp_size_group=inv['imp_size_group']
    )
    return key

def convert_inv_val_mp(invs_val):
    val_map = {}
    for inv in invs_val:
        key = format_inv_val(inv)
        val_map[key] = inv['adjust_val']

    return val_map


def convert_inv_info_mp(invs_info):
    info_mp = defaultdict(dict)
    for inv in invs_info:
        # key = format_inv_key(inv, trans=False)
        key = format_inv(inv)
        info_mp['cost'][key] = inv['cost'] / 10000000.
        info_mp['shows'][key] = inv['shows']
        info_mp['clicks'][key] = inv['clicks']
        info_mp['installs'][key] = inv['installs']
    return info_mp


def _get_adjust_dict(cid, inv, adjust_val, trans=True):
    if trans:
       inv = format_idash_inv(inv)
    return {
        "exchange": inv['exchange'],
        "device_type": inv['device_type'],
        "adjust_val": adjust_val,
        "adjust_by_robot": True,
        "imp_size_group": inv['imp_size_group'],
        "tagid": inv['tagid'],
        "app_type": inv['app_type'],
        "app_id": inv['app_id'],
        "cid": cid
    }


def get_adjust_dict(cid, inv, adjust_val, trans=True):
    # if trans:
    #    inv = format_idash_inv(inv)
    return {
        "app_id": inv['app_id'],
        "imp_size_group": inv['imp_size_group'],
        "exchange": inv['exchange'],
        "app_type": inv['app_type'],
        "device_type": inv['device_type'],
        "tagid": inv['tagid'],
        "cmp_id": cid,
        "adjust_val": adjust_val,
        "adjust_by_robot": 1
    }


def get_inv_query_dict(inv, oid, cid):

    query_inv = {
        'oid': oid,
        'cid': cid,
        'app_id': inv['app_id'],
        'imp_size_group': inv['imp_size_group'],
        'imp_is_instl': inv['imp_is_instl'],
        'imp_width': inv['imp_width'],
        'imp_height': inv['imp_height'],
        'tagid': inv['tagid'],
        'exchange': inv['exchange'],
        'device_type': inv['device_type'],
        'inv_hash': inv['inv_hash'],
        'score_type': c.score_type_mp.get(cid),
    }

    return query_inv


def lookupImpSizeGroup(row):
    width = row['imp_width']
    height = row['imp_height']
    isFullScreen = row['imp_is_instl']
    impType = row['imp_type']
    deviceType = row['device_type']

    if impType == 2:
        return "video"
    if impType == 4:
        return "native"
    if deviceType == 2:
        if width * height >= 90000:
            return "large"
        elif width >= 200 and height >= 200:
            return "medium"
        else:
            return "small"
    else:
        long = 700
        short = 100
        if isFullScreen == 1:
            if width > long and height > long:
                return "full_large"
            else:
                return "full_medium"
        else:
            if width > long:
                return "large"
            elif width <= long and height > short:
                return "medium"
            else:
                return "small"

def _adjust_weight(idash_endpoint, cid, adjust_rules):

    if len(adjust_rules) == 0:
        return None

    request = {
        "type": "ai_optimizer",
        "command": json.dumps({"cid": cid, "adjust_rules": adjust_rules}),
        "cid": cid
    }

    r = idash_endpoint._request_post(url='https://api.idash.appier.net/create/job_queue',
                                     data=request).json()
    return r

def adjust_weight(idash_endpoint, cid, adjust_rules):
    feature_keys = c.INV_FEATURE_KEYS['fix_price']
    adjust_db_fields = feature_keys + ['adjust_val', 'adjust_by_robot']
    adjust_rule_tuple = []
    for rule in adjust_rules:
        adjust_rule_tuple.append('(\'%s\',' % cid + ','.join(['\'' + str(rule[field]) + '\'' for field in adjust_db_fields]) + ')')

    data = get_all_campaign_data.get_beethoven_all_params(cid)
    data['data']['inv_adjust'] = adjust_rules
    success = idash_endpoint.send_bidder_aggregator(data, cid)

    return success


def inv_adjust_hash(inv):
    fstr = "{},{},{},{},{},{}".format(
        inv['app_id'], inv['imp_size_group'], inv['exchange'],
        inv['app_type'], inv['device_type'], inv['tagid'])
    return fstr
