import os
import boto3
from constants import S3_KEY_ID, S3_CREDENTIAL
import logging
import shutil
import urllib

import os
import boto3

from app.utils.init_logger import init_logger
logger = init_logger(__name__)

def getS3Client(key_id, access_key):
    return boto3.client('s3',
                        aws_access_key_id=key_id,
                        aws_secret_access_key=access_key)


def getS3Resource(key_id, access_key):
    return boto3.resource('s3',
                          aws_access_key_id=key_id,
                          aws_secret_access_key=access_key)


def download_dir(client, resource, prefix, local, bucket, key_id, access_key):
    paginator = client.get_paginator('list_objects')
    total_downloaded = 0
    for result in paginator.paginate(Bucket=bucket, Delimiter='/', Prefix=prefix):
        if result.get('CommonPrefixes') is not None:
            for subdir in result.get('CommonPrefixes'):
                _len_prefix = len(prefix)
                _folder = subdir.get('Prefix')[_len_prefix:].lstrip("/").rstrip("/")
                _local = local if (len(_folder) == 0) else local + os.sep + _folder
                download_dir(client, resource, subdir.get('Prefix'), _local, bucket, key_id=key_id,
                             access_key=access_key)
        if result.get('Contents') is not None:
            for f in result.get('Contents'):
                ff = f.get('Key')
                if ff[-1] == "/":
                    ff = ff[:-1]

                basename = os.path.basename(ff)
                target_dirname = os.path.dirname(local + os.sep + basename)
                target_file = local + os.sep + basename
                if not os.path.exists(target_dirname):
                    os.makedirs(target_dirname)
                logger.debug("Download %s to %s", f.get('Key'), target_file)
                total_downloaded += 1
                resource.meta.client.download_file(bucket, f.get('Key'), target_file)

    logger.info("Total Download from %s to %s: %s", prefix, local, total_downloaded)


def s3_download_dir(prefix, local, bucket, key_id, access_key, replace=False):
    client = getS3Client(key_id=key_id, access_key=access_key)
    resource = getS3Resource(key_id=key_id, access_key=access_key)
    if not os.path.exists(os.path.dirname(local)):
        os.makedirs(os.path.dirname(local))
    # done_file = local + ".download"
    if not os.path.exists(local):
        download_dir(client, resource, prefix, local, bucket, key_id=key_id, access_key=access_key)
    elif replace:
        shutil.rmtree(local, ignore_errors=True)
        os.makedirs(local, exist_ok=True)
        download_dir(client, resource, prefix, local, bucket, key_id=key_id, access_key=access_key)
    else:
        logger.info("The destination exists, skip")


class UploadS3:
    def __init__(self,
                 s3_bucket,
                 s3_key_id=S3_KEY_ID,
                 s3_secret_id=S3_CREDENTIAL):
        self.s3_bucket = s3_bucket
        s3_resource = boto3.resource('s3', aws_access_key_id=s3_key_id, aws_secret_access_key=s3_secret_id)
        self.s3_bucket_obj = s3_resource.Bucket(s3_bucket)

    def _do_upload(self, nfs_path, s3_object_path):
        obj = self.s3_bucket_obj.Object(s3_object_path)
        if os.path.isfile(nfs_path):
            logging.info('{} -> s3://{}/{}'.format(nfs_path, self.s3_bucket, s3_object_path))
            obj.upload_file(nfs_path)
        elif os.path.isdir(nfs_path):
            for x_sub_file in os.listdir(nfs_path):
                if x_sub_file.startswith("."):
                    continue
                else:
                    self._do_upload(
                        nfs_path="{}/{}".format(nfs_path, x_sub_file),
                        s3_object_path="{}/{}".format(s3_object_path, x_sub_file)
                    )

    def do_upload(self, nfs_path, s3_object_path):
        if not os.path.exists(nfs_path):
            raise Exception('Upload to S3 Fail, path {} does not exist'.format(nfs_path))

        s3_object_path = s3_object_path.strip("/")
        x_real_path = nfs_path if not os.path.islink(nfs_path) else os.path.realpath(nfs_path)

        self._do_upload(nfs_path=x_real_path, s3_object_path=s3_object_path)


def upload_file_to_s3(nfs_path, s3_bucket, s3_object_path,
                      s3_key_id=S3_KEY_ID,
                      s3_secret_id=S3_CREDENTIAL):
    x_upload = UploadS3(s3_bucket=s3_bucket, s3_key_id=s3_key_id, s3_secret_id=s3_secret_id)
    x_upload.do_upload(
        nfs_path=nfs_path,
        s3_object_path=s3_object_path
    )


def upload_file_to_s3_fullpath(nfs_path, s3_full_path,
                               s3_key_id=S3_KEY_ID,
                               s3_secret_id=S3_CREDENTIAL):
    print('[upload_to_s3]', nfs_path, s3_full_path)
    s3_url = urllib.parse.urlparse(s3_full_path)
    s3_bucket = s3_url.netloc
    s3_object_path = s3_url.path[1:]

    upload_file_to_s3(
        nfs_path=nfs_path,
        s3_bucket=s3_bucket,
        s3_object_path=s3_object_path,
        s3_key_id=s3_key_id,
        s3_secret_id=s3_secret_id
    )