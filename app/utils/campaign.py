from .mysql_utils import MySQLDB


class CampaignCacheDB:
    def __init__(self, db_credential):
        self.db = MySQLDB(db_credential)

    def get_available_oid_site_id(self):
        query = """
            SELECT oid,
                retargeting_site_id,
                development_customized_field
            FROM order_info
            WHERE status_code in ('new', 'running', 'paused')
                AND `development_customized_field` LIKE '%enable_ai_appinstall_rtb_services%'
                AND length(retargeting_site_id) > 0
        """
        result = self.db.get_query_result(query)
        return result
