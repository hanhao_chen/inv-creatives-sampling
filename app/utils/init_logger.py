import logging

logging_levels = {
    'default': logging.INFO,
    'external_optimization_core.db': logging.ERROR,
    'external_optimization_core.module.CEventMetric': logging.WARNING,
    'external_optimization_core.module.CBWInventory': logging.WARNING,
    'external_optimization_core.module.CEndpointAllAction': logging.WARNING
}


def init_logger(module_name):
    lvl = logging_levels.get(module_name) or logging_levels.get('default')

    # console handler: default logging to stderr
    ch = logging.StreamHandler()

    fmt = logging.Formatter(
        "[%(process)d][%(levelname)s] %(asctime)s - %(name)s.%(funcName)s:%(lineno)d - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S")
    ch.setFormatter(fmt)

    _logger = logging.getLogger(module_name)
    _logger.setLevel(lvl)
    _logger.addHandler(ch)

    return _logger


logger = init_logger(__name__)
