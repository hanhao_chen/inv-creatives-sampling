# -*- coding: utf-8 -*-
import os
import json
import requests

valid_env_list = ["production", "staging", "dev"]


def get_env():
    env = os.environ.get("ENV", "dev")
    if env not in valid_env_list:
        raise ValueError("Invalid ENV setting")
    return env


def slack_message(message, slack_webhook_url, name="external-optimization"):
    req = {
        "username": name,
        "text": message,
    }
    requests.post(slack_webhook_url, data=json.dumps(req))
