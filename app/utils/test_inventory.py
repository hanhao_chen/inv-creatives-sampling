from typing import Dict
from scipy.stats import binom
import math
import numpy as np
import random


def test_inventory(
        cpm: float,
        click: int,
        cost: float,
        cpc: float,
        type_1_error: float = None,
        cpc_alternative: float = None,
        probability_value=None,
) -> Dict:
    """test if the inventory is rejected.

    Keyword arguments:
    :param cpm: The cost per mile
    :param click: The number of clicks
    :param cost: The cost. The rule: `number of impression = cost / cpm * 1000` should be satisfied.
    :param cpc: The target cpc whose unit should be the same as the cost. i.e., the empirical cpc should be the `cost / click`
    :param type_1_error: The desired type I error. Default is `0.05`
    :param cpc_alternative: The alternative cpc. Default is `2 * cpc`
    :param probability_value: float value for unit test.
    :return: A dictionary with key:
      threshold: the maximal click to reject the inventory
      sample_size: the estimated sample_size which is calculated via `cost / cpm * 1000`
      random_weight: the smoothing probability of rejecting when click is equal to threshold + 1
      power: the power of the test based on the cpc_alternative
      result: a boolean value to indicate whether the inventory is rejected or not
    """
    if cost < cpc * 10:
        return {"result": False}

    if type_1_error is None:
        type_1_error = 0.05
    if cpc_alternative is None:
        cpc_alternative = cpc * 2
    ctr_0 = cpm / (cpc * 1000)
    ctr_a = cpm / (cpc_alternative * 1000)
    sample_size = math.floor(cost / cpm * 1000)
    x = 0
    while (binom.cdf(x, sample_size, ctr_0) < type_1_error):
        x += 1
    x -= 1
    if x < 0:
        exp_design = {
            "threshold": np.nan,
            "sample_size": sample_size,
            "random_weight": np.nan,
            "power": np.nan,
            "result": np.nan,
        }
    else:
        alpha_1 = binom.cdf(x, sample_size, ctr_0)
        alpha_2 = binom.cdf(x + 1, sample_size, ctr_0)
        w = (type_1_error - alpha_2) / (alpha_1 - alpha_2)
        if click <= x:
            result = False
        elif click == x + 1:
            if probability_value is None:
                probability_value = random.random()
            result = probability_value < w
        else:
            result = True
        exp_design = {
            "threshold": x,
            "sample_size": sample_size,
            "random_weight": w,
            "power": w * binom.cdf(x, sample_size, ctr_a) + (1 - w) * binom.cdf(x + 1, sample_size, ctr_a),
            "result": result
        }
    return exp_design