from requests.auth import HTTPBasicAuth

from .request_utils import RequestUtils


class IDashV3Utils(RequestUtils):
    MAX_RETRIES = 10
    TIMEOUT = 600

    def __init__(self, credential, expired_time=3600):
        super().__init__(expired_time=expired_time)
        self.host = credential["host"]
        self.username = credential["username"]
        self.password = credential["password"]
        self.auth = HTTPBasicAuth(self.username, self.password)

    def get_rtb_app_install_config(self):
        path = '/restful/v0/ai/model/rtb_app_install'

        response = self.send_get_request(self.host + path, timeout=self.TIMEOUT, auth=self.auth)
        return response.json()

    def get_cid_model_status(self, cid):
        path = '/restful/v0/ai/model/bidding/{}'.format(cid)

        response = self.send_get_request(self.host + path, timeout=self.TIMEOUT, auth=self.auth)
        return response.json()

    def get_user_list_ids(self, cid):

        params = (
            ('ad_scope', 'ad_group'),
            ('ids', cid),
        )
        url = self.host + '/restful/v0/audience_list_binding/ads'
        response = self.send_get_request(url, params=params, auth=self.auth)
        return response.json()


if __name__ == "__main__":
    host = 'https://api.idash.appier.org'
    username = 'robot-ai-rtb-app-install'
    password = 'L3VelCWA6HaiPfWbEeSAkRyXQyn95j9qbi69rfqPL0NTT4Eag0cN7A'
    idash_v3 = IDashV3Utils(host, username, password)
    import ipdb; ipdb.set_trace()
