# -*- coding: utf-8 -*-

import pymysql


class BaseDB(object):

    def __init__(self, host, port, user, password, db_name, charset='utf8'):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.db_name = db_name
        self.charset = charset

        self.conn = None

    def execute_query(self, sql, args=None):
        cursor = self.get_conn().cursor(pymysql.cursors.DictCursor)
        cursor.execute(sql, args)
        return cursor

    def execute_query_many(self, sql, data):
        cursor = self.get_conn().cursor(pymysql.cursors.DictCursor)
        cursor.executemany(sql, data)
        return cursor

    def get_conn(self):
        if not self.conn:
            self.conn = pymysql.connect(host=self.host,
                                        port=self.port,
                                        user=self.user,
                                        passwd=self.password,
                                        db=self.db_name,
                                        charset=self.charset,
                                        ssl={'ssl': {'key': '', 'cert': '', 'ca': ''}})
        return self.conn

    def close(self):
        if self.conn and self.conn.open:
            self.conn.close()
