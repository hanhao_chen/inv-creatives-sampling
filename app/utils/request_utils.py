import requests
import time
from functools import wraps

from app.utils.init_logger import init_logger

logger = init_logger(__name__)


def request_exception_handler(max_retry):
    def handler(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            # args[0]: self
            # args[1]: url
            response = None
            for retry in range(max_retry):
                try:
                    response = func(*args, **kwargs)
                    logger.warning('request to %s, status %s', args[1], response.status_code)
                    if response.status_code == 200:

                        resp_json = response.json()

                        if 'success' in resp_json and resp_json['success'] is False:
                            logger.exception('Request of %s result error: %s, count: %d',
                                                     args[1], resp_json, retry + 1)
                        else:
                            return response

                    if response.status_code in range(400, 500):
                        response.raise_for_status()
                    logger.warning('request to %s failed, retrying...', args[1])

                except requests.exceptions.Timeout:
                    logger.exception('Timeout of %s, count: %d', args[1], retry + 1)
                except requests.exceptions.ConnectionError:
                    logger.exception('Connection error of %s, count: %d', args[1], retry + 1)
                except requests.exceptions.RequestException:
                    logger.exception('Request exception of %s, count: %d', args[1], retry + 1)

                if retry == max_retry - 1:
                    raise Exception("request to %s failed", args[1])
                time.sleep(0.5)
            return response
        return wrapper
    return handler


class RequestUtils:
    def __init__(self, expired_time=3600):
        self.expired_time = expired_time
        self.cookies = None
        self.cookies_timestamp = None

    def login(self):
        pass

    @request_exception_handler(5)
    def send_post_request(self, url, data=None, json=None, headers=None, timeout=180, files=None,
                          is_login_request=False):
        if not is_login_request:
            self.login()
        return requests.post(
            url, data=data, json=json, headers=headers, cookies=self.cookies, timeout=timeout, files=files)

    @request_exception_handler(5)
    def send_get_request(self, url, params=None, headers=None, timeout=180, auth=None):
        self.login()
        return requests.get(url, params=params, headers=headers, cookies=self.cookies, timeout=timeout, auth=auth)
