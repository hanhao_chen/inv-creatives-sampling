# -*- coding: utf-8 -*-
from __future__ import absolute_import
import csv
import datetime
from collections import defaultdict

from app.utils.db import MySQL_DB_AI_DEEP_FUNNEL, MySQL_DB_CAMPAIGN
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.common_utils import in_scope
from app.external_optimization.deep_funnel.utils.constants import APPIER_TO_USD, HYDRA_MODELS

logger = init_logger(__name__)


class HydraOperationSuggestion(object):
    FIELDNAMES = ['date', 'country', 'oid', 'partner_id', 'cid_event_type', 'cid_event_ids_sha1',
                  'cid_goal_type', 'cid_goal_value', 'manager', 'oid_name', 'hydra_adgroups', 'cm_installs', 'cm_kpi',
                  'hydra_installs', 'hydra_kpi', 'action_items', 'action_summary']

    def __init__(self, idash_endpoint, query_db, google_sheet):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.google_sheet = google_sheet
        self.df_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.campaign_client = query_db.db_clients[MySQL_DB_CAMPAIGN]

    def get_raw_data(self, date):
        query = """
        select
            date, n_days, country, oid, partner_id, cid_event_type, cid_event_ids_sha1, cid_goal_value, cid_goal_type,
            oid_name, cm_installs, cm_kpi, hydra_installs, hydra_adgroups, hydra_kpi
        from
            hydra_usage_monitor
        where
            date = '{}'
            and n_days = 30
            and partner_id != 'all'
            and cm_installs > 100
            and hydra_installs / cm_installs < 0.2
            and cid_goal_type not in ('CVR', 'CPA')
            and major_event_types = 'app_install'
        order by
            cm_installs desc
        """.format(date.strftime('%Y-%m-%d'))
        logger.info(query)
        return self.query_db.get_query_result(query, db_client=self.df_client)

    def query_cid_config(self):
        query = """
            SELECT oid, cid, ext_channel, enabled, name
            FROM campaign
            WHERE optimization_model IN ({})
        """.format(in_scope(HYDRA_MODELS))
        hydra_cids = defaultdict(list)
        cid_config = {}
        for row in self.query_db.get_query_result(query, db_client=self.campaign_client):
            hydra_cids[(row['oid'], row['ext_channel'])].append(row['cid'])
            cid_config[row['cid']] = row

        return cid_config, hydra_cids

    def query_oid_config(self):
        query = """
        select
            o.oid, o.status_code, c.manager
        from
        (select oid, status_code from order_info) as o
        left outer join
        (select oid, manager from campaign group by oid) as c
        on o.oid = c.oid
        """
        return {row['oid']: row for row in self.query_db.get_query_result(query, db_client=self.campaign_client)}

    def query_hydra_status(self, date):
        query = """
        select
            b.cid, b.enabled, b.reason
        from
        (select cid, max(date) as last_date from deep_funnel_cid_status where date <= '{}' group by cid) as a
        left outer join
        (select cid, date, enabled, reason from deep_funnel_cid_status) as b
        on a.last_date=b.date and a.cid=b.cid
        """.format(date.strftime('%Y-%m-%d %H:%M:%S'))
        logger.info(query)
        return {row['cid']: row for row in self.query_db.get_query_result(query, db_client=self.df_client)}

    def query_cid_bidprice(self, date, hydra_cids):
        query = """
        select distinct(cid)
        from deep_funnel_cid_event_type_performance
        where
            n_day = 7 and goal_type = 'PRAT_RANGE' and major_count > 0 and update_time > '{}'
        """.format((date - datetime.timedelta(days=7)).strftime('%Y-%m-%d %H:%M:%S'))
        alive_cids = [row['cid'] for row in self.query_db.get_query_result(query, db_client=self.df_client)]

        query = """
            SELECT cid, oid, ext_channel
            FROM campaign
            WHERE is_rtb = 0 AND oid IN ({}) AND ((cid IN ({}) AND enabled = 1) OR optimization_model IN ({}))
        """.format(in_scope((key[0] for key in hydra_cids)), in_scope(alive_cids), in_scope(HYDRA_MODELS))
        cid2key = {
            row['cid']: (row['oid'], row['ext_channel'])
            for row in self.query_db.get_query_result(query, db_client=self.campaign_client)
        }

        cid_bidprice = {}
        bid_prices = self.idash_endpoint.read_campaigns_by_cids(cid2key.keys(), fields=['ext_bid_price'])
        for row in bid_prices:
            key = cid2key[row['cid']]
            group = 'hydra' if row['cid'] in hydra_cids[key] else 'cm'
            if key not in cid_bidprice:
                cid_bidprice[key] = {'hydra': {}, 'cm': {}}
            cid_bidprice[key][group][row['cid']] = float(row['ext_bid_price']) * APPIER_TO_USD

        return cid_bidprice

    def get_cid_hydra_label(self, cid_name):
        if cid_name.startswith('[Hydra_OID]'):
            return 'HydraOID'
        elif cid_name.startswith('[Hydra_SM]'):
            return 'HydraSM'
        elif cid_name.startswith('[Hydra_BL]'):
            return 'HydraBL'
        elif cid_name.startswith('[Hydra_W'):
            return 'HydraWL'

        return None

    def recommend_action_items(self, raw_data, hydra_cids, cid_config, oid_config, last_hydra_status, cid_bidprice):
        rows = []
        for row in raw_data:
            if oid_config[row['oid']]['status_code'].lower() in ('finished', 'new', 'stopped'):
                continue
            row['manager'] = oid_config[row['oid']]['manager']

            action_items = []
            no_minor_event = row['cm_kpi'] <= 0
            cm_reach_gaol = row['cid_goal_value'] <= row['cm_kpi'] and not no_minor_event
            has_hydra_adgroups = row['hydra_adgroups'] > 0

            if no_minor_event:
                action_items.append('check no minor event')
            if not cm_reach_gaol:
                action_items.append('lower minor goal value')
            if not has_hydra_adgroups:
                action_items.append('create hydra adgroup')
            else:
                need_review_hydra_performance = cm_reach_gaol and row['hydra_installs'] > 50 \
                    and row['cid_goal_value'] > row['hydra_kpi']
                if need_review_hydra_performance:
                    action_items.append('review hydra performance')
            action_summary = set(action_items)

            oid_partner = (row['oid'], row['partner_id'])
            cm_prices = cid_bidprice.get(oid_partner, {}).get('cm')
            max_cm_price = max(cm_prices.values()) if cm_prices else None
            max_price_cm_cid = max(cm_prices, key=cm_prices.get) if cm_prices else None
            for cid in hydra_cids[oid_partner]:
                hydra_label = self.get_cid_hydra_label(cid_config[cid]['name'])
                if hydra_label is None:
                    continue

                enabled = cid_config[cid]['enabled']
                cm_close_it = not enabled and last_hydra_status.get(cid, {}).get('enabled') != 0
                if cm_close_it and not need_review_hydra_performance:
                    action_items.append('open {} cid: {}'.format(hydra_label, cid))
                    action_summary.add('open hydra cids')

                hydra_close_it = not enabled and last_hydra_status.get(cid, {}).get('enabled') == 0
                if hydra_close_it and last_hydra_status[cid]['reason'] != 'empty_list':
                    action_items.append('check {} cid: {} goal setting'.format(hydra_label, cid))
                    action_summary.add('check hydra goal setting')

                if enabled and not need_review_hydra_performance and hydra_label in ('HydraOID', 'HydraSM', 'HydraWL') \
                        and max_cm_price and cid_bidprice[oid_partner]['hydra'].get(cid, 0.0) < max_cm_price:
                    action_items.append('increase {} cid: {} bid price to {} like cid: {}'.format(
                        hydra_label, cid, max_cm_price, max_price_cm_cid))
                    action_summary.add('increase bid price to CM\'s adgroup')

            row['action_items'] = '\n'.join(action_items)
            row['action_summary'] = '\n'.join(action_summary)
            row['date'] = row['date'].strftime('%Y-%m-%d')
            rows.append(row)

        return rows

    def get_hydra_operation_suggestions(self, date):
        raw_data = self.get_raw_data(date)
        cid_config, hydra_cids = self.query_cid_config()
        oid_config = self.query_oid_config()
        last_hydra_status = self.query_hydra_status(date)
        cid_bidprice = self.query_cid_bidprice(date, hydra_cids)
        return self.recommend_action_items(
            raw_data, hydra_cids, cid_config, oid_config, last_hydra_status, cid_bidprice)

    def output_csvfile(self, filename, rows):
        with open(filename, 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(self.FIELDNAMES)
            for row in rows:
                writer.writerow([u'{}'.format(row[key]).encode('utf-8') for key in self.FIELDNAMES])

    def output_spreadsheet(self, sheet_name, rows):
        sheet_content = [self.FIELDNAMES + ['CM note', 'PM note']]
        for row in rows:
            sheet_content.append([row[key] for key in self.FIELDNAMES] + ['', ''])
        self.google_sheet.update_sheet_content(sheet_name, sheet_content)

    def output_mysql_table(self, table_name, rows):
        self.init_db_table(table_name)
        update_content = []
        for row in rows:
            update_content.append([u'{}'.format(row[key]).replace('\n', ';') for key in self.FIELDNAMES])

        query = """
        REPLACE INTO {}
        ({})
        VALUES ({})
        """.format(table_name, ','.join(self.FIELDNAMES), ','.join(['%s'] * len(self.FIELDNAMES)))
        logger.info(query)
        self.query_db.execute_query_many(query, update_content, db_client=self.df_client)

    def init_db_table(self, table_name):
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `date` date NOT NULL,
        `country` varchar(32) NOT NULL DEFAULT '',
        `oid` varchar(32) NOT NULL DEFAULT '',
        `partner_id` varchar(32) NOT NULL DEFAULT '',
        `cid_event_type` varchar(32) NOT NULL DEFAULT '',
        `cid_event_ids_sha1` varchar(8) NOT NULL DEFAULT '',
        `cid_goal_type` varchar(32) NOT NULL DEFAULT '',
        `cid_goal_value` float NOT NULL DEFAULT '0',
        `manager` varchar(64) NOT NULL DEFAULT '',
        `oid_name` varchar(64) NOT NULL DEFAULT '',
        `hydra_adgroups` int(11) NOT NULL DEFAULT '0',
        `cm_installs` float NOT NULL DEFAULT '-1',
        `cm_kpi` float NOT NULL DEFAULT '-1',
        `hydra_installs` float NOT NULL DEFAULT '-1',
        `hydra_kpi` float NOT NULL DEFAULT '-1',
        `action_items` text NOT NULL DEFAULT '',
        `action_summary` text NOT NULL DEFAULT '',
        PRIMARY KEY (`date`, `country`, `oid`, `partner_id`,
                     `cid_event_type`, `cid_event_ids_sha1`, `cid_goal_type`, `cid_goal_value`),
        KEY `oid` (`oid`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_client)
