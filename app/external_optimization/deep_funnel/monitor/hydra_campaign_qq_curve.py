import csv

from app.utils.db import MySQL_DB_AI_DEEP_FUNNEL, MySQL_DB_CAMPAIGN
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.constants import HYDRA_MODELS
from app.external_optimization.deep_funnel.utils.optimization_goal_query_agent import OidGoalQueryAgent
from app.external_optimization.deep_funnel.utils.init_mysql_tables import MySQLTables

logger = init_logger(__name__)


class CampaignQQCurveTable(MySQLTables):
    COLUMNS = ['date', 'n_day', 'oid', 'cid', 'partner_id', 'event_type', 'goal_type', 'goal_value',
               'adgroup_type', 'achieve_goal_rate', 'installs']

    def __init__(self, query_db, campaign_qq_curve_table):
        super(CampaignQQCurveTable, self).__init__(query_db, query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL])
        self.campaign_qq_curve_table = campaign_qq_curve_table
        self.init_tables_from_config(None)

    def init_tables_from_config(self, _table_config):
        query = """
        CREATE TABLE IF NOT EXISTS `{}` (
            `date` date NOT NULL,
            `n_day` int(20) NOT NULL,
            `cid` varchar(32) NOT NULL,
            `achieve_goal_rate` float NOT NULL,
            `oid` varchar(32) NOT NULL,
            `partner_id` varchar(32) NOT NULL,
            `event_type` varchar(32) NOT NULL,
            `goal_type` varchar(32) NOT NULL,
            `goal_value` float NOT NULL,
            `adgroup_type` varchar(16) NOT NULL,
            `installs` int(20) NOT NULL DEFAULT '0',
            PRIMARY KEY (`date`, `n_day`, `cid`, `achieve_goal_rate`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        PARTITION BY RANGE (TO_DAYS(`date`)) (
            {}
        );
        """.format(self.campaign_qq_curve_table, ',\n\t\t'.join(
            'PARTITION {} VALUES LESS THAN ({})'.format(p_name, self.get_partitioning_value('date', p_boundary))
            for (p_name, p_boundary) in self.get_monthly_partitions()))
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def update_campaign_qq_curve_table(self, rows):
        query = """
        REPLACE INTO {} ({}) VALUES ({})
        """.format(self.campaign_qq_curve_table, ','.join(self.COLUMNS), ','.join(['%s'] * len(self.COLUMNS)))
        logger.info(query)
        self.query_db.execute_query_many(query, rows, db_client=self.db_client)

    def write_local_file(self, filename, rows):
        with open(filename, 'w') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(self.COLUMNS)
            writer.writerows(rows)


class CampaignQQCurveData(object):

    def __init__(self, query_db):
        self.query_db = query_db
        self.df_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.campaign_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.oid_goal_query_agent = OidGoalQueryAgent(query_db)

    def get_qq_curve_data(self, date_str, n_day):
        oids = self.get_oids(date_str, n_day)
        logger.info('update %s oids for %sD on %s', len(oids), n_day, date_str)
        rows = []
        for oid in oids:
            rows.extend(self.get_campaign_qq_curve_data(date_str, n_day, oid))

        return rows

    def get_oids(self, date_str, n_day):
        query = """
        SELECT
            distinct(oid)
        FROM
            hydra_usage_monitor
        WHERE
            date = '{}'
            and n_days = {}
            and partner_id != 'all'
            and major_event_types = 'app_install'
            and cid_goal_type not in ('CPA', 'CVR')
        """.format(date_str, n_day)
        logger.info(query)
        return [row['oid'] for row in self.query_db.get_query_result(query, db_client=self.df_client)]

    def get_campaign_qq_curve_data(self, date_str, n_day, oid):
        minor_goals = self.oid_goal_query_agent.get_oid_minor_goals(oid)
        if not minor_goals:
            logger.warning('oid %s no goal', oid)
            return []

        cids_config = self.get_cids_config(oid)
        cids_performance = self.get_cids_performance(minor_goals[0], n_day, cids_config.keys())

        rows = []
        for cid, performance_data in cids_performance.iteritems():
            row_template = [
                date_str, n_day, oid, cid, cids_config[cid]['partner_id'], minor_goals[0].event_type,
                minor_goals[0].goal_type, minor_goals[0].goal_value, cids_config[cid]['adgroup_type']
            ]
            bins = {str(i * 0.05): 0.0 for i in range(21)}
            for metrics in performance_data.values():
                if not metrics['major_count'] or not metrics['installs']:
                    continue
                kpi = float(metrics['minor_count']) / metrics['major_count']
                bins[str(int(min(1.0, kpi / minor_goals[0].goal_value) * 20) * 0.05)] += metrics['installs']

            for achieve_goal_rate, installs in bins.iteritems():
                rows.append(row_template + [achieve_goal_rate, installs])

        return rows

    def get_cids_config(self, oid):
        query = """
        SELECT
            cid, ext_channel as partner_id, IF(optimization_model in ({}), 'hydra', 'cm') as adgroup_type
        FROM
            campaign
        WHERE
            oid = '{}'
            and is_rtb = 0
        """.format(','.join("'{}'".format(model) for model in HYDRA_MODELS), oid)
        logger.info(query)
        cids_config = {row['cid']: row for row in self.query_db.get_query_result(query, db_client=self.campaign_client)}
        return cids_config

    def get_cids_performance(self, minor_goal, n_day, cids):
        # TODO: refactor with HydraPerformanceCache
        if minor_goal.event_ids:
            metrics_table = 'deep_funnel_cid_event_id_app_performance'
            event_cond = 'event_id in ({})'.format(
                ','.join("'{}'".format(event_id.replace("'", "\\'")) for event_id in minor_goal.event_ids))
        else:
            metrics_table = 'deep_funnel_cid_event_type_app_performance'
            event_cond = 'event_type = \'{}\''.format(minor_goal.event_type)

        query = """
        SELECT
            quality.cid, quality.subpublisher, quality.major_count, quality.minor_count, quantity.installs
        FROM
        (
            SELECT
                cid, subpublisher, sum(major_count) / count(1) as major_count, sum(minor_count) as minor_count
            FROM
                {0}
            WHERE
                cid in ({1})
                and {2}
                and goal_type = '{3}'
                and n_day = {4}
            GROUP BY
                cid, subpublisher
        ) as quality
        LEFT OUTER JOIN
        (
            SELECT
                cid, subpublisher, sum(major_count) / count(1) as installs
            FROM
                {0}
            WHERE
                cid in ({1})
                and {2}
                and goal_type = 'PRAT-30D'
                and n_day = {4}
            GROUP BY
                cid, subpublisher
        ) as quantity
        ON quality.cid = quantity.cid and quality.subpublisher = quantity.subpublisher
        """.format(metrics_table, ','.join('\'' + cid + '\'' for cid in cids), event_cond, minor_goal.goal_type, n_day)
        logger.info(query)
        cids_performance = {}
        for row in self.query_db.get_query_result(query, db_client=self.df_client):
            if row['cid'] not in cids_performance:
                cids_performance[row['cid']] = {}
            cids_performance[row['cid']][row['subpublisher']] = row

        return cids_performance
