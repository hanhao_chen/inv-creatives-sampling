from __future__ import absolute_import, division
import json
import datetime
from collections import defaultdict

from app.utils.db import MySQL_DB_AI_DEEP_FUNNEL
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.common_utils import safe_div


logger = init_logger(__name__)


class HydraAlertMonitor(object):
    """AI-5880 Daily alert for Hydra cids
    """

    SCHEMA = ['date', 'cid', 'cid_begin_date', 'cid_name', 'oid', 'oid_name', 'partner_id', 'manager',
              'kpi_goal_type', 'kpi_event_type', 'kpi_goal_value', 'hydra_label',
              'current_whitelist_size', 'previous_whitelist_size', 'current_blacklist_size', 'previous_blacklist_size',
              'current_kpi', 'previous_kpi', 'current_cid_installs', 'previous_cid_installs',
              'whitesize_sudden_drop', 'blacksize_sudden_burst', 'install_sudden_drop', 'install_sudden_burst']
    INPUT_KEYS = ['date', 'cid', 'cid_begin_date', 'cid_name', 'oid', 'oid_name', 'partner_id', 'manager',
                  'kpi_goal_type', 'kpi_event_type', 'kpi_goal_value', 'hydra_label']
    KEYS = ['date', 'cid']

    def __init__(self, query_db, check_date, output_table_name, input_table_name):
        self.query_db = query_db
        self.check_date = check_date
        self.the_check_datetime = datetime.datetime.strptime(check_date, '%Y%m%d')
        self.output_table_name = output_table_name
        self.input_table_name = input_table_name

        self.deep_funnel_client = self.query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.init_db_table()

    def init_db_table(self):
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `date` date NOT NULL,
        `cid` varchar(32) NOT NULL DEFAULT '',
        `cid_begin_date` date NOT NULL,
        `cid_name` varchar(64) NOT NULL DEFAULT '',
        `oid` varchar(32) NOT NULL DEFAULT '',
        `oid_name` varchar(64) NOT NULL DEFAULT '',
        `partner_id` varchar(32) NOT NULL DEFAULT '',
        `manager` varchar(32) NOT NULL DEFAULT '',
        `kpi_goal_type` varchar(32) NOT NULL DEFAULT '',
        `kpi_event_type` varchar(32) NOT NULL DEFAULT '',
        `kpi_goal_value` float NOT NULL DEFAULT '0',
        `hydra_label` varchar(32) NOT NULL DEFAULT '',
        `current_whitelist_size` float NOT NULL DEFAULT '-1',
        `previous_whitelist_size` float NOT NULL DEFAULT '-1',
        `current_blacklist_size` float NOT NULL DEFAULT '-1',
        `previous_blacklist_size` float NOT NULL DEFAULT '-1',
        `current_kpi` float NOT NULL DEFAULT '-1',
        `previous_kpi` float NOT NULL DEFAULT '-1',
        `current_cid_installs` float NOT NULL DEFAULT '-1',
        `previous_cid_installs` float NOT NULL DEFAULT '-1',
        `whitesize_sudden_drop` tinyint(1) NOT NULL DEFAULT '0',
        `blacksize_sudden_burst` tinyint(1) NOT NULL DEFAULT '0',
        `install_sudden_drop` tinyint(1) NOT NULL DEFAULT '0',
        `install_sudden_burst` tinyint(1) NOT NULL DEFAULT '0',
        `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`date`, `cid`),
        KEY `cid` (`cid`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(self.output_table_name)
        logger.debug(query)
        self.query_db.execute_query(query, db_client=self.deep_funnel_client)

    def update_alert_monitor(self):
        logger.info('update hydra alert monitor on %s into %s', self.check_date, self.output_table_name)

        check_date_data = self._get_check_date_data()
        self._fill_previous_status(check_date_data)
        alert_cids = self._fill_alert_status(check_date_data)
        rows = self._get_rows_to_db(check_date_data)
        self.update_db_content(rows)

        if alert_cids:
            log_str = '\n'.join('cid: {}, alerts: {}'.format(cid, json.dumps(reasons))
                                for cid, reasons in alert_cids.iteritems())
            logger.error('get %s cid alerts\n%s', len(alert_cids), log_str)

    def _get_check_date_data(self):
        query = 'select {} from {} where date = \'{}\''.format(
            ', '.join(self.INPUT_KEYS), self.input_table_name, self.check_date)
        logger.info(query)
        check_date_data = self.query_db.get_query_result(query, db_client=self.deep_funnel_client)
        logger.info('get %s hydra cids on %s', len(check_date_data), self.check_date)
        return check_date_data

    def _fill_previous_status(self, check_date_data, decay_rate=0.9):
        previous_status = defaultdict(lambda: defaultdict(float))

        # get previous/current installs and whitelist size
        previous_date = (self.the_check_datetime - datetime.timedelta(days=1)).strftime('%Y%m%d')
        query = """
        select
            cid, date, whitelist_size, blacklist_size, cid_installs
        from {}
        where
            date in (\'{}\', \'{}\')
        """.format(self.input_table_name, previous_date, self.check_date)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.deep_funnel_client)
        for row in results:
            prefix = 'current' if row['date'].strftime('%Y%m%d') == self.check_date else 'previous'
            previous_status[row['cid']]['{}_whitelist_size'.format(prefix)] = float(row['whitelist_size'])
            previous_status[row['cid']]['{}_blacklist_size'.format(prefix)] = float(row['blacklist_size'])
            previous_status[row['cid']]['{}_cid_installs'.format(prefix)] = float(row['cid_installs'])

        # get previous/current 30D decayed KPI
        kpi_query_date = (self.the_check_datetime - datetime.timedelta(days=31)).strftime('%Y%m%d')
        query = 'select cid, date, cid_major, cid_minor from {} where date between \'{}\' and \'{}\''.format(
            self.input_table_name, kpi_query_date, self.check_date)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.deep_funnel_client)
        for row in results:
            delta_days = (self.the_check_datetime.date() - row['date']).days
            current_decay_rate = decay_rate ** delta_days
            previous_decay_rate = current_decay_rate / decay_rate
            if delta_days <= 30:
                previous_status[row['cid']]['current_cid_major'] += (float(row['cid_major']) * current_decay_rate)
                previous_status[row['cid']]['current_cid_minor'] += (float(row['cid_minor']) * current_decay_rate)
            if delta_days >= 1:
                previous_status[row['cid']]['previous_cid_major'] += (float(row['cid_major']) * previous_decay_rate)
                previous_status[row['cid']]['previous_cid_minor'] += (float(row['cid_minor']) * previous_decay_rate)

        # fill previous/current metrics
        for row in check_date_data:
            for field in ('current_whitelist_size', 'previous_whitelist_size',
                          'current_blacklist_size', 'previous_blacklist_size',
                          'current_cid_installs', 'previous_cid_installs'):
                row[field] = previous_status[row['cid']][field]
            for tag in ('current', 'previous'):
                row['{}_kpi'.format(tag)] = safe_div(previous_status[row['cid']]['{}_cid_minor'.format(tag)],
                                                     previous_status[row['cid']]['{}_cid_major'.format(tag)],
                                                     default=0.0)

    def _fill_alert_status(self, check_date_data):
        def is_stable(row, current_field, previous_field, threshold):
            return row[current_field] > 0 and row[previous_field] > 0 \
                and abs(row[current_field] - row[previous_field]) / row[previous_field] < threshold

        def is_drop(row, current_field, previous_field, threshold):
            return row[current_field] > 0 and row[previous_field] > 0 and row[previous_field] > row[current_field] \
                and (row[previous_field] - row[current_field]) / row[previous_field] >= threshold

        def is_burst(row, current_field, previous_field, threshold):
            return row[current_field] > 0 and row[previous_field] > 0 and row[previous_field] < row[current_field] \
                and (row[current_field] - row[previous_field]) / row[previous_field] >= threshold

        alert_cids = defaultdict(list)
        for row in check_date_data:
            is_stable_performance = is_stable(row, 'current_kpi', 'previous_kpi', 0.1)
            is_whitesize_drop = is_drop(row, 'current_whitelist_size', 'previous_whitelist_size', 0.3)
            is_blacksize_burst = is_burst(row, 'current_blacklist_size', 'previous_blacklist_size', 0.3)
            is_install_drop = is_drop(row, 'current_cid_installs', 'previous_cid_installs', 0.3)
            is_install_burst = is_burst(row, 'current_cid_installs', 'previous_cid_installs', 0.3)
            is_enough_install = row['previous_cid_installs'] >= 30.0
            has_install = row['current_cid_installs'] > 0 or row['previous_cid_installs'] > 0

            row['whitesize_sudden_drop'] = row['hydra_label'].startswith('White') and is_stable_performance \
                and (row['current_whitelist_size'] == -1 or is_whitesize_drop) and has_install
            row['blacksize_sudden_burst'] = row['hydra_label'].startswith('Black') and is_stable_performance \
                and is_blacksize_burst and has_install
            row['install_sudden_drop'] = row['hydra_label'] != 'Garbage' and is_stable_performance \
                and is_install_drop and is_enough_install
            row['install_sudden_burst'] = row['hydra_label'] != 'Gargabe' and is_stable_performance \
                and is_install_burst and is_enough_install

            for reason in ('whitesize_sudden_drop', 'blacksize_sudden_burst',
                           'install_sudden_drop', 'install_sudden_burst'):
                if row[reason]:
                    alert_cids[row['cid']].append(reason)

        return alert_cids

    def _get_rows_to_db(self, check_date_data):
        def boolean_to_str(value):
            return '1' if value else '0'

        rows = []
        for row in check_date_data:
            rows.append([row['date'].strftime('%Y%m%d'), row['cid'], row['cid_begin_date'], row['cid_name'], row['oid'],
                         row['oid_name'], row['partner_id'], row['manager'], row['kpi_goal_type'],
                         row['kpi_event_type'], row['kpi_goal_value'], row['hydra_label'],
                         str(row['current_whitelist_size']), str(row['previous_whitelist_size']),
                         str(row['current_blacklist_size']), str(row['previous_blacklist_size']),
                         str(row['current_kpi']), str(row['previous_kpi']),
                         str(row['current_cid_installs']), str(row['previous_cid_installs']),
                         boolean_to_str(row['whitesize_sudden_drop']), boolean_to_str(row['blacksize_sudden_burst']),
                         boolean_to_str(row['install_sudden_drop']), boolean_to_str(row['install_sudden_burst'])])

        return rows

    def update_db_content(self, rows):
        query = """
        INSERT INTO {}
        ({})
        VALUES ({})
        ON DUPLICATE KEY UPDATE {}
        """.format(self.output_table_name, ','.join(self.SCHEMA), ','.join(['%s'] * len(self.SCHEMA)),
                   ','.join('`{0}`=values(`{0}`)'.format(col) for col in self.SCHEMA if col not in self.KEYS))
        logger.info(query)
        self.query_db.execute_query_many(query, rows, db_client=self.deep_funnel_client)
