from __future__ import absolute_import, division
import json
import datetime
from collections import defaultdict

from app.module.CQueryDB import CQueryDB
from app.utils.init_logger import init_logger
from app.utils.util import date_to_timestamp

from app.external_optimization.deep_funnel.utils.constants import APPIER_TO_USD, HYDRA_MODELS
from app.external_optimization.deep_funnel.utils.common_utils import patch_metric_row, \
    get_external_capability, AppUsage

logger = init_logger(__name__)


class HydraPerformanceMonitor(object):
    """Daily performance monitory for Hydra cids. """
    SCHEMA = [
        'date', 'cid', 'cid_begin_date', 'cid_name', 'oid', 'oid_name', 'partner_id', 'root_cid', 'manager',
        'optimization_model', 'whitelist_size', 'blacklist_size', 'roaslist_diff', 'kpi_goal_type', 'kpi_event_type',
        'kpi_goal_value', 'cid_status', 'hydra_label', 'cid_bid_price', 'cid_budget', 'oid_partner_bid_prices',
        'oid_partner_budgets', 'cid_major', 'cid_minor', 'cid_kpi', 'oid_partner_major', 'oid_partner_minor',
        'oid_partner_kpi', 'oid_major', 'oid_minor', 'oid_kpi', 'cid_clicks', 'cid_installs', 'oid_partner_clicks',
        'oid_partner_installs', 'oid_clicks', 'oid_installs', 'oid_hydra_clicks', 'oid_hydra_installs'
    ]
    KEYS = ['date', 'cid']

    def __init__(self, idash_endpoint, query_db, adgroup_trees, root_cids, start_date, output_table_name,
                 cid_config_history_agent, tz=8):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.adgroup_trees = adgroup_trees
        self.root_cids = root_cids
        self.start_date = start_date
        self.output_table_name = output_table_name
        self.cid_config_history_agent = cid_config_history_agent
        self.tz = tz

        self.external_capability = get_external_capability(query_db)
        self.campaign_cache_client = self.query_db.get_db_client_by_table(CQueryDB.TABLE_CAMPAIGN)
        self.deep_funnel_client = self.query_db.get_db_client_by_table(CQueryDB.TABLE_DF_PREDICTION_EXTENTION)
        self.fraud_client = self.query_db.get_db_client_by_table(CQueryDB.TABLE_EXT_AI_PERDAY_STREAM_HOUR)
        self.all_cids = self._init_all_cids()
        self.cid_optimization_goals = self._get_cid_optimization_goals()
        self.cid_config = self._get_cid_config()
        end_date = datetime.datetime.strptime(
            (datetime.datetime.utcnow() + datetime.timedelta(hours=tz)).strftime('%Y%m%d'), '%Y%m%d').date()
        self.cid_daily_optimization_model = cid_config_history_agent.get_cids_daily_config(
            datetime.datetime.strptime(start_date, '%Y%m%d').date(), end_date, 'optimization_model',
            cids=self.all_cids, tz=tz, raise_exception=False)
        self.init_db_table()
        self.raise_exception = False

    def init_db_table(self):
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `date` date NOT NULL,
        `cid` varchar(32) NOT NULL DEFAULT '',
        `cid_begin_date` date NOT NULL,
        `cid_name` varchar(64) NOT NULL DEFAULT '',
        `oid` varchar(32) NOT NULL DEFAULT '',
        `oid_name` varchar(64) NOT NULL DEFAULT '',
        `partner_id` varchar(32) NOT NULL DEFAULT '',
        `root_cid` varchar(32) NOT NULL DEFAULT '',
        `manager` varchar(32) NOT NULL DEFAULT '',
        `optimization_model` varchar(64) NOT NULL DEFAULT '',
        `whitelist_size` float NOT NULL DEFAULT '0',
        `blacklist_size` float NOT NULL DEFAULT '0',
        `roaslist_diff` float NOT NULL DEFAULT '0',
        `kpi_goal_type` varchar(32) NOT NULL DEFAULT '',
        `kpi_event_type` varchar(32) NOT NULL DEFAULT '',
        `kpi_goal_value` float NOT NULL DEFAULT '0',
        `cid_status` tinyint(1) NOT NULL,
        `hydra_label` varchar(32) NOT NULL DEFAULT '',
        `cid_bid_price` float NOT NULL DEFAULT '0',
        `cid_budget` float NOT NULL DEFAULT '0',
        `oid_partner_bid_prices` text NOT NULL,
        `oid_partner_budgets` text NOT NULL,
        `cid_major` float NOT NULL DEFAULT '-1',
        `cid_minor` float NOT NULL DEFAULT '-1',
        `cid_kpi` float NOT NULL DEFAULT '-1',
        `oid_partner_major` float NOT NULL DEFAULT '-1',
        `oid_partner_minor` float NOT NULL DEFAULT '-1',
        `oid_partner_kpi` float NOT NULL DEFAULT '-1',
        `oid_major` float NOT NULL DEFAULT '-1',
        `oid_minor` float NOT NULL DEFAULT '-1',
        `oid_kpi` float NOT NULL DEFAULT '-1',
        `cid_clicks` float NOT NULL DEFAULT '-1',
        `cid_installs` float NOT NULL DEFAULT '-1',
        `oid_partner_clicks` float NOT NULL DEFAULT '-1',
        `oid_partner_installs` float NOT NULL DEFAULT '-1',
        `oid_clicks` float NOT NULL DEFAULT '-1',
        `oid_installs` float NOT NULL DEFAULT '-1',
        `oid_hydra_clicks` float NOT NULL DEFAULT '-1',
        `oid_hydra_installs` float NOT NULL DEFAULT '-1',
        `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`date`, `cid`),
        KEY `cid` (`cid`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(self.output_table_name)
        logger.debug(query)
        self.query_db.execute_query(query, db_client=self.deep_funnel_client)

    def _init_all_cids(self):
        all_cids = set()
        for root_cid in self.root_cids:
            all_cids |= self.adgroup_trees.get_tree_cids_set(root_cid)

        return all_cids

    def _get_cid_config(self):
        query = """
        select
            a.oid, a.oid_name, a.goalcpa, b.cid, b.ext_channel, b.manager, b.name as cid_name,
            b.optimization_model, b.begin_date
        from
        (
            select aa.oid, bb.name as oid_name, bb.goalcpa
            from
            (select distinct(oid) from campaign where cid in ({})) as aa
            left outer join
            (select oid, name, goalcpa from order_info) as bb
            on aa.oid = bb.oid
        ) as a
        left outer join
        (
            select
                cid, ext_channel, oid, manager, name, optimization_model,
                from_unixtime(begin_at, '%Y%m%d') as begin_date
            from campaign
        ) as b
        on a.oid = b.oid
        """.format(','.join(['\'{}\''.format(cid) for cid in self.all_cids]))
        logger.debug(query)
        cid_config = {}
        oid_partner_cids = defaultdict(set)
        result = self.query_db.get_query_result(query, db_client=self.campaign_cache_client)
        for row in result:
            row['goalcpa'] = float(row['goalcpa']) * APPIER_TO_USD
            cid_config[row['cid']] = row
            oid_partner_cids[(row['oid'], row['ext_channel'])].add(row['cid'])

        for cid, config in cid_config.iteritems():
            cid_config[cid]['partner_cids'] = list(
                oid_partner_cids[(config['oid'], config['ext_channel'])] - set([cid]))

        result = self.idash_endpoint.read_campaigns_by_cids(
            list(cid_config.keys()), fields=['enabled', 'daily_max_budget', 'ext_bid_price'])
        for row in result:
            cid_config[row['cid']].update({
                'enabled': row['enabled'],
                'daily_max_budget': float(row['daily_max_budget']) * APPIER_TO_USD,
                'ext_bid_price': float(row['ext_bid_price']) * APPIER_TO_USD
            })

        return cid_config

    def _get_cid_optimization_goals(self):
        query = """select cid, event_id, event_type from event_id where cid in ({})""".format(
            ','.join(['\'{}\''.format(cid) for cid in self.all_cids]))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_cache_client)
        default_eventid_set = defaultdict(set)
        for row in result:
            default_eventid_set[(row['cid'], row['event_type'])].add(row['event_id'])

        query = """
        select
            a.cid, a.type as goal_type, a.goal_value, a.event_type, b.event_ids
        from
        (
            select
                aa.cid, aa.event_ids_sha1, aa.type, aa.event_type, aa.goal_value
            from
            (
                select
                    cid, event_ids_sha1, type, event_type, goal_value, order_seq
                from optimization_goal
                where
                    cid in ({}) and is_major = 0
            ) as aa
            left outer join
            (
                select cid,  min(order_seq) as target_order
                from optimization_goal
                where is_major = 0 and order_seq != 0 group by cid
            ) as bb
            on aa.cid = bb.cid
            where aa.order_seq = bb.target_order
        ) as a
        left outer join
        (
            select * from optimization_goal_eids_hash
        ) as b
        on a.event_ids_sha1 = b.event_ids_sha1
        """.format(','.join(['\'{}\''.format(cid) for cid in self.all_cids]))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_cache_client)

        cid_optimization_goals = {}
        for row in result:
            cid_optimization_goals[row['cid']] = {
                'goal_type': row['goal_type'],
                'goal_value': row['goal_value'] * (APPIER_TO_USD if 'ROAS' in row['goal_type'] else 1.0),
                'event_type': row['event_type'],
                'event_ids': row['event_ids'].split(';')
                if row['event_ids'] else list(default_eventid_set[(row['cid'], row['event_type'])])
            }

        no_goal_cids = self.root_cids - set(cid_optimization_goals.keys())
        if no_goal_cids:
            logger.warning('no goal cids: %s', no_goal_cids)
        self.root_cids &= set(cid_optimization_goals.keys())
        self.all_cids = self._init_all_cids()

        return cid_optimization_goals

    def update_performance_monitor(self):
        logger.info('update hydra performance monitor from %s into %s', self.start_date, self.output_table_name)
        whitelist_size, blacklist_size, roaslist_diff = self._get_bwlist_size()
        logger.info('get kpi metrics')
        kpi_metrics = self._get_kpi_metrics()
        logger.info('get quantity metrics')
        quantity_metrics = self._get_quantity_metrics()

        daily_sheet_content = self._get_sheet_content(
            blacklist_size, whitelist_size, roaslist_diff, kpi_metrics, quantity_metrics)
        query = """
        INSERT INTO {}
        ({})
        VALUES ({})
        ON DUPLICATE KEY UPDATE {}
        """.format(self.output_table_name, ','.join(self.SCHEMA), ','.join(['%s'] * len(self.SCHEMA)),
                   ','.join('`{0}`=values(`{0}`)'.format(col) for col in self.SCHEMA if col not in self.KEYS))
        logger.info(query)
        logger.info(daily_sheet_content[0])
        self.query_db.execute_query_many(query, daily_sheet_content, db_client=self.deep_funnel_client)
        if self.raise_exception:
            logger.error('has log error')

    def _get_bwlist_size(self):
        whitelist_size = defaultdict(lambda: defaultdict(int))
        blacklist_size = defaultdict(lambda: defaultdict(int))
        roaslist_diff = defaultdict(lambda: defaultdict(int))
        query = """
        select
            date_hour, cid, partner_id, hydra_label, require_publisher, require_sub_publisher,
            reject_publisher, reject_sub_publisher, note
        from
            hydra_list_control
        where
            cid in ({}) and date_hour >= '{}'
        """.format(','.join(['\'{}\''.format(cid) for cid in self.all_cids]),
                   datetime.datetime.strptime(self.start_date, '%Y%m%d').strftime('%Y-%m-%d 00:00:00'))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.deep_funnel_client)
        for row in result:
            the_datetime = row['date_hour'] + datetime.timedelta(hours=self.tz)
            date_hr_str = the_datetime.strftime('%Y%m%d %H')
            date_str = the_datetime.strftime('%Y%m%d')
            note = json.loads(row['note']) if row['note'] else {}
            app_usage = AppUsage.get_pub_sub_usage(self.external_capability[row['partner_id']])
            if app_usage == AppUsage.BOTH:
                whitelist_field = 'require_publisher'
                if row['hydra_label'] == 'HydraBL':
                    blacklist_field = 'reject_sub_publisher'
                elif row['hydra_label'] != 'legacy':
                    blacklist_field = 'reject_publisher' if row[whitelist_field] == '' else 'reject_sub_publisher'
                else:
                    blacklist_field = 'reject_publisher' \
                        if row[whitelist_field] == '' and row['reject_publisher'] != '' else 'reject_sub_publisher'
            elif app_usage.needs_sub_performance:
                whitelist_field = 'require_sub_publisher'
                blacklist_field = 'reject_sub_publisher'
            else:
                whitelist_field = 'require_publisher'
                blacklist_field = 'reject_publisher'

            others_blacklist_field = 'others_sub_count' \
                if blacklist_field == 'reject_sub_publisher' else 'others_pub_count'
            others_blacklist_cnt = note.get(others_blacklist_field, 0) \
                if 'legacy_b_log' not in note else note['legacy_b_log'].get(others_blacklist_field, 0)

            for (field, list_size) in [(whitelist_field, whitelist_size), (blacklist_field, blacklist_size)]:
                size = len(row[field].split(',')) if row[field] else 0
                if field == blacklist_field:
                    size += others_blacklist_cnt
                list_size[date_hr_str][row['cid']] = size
                list_size[date_str][row['cid']] += size
                if size:
                    list_size[date_str][(row['cid'], 'cnt')] += 1

            cid_roaslist_diff = note.get('roas_removed_sub_cnt', 0) + note.get('roas_removed_pub_cnt', 0) \
                if row['hydra_label'] == 'HydraBL' \
                else note.get('roas_added_sub_cnt', 0) + note.get('roas_added_pub_cnt', 0)
            roaslist_diff[date_str][row['cid']] += cid_roaslist_diff
            if cid_roaslist_diff:
                roaslist_diff[date_str][(row['cid'], 'cnt')] += 1

        return whitelist_size, blacklist_size, roaslist_diff

    def _get_kpi_metrics(self):
        key_to_root_cid = defaultdict(list)
        for root_cid in self.root_cids:
            oid = self.cid_config[root_cid]['oid']
            ext_channel = self.cid_config[root_cid]['ext_channel']
            key_to_root_cid[oid].append(root_cid)
            key_to_root_cid[(oid, ext_channel)].append(root_cid)

        cid2root_cid = {}
        for cid in self.cid_config:
            oid = self.cid_config[cid]['oid']
            ext_channel = self.cid_config[cid]['ext_channel']
            if (oid, ext_channel) in key_to_root_cid:
                cid2root_cid[cid] = key_to_root_cid[(oid, ext_channel)][0]
            elif oid in key_to_root_cid:
                cid2root_cid[cid] = key_to_root_cid[oid][0]
            else:
                raise ValueError('cid: {} does not match root_cid'.format(cid))

        def get_kpi_query(cids, start_date):
            query = """
            select
                cid, event_id, goal_type, date, major_count, minor_count
            from
                deep_funnel_cid_event_id_daily_performance
            where
                cid in ({})
                and date >= '{}'
            """.format(','.join(['\'{}\''.format(cid) for cid in cids]), start_date)
            logger.debug(query)
            return query

        batch_size = 5000
        query_cids = list(self.cid_config.keys())
        kpi_metrics = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
        counted_major_keys = set()
        cid_goal_types = defaultdict(set)
        for idx in range(0, len(query_cids), batch_size):
            logger.info('query %s round %s cids', idx, batch_size)
            query = get_kpi_query(query_cids[idx: idx + batch_size], self.start_date)
            result = self.query_db.get_query_result(query, db_client=self.deep_funnel_client)
            for row in result:
                if row['cid'] not in self.cid_config:  # has cid.lower() in raw log
                    continue
                date = row['date'].strftime('%Y%m%d')
                cid = row['cid']
                min_major_count = self.cid_config[cid]['goalcpa'] if row['goal_type'].startswith('ROAS') else 1
                patch_metric_row(row, row['goal_type'], min_major_count, self.cid_config[cid]['goalcpa'])
                root_cid = cid2root_cid[cid]

                if row['event_id'] not in self.cid_optimization_goals[root_cid]['event_ids']:
                    continue
                cid_goal_types[root_cid].add(row['goal_type'])
                if row['goal_type'] != self.cid_optimization_goals[root_cid]['goal_type']:
                    continue

                oid = self.cid_config[cid]['oid']
                partner = self.cid_config[cid]['ext_channel']

                if (date, cid) not in counted_major_keys:
                    for field in [cid, (oid, partner), oid]:
                        kpi_metrics[date][field]['major'] += row['major_count']
                    counted_major_keys.add((date, cid))
                for field in [cid, (oid, partner), oid]:
                    kpi_metrics[date][field]['minor'] += row['minor_count']

        for root_cid in set(cid2root_cid.values()):
            if self.cid_optimization_goals[root_cid]['goal_type'] not in cid_goal_types[root_cid]:
                logger.warning('cid: %s does not has goal type %s',
                               root_cid, self.cid_optimization_goals[root_cid]['goal_type'])

        return kpi_metrics

    def _get_quantity_metrics(self):
        quantity_metrics = defaultdict(lambda: defaultdict(lambda: defaultdict(int)))
        query = """
        select
            cid, from_unixtime(time + {0} * 3600, '%Y%m%d') as click_date, sum(clicks) as clicks
        from {1}
        where cid in ({2}) and time >= {3}
        group by
            cid, from_unixtime(time + {0} * 3600, '%Y%m%d')
        order by NULL
        """.format(self.tz, CQueryDB.TABLE_EXT_AI_PERDAY_STREAM_HOUR,
                   ','.join(['\'{}\''.format(cid) for cid in self.cid_config]),
                   date_to_timestamp(self.start_date, tz=self.tz))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.fraud_client)
        for row in result:
            if row['cid'] not in self.cid_config:  # has cid.lower() in raw log
                continue
            oid = self.cid_config[row['cid']]['oid']
            partner = self.cid_config[row['cid']]['ext_channel']
            date = row['click_date']
            fields = [row['cid'], (oid, partner), oid]
            if self.cid_config[row['cid']]['optimization_model'] in HYDRA_MODELS:
                fields.append((oid, 'hydra'))
            for field in fields:
                quantity_metrics[date][field]['clicks'] += int(row['clicks'])

        query = """
        select
            cid, from_unixtime(action_time + {0} * 3600, '%Y%m%d') as install_date, count(1) as installs
        from {1}
        where cid in ({2}) and action_time >= {3}
        group by
            cid, from_unixtime(action_time + {0} * 3600, '%Y%m%d')
        order by NULL
        """.format(self.tz, CQueryDB.TABLE_EXT_AI_PERDAY_ACTION_TIME,
                   ','.join(['\'{}\''.format(cid) for cid in self.cid_config]),
                   date_to_timestamp(self.start_date, tz=self.tz))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.fraud_client)
        for row in result:
            if row['cid'] not in self.cid_config:  # has cid.lower() in raw log
                continue
            oid = self.cid_config[row['cid']]['oid']
            partner = self.cid_config[row['cid']]['ext_channel']
            date = row['install_date']
            fields = [row['cid'], (oid, partner), oid]
            if self.cid_config[row['cid']]['optimization_model'] in HYDRA_MODELS:
                fields.append((oid, 'hydra'))
            for field in fields:
                quantity_metrics[date][field]['installs'] += int(row['installs'])

        return quantity_metrics

    def _get_sheet_content(self, blacklist_size, whitelist_size, roaslist_diff, kpi_metrics, quantity_metrics):
        daily_sheet_content = []
        end_datetime = datetime.datetime.utcnow() + datetime.timedelta(hours=self.tz) - datetime.timedelta(hours=2)
        the_datetime = datetime.datetime.strptime(self.start_date, '%Y%m%d')
        while the_datetime <= end_datetime:
            date_str = the_datetime.strftime('%Y%m%d')
            for root_cid in self.root_cids:
                for cid, hydra_label in self.adgroup_trees.get_tree_cids_label(root_cid).iteritems():
                    optimization_model = self.cid_daily_optimization_model[cid].get(the_datetime.date())
                    if optimization_model not in HYDRA_MODELS:
                        continue
                    begin_date = self.cid_config[cid]['begin_date']
                    if the_datetime < datetime.datetime.strptime(begin_date, '%Y%m%d'):
                        continue
                    quantity = quantity_metrics[date_str]
                    if cid not in self.cid_optimization_goals:
                        if quantity[cid]['clicks'] > 0 or quantity[cid]['installs'] > 0:
                            self.raise_exception = True
                        logger.error('cid: %s does not have goals', cid)
                        continue
                    goals = self.cid_optimization_goals[cid]
                    oid = self.cid_config[cid]['oid']
                    oid_name = self.cid_config[cid]['oid_name']
                    partner = self.cid_config[cid]['ext_channel']
                    oid_partner = (oid, partner)
                    cid_status = 1 if self.cid_config[cid]['enabled'] else 0
                    cid_bid_price = self.cid_config[cid]['ext_bid_price']
                    cid_budget = self.cid_config[cid]['daily_max_budget']
                    partner_data = sorted([
                        (self.cid_config[partner_cid]['ext_bid_price'],
                         self.cid_config[partner_cid]['daily_max_budget'])
                        for partner_cid in self.cid_config[cid]['partner_cids']], reverse=True)
                    oid_partner_bid_prices = '"{}"'.format(json.dumps([item[0] for item in partner_data]))
                    oid_partner_budgets = '"{}"'.format(json.dumps([item[1] for item in partner_data]))

                    kpi_values = {}
                    for field in [cid, oid_partner, oid]:
                        kpi = kpi_metrics[date_str][field]
                        major = kpi.get('major', 0)
                        minor = kpi.get('minor', 0)
                        kpi_rate = float(minor) / major if major else -1
                        kpi_values[field] = {'major': major, 'minor': minor, 'kpi': kpi_rate}

                    is_blacklist_cid = hydra_label == 'Black Sampling'
                    whitelist_cnt = whitelist_size[date_str][(cid, 'cnt')]
                    blacklist_cnt = blacklist_size[date_str][(cid, 'cnt')]
                    roaslist_cnt = roaslist_diff[date_str][(cid, 'cnt')]
                    cid_blacklist_size = float(blacklist_size[date_str][cid]) / blacklist_cnt \
                        if blacklist_cnt else (0 if is_blacklist_cid else -1)
                    cid_whitelist_size = float(whitelist_size[date_str][cid]) / whitelist_cnt if whitelist_cnt else -1
                    cid_roaslist_diff_size = float(roaslist_diff[date_str][cid]) / roaslist_cnt if roaslist_cnt else -1

                    daily_row = [
                        date_str, cid, begin_date, self.cid_config[cid]['cid_name'], oid, oid_name, partner, root_cid,
                        self.cid_config[cid]['manager'], optimization_model, cid_whitelist_size, cid_blacklist_size,
                        cid_roaslist_diff_size, goals['goal_type'], goals['event_type'], goals['goal_value'],
                        cid_status, hydra_label, cid_bid_price, cid_budget, oid_partner_bid_prices, oid_partner_budgets,
                        kpi_values[cid]['major'], kpi_values[cid]['minor'], kpi_values[cid]['kpi'],
                        kpi_values[oid_partner]['major'], kpi_values[oid_partner]['minor'],
                        kpi_values[oid_partner]['kpi'], kpi_values[oid]['major'], kpi_values[oid]['minor'],
                        kpi_values[oid]['kpi'], quantity[cid]['clicks'], quantity[cid]['installs'],
                        quantity[oid_partner]['clicks'], quantity[oid_partner]['installs'],
                        quantity[oid]['clicks'], quantity[oid]['installs'],
                        quantity[(oid, 'hydra')]['clicks'], quantity[(oid, 'hydra')]['installs']
                    ]
                    daily_sheet_content.append(daily_row[:7] + map(str, daily_row[7:]))

            the_datetime += datetime.timedelta(days=1)

        return daily_sheet_content
