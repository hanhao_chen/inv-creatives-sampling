from __future__ import absolute_import, division
import json
import calendar
import datetime
from collections import defaultdict

from app.module.CQueryDB import CQueryDB
from app.utils.partner_utils import get_df_ready_partners
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.constants import APPIER_TO_USD, HYDRA_MODELS, HYDRA_ROAS_MODELS
from app.external_optimization.deep_funnel.utils.common_utils import get_external_capability, AppUsage, \
    safe_div, patch_metric_row

logger = init_logger(__name__)


class HydraUsageMonitor(object):
    """Month/Two-weeks/Week Hydra usage monitor. """
    SCHEMA = [
        'date', 'n_days', 'country', 'oid', 'partner_id', 'cid_event_type', 'cid_event_ids_sha1', 'cid_goal_type',
        'cid_goal_value', 'major_event_types', 'oid_name', 'partner_app_id_usage', 'cm_adgroups', 'hydra_adgroups',
        'cm_installs', 'cm_kpi', 'cm_major', 'cm_minor', 'hydra_white_installs', 'hydra_white_kpi',
        'hydra_white_major', 'hydra_white_minor', 'hydra_black_installs', 'hydra_black_kpi', 'hydra_black_major',
        'hydra_black_minor', 'hydra_installs', 'hydra_kpi', 'hydra_major', 'hydra_minor', 'hydra_roas_installs',
        'hydra_roas_kpi', 'hydra_roas_major', 'hydra_roas_minor', 'roaslist_diff'
    ]
    KEYS = [
        'date', 'n_days', 'country', 'oid', 'partner_id',
        'cid_event_type', 'cid_event_ids_sha1', 'cid_goal_type', 'cid_goal_value'
    ]

    def __init__(self, end_date_str, n_days, query_db, output_table_name,
                 cid_minor_goal_history_agent, cid_config_history_agent, tz=8):
        self.end_date_str = end_date_str
        self.n_days = n_days
        self.query_db = query_db
        self.output_table_name = output_table_name
        self.cid_minor_goal_history_agent = cid_minor_goal_history_agent
        self.tz = tz

        self.end_date = datetime.datetime.strptime(end_date_str, '%Y%m%d').date()
        self.start_date = self.end_date - datetime.timedelta(days=n_days)
        if n_days == 1:
            self.end_date = self.start_date

        self.external_capability = get_external_capability(self.query_db)
        self.campaign_cache_client = self.query_db.get_db_client_by_table(CQueryDB.TABLE_CAMPAIGN)
        self.deep_funnel_client = self.query_db.get_db_client_by_table(CQueryDB.TABLE_DF_PREDICTION_EXTENTION)
        self.fraud_client = self.query_db.get_db_client_by_table(CQueryDB.TABLE_EXT_AI_PERDAY_STREAM_HOUR)

        self.oid_partner_config, self.cid_config = self.get_all_config()
        self.cid_minor_goal_history = self.cid_minor_goal_history_agent.get_cids_daily_minor_goal(
            self.start_date, self.end_date, cids=self.cid_config.keys(), tz=self.tz, raise_exception=False)
        self.cid_daily_optimization_model = cid_config_history_agent.get_cids_daily_config(
            self.start_date, self.end_date, 'optimization_model', cids=self.cid_config.keys(), tz=self.tz,
            raise_exception=False)
        self.init_db_table()

    def init_db_table(self):
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `date` date NOT NULL,
        `n_days` int(11) NOT NULL DEFAULT '0',
        `country` varchar(32) NOT NULL DEFAULT '',
        `oid` varchar(32) NOT NULL DEFAULT '',
        `partner_id` varchar(32) NOT NULL DEFAULT '',
        `cid_event_type` varchar(32) NOT NULL DEFAULT '',
        `cid_event_ids_sha1` varchar(8) NOT NULL DEFAULT '',
        `cid_goal_type` varchar(32) NOT NULL DEFAULT '',
        `cid_goal_value` float NOT NULL DEFAULT '0',
        `major_event_types` varchar(256) NOT NULL DEFAULT '',
        `oid_name` varchar(64) NOT NULL DEFAULT '',
        `partner_app_id_usage` varchar(32) NOT NULL DEFAULT '',
        `cm_adgroups` int(11) NOT NULL DEFAULT '0',
        `hydra_adgroups` int(11) NOT NULL DEFAULT '0',
        `cm_installs` float NOT NULL DEFAULT '-1',
        `cm_kpi` float NOT NULL DEFAULT '-1',
        `cm_major` float NOT NULL DEFAULT '-1',
        `cm_minor` float NOT NULL DEFAULT '-1',
        `hydra_white_installs` float NOT NULL DEFAULT '-1',
        `hydra_white_kpi` float NOT NULL DEFAULT '-1',
        `hydra_white_major` float NOT NULL DEFAULT '-1',
        `hydra_white_minor` float NOT NULL DEFAULT '-1',
        `hydra_black_installs` float NOT NULL DEFAULT '-1',
        `hydra_black_kpi` float NOT NULL DEFAULT '-1',
        `hydra_black_major` float NOT NULL DEFAULT '-1',
        `hydra_black_minor` float NOT NULL DEFAULT '-1',
        `hydra_installs` float NOT NULL DEFAULT '-1',
        `hydra_kpi` float NOT NULL DEFAULT '-1',
        `hydra_major` float NOT NULL DEFAULT '-1',
        `hydra_minor` float NOT NULL DEFAULT '-1',
        `hydra_roas_installs` float NOT NULL DEFAULT '-1',
        `hydra_roas_kpi` float NOT NULL DEFAULT '-1',
        `hydra_roas_major` float NOT NULL DEFAULT '-1',
        `hydra_roas_minor` float NOT NULL DEFAULT '-1',
        `roaslist_diff` float NOT NULL DEFAULT '-1',
        `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`date`, `n_days`, `country`, `oid`, `partner_id`,
                     `cid_event_type`, `cid_event_ids_sha1`, `cid_goal_type`, `cid_goal_value`),
        KEY `oid` (`oid`),
        KEY `partner_id` (`partner_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(self.output_table_name)
        logger.debug(query)
        self.query_db.execute_query(query, db_client=self.deep_funnel_client)

    def get_all_config(self):
        df_partners = get_df_ready_partners(self.query_db)
        partner_usage = {}
        for partner in df_partners:
            try:
                app_usage = AppUsage.get_pub_sub_usage(self.external_capability[partner])
            except Exception:
                app_usage = 'exception'
            partner_usage[partner] = app_usage
        logger.info('partner_usage: %s', partner_usage)

        installed_cids = self.get_cids_with_installs()
        query = """
        select
            a.cid, a.oid, a.ext_channel, a.optimization_model, a.country
        from
        (
            select cid, oid, ext_channel, optimization_model, country
            from campaign where ext_channel in ({})
        ) as a
        left outer join
        (
            select oid, vertical from order_info
        ) as b
        on a.oid = b.oid
        where b.vertical = 'app_install'
        """.format(','.join('\'{}\''.format(partner) for partner in df_partners))
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_cache_client)
        cid_config = {row['cid']: row for row in result}

        hydra_white_cids, hydra_black_cids = self.get_hydra_cids(cid_config, partner_usage)
        oid_config = self.get_oid_config(cid_config)

        oid_partner_config = {}
        for cid in list(cid_config.keys()):
            if cid not in installed_cids and cid not in hydra_white_cids and cid not in hydra_black_cids:
                del cid_config[cid]
                continue
            oid_partner = (cid_config[cid]['oid'], cid_config[cid]['ext_channel'])
            if oid_partner not in oid_partner_config:
                oid_partner_config[oid_partner] = {
                    'hydra_white_cids': [],
                    'hydra_black_cids': [],
                    'cm_cids': [],
                    'partner_app_id_usage': partner_usage[cid_config[cid]['ext_channel']],
                    'country': cid_config[cid]['country'],
                    'goalcpa': oid_config[cid_config[cid]['oid']]['goalcpa'],
                    'major_types': set()
                }
                oid_partner_config[oid_partner].update(oid_config[cid_config[cid]['oid']])

            if cid in hydra_white_cids:
                oid_partner_config[oid_partner]['hydra_white_cids'].append(cid)
            elif cid in hydra_black_cids:
                oid_partner_config[oid_partner]['hydra_black_cids'].append(cid)
            else:
                oid_partner_config[oid_partner]['cm_cids'].append(cid)

        query = """
        select cid, event_type from event_id where major=1 and event_type like 'app_%' and cid in ({})
        """.format(','.join('\'{}\''.format(cid) for cid in cid_config))
        result = self.query_db.get_query_result(query, db_client=self.campaign_cache_client)
        for row in result:
            oid_partner = (cid_config[row['cid']]['oid'], cid_config[row['cid']]['ext_channel'])
            oid_partner_config[oid_partner]['major_types'].add(row['event_type'])

        for oid_partner, config in list(oid_partner_config.iteritems()):
            oid_key = (oid_partner[0], 'all')
            if oid_key not in oid_partner_config:
                oid_partner_config[oid_key] = {
                    'hydra_white_cids': [],
                    'hydra_black_cids': [],
                    'cm_cids': [],
                    'partner_app_id_usage': 'all',
                    'country': config['country'],
                    'major_types': set()
                }

            for field in ('hydra_white_cids', 'hydra_black_cids', 'cm_cids'):
                oid_partner_config[oid_key][field].extend(config[field])
            oid_partner_config[oid_key].update(oid_config[oid_key[0]])
            oid_partner_config[oid_key]['major_types'] |= config['major_types']

        return oid_partner_config, cid_config

    def get_cids_with_installs(self):
        query = 'select distinct(cid) from {} where action_time between {} and {}'.format(
            CQueryDB.TABLE_EXT_AI_PERDAY_ACTION_TIME,
            calendar.timegm(self.start_date.timetuple()) - self.tz * 3600,
            calendar.timegm(self.end_date.timetuple()) - self.tz * 3600 + 86399)
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.fraud_client)
        return set([row['cid'] for row in result])

    def get_hydra_cids(self, cid_config, partner_usage):
        query = 'select distinct(cid) from hydra_list_control where date_hour between \'{}\' and \'{}\''.format(
            self.start_date.strftime('%Y-%m-%d 00:00:00'), self.end_date.strftime('%Y-%m-%d 23:00:00'))
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.deep_funnel_client)
        hydra_cids = {row['cid'] for row in result
                      if row['cid'] in cid_config and cid_config[row['cid']]['optimization_model'] in HYDRA_MODELS}

        query = """
        select result from idash_job_queue
        where
            (application_params like '%%stable_sampling_black%%' or
            application_params like '%%add_blacklist_field%%') and
            job_status = 'success'
        """
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.deep_funnel_client)
        hydra_black_cids = set([json.loads(row['result'])['data']['cid'] for row in result])
        for cid in hydra_cids:
            if cid in cid_config and partner_usage[cid_config[cid]['ext_channel']].is_black_usage:
                hydra_black_cids.add(cid)

        query = "select global_cid as cid from deep_funnel_adgroup_trees where is_transparent = 1"
        logger.info(query)
        transparent_cids = {
            row['cid'] for row in self.query_db.get_query_result(query, db_client=self.deep_funnel_client)
            if row['cid'] in hydra_cids
        }
        hydra_black_cids |= transparent_cids

        hydra_white_cids = hydra_cids - hydra_black_cids
        return hydra_white_cids, hydra_black_cids

    def get_oid_config(self, cid_config):
        oids = set([value['oid'] for value in cid_config.values()])
        query = 'select oid, name, goalcpa from order_info where oid in ({})'.format(
            ','.join(['\'{}\''.format(oid) for oid in oids]))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_cache_client)
        oid_config = {}
        for row in result:
            row['goalcpa'] = float(row['goalcpa']) * APPIER_TO_USD
            oid_config[row['oid']] = row

        return oid_config

    def update_usage_monitor(self):
        logger.info('update hydra usage monitor from %s to %s into %s',
                    self.start_date.strftime('%Y%m%d'), self.end_date.strftime('%Y%m%d'), self.output_table_name)
        kpi_metrics = self._get_kpi_metrics()
        quantity_metrics = self._get_quantity_metrics()
        roasdiff_size = self._get_roasdiff_size()

        sheet_content = self._get_sheet_content(kpi_metrics, quantity_metrics, roasdiff_size)
        query = """
        INSERT INTO {}
        ({})
        VALUES ({})
        ON DUPLICATE KEY UPDATE {}
        """.format(self.output_table_name, ','.join(self.SCHEMA), ','.join(['%s'] * len(self.SCHEMA)),
                   ','.join('`{0}`=values(`{0}`)'.format(col) for col in self.SCHEMA if col not in self.KEYS))
        logger.info(query)
        logger.info(sheet_content[0])
        self.query_db.execute_query_many(query, sheet_content, db_client=self.deep_funnel_client)

    def _get_kpi_metrics(self):
        query = 'select cid, event_id, event_type from event_id where cid in ({})'.format(
            ','.join(['\'{}\''.format(cid) for cid in self.cid_config]))
        result = self.query_db.get_query_result(query, db_client=self.campaign_cache_client)
        default_eventid_set = defaultdict(set)
        for row in result:
            default_eventid_set[(self.cid_config[row['cid']]['oid'], row['event_type'])].add(row['event_id'])

        kpi_metrics = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(float))))
        query = """
        select
            cid, event_id, goal_type, date, major_count, minor_count
        from
            deep_funnel_cid_event_id_daily_performance
        where
            cid in ({})
            and date between '{}' and '{}'
        order by NULL
        """.format(','.join(['\'{}\''.format(cid) for cid in self.cid_config]),
                   self.start_date.strftime('%Y-%m-%d'),
                   self.end_date.strftime('%Y-%m-%d'))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.deep_funnel_client)

        counted_major_keys = set()
        for row in result:
            cid = row['cid']
            minor_goal = self.cid_minor_goal_history.get(cid, {}).get(row['date'])
            if minor_goal is None or minor_goal.event_type == 'no_goal':
                continue
            oid = self.cid_config[cid]['oid']
            target_event_ids = default_eventid_set[(oid, minor_goal.event_type)] \
                if minor_goal.event_ids is None or minor_goal.event_ids == ('', ) else minor_goal.event_ids
            if minor_goal.goal_type != row['goal_type'] or row['event_id'] not in target_event_ids:
                continue

            oid_partner = (oid, self.cid_config[cid]['ext_channel'])
            min_major_count = self.oid_partner_config[oid_partner]['goalcpa'] \
                if row['goal_type'].startswith('ROAS') else 1
            patch_metric_row(row, row['goal_type'], min_major_count, self.oid_partner_config[oid_partner]['goalcpa'])
            optimization_model = self.cid_daily_optimization_model[cid].get(row['date']) \
                or self.cid_config[cid]['optimization_model']

            major_key = (row['date'].strftime('%Y%m%d'), cid)
            if major_key not in counted_major_keys:
                kpi_metrics[oid_partner][minor_goal][(cid, optimization_model)]['major'] += row['major_count']
                kpi_metrics[(oid, 'all')][minor_goal][(cid, optimization_model)]['major'] += row['major_count']
                counted_major_keys.add(major_key)
            kpi_metrics[oid_partner][minor_goal][(cid, optimization_model)]['minor'] += row['minor_count']
            kpi_metrics[(oid, 'all')][minor_goal][(cid, optimization_model)]['minor'] += row['minor_count']

        return kpi_metrics

    def _get_quantity_metrics(self):
        quantity_metrics = defaultdict(lambda: defaultdict(lambda: defaultdict(lambda: defaultdict(int))))
        query = """
        select
            cid, from_unixtime(action_time + {0} * 3600, '%Y%m%d') as date, count(1) as installs
        from {1}
        where cid in ({2}) and action_time between {3} and {4}
        group by
            cid, from_unixtime(action_time + {0} * 3600, '%Y%m%d')
        order by NULL
        """.format(
            self.tz, CQueryDB.TABLE_EXT_AI_PERDAY_ACTION_TIME,
            ','.join(['\'{}\''.format(tmp_cid) for tmp_cid in self.cid_config]),
            calendar.timegm(self.start_date.timetuple()) - self.tz * 3600,
            calendar.timegm(self.end_date.timetuple()) - self.tz * 3600 + 86399)

        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.fraud_client)
        for row in result:
            cid = row['cid']
            if cid not in self.cid_config:
                continue
            query_date = datetime.datetime.strptime(row['date'], '%Y%m%d').date()
            minor_goal = self.cid_minor_goal_history.get(cid, {}).get(query_date)
            if minor_goal is None or minor_goal.event_type == 'no_goal':
                continue
            oid = self.cid_config[cid]['oid']
            oid_partner = (oid, self.cid_config[cid]['ext_channel'])
            optimization_model = self.cid_daily_optimization_model[cid].get(query_date) \
                or self.cid_config[cid]['optimization_model']
            quantity_metrics[oid_partner][minor_goal][(cid, optimization_model)]['installs'] += int(row['installs'])
            quantity_metrics[(oid, 'all')][minor_goal][(cid, optimization_model)]['installs'] += int(row['installs'])

        return quantity_metrics

    def _get_roasdiff_size(self):
        roasdiff_size = defaultdict(lambda: defaultdict(lambda: defaultdict(float)))
        query = """
        SELECT
            date, cid, optimization_model,
            SUM(IF(roaslist_diff > 0, roaslist_diff, 0)) as roaslist_diff, count(1) as cnt
        FROM
            hydra_performance_monitor
        WHERE
            date between '{}' and '{}'
        GROUP BY
            date, cid, optimization_model
        ORDER BY
            NULL
        """.format(self.start_date.strftime('%Y-%m-%d'), self.end_date.strftime('%Y-%m-%d'))
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.deep_funnel_client)
        for row in result:
            cid = row['cid']
            if cid not in self.cid_config:
                continue
            minor_goal = self.cid_minor_goal_history.get(cid, {}).get(row['date'])
            if minor_goal is None or minor_goal.event_type == 'no_goal':
                continue
            oid = self.cid_config[cid]['oid']
            oid_partner = (oid, self.cid_config[cid]['ext_channel'])
            optimization_model = self.cid_daily_optimization_model[cid].get(row['date']) \
                or self.cid_config[cid]['optimization_model']
            if optimization_model not in HYDRA_ROAS_MODELS:
                continue
            for field in ('cnt', 'roaslist_diff'):
                roasdiff_size[oid_partner][minor_goal][field] += float(row[field])
                roasdiff_size[(oid, 'all')][minor_goal][field] += float(row[field])

        return roasdiff_size

    def _get_sheet_content(self, kpi_metrics, quantity_metrics, roasdiff_size):
        sheet_content = []

        def add_metrics(data, installs, major, minor):
            data['installs'] += installs
            data['major'] += major
            data['minor'] += minor

        for oid_partner, config in self.oid_partner_config.iteritems():
            cid_minor_goals = set()
            for field in ('cm_cids', 'hydra_white_cids', 'hydra_black_cids'):
                for cid in config[field]:
                    cid_minor_goals |= set(
                        minor_goal for minor_goal in self.cid_minor_goal_history.get(cid, {}).values()
                        if minor_goal is not None and minor_goal.event_type != 'no_goal')

            for minor_goal in cid_minor_goals:
                roaslist_data = roasdiff_size[oid_partner][minor_goal]
                roaslist_diff = safe_div(roaslist_data['roaslist_diff'], roaslist_data['cnt'], -1)

                metrics = defaultdict(lambda: defaultdict(int))
                for (cid, optimization_model) in set(kpi_metrics[oid_partner][minor_goal].keys()
                                                     + quantity_metrics[oid_partner][minor_goal].keys()):
                    quality = kpi_metrics[oid_partner][minor_goal][(cid, optimization_model)]
                    quantity = quantity_metrics[oid_partner][minor_goal][(cid, optimization_model)]
                    if cid in config['cm_cids']:
                        add_metrics(metrics['cm'], quantity['installs'], quality['major'], quality['minor'])
                    else:
                        if cid in config['hydra_white_cids']:
                            add_metrics(
                                metrics['hydra_white'], quantity['installs'], quality['major'], quality['minor'])
                        elif cid in config['hydra_black_cids']:
                            add_metrics(
                                metrics['hydra_black'], quantity['installs'], quality['major'], quality['minor'])
                        else:
                            raise RuntimeError('uncategorized cid {}'.format(cid))

                        if optimization_model in HYDRA_ROAS_MODELS:
                            add_metrics(
                                metrics['hydra_roas'], quantity['installs'], quality['major'], quality['minor'])

                for field in ('installs', 'major', 'minor'):
                    metrics['hydra'][field] = metrics['hydra_white'][field] + metrics['hydra_black'][field]

                hashed_eids, _eids_str = self.cid_minor_goal_history_agent.get_event_ids_sha1(minor_goal.event_ids)
                row = [
                    self.end_date_str, self.n_days, config['country'], oid_partner[0], oid_partner[1],
                    minor_goal.event_type, hashed_eids, minor_goal.goal_type, minor_goal.goal_value,
                    ';'.join(config['major_types']), config['name'], str(config['partner_app_id_usage']),
                    len(config['cm_cids']), len(config['hydra_white_cids']) + len(config['hydra_black_cids'])
                ]
                for field in ('cm', 'hydra_white', 'hydra_black', 'hydra', 'hydra_roas'):
                    row.extend([
                        metrics[field]['installs'], safe_div(metrics[field]['minor'], metrics[field]['major'], -1),
                        metrics[field]['major'], metrics[field]['minor']])
                row.append(roaslist_diff)
                sheet_content.append(row)

        return sheet_content
