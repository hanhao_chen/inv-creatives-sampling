"""Define base classes for slack applications. """
import json

from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.common_utils import send_post_request, send_get_request

logger = init_logger(__name__)


class SlackAlertsAgent(object):
    """Base class for sending slack message through incoming webhooks. """

    def __init__(self, webhooks_url, auth_token, query_db, db_client, logging_table_name):
        self.webhooks_url = webhooks_url
        self.auth_token = auth_token
        self.query_db = query_db
        self.db_client = db_client
        self.logging_table_name = logging_table_name
        self.init_mysql_logging_table()

    def init_mysql_logging_table(self):
        """Initialize the MySQL table for logging slack messages. """
        raise NotImplementedError

    def update_mysql_logging_table(self, message):
        """Update a slack message into logging table. """
        raise NotImplementedError

    def get_post_body(self, message):
        """Get the post body of slack message for POST.
        :param message: message in json format
        :return post_body: a json string
        """
        return json.dumps(message)

    def send_alert_message(self, message, timeout=30, max_retry=3):
        """Send a slack message through the incoming webhooks.
        :param message: message in json format
        :param timeout:
        :param max_retry:
        :return response: the response of POST request
        """
        self.update_mysql_logging_table(message)
        post_body = self.get_post_body(message)
        response = send_post_request(self.webhooks_url, data=post_body, timeout=timeout, max_retry=max_retry)
        return response

    def get_slack_userid_from_mail(self, email, timeout=30, max_retry=3):
        """Get slack user_id from user's mail which can be use to tag users in a message, like <@{user_id}>.
        From https://api.slack.com/methods/users.lookupByEmail,
        this endpoint requires an auth token with `users:read.email` scope.

        :param email: slack user's account email
        :param timeout:
        :param max_retry:
        :return user_id: the slack user's id
        """
        url = 'https://slack.com/api/users.lookupByEmail?email={}&token={}'.format(email, self.auth_token)
        response = send_get_request(url, timeout=timeout, max_retry=max_retry).json()
        if not response['ok']:
            raise RuntimeError('{} failed with {}'.format(url, response))

        return response['user']['id']
