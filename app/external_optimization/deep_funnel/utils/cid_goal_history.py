"""Define class for updating cid minor goal history and getting cid daily minor goal. """
import calendar
import datetime
import hashlib
import math
import time
from collections import defaultdict, namedtuple
import numpy as np

from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.constants import APPIER_TO_USD

logger = init_logger(__name__)


class CidGoalHistory(object):
    """Provides interfaces for updating cid minor goal history and getting cid daily minor goal. """
    MINOR_GOAL = namedtuple('minor_goal', ['event_type', 'event_ids', 'goal_type', 'goal_value'])

    def __init__(self, idash_endpoint, query_db, db_client, cid_minor_goal_table, cid_event_ids_hash_table,
                 job_status_table, job_name):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.db_client = db_client
        self.cid_minor_goal_table = cid_minor_goal_table
        self.cid_event_ids_hash_table = cid_event_ids_hash_table
        self.job_status_table = job_status_table
        self.job_name = job_name
        self.minor_goal_history = None

    def get_recorded_cids(self):
        """Gets current recorded cids.
        :return recorded_cids: set of cids
        """
        query = 'select distinct cid from {}'.format(self.cid_minor_goal_table)
        recorded_cids = set(row['cid'] for row in self.query_db.get_query_result(query, db_client=self.db_client))
        return recorded_cids

    def get_checkpoint_timestamp(self):
        """Get the timestamp of last update.
        :return timestamp: integer timestamp
        """
        query = 'select max(timestamp) as timestamp from {} where job_name = \'{}\''.format(
            self.job_status_table, self.job_name)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.db_client)
        return calendar.timegm(results[0]['timestamp'].timetuple()) if results[0]['timestamp'] else 0

    def _parser_a_history_log(self, history):
        for change in history['changes']:
            if change['key'] == 'optimizer_options' and 'errorMsg' not in change:
                origin = change['origin'].get('optimization_goal_event', {}).get('minor')
                updated = change['updated'].get('optimization_goal_event', {}).get('minor')
                return self._parser_minor_goal(origin), self._parser_minor_goal(updated)

        return None, None

    def _parser_minor_goal(self, minor):
        # skip old changes that contains eids rather than x_eids, like cid: _9FYHnNGT7avPJT3U2nifQ
        return self.MINOR_GOAL(minor[0]['event_type'], minor[0]['x_eids'], minor[0]['goal'], minor[0]['goal_value']) \
            if minor and 'x_eids' in minor[0] else self.MINOR_GOAL('no_goal', ['no_goal'], 'no_goal', 0.0)

    def _get_new_cids_history(self, cids, batch_size=1000):
        history_records = []
        query_fields = ['history', 'optimizer_options', 'begin_at']
        logger.info('query %s new cids history', len(cids))
        for batch_index in range(int(math.ceil(float(len(cids)) / batch_size))):
            query_cids = cids[batch_index * batch_size: (batch_index + 1) * batch_size]
            logger.info('query new cids batch %s', batch_index)
            results = self.idash_endpoint.read_campaigns_by_cids(query_cids, fields=query_fields)
            for cid_result in results:
                begin_at = int(float(cid_result['begin_at']))  # some cid has float begin_at
                cid_history_records = []
                if 'history' in cid_result:
                    cid_result['history'].sort(key=lambda item: item['updated_at'])
                    for history in cid_result['history']:
                        origin_goal, updated_goal = self._parser_a_history_log(history)
                        if origin_goal == updated_goal:
                            continue
                        if not cid_history_records:
                            cid_history_records.append((cid_result['cid'], begin_at, origin_goal))
                        cid_history_records.append((cid_result['cid'], history['updated_at'], updated_goal))

                if not cid_history_records:
                    minor = cid_result.get('optimizer_options', {}).get('optimization_goal_event', {}).get('minor')
                    cid_history_records.append((cid_result['cid'], begin_at, self._parser_minor_goal(minor)))
                history_records.extend(cid_history_records)

        return history_records

    def _get_old_cids_history(self, cids, modified_at_after, batch_size=1000):
        history_records = []
        logger.info('query %s old cids history', len(cids))
        batch_size = batch_size if modified_at_after < int(time.time()) - 3 * 86400 else len(cids) + 1
        for batch_index in range(int(math.ceil(float(len(cids)) / batch_size))):
            logger.info('query old cids batch %s', batch_index)
            query_cids = cids[batch_index * batch_size: (batch_index + 1) * batch_size]
            results = self.idash_endpoint.read_campaigns_by_cids(
                query_cids, fields=['history'], modified_at_after=modified_at_after)
            for cid_result in results:
                if 'history' in cid_result:
                    cid_result['history'].sort(key=lambda item: item['updated_at'])
                    for history in cid_result['history']:
                        origin_goal, updated_goal = self._parser_a_history_log(history)
                        if origin_goal == updated_goal or history['updated_at'] < modified_at_after:
                            continue
                        history_records.append((cid_result['cid'], history['updated_at'], updated_goal))

        return history_records

    def get_event_ids_sha1(self, event_ids):
        """Gets sha1 hashed id from event id lists.
        :param event_ids: list of event id
        :return hashed_eids: 8 characters sha1 hashed id
        :return eids_str: event id string before sha1
        """
        eids_str = ';'.join(event_ids) if event_ids else ''
        hashed_eids = hashlib.sha1(eids_str).hexdigest()[:8]
        return hashed_eids, eids_str

    def _update_history_records(self, history_records):
        event_ids_map = {}
        rows = []
        for record in history_records:
            cid = record[0]
            timestamp = datetime.datetime.fromtimestamp(record[1]).strftime('%Y-%m-%d %H:%M:%S')
            minor_goal = record[2]

            hashed_eids, eids_str = self.get_event_ids_sha1(minor_goal.event_ids)
            goal_value = minor_goal.goal_value * APPIER_TO_USD \
                if minor_goal.goal_type.startswith('ROAS') else minor_goal.goal_value
            event_ids_map[hashed_eids] = eids_str
            rows.append([cid, timestamp, minor_goal.event_type, hashed_eids, minor_goal.goal_type, goal_value])

        query = """
        REPLACE INTO {}
        (cid, timestamp, event_type, event_ids_sha1, goal_type, goal_value)
        VALUES (%s, %s, %s, %s, %s, %s)
        """.format(self.cid_minor_goal_table)
        logger.info(query)
        self.query_db.execute_query_many(query, rows, db_client=self.db_client)
        return event_ids_map

    def _update_event_ids_hash_table(self, event_ids_map):
        query = """
        REPLACE INTO {}
        (event_ids_sha1, event_ids)
        VALUES (%s, %s)
        """.format(self.cid_event_ids_hash_table)
        logger.info(query)
        self.query_db.execute_query_many(query, event_ids_map.items(), db_client=self.db_client)

    def _update_job_status_table(self, update_timestamp):
        query = """
        INSERT INTO {}
        (job_name, timestamp)
        VALUES ('{}', '{}')
        """.format(self.job_status_table, self.job_name, update_timestamp)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def update_cids_goal_history(self, cids):
        """Update minor goal history for input cids.
        * recorded cids will only query changes since last update.
        * new cids will query in batch.
        * event_ids_sha1 and it's event_id_str will be updated in the `cid_event_ids_hash_table`.
        * job status timestamp will be updated in the `job_status_table`.

        :param cids: list of cids to be updated
        """
        update_timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        modified_at_after = self.get_checkpoint_timestamp()
        recorded_cids = self.get_recorded_cids()
        history_records = []
        history_records.extend(self._get_new_cids_history(list(set(cids) - recorded_cids)))
        history_records.extend(self._get_old_cids_history(list(set(cids) & recorded_cids), modified_at_after))
        if history_records:
            logger.info('update %s rows', len(history_records))
            event_ids_map = self._update_history_records(history_records)
            self._update_event_ids_hash_table(event_ids_map)
        else:
            logger.info('no campaign setting changed since last update')
        self._update_job_status_table(update_timestamp)

    def _load_cid_minor_goal_history(self, tz):
        self.minor_goal_history = defaultdict(dict)
        query = """
        SELECT
            a.cid, a.timestamp, a.event_type, b.event_ids, a.goal_type, a.goal_value
        FROM
        (SELECT cid, timestamp, event_type, event_ids_sha1, goal_type, goal_value FROM {}) as a
        LEFT OUTER JOIN
        (SELECT event_ids_sha1, event_ids FROM {}) as b
        ON a.event_ids_sha1 = b.event_ids_sha1
        """.format(self.cid_minor_goal_table, self.cid_event_ids_hash_table)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.db_client)

        for row in results:
            self.minor_goal_history[row['cid']][row['timestamp'] + datetime.timedelta(hours=tz)] = self.MINOR_GOAL(
                row['event_type'], tuple(row['event_ids'].split(';')) if row['event_ids'] else ('', ),
                row['goal_type'], row['goal_value'])

    def _get_cid_daily_minor_goal(self, cid, start_date, end_date):
        daily_minor_goal = {}
        matched_index = 0
        update_datetimes = sorted(self.minor_goal_history[cid].keys())
        logger.debug(update_datetimes)
        last_index = len(update_datetimes) - 1

        the_date = start_date
        while the_date <= end_date:
            # get to the last change before the date
            the_date_indices = []
            while matched_index != last_index and update_datetimes[matched_index + 1].date() < the_date:
                matched_index += 1
            if update_datetimes[matched_index].date() > the_date:
                logger.debug('cid %s on %s does not have goal', cid, the_date)
                the_date = the_date + datetime.timedelta(days=1)
                continue
            the_date_indices.append(matched_index)

            # append same date updates
            while matched_index != last_index and update_datetimes[matched_index + 1].date() == the_date:
                matched_index += 1
                the_date_indices.append(matched_index)

            if len(the_date_indices) <= 1:
                daily_minor_goal[the_date] = self.minor_goal_history[cid][update_datetimes[matched_index]]
            else:
                the_datetime = datetime.datetime.combine(the_date, datetime.time())
                time_ranges = [update_datetimes[the_date_indices[1]] - the_datetime]
                for index in xrange(1, len(the_date_indices) - 1):
                    time_ranges.append(update_datetimes[index + 1] - update_datetimes[index])
                time_ranges.append(the_datetime + datetime.timedelta(days=1) - update_datetimes[the_date_indices[-1]])
                logger.debug(the_date_indices)
                logger.debug(time_ranges)
                # use the change that uses longest in the date
                longest_index = the_date_indices[np.argmax(time_ranges)]
                daily_minor_goal[the_date] = self.minor_goal_history[cid][update_datetimes[longest_index]]

            logger.debug('cid %s use %s on %s (%s)', cid, daily_minor_goal[the_date], the_date, len(the_date_indices))
            the_date = the_date + datetime.timedelta(days=1)

        return daily_minor_goal

    def get_cids_daily_minor_goal(self, start_date, end_date, cids=None, tz=0, raise_exception=True):
        """Get cid's daily minor goal from start date to end date.
        :param start_datetime: start date, <datetime.date>.
        :param end_datetime: end date, <datetime.date>.
        :param cids: list of query cids, raise exception if any query cid does not has minor goal history.
        :param tz: timezone
        :return cid_daily_minor_goal: dict of [cid][datetime] to <MINOR_GOAL>.
        """
        cid_daily_minor_goal = defaultdict(dict)
        if self.minor_goal_history is None:
            self._load_cid_minor_goal_history(tz)

        for cid in self.minor_goal_history:
            if cids and cid not in cids:
                continue
            cid_daily_minor_goal[cid] = self._get_cid_daily_minor_goal(cid, start_date, end_date)

        if cids and set(cid_daily_minor_goal.keys()) != set(cids):
            msg = 'missing cids: {}'.format(set(cids) - set(cid_daily_minor_goal.keys()))
            if raise_exception:
                raise RuntimeError(msg)
            logger.warning(msg)
        return cid_daily_minor_goal
