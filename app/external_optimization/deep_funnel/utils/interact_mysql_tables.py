import constants as c
import math
import pandas as pd
import numpy as np
import arrow
import hashlib
from collections import defaultdict
from app.utils.init_logger import init_logger
from app.utils.db import MySQL_DB_AIBE_DEV, MySQL_DB_CAMPAIGN, MySQL_DB_RTB_OPT
from app.utils.base_tool import get_inv_query_dict, get_adjust_dict, format_idash_inv, lookupImpSizeGroup, format_inv_key_raw
from get_campaign_data_universal import get_all_campaign_data

logger = init_logger(__name__)

def hash_df(row):
    hash_str = format_inv_key_raw(row)
    sha1 = hashlib.md5(hash_str.encode('utf-8'))
    return sha1.hexdigest()[:7]

class InvExploitation(object):
    def __init__(self, idash_endpoint, query_db, listctrl_cfg):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.df_rtb_opt_client = query_db.db_clients[MySQL_DB_RTB_OPT]
        # self.df_db_prd_client = query_db.db_clients[MySQL_RTB_DB_PRD]
        self.df_db_client = query_db.db_clients[MySQL_DB_AIBE_DEV]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.inv_table = listctrl_cfg['inv_creative_table']
        self.order_info = listctrl_cfg['order_info_table']
        self.campaign = listctrl_cfg['campaign_table']
        self.goal_value_table = listctrl_cfg['goal_adjust_table']
        self.beethoven_perday_table = listctrl_cfg['beethoven_perday_table']
        self.beethoven_adjust_table = listctrl_cfg['beethoven_adjust_table']
        self.agg_oid_perf = listctrl_cfg['oid_cum_performance_table']
        self.cid_sample_perf = listctrl_cfg['cid_daily_performance_table']
        self.country_oid_rt_uu_table = listctrl_cfg['country_oid_rt_uu_table']

    def get_oid(self, cid):
        oid = self.idash_endpoint.get_cid_status(cid).get('oid')
        return oid

    def get_goal_cpa(self, oid):
        query = """SELECT oid, goalcpa FROM {} where oid='{}'""".format(self.order_info, oid)
        logger.info(query)
        goal_result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        oid2goal = {row['oid']: row['goalcpa'] for row in goal_result}
        return oid2goal.get(oid)

    def get_cid_goal_cpm(self, cid):
        goal_cpm = self.idash_endpoint.get_cid_status(cid).get('optimization_goal_cpm')
        goal_cpm /= 10000000
        return goal_cpm

    def gather_latest7_inv_perf(self, oid, inv_hash):

        start_date = arrow.utcnow().shift(days=-7).format('YYYY-MM-DD')
        query = """
        select
        bundle_id,
        app_id,
        partner_id,
        imp_is_instl,
        imp_type,
        imp_width,
        imp_height,
        app_type,
        device_type,
        tagid,
        inv_hash as inv_hash,
        sum(cost)/sum(shows)/10000 as cpm,
        sum(shows)/sum(bids) as bid2show,
        sum(shows) as shows,
        sum(clicks) as clicks,
        sum(_shows) as _shows,
        sum(_clicks) as _clicks,
        sum(_clicks)/sum(_shows) as _ctr,
        sum(clicks_uu) as clicks_uu,
        sum(clicks_rt_uu) as clicks_rt_uu,
        sum(clicks_rt_uu)/sum(_clicks) as ctcvr
        from `{table}`
        where date>='{start_date}' AND oid='{oid}' and inv_hash in ({inv_hash})
        group by 1,2,3,4,5,6,7,8,9,10,11
        """.format(table=self.country_oid_rt_uu_table,
                   start_date=start_date,
                   oid=oid,
                   inv_hash=','.join(["'{}'".format(hash) for hash in inv_hash]))
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        inv_info_dict = defaultdict(dict)
        for row in results:
            inv_info_dict[(row['inv_hash'], row['app_id'])]['_clicks'] = int(row['_clicks'])
            inv_info_dict[(row['inv_hash'], row['app_id'])]['_shows'] = int(row['_shows'])
            inv_info_dict[(row['inv_hash'], row['app_id'])]['clicks'] = int(row['clicks'])
            inv_info_dict[(row['inv_hash'], row['app_id'])]['shows'] = int(row['shows'])
            inv_info_dict[(row['inv_hash'], row['app_id'])]['clicks_rt_uu'] = int(row['clicks_rt_uu'])
            inv_info_dict[(row['inv_hash'], row['app_id'])]['clicks_uu'] = int(row['clicks_uu'])
            inv_info_dict[(row['inv_hash'], row['app_id'])]['bid2show'] = float(row['bid2show'])
            inv_info_dict[(row['inv_hash'], row['app_id'])]['cpm'] = float(row['cpm'])

        return inv_info_dict

    def gather_cid_win_rate(self, cid):
        end_date = arrow.utcnow().shift().format('YYYY-MM-DD')
        start_date = arrow.utcnow().shift(days=-7).format('YYYY-MM-DD')
        query = """
        select
        app_id,
        partner_id,
        imp_is_instl,
        imp_type,
        imp_width,
        imp_height,
        app_type,
        device_type,
        tag_id as tagid,
        inv_hash as inv_hash,
        sum(cost) as cost,
        sum(bids) as bids,
        sum(shows) as shows,
        sum(clicks) as clicks,
        sum(actions) as actions
        from `{table}`
        where date <= '{end_date}' AND date >= '{start_date}' AND cid='{cid}'
        group by 1,2,3,4,5,6,7
        """.format(table=self.cid_sample_perf,
                   cid=cid,
                   end_date=end_date,
                   start_date=start_date)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        inv_win_dict = defaultdict(dict)
        for row in results:
            inv_win_dict[(row['inv_hash'], row['app_id'])]['bid2show'] = float(row['shows']) / float(row['bids'])

        return inv_win_dict

    def gather_all_inv_list(self, oid):

        query = """
        select
        bundle_id,
        app_id,
        partner_id,
        imp_is_instl,
        imp_type,
        imp_width,
        imp_height,
        app_type,
        device_type,
        tagid,
        inv_hash as inv_hash,
        sum(shows) as shows,
        sum(clicks) as clicks,
        sum(_shows) as _shows,
        sum(_clicks) as _clicks,
        sum(_clicks)/sum(_shows) as _ctr,
        sum(clicks_uu) as clicks_uu,
        sum(clicks_rt_uu) as clicks_rt_uu,
        sum(clicks_rt_uu)/sum(_clicks) as ctcvr
        from `{table}`
        where oid='{oid}'
        group by 1,2,3,4,5,6,7,8,9,10,11
        having sum(clicks_rt_uu) > 0
        """.format(table=self.country_oid_rt_uu_table,
                   oid=oid)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for idx, _ in enumerate(results):
            results[idx]['imp_size_group'] = lookupImpSizeGroup(results[idx])
            results[idx]['exchange'] = results[idx]['partner_id'].split('_')[0]
            results[idx]['_ctr'] = float(results[idx]['_ctr'])
            results[idx]['ctcvr'] = float(results[idx]['ctcvr'])

        return results

    def init_inv_weight(self, invs, cid):
        adjust_rules_init = []
        cid_goal_cpm = self.get_cid_goal_cpm(cid)
        logger.info('init inventory weight, goal cpm: %f', cid_goal_cpm)

        for inv in invs:
            adjust_val = inv['cpm'] / cid_goal_cpm

            adjust_dict = get_adjust_dict(cid, inv, adjust_val)
            adjust_rules_init.append(adjust_dict)

            logger.info('init status, exchange: %s, app_id: %s, cpm: %f, adjust_val: %f',
                        inv['exchange'], inv['app_id'], inv['cpm'], adjust_val)

        return adjust_rules_init

    def gather_same_inv(self, oid, invs):
        if len(invs) == 0:
            return []
        query = """
        SELECT
        bundle_id,
        app_id,
        partner_id,
        imp_is_instl,
        imp_type,
        imp_width,
        imp_height,
        app_type,
        device_type,
        tagid
        from `{table}`
        where date=(select max(date) from `{table}`) AND
        oid='{oid}' AND
        (partner_id, app_id) in ({tuples})
        GROUP BY 1,2,3,4,5,6,7,8,9,10
        """.format(table=self.agg_oid_perf,
                   oid=oid,
                   tuples=','.join(["('{}', '{}')".format(row['partner_id'], row['app_id'])
                                    for row in invs]))
        # logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for idx, r in enumerate(results):
            results[idx]['imp_size_group'] = lookupImpSizeGroup(results[idx])
            results[idx]['exchange'] = results[idx]['partner_id'].split('_')[0]

        return results

    def gen_enhance_rules(self, cid, invs):

        whitelist = set()
        tagid = list()
        for inv in invs:
            whitelist.add('{}:{}'.format(inv['partner_id'], inv['app_id']))
            tagid.append([inv['partner_id'], inv['app_id'], inv['tagid']])

        enhance_rules = self.idash_endpoint.get_enhance_rules_status(cid)
        if len(enhance_rules):
            checked = set()
            for rule_dict in enhance_rules:
                if rule_dict.get('enhancement_type') == 'white' and \
                        rule_dict.get('source') == 'publisher_id_or_deal_id' and \
                        rule_dict.get('operator') == 'in':
                    checked.add('publisher_id_or_deal_id')
                    rule_dict['value'] = list(whitelist)
                    # rule_dict['value'] = list(set(rule_dict['value']) | set(self.df_adgroup_trees[root_cid]['whitelist']))

                if rule_dict.get('enhancement_type') == 'white' and \
                        rule_dict.get('source') == 'tag_id' and \
                        rule_dict.get('operator') == 'in':
                    checked.add('tag_id')
                    rule_dict['value'] = list(tagid)

            if 'publisher_id_or_deal_id' not in checked:
                whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
                whitelist_update['value'] = list(whitelist)
                enhance_rules.append(whitelist_update)
            if 'tag_id' not in checked:
                tagid_update = c.TAGID_UPDATE_TEMPLATE.copy()
                tagid_update['value'] = list(tagid)
                enhance_rules.append(tagid_update)

            update_dict = {'enhance_rules': enhance_rules}
        else:
            whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
            whitelist_update['value'] = list(whitelist)
            tagid_update = c.TAGID_UPDATE_TEMPLATE.copy()
            tagid_update['value'] = list(tagid)
            update_dict = {"enhance_rules": [whitelist_update, tagid_update]}

        return update_dict

    def gather_oid_cids(self, oid):

        query = """
        SELECT cid from {campaign} where oid='{oid}'
        """.format(campaign=self.campaign, oid=oid)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.campaign_db_client)

        return [row['cid'] for row in results]

    def gather_oid_inv_beethoven(self, oid):
        cids = self.gather_oid_cids(oid)
        start_date = arrow.utcnow().shift(days=-7).timestamp
        query = """
        SELECT app_id, exchange, app_type,
        device_type, tagid, imp_size_group
        from `{table}`
        where time>='{start_date}' AND cid in ({cids})
        group by 1,2,3,4,5,6
        """.format(table=self.beethoven_perday_table,
                   start_date=start_date,
                   cids=','.join(["'{}'".format(cid) for cid in cids])
                   )
        # logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_rtb_opt_client)

        return results

    def adjust_weight(self, cid, adjust_rules):
        if len(adjust_rules) == 0:
            return True
        feature_keys = c.INV_FEATURE_KEYS['fix_price']
        adjust_db_fields = feature_keys + ['adjust_val', 'adjust_by_robot']
        adjust_rule_tuple = []
        for rule in adjust_rules:
            adjust_rule_tuple.append(
                '(\'%s\',' % cid + ','.join(['\'' + str(rule[field]) + '\'' for field in adjust_db_fields]) + ')')
        query = 'INSERT INTO %s (cid, %s) VALUES %s ON DUPLICATE KEY UPDATE adjust_val = VALUES(adjust_val), adjust_by_robot=VALUES(adjust_by_robot)' \
                % (self.beethoven_adjust_table, ', '.join(adjust_db_fields), ','.join(adjust_rule_tuple))
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_rtb_opt_client)

        data = get_all_campaign_data.get_beethoven_all_params(cid)
        success = self.idash_endpoint.send_bidder_aggregator(data, cid)

        return success

    def gather_cid_adjust_val(self, cid):
        query = """
        select app_id, tagid, exchange, app_type,
        device_type, imp_size_group, adjust_val
        from {table}
        where cid='{cid}'
        """.format(table=self.beethoven_adjust_table,
                   cid=cid)
        results = self.query_db.get_query_result(query, db_client=self.df_rtb_opt_client)
        return results

class InvTableCenter(object):

    def __init__(self, idash_endpoint, query_db, listctrl_cfg):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.df_rtb_opt_client = query_db.db_clients[MySQL_DB_RTB_OPT]
        # self.df_db_prd_client = query_db.db_clients[MySQL_RTB_DB_PRD]
        self.df_db_client = query_db.db_clients[MySQL_DB_AIBE_DEV]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.inv_table = listctrl_cfg['inv_creative_table']
        self.order_info = listctrl_cfg['order_info_table']
        self.campaign = listctrl_cfg['campaign_table']
        self.goal_value_table = listctrl_cfg['goal_adjust_table']
        self.beethoven_perday_table = listctrl_cfg['beethoven_perday_table']
        self.beethoven_adjust_table = listctrl_cfg['beethoven_adjust_table']
        self.agg_oid_perf = listctrl_cfg['oid_cum_performance_table']
        self.cid_sample_perf = listctrl_cfg['cid_daily_performance_table']
        self.oid_cost_hf_table = listctrl_cfg['oid_cost_hf_table']

    def gather_cid_adjust_val(self, cid):
        query = """
        select app_id, tagid, exchange, app_type,
        device_type, imp_size_group, adjust_val
        from {table}
        where cid='{cid}'
        """.format(table=self.beethoven_adjust_table,
                   cid=cid)
        results = self.query_db.get_query_result(query, db_client=self.df_rtb_opt_client)
        return results

    def get_cid_goal_value_status(self, cid):

        query = """SELECT cid, goal_cpm FROM {} where cid='{}'""".format(self.goal_value_table, cid)
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.df_db_client)

        return result

    def update_cid_goal(self, cid, goal_cpm):

        query = """UPDATE {table} SET goal_cpm={goal_cpm} where cid='{cid}'
                """.format(table=self.goal_value_table, cid=cid, goal_cpm=goal_cpm)
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def insert_cid_goal(self, cid, goal_cpm):

        query = """INSERT INTO {table}
            (`cid`, `goal_cpm`, `update_time`)
            VALUES ('{cid}', '{goal_cpm}', CURRENT_TIMESTAMP)""".format(table=self.goal_value_table,
                                                                        cid=cid, goal_cpm=goal_cpm)
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def get_cid_goal_cpm(self, cid):
        goal_cpm = self.idash_endpoint.get_cid_status(cid).get('optimization_goal_cpm')
        goal_cpm /= 10000000
        return goal_cpm

    def scale_inv_weight(self, invs, cid, scale):
        adjust_rules_scale = []
        logger.info('scale inventory weight, scale: %f', scale)
        r_device_mp = {v: k for k, v in c.device_type_mp.items()}
        r_app_mp = {v: k for k, v in c.app_type_mp.items()}

        pass

    def init_inv_weight(self, invs, oid, cid, force=False):
        adjust_rules_init = []
        cid_goal_cpm = self.get_cid_goal_cpm(cid)
        logger.info('init inventory weight, goal cpm: %f', cid_goal_cpm)

        for inv in invs:
            tmp_inv = inv.copy()
            if tmp_inv['sampling'] == 1 and not force:
                logger.info('Is sampling, exchange: %s, app_id: %s, cpm: %f, bid2show: %f, adjust_val: sampling',
                            tmp_inv['exchange'], tmp_inv['app_id'], tmp_inv['cpm'], tmp_inv['bid2show'])
                adjust_dict = get_adjust_dict(cid, tmp_inv, tmp_inv['adjust_weight'])
                adjust_rules_init.append(adjust_dict)
                continue
            if tmp_inv['bid2show'] == 0:
                adjust_val = inv['cpm'] / cid_goal_cpm
            elif tmp_inv['bid2show'] >= 0.4 and tmp_inv['bid2show'] < 0.6:
                adjust_val = inv['cpm'] / cid_goal_cpm
            elif tmp_inv['bid2show'] < 0.4:
                if tmp_inv['cpm'] > cid_goal_cpm:
                    adjust_val = tmp_inv['cpm'] / cid_goal_cpm * 1.5
                else:
                    adjust_val = 1.5
            else:
                adjust_val = tmp_inv['cpm'] / cid_goal_cpm * 0.9

            adjust_dict = get_adjust_dict(cid, tmp_inv, adjust_val)
            adjust_rules_init.append(adjust_dict)

            self.update_sampling_inv(oid, cid, inv, use_hash=True)
            inv['sampling'] = 1
            self.update_adjust_weight(oid, cid, inv, adjust_val, use_hash=True)

            logger.info('init status, exchange: %s, app_id: %s, cpm: %f, bid2show: %f, adjust_val: %f',
                        tmp_inv['exchange'], tmp_inv['app_id'], tmp_inv['cpm'], tmp_inv['bid2show'], adjust_val)

        return adjust_rules_init

    def gather_oid_cids(self, oid):

        query = """
        SELECT cid from {campaign} where oid='{oid}'
        """.format(campaign=self.campaign, oid=oid)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.campaign_db_client)

        return [row['cid'] for row in results]

    def gather_oid_inv_beethoven(self, oid):
        cids = self.gather_oid_cids(oid)
        start_date = arrow.utcnow().shift(days=-7).timestamp
        query = """
        SELECT app_id, exchange, app_type,
        device_type, tagid, imp_size_group
        from `{table}`
        where time>='{start_date}' AND cid in ({cids})
        group by 1,2,3,4,5,6
        """.format(table=self.beethoven_perday_table,
                   start_date=start_date,
                   cids=','.join(["'{}'".format(cid) for cid in cids])
                   )
        # logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_rtb_opt_client)

        return results

    def gather_same_inv(self, oid, invs):
        if len(invs) == 0:
            return []
        query = """
        SELECT app_id, partner_id, imp_is_instl, imp_type,
        imp_width, imp_height, app_type,
        device_type, tagid, imp_size_group
        from `{table}`
        where date=(select max(date) from `{table}`) AND
        oid='{oid}' AND
        (partner_id, app_id) in ({tuples})
        GROUP BY 1,2,3,4,5,6,7,8,9,10
        """.format(table=self.agg_oid_perf,
                   oid=oid,
                   tuples=','.join(["('{}', '{}')".format(row['partner_id'], row['app_id'])
                                    for row in invs]))
        # logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for idx, r in enumerate(results):
            results[idx]['exchange'] = results[idx]['partner_id'].split('_')[0]

        return results

    def gather_cid_invs(self, cid):
        end_date = arrow.utcnow().shift().format('YYYY-MM-DD')
        start_date = arrow.utcnow().shift(days=-7).format('YYYY-MM-DD')
        query = """
        select
        app_id,
        partner_id,
        imp_is_instl,
        imp_type,
        imp_width,
        imp_height,
        app_type,
        device_type,
        tag_id as tagid,
        inv_hash as inv_hash,
        sum(cost) as cost,
        sum(bids) as bids,
        sum(shows) as shows,
        sum(clicks) as clicks,
        sum(actions) as actions
        from `{table}`
        where date <= '{end_date}' AND date >= '{start_date}' AND cid='{cid}'
        group by 1,2,3,4,5,6,7
        """.format(table=self.cid_sample_perf,
                   cid=cid,
                   end_date=end_date,
                   start_date=start_date)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for idx, r in enumerate(results):
            results[idx]['cost'] = float(results[idx]['cost'])

        return results

    def gather_cid_adjust_val(self, cid):
        query = """
        select app_id, tagid, exchange, app_type,
        device_type, imp_size_group, adjust_val
        from {table}
        where cid='{cid}'
        """.format(table=self.beethoven_adjust_table,
                   cid=cid)
        results = self.query_db.get_query_result(query, db_client=self.df_rtb_opt_client)
        return results

    def gather_oid_cost(self, oid, hf_table):

        if hf_table:
            query = """
            SELECT app_id, bundle_id, partner_id, imp_is_instl, imp_type,
            imp_width, imp_height, app_type,
            device_type, tagid, sum(cost) as cost, sum(shows) as shows, sum(clicks) as clicks
            from {table}
            where oid='{oid}'
            group by 1,2,3,4,5,6,7,8,9,10
            """.format(table=self.oid_cost_hf_table,
                       oid=oid)
            logger.info(query)
            results = self.query_db.get_query_result(query, db_client=self.df_db_client)
            for row in results:
                row['cost'] = float(row['cost'])
                row['shows'] = int(row['shows'])
                row['clicks'] = int(row['clicks'])
        else:
            query = """
            SELECT app_id, partner_id, imp_is_instl, imp_type,
            imp_width, imp_height, app_type,
            device_type, tagid, imp_size_group, cost, shows, clicks
            from {table}
            where date=(select max(date) from `{table}`) AND oid='{oid}'
            """.format(table=self.agg_oid_perf,
                       oid=oid)
            logger.info(query)
            results = self.query_db.get_query_result(query, db_client=self.df_db_client)
            for idx, r in enumerate(results):
                results[idx]['cost'] = float(results[idx]['cost'])
                results[idx]['shows'] = float(results[idx]['shows'])
                results[idx]['clicks'] = float(results[idx]['clicks'])
        return results

    def get_country(self, cid):
        country = self.idash_endpoint.get_cid_status(cid).get('require_regions')[0]
        return country

    def get_oid(self, cid):
        if cid == 'iaVBIZ-FQoOLBwVAPQ984Q':
            return 'CQG5GWkBQ2681wYr0erwtw'
        oid = self.idash_endpoint.get_cid_status(cid).get('oid')
        return oid

    def get_goal_cpa(self, oid):
        query = """SELECT oid, goalcpa FROM {} where oid='{}'""".format(self.order_info, oid)
        logger.info(query)
        goal_result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        oid2goal = {row['oid']: row['goalcpa'] for row in goal_result}
        return oid2goal.get(oid)

    def get_goal_cpc(self, cid):
        cpc = self.idash_endpoint.get_cid_status(cid).get('optimization_goal_cpc')/10000000
        return cpc

    def get_topn_inv(self, cid, oid, traffic=None, topn=10, all=False):

        if oid in ('8UqNB8jgRyWTGRPrXDHGlA', 'WqvPQ27vQaK3LHzeBLNZeA') and topn != -1:
            topn = 50

        if all:
            complete = "'0', '1'"
        else:
            complete = "'0'"

        score_type = c.score_type_mp.get(cid)
        if score_type.startswith('vp_bundle'):
            query = """
                    select app_id, partner_id, score,
                    partner_id, device_type, imp_size_group,
                    imp_is_instl, imp_type, imp_width, imp_height,
                    tagid, app_type, cpm, bid2show, sampling, inv_hash,
                    adjust_weight, dealid, deal_count
                    from {}
                    where complete_sampling in ({}) AND score_type='{}'
                    AND rt_uu >= 5 AND oid='{}' AND imp_type!=2
                    ORDER BY score DESC
                    """.format(self.inv_table, complete, score_type, oid)
            results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        elif score_type.startswith('ctcv'):
            query = """
                    select app_id, partner_id, score,
                    partner_id, device_type, imp_size_group,
                    imp_is_instl, imp_type, imp_width, imp_height,
                    tagid, app_type, cpm, bid2show, sampling, inv_hash,
                    adjust_weight, dealid, deal_count
                    from {}
                    where complete_sampling in ({}) AND score_type='{}'
                    AND oid='{}' AND imp_type!=2
                    ORDER BY score DESC
                    """.format(self.inv_table, complete, score_type, oid)
            logger.info(query)
            results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        elif score_type.startswith('cpi'):
            query = """
                    select app_id, partner_id, score,
                    partner_id, device_type, imp_size_group,
                    imp_is_instl, imp_type, imp_width, imp_height,
                    tagid, app_type, target_cpm as cpm, bid2show, sampling, inv_hash,
                    adjust_weight, dealid, deal_count
                    from {}
                    where complete_sampling in ({}) AND score_type='{}'
                    AND oid='{}' AND imp_type!=2 and est_cpi is not Null
                    ORDER BY est_cpi ASC
                    """.format(self.inv_table, complete, score_type, oid)
            logger.info(query)
            results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        elif score_type.startswith('country_whitelist'):
            query = """
                    select app_id, partner_id, score,
                    partner_id, device_type, imp_size_group,
                    imp_is_instl, imp_type, imp_width, imp_height,
                    tagid, app_type, cpm, bid2show, sampling, inv_hash,
                    adjust_weight, dealid, deal_count
                    from {}
                    where complete_sampling in ({}) AND score_type='{}'
                    AND score <= 20 AND oid='{}' AND imp_type!=2
                    ORDER BY score ASC
                    """.format(self.inv_table, complete, score_type, oid)
            results = self.query_db.get_query_result(query, db_client=self.df_db_client)

        for idx, r in enumerate(results):
            results[idx]['exchange'] = results[idx]['partner_id'].split('_')[0]

        if topn == -1:
            return results

        return results[:topn]

    def update_spending(self, oid, cid, inv, cost, goal_price):

        query = """
        UPDATE {} SET status={}, goal_price={} WHERE oid='{}' AND cid='{}' AND inv_hash='{}'
        """.format(self.inv_table,
                   cost,
                   goal_price,
                   oid,
                   cid,
                   inv['inv_hash'])
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def update_adjust_weight(self, oid, cid, inv, weight, use_hash=False):
        if use_hash:
            query = """
            UPDATE {} SET adjust_weight={} WHERE sampling=1 AND oid='{}' AND cid='{}' AND inv_hash='{}'
            """.format(self.inv_table,
                       weight,
                       oid,
                       cid,
                       inv['inv_hash'])
        else:
            query = """
            UPDATE {} SET adjust_weight={} WHERE sampling=1 AND oid='{}' AND cid='{}' AND {}
            """.format(self.inv_table,
                       weight,
                       oid,
                       cid,
                       ' AND '.join(["{}='{}'".format(k, v) for k, v in inv.items()]))
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def update_sampling_inv(self, oid, cid, inv, use_hash=False):

        if use_hash:
            query = """
            UPDATE {} SET sampling=1 WHERE oid='{}' AND cid='{}' AND inv_hash='{}'
            """.format(self.inv_table,
                       oid,
                       cid,
                       inv['inv_hash'])
        else:
            query = """
            UPDATE {} SET sampling=1 WHERE oid='{}' AND cid='{}' AND {}
            """.format(self.inv_table,
                       oid,
                       cid,
                       ' AND '.join(["{}='{}'".format(k, v) for k, v in inv.items()]))
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def update_complete_inv(self, oid, cid, inv):

        query = """
        UPDATE {} SET complete_sampling=1, sampling=0 WHERE oid='{}' AND cid='{}' AND {}
        """.format(self.inv_table,
                   oid,
                   cid,
                   ' AND '.join(["{}='{}'".format(k, v) for k, v in inv.items()]))
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def gen_enhance_rules(self, cid, invs):

        whitelist = set()
        tagid = list()
        for inv in invs:
            if inv['dealid'] != "" and inv['deal_count'] == 1:
                whitelist.add(inv['dealid'])
            else:
                whitelist.add('{}:{}'.format(inv['partner_id'], inv['app_id']))
                tagid.append([inv['partner_id'], inv['app_id'], inv['tagid']])

        enhance_rules = self.idash_endpoint.get_enhance_rules_status(cid)
        if len(enhance_rules):
            checked = set()
            for rule_dict in enhance_rules:
                if rule_dict.get('enhancement_type') == 'white' and \
                        rule_dict.get('source') == 'publisher_id_or_deal_id' and \
                        rule_dict.get('operator') == 'in':
                    checked.add('publisher_id_or_deal_id')
                    rule_dict['value'] = list(whitelist)
                    # rule_dict['value'] = list(set(rule_dict['value']) | set(self.df_adgroup_trees[root_cid]['whitelist']))

                if rule_dict.get('enhancement_type') == 'white' and \
                        rule_dict.get('source') == 'tag_id' and \
                        rule_dict.get('operator') == 'in':
                    checked.add('tag_id')
                    rule_dict['value'] = list(tagid)

            if 'publisher_id_or_deal_id' not in checked:
                whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
                whitelist_update['value'] = list(whitelist)
                enhance_rules.append(whitelist_update)
            if 'tag_id' not in checked:
                tagid_update = c.TAGID_UPDATE_TEMPLATE.copy()
                tagid_update['value'] = list(tagid)
                enhance_rules.append(tagid_update)

            update_dict = {'enhance_rules': enhance_rules}
        else:
            whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
            whitelist_update['value'] = list(whitelist)
            tagid_update = c.TAGID_UPDATE_TEMPLATE.copy()
            tagid_update['value'] = list(tagid)
            update_dict = {"enhance_rules": [whitelist_update, tagid_update]}

        return update_dict

    def adjust_weight(self, cid, adjust_rules):
        if len(adjust_rules) == 0:
            return True
        feature_keys = c.INV_FEATURE_KEYS['fix_price']
        adjust_db_fields = feature_keys + ['adjust_val', 'adjust_by_robot']
        adjust_rule_tuple = []
        for rule in adjust_rules:
            adjust_rule_tuple.append(
                '(\'%s\',' % cid + ','.join(['\'' + str(rule[field]) + '\'' for field in adjust_db_fields]) + ')')
        query = 'INSERT INTO %s (cid, %s) VALUES %s ON DUPLICATE KEY UPDATE adjust_val = VALUES(adjust_val), adjust_by_robot=VALUES(adjust_by_robot)' \
                % (self.beethoven_adjust_table, ', '.join(adjust_db_fields), ','.join(adjust_rule_tuple))
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_rtb_opt_client)

        data = get_all_campaign_data.get_beethoven_all_params(cid)
        success = self.idash_endpoint.send_bidder_aggregator(data, cid)

        return success


class InvDynamicTableCenter(object):

    def __init__(self, idash_endpoint, query_db, listctrl_cfg):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.cid_status = {}
        self.df_rtb_opt_client = query_db.db_clients[MySQL_DB_RTB_OPT]
        # self.df_db_prd_client = query_db.db_clients[MySQL_RTB_DB_PRD]
        self.df_db_client = query_db.db_clients[MySQL_DB_AIBE_DEV]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.inv_table = listctrl_cfg['inv_sampling_table']
        self.inv_complete_table = listctrl_cfg['inv_complete_table']
        self.country_oid_rt_uu_table = listctrl_cfg['country_oid_rt_uu_table']
        self.site_id_country_inv_rt_uu_table = listctrl_cfg['site_id_country_inv_rt_uu_table']
        self.site_id_country_inv_ctr_table = listctrl_cfg['site_id_country_inv_ctr_table']
        self.order_info = listctrl_cfg['order_info_table']
        self.campaign = listctrl_cfg['campaign_table']
        self.creative_table = listctrl_cfg['creative_table']
        self.creative_spec_table = listctrl_cfg['creative_spec_table']
        self.goal_value_table = listctrl_cfg['goal_adjust_table']
        self.beethoven_perday_table = listctrl_cfg['beethoven_perday_table']
        self.beethoven_adjust_table = listctrl_cfg['beethoven_adjust_table']
        self.agg_oid_perf = listctrl_cfg['oid_cum_performance_table']
        self.cid_sample_perf = listctrl_cfg['cid_daily_performance_table']
        self.impbid_ctcv_table = listctrl_cfg['impbid_ctcv_table']
        self.oid_cost_hf_table = listctrl_cfg['oid_cost_hf_table']
        self.appanni = listctrl_cfg['appanni_info']
        self.appanni_cate = listctrl_cfg['appanni_cate']

    def get_inv_sampling_info(self, **kwargs):
        query = """
            SELECT oid, cid, sampling_list, max(update_time) FROM `inventory_sampling_info`
        WHERE `oid` = '{oid}'
        AND `cid` = '{cid}'
        GROUP BY 1, 2
        """.format(**kwargs)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        return results

    def insert_inv_sampling_info(self, **kwargs):
        query = """
            INSERT INTO `inventory_sampling_info`
        (`oid`, `cid`, `country`, `imp_type`, `is_rewarded`, `ssp`, `sampling_size`, `sampling_list`, `update_time`)
        VALUES
        ('{oid}', '{cid}', '{country}', '{imp_type}', '{is_rewarded}', '{ssp}', '{sampling_size}', '{sampling_list}',
         CURRENT_TIMESTAMP)
        """.format(**kwargs)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def get_cid_goal_value_status(self, cid):

        query = """SELECT cid, goal_cpm FROM {} where cid='{}'""".format(self.goal_value_table, cid)
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.df_db_client)

        return result

    def update_cid_goal(self, cid, goal_cpm):

        query = """UPDATE {table} SET goal_cpm={goal_cpm} where cid='{cid}'
                """.format(table=self.goal_value_table, cid=cid, goal_cpm=goal_cpm)
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def insert_cid_goal(self, cid, goal_cpm):

        query = """INSERT INTO {table}
            (`cid`, `goal_cpm`, `update_time`)
            VALUES ('{cid}', '{goal_cpm}', CURRENT_TIMESTAMP)""".format(table=self.goal_value_table,
                                                                        cid=cid, goal_cpm=goal_cpm)
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def get_cid_goal_cpm(self, cid):
        goal_cpm = self.idash_endpoint.get_cid_status(cid).get('optimization_goal_cpm')
        goal_cpm /= 10000000
        return goal_cpm

    def scale_inv_weight(self, invs, cid, scale):
        adjust_rules_scale = []
        logger.info('scale inventory weight, scale: %f', scale)
        r_device_mp = {v: k for k, v in c.device_type_mp.items()}
        r_app_mp = {v: k for k, v in c.app_type_mp.items()}

        pass

    def init_inv_weight(self, invs, oid, cid, force=False):
        adjust_rules_init = []
        cid_goal_cpm = self.get_goal_cpm(cid)
        model_type = self.get_model_type(cid)
        cid_goal_cpa = self.get_cid_goal_cpa(cid)
        cid_goal_cpc = self.get_goal_cpc(cid)
        logger.info('init inventory weight, goal cpm: %f', cid_goal_cpm)

        # avg_cpi = sum([inv['est_cpi'] for inv in invs])/len(invs)

        for inv in invs:
            if inv['sampling'] == 1 and not force:
                logger.info('Is sampling, exchange: %s, app_id: %s, cpm: %f, bid2show: %f, adjust_val: sampling',
                            inv['exchange'], inv['app_id'], inv['cpm'], inv['bid2show'])
                if model_type in ('lobster', 'fix_price'):
                    continue
                adjust_dict = get_adjust_dict(cid, inv, inv['adjust_weight'])
                adjust_rules_init.append(adjust_dict)
                continue

            adjust_val = 1
            adjust_dict = get_adjust_dict(cid, inv, adjust_val)
            adjust_rules_init.append(adjust_dict)

            self.update_sampling_inv(oid, cid, inv, use_hash=True)
            inv['sampling'] = 1
            self.update_adjust_weight(oid, cid, inv, adjust_val, use_hash=True)

            logger.info('init status, exchange: %s, app_id: %s, cpm: %f, bid2show: %f, adjust_val: %f',
                        inv['exchange'], inv['app_id'], inv['cpm'], inv['bid2show'], adjust_val)

        return adjust_rules_init

    def gather_oid_cids(self, oid):

        query = """
        SELECT cid from {campaign} where oid='{oid}'
        """.format(campaign=self.campaign, oid=oid)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.campaign_db_client)

        return [row['cid'] for row in results]

    def gather_oid_inv_beethoven(self, oid):
        cids = self.gather_oid_cids(oid)
        start_date = arrow.utcnow().shift(days=-7).timestamp
        query = """
        SELECT app_id, exchange, app_type,
        device_type, tagid, imp_size_group
        from `{table}`
        where time>='{start_date}' AND cid in ({cids})
        group by 1,2,3,4,5,6
        """.format(table=self.beethoven_perday_table,
                   start_date=start_date,
                   cids=','.join(["'{}'".format(cid) for cid in cids])
                   )
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_rtb_opt_client)

        return results

    def gather_same_inv(self, oid, invs):
        if len(invs) == 0:
            return []
        query = """
        SELECT app_id, partner_id, imp_is_instl, imp_type,
        imp_width, imp_height, app_type,
        device_type, tagid, imp_size_group
        from `{table}`
        where date=(select max(date) from `{table}`) AND
        oid='{oid}' AND
        (partner_id, app_id) in ({tuples})
        GROUP BY 1,2,3,4,5,6,7,8,9,10
        """.format(table=self.agg_oid_perf,
                   oid=oid,
                   tuples=','.join(["('{}', '{}')".format(row['partner_id'], row['app_id'])
                                    for row in invs]))
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for idx, r in enumerate(results):
            results[idx]['exchange'] = results[idx]['partner_id'].split('_')[0]

        return results

    def gather_cid_invs(self, cid):
        end_date = arrow.utcnow().shift().format('YYYY-MM-DD')
        start_date = arrow.utcnow().shift(days=-7).format('YYYY-MM-DD')
        query = """
        select
        app_id,
        partner_id,
        imp_is_instl,
        imp_type,
        imp_width,
        imp_height,
        app_type,
        device_type,
        tag_id as tagid,
        inv_hash as inv_hash,
        sum(cost) as cost,
        sum(bids) as bids,
        sum(shows) as shows,
        sum(clicks) as clicks,
        sum(actions) as actions
        from `{table}`
        where date <= '{end_date}' AND date >= '{start_date}' AND cid='{cid}'
        group by 1,2,3,4,5,6,7
        """.format(table=self.cid_sample_perf,
                   cid=cid,
                   end_date=end_date,
                   start_date=start_date)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for idx, r in enumerate(results):
            results[idx]['cost'] = float(results[idx]['cost'])

        return results

    def gather_cid_adjust_val(self, cid):
        query = """
        select app_id, tagid, exchange, app_type,
        device_type, imp_size_group, adjust_val
        from {table}
        where cid='{cid}'
        """.format(table=self.beethoven_adjust_table,
                   cid=cid)
        results = self.query_db.get_query_result(query, db_client=self.df_rtb_opt_client)
        return results

    def gather_oid_info(self, oid, hf_table, level=0):

        if hf_table:
            query = """
            SELECT app_id, bundle_id, partner_id, imp_is_instl, imp_type,
            imp_width, imp_height, app_type,
            device_type, tagid, sum(cost) as cost, sum(shows) as shows, sum(clicks) as clicks
            from {table}
            where oid='{oid}'
            group by 1,2,3,4,5,6,7,8,9,10
            """.format(table=self.oid_cost_hf_table,
                       oid=oid)
            logger.info(query)
            results = self.query_db.get_query_result(query, db_client=self.df_db_client)
            for row in results:
                row['cost'] = float(row['cost'])
                row['shows'] = int(row['shows'])
                row['clicks'] = int(row['clicks'])
        else:
            if level == 0:
                query = """
                SELECT partner_id, bundle_id, app_id, imp_is_instl, imp_type,
                imp_width, imp_height, app_type,
                device_type, tagid,
                SUM(cost) AS cost,
                SUM(shows) AS shows,
                SUM(clicks) AS clicks,
                SUM(installs) AS installs
                from {table}
                where oid='{oid}'
                GROUP BY 1, 2, 3, 4, 5, 6, 7, 8, 9
                """.format(table=self.agg_oid_perf, oid=oid)
            elif level == 1:
                query = """
                SELECT partner_id, bundle_id, app_id, imp_is_instl, imp_type,
                SUM(cost) AS cost,
                SUM(shows) AS shows,
                SUM(clicks) AS clicks,
                SUM(installs) AS installs
                from {table}
                where oid='{oid}'
                GROUP BY 1, 2, 3, 4, 5
                """.format(table=self.agg_oid_perf, oid=oid)
            else:
                raise ValueError("Level {} is not supported")
            logger.info(query)
            results = self.query_db.get_query_result(query, db_client=self.df_db_client)
            for idx, r in enumerate(results):
                results[idx]['imp_width'] = results[idx].get("imp_width", -1)
                results[idx]['imp_height'] = results[idx].get("imp_height", -1)
                results[idx]['app_type'] = results[idx].get("app_type", -1)
                results[idx]['device_type'] = results[idx].get("device_type", -1)
                results[idx]['tagid'] = results[idx].get("tagid", "")
                results[idx]['app_type'] = int(results[idx]['app_type']) if results[idx]['app_type'] != 'other' else 0
                results[idx]['device_type'] = int(results[idx]['device_type']) if results[idx]['device_type'] != 'other' else 0
                results[idx]['imp_size_group'] = lookupImpSizeGroup(results[idx])
                results[idx]['cost'] = float(results[idx]['cost'])
                results[idx]['shows'] = int(results[idx]['shows'])
                results[idx]['clicks'] = int(results[idx]['clicks'])
                results[idx]['installs'] = int(results[idx]['installs'])
        return results

    def get_country(self, cid):
        if cid not in self.cid_status:
            self.cid_status[cid] = self.idash_endpoint.get_cid_status(cid)
        country = self.cid_status[cid].get('require_regions')[0]
        return country

    def get_oid(self, cid):
        if cid not in self.cid_status:
            self.cid_status[cid] = self.idash_endpoint.get_cid_status(cid)
        oid = self.cid_status[cid].get('oid')
        return oid

    def get_goal_cpm(self, cid):
        if cid not in self.cid_status:
            self.cid_status[cid] = self.idash_endpoint.get_cid_status(cid)
        cpm = self.cid_status[cid].get('optimization_goal_cpm') / 10000000
        return cpm

    def get_cid_goal_cpa(self, cid):
        if cid not in self.cid_status:
            self.cid_status[cid] = self.idash_endpoint.get_cid_status(cid)
        cpa = self.cid_status[cid].get('optimization_goal_cpa')/10000000
        return cpa

    def get_goal_cpc(self, cid):
        if cid not in self.cid_status:
            self.cid_status[cid] = self.idash_endpoint.get_cid_status(cid)
        cpc = self.cid_status[cid].get('optimization_goal_cpc')/10000000
        return cpc

    def get_model_type(self, cid):
        if cid not in self.cid_status:
            self.cid_status[cid] = self.idash_endpoint.get_cid_status(cid)
        model_type = self.cid_status[cid].get('optimization_model')
        return model_type

    def get_goal_cpa(self, oid):
        query = """SELECT oid, goalcpa FROM {} where oid='{}'""".format(self.order_info, oid)
        logger.info(query)
        goal_result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        oid2goal = {row['oid']: row['goalcpa'] for row in goal_result}
        return oid2goal.get(oid)

    def get_site_id_country_inv_rt_uu(self, site_id, country, imp_type, is_rewarded, ssp):
        start_date = arrow.utcnow().shift(days=-7).format('YYYY-MM-DD')
        if ssp:
            parts = []
            for par in ssp:
                parts += c.ssp_enhance_map[par]
            ssp_condition = "AND partner_id in ({})".format(','.join(["'{}'".format(par) for par in parts]))
        else:
            ssp_condition = ""
        if imp_type == 2:
            query = """
                select
                app_id,
                partner_id,
                bundle_id,
                imp_is_instl,
                imp_type,
                is_rewarded,
                sum(uu) as uu,
                sum(rt_uu) as rt_uu,
                cast(sum(rt_uu) as decimal(15,5))/cast(sum(uu) as decimal(15,5)) as score
                from {table_name}
                where date>='{date}' and site_id='{site_id}' and country='{country}' {ssp_condition}
                group by 1,2,3,4,5,6
                having sum(uu) > 1000 and imp_type = 2 and is_rewarded={is_rewarded}
                ORDER BY score DESC
            """.format(table_name=self.site_id_country_inv_rt_uu_table,
                       date=start_date,
                       site_id=site_id,
                       country=country,
                       ssp_condition=ssp_condition,
                       is_rewarded=is_rewarded)
        else:
            query = """
                select
                app_id,
                partner_id,
                bundle_id,
                imp_is_instl,
                imp_type,
                imp_width,
                imp_height,
                device_type,
                app_type,
                tagid,
                inv_hash,
                is_rewarded,
                sum(uu) as uu,
                sum(rt_uu) as rt_uu,
                cast(sum(rt_uu) as decimal(15,5))/cast(sum(uu) as decimal(15,5)) as score
                from {table_name}
                where date>='{date}' and site_id='{site_id}' and country='{country}' {ssp_condition}
                group by 1,2,3,4,5,6,7,8,9,10,11,12
                having sum(uu) > 1000 and imp_type != 2 and is_rewarded={is_rewarded}
                ORDER BY score DESC
            """.format(table_name=self.site_id_country_inv_rt_uu_table,
                       date=start_date,
                       site_id=site_id,
                       country=country,
                       ssp_condition=ssp_condition,
                       is_rewarded=is_rewarded)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for row in results:
            row['imp_width'] = row.get("imp_width", "-1")
            row['imp_height'] = row.get("imp_height", "-1")
            row['tagid'] = row.get("tagid", "")
            row["app_type"] = row.get("app_type", "-1")
            row["device_type"] = row.get("device_type", "-1")
            row['imp_size_group'] = lookupImpSizeGroup(row)
            row['exchange'] = row['partner_id'].split('_')[0]
            row['score'] = float(row['score']) * np.log(float(row['rt_uu']))
            row['inv_hash'] = row.get("inv_hash", hash_df(row))
        return results

    def get_imp_ctcv_sample_list(self, oid, is_rewarded, ssp):
        parts = []
        for par in ssp:
            parts += c.ssp_enhance_map[par]
        rt_threshold = 0
        start_date = arrow.utcnow().shift(days=-7).format('YYYY-MM-DD')
        if len(parts) == 0:
            query = """
            select
            oid,
            app_id,
            partner_id,
            bundle_id,
            imp_is_instl,
            imp_type,
            imp_width,
            imp_height,
            device_type,
            app_type,
            tagid,
            inv_hash,
            sum(uu) as uu,
            sum(rt_uu) as rt_uu,
            cast(sum(rt_uu) as decimal(15,5))/cast(sum(uu) as decimal(15,5)) as score
            from {}
            where date>='{}' and oid='{}' and is_rewarded={}
            group by 1,2,3,4,5,6,7,8,9,10,11,12
            having sum(uu) > 1000 and imp_type=2 and sum(install_rt_uu) >= {}
            ORDER BY score DESC
            """.format(self.impbid_ctcv_table,
                       start_date,
                       oid,
                       is_rewarded,
                       rt_threshold)
        else:
            query = """
            select
            oid,
            app_id,
            partner_id,
            bundle_id,
            imp_is_instl,
            imp_type,
            imp_width,
            imp_height,
            device_type,
            app_type,
            tagid,
            inv_hash,
            sum(uu) as uu,
            sum(rt_uu) as rt_uu,
            cast(sum(rt_uu) as decimal(15,5))/cast(sum(uu) as decimal(15,5)) as score
            from {}
            where date>='{}' and oid='{}' and is_rewarded={} and partner_id in ({})
            group by 1,2,3,4,5,6,7,8,9,10,11,12
            having sum(uu) > 1000 and imp_type=2 and sum(install_rt_uu) >= {}
            ORDER BY score DESC
            """.format(self.impbid_ctcv_table,
                       start_date,
                       oid,
                       is_rewarded,
                       ','.join(["'{}'".format(par) for par in parts]),
                       rt_threshold)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for row in results:
            row['imp_size_group'] = lookupImpSizeGroup(row)
            row['exchange'] = row['partner_id'].split('_')[0]
            row['cpm'] = min(5, float(row['score']) * (self.get_goal_cpa(oid) / 10000000.) * 1000)
            row['score'] = float(row['score']) * np.log(float(row['uu']))

        return results

    def get_cid_creatives_idash(self, cid):

        creatives_info = self.idash_endpoint.get_creatives(cid).json()
        creatives = set()
        for c_dict in creatives_info:
            if c_dict['enabled'] == "true":
                if c_dict['creative_type'] == "native":
                    creatives.add((-1, -1))
                else:
                    creatives.add((int(c_dict['spec']['w']), int(c_dict['spec']['h'])))
        return list(creatives)

    def get_cid_creatives(self, cid):
        query = """
                select width, height, creative_type
                from {creative_spec}
                where crid in (SELECT crid from {creative} where cid='{cid}')
                """.format(creative_spec=self.creative_spec_table,
                           creative=self.creative_table,
                           cid=cid)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        creative_size = set()
        for row in results:
            creative_size.add((row['width'], row['height']))

        return creative_size

    def get_ctcv_bundle_list(self, oid):
        start_date = arrow.utcnow().shift(days=-30).format('YYYY-MM-DD')
        query = """
            select
            bundle_id,
            sum(uu) as uu,
            sum(rt_uu) as rt_uu,
            sum(cost) as cost,
            sum(bids) as bids,
            sum(shows) as shows,
            sum(clicks) as clicks,
            sum(country_shows_rt_uu) as country_shows_rt_uu,
            sum(country_clicks_rt_uu) as country_clicks_rt_uu,
            sum(shows)/sum(bids) as bid2show,
            sum(cost)/10000000/sum(shows)*1000 as cpm,
            sum(rt_uu)/sum(uu) as rt_ratio,
            sum(shows_rt_uu) as shows_rt_uu
            from {}
            where date>='{}' and oid='{}' and imp_type!=2
            group by 1
            having sum(shows) > 100
            order by shows_rt_uu DESC
            """.format(self.country_oid_rt_uu_table,
                       start_date,
                       oid)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        return results

    def get_site_id_country_inv_ctr(self, cid, site_id, country, imp_type, is_rewarded, creative_size):
        start_date = arrow.utcnow().shift(days=-7).format('YYYY-MM-DD')
        if creative_size:
            creative_condition = "AND (imp_width, imp_height) in ({})".format(','.join(map(repr, creative_size)))
        else:
            creative_condition = ""
        if imp_type == 2:
            query = """
                select app_id, partner_id, bundle_id, imp_is_instl, imp_type, is_rewarded,
                sum(costs) as cost,
                sum(shows) as shows,
                sum(clicks) as clicks,
                sum(country_shows_rt_uu) as country_shows_rt_uu,
                sum(country_clicks_rt_uu) as country_clicks_rt_uu,
                sum(costs)/1e7/sum(shows)*1e3 as cpm
                from {table_name}
                where date>='{date}' and site_id='{site_id}' and country='{country}' {creative_condition}
                group by 1,2,3,4,5,6
                having imp_type = 2 and sum(clicks)/sum(shows) > 0.00 and
                is_rewarded = {is_rewarded}
                """.format(table_name=self.site_id_country_inv_ctr_table,
                           date=start_date,
                           site_id=site_id,
                           country=country,
                           creative_condition=creative_condition,
                           is_rewarded=is_rewarded)
        else:
            query = """
                select app_id, partner_id, bundle_id, imp_is_instl, imp_type, imp_width, imp_height,
                device_type, app_type, tagid, is_rewarded, inv_hash,
                sum(costs) as cost,
                sum(shows) as shows,
                sum(clicks) as clicks,
                sum(country_shows_rt_uu) as country_shows_rt_uu,
                sum(country_clicks_rt_uu) as country_clicks_rt_uu,
                sum(costs)/1e7/sum(shows)*1e3 as cpm
                from {table_name}
                where date>='{date}' and site_id='{site_id}' and country='{country}' {creative_condition}
                group by 1,2,3,4,5,6,7,8,9,10,11,12
                having imp_type != 2 and sum(clicks)/sum(shows) > 0.00 and
                is_rewarded = {is_rewarded}
                """.format(table_name=self.site_id_country_inv_ctr_table,
                           date=start_date,
                           site_id=site_id,
                           country=country,
                           creative_condition=creative_condition,
                           is_rewarded=is_rewarded)

        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)

        click_rt_uu = 0.44
        show_rt_uu = 10
        for row in results:
            click_rt_uu += int(row['country_clicks_rt_uu'])
            show_rt_uu += int(row['country_shows_rt_uu'])

        for row in results:
            if self.get_model_type(cid) == 'lobster':
                row['ctr'] = (float(row['country_clicks_rt_uu'])+(float(row['clicks'])/float(row['shows']))*100)/(float(row['country_shows_rt_uu'])+100)
            else:
                if float(row['country_shows_rt_uu']) >= 15:
                    row['ctr'] = (float(row['country_clicks_rt_uu'])+float(row['clicks'])/float(row['shows'])*15)/(float(row['country_shows_rt_uu'])+15)
                else:
                    row['ctr'] = (float(row['country_clicks_rt_uu'])+float(row['clicks'])/float(row['shows'])*1)/(float(row['country_shows_rt_uu'])+1)
            row['imp_width'] = row.get("imp_width", "-1")
            row['imp_height'] = row.get("imp_height", "-1")
            row['tagid'] = row.get("tagid", "")
            row["app_type"] = row.get("app_type", "-1")
            row["device_type"] = row.get("device_type", "-1")
            row['inv_hash'] = row.get("inv_hash", hash_df(row))
        return results


    def get_ctcv_sample_list(self, cid, oid, creative_size):

        start_date = arrow.utcnow().shift(days=-7).format('YYYY-MM-DD')
        if len(creative_size) > 0:
            query = """
            select
            oid,
            app_id,
            partner_id,
            bundle_id,
            imp_is_instl,
            imp_type,
            imp_width,
            imp_height,
            device_type,
            app_type,
            tagid,
            inv_hash,
            sum(uu) as uu,
            sum(rt_uu) as rt_uu,
            sum(cost) as cost,
            sum(bids) as bids,
            sum(shows) as shows,
            sum(clicks) as clicks,
            sum(country_shows_rt_uu) as country_shows_rt_uu,
            sum(country_clicks_rt_uu) as country_clicks_rt_uu,
            sum(shows)/sum(bids) as bid2show,
            sum(cost)/10000000/sum(shows)*1000 as cpm
            from {}
            where date>='{}' and oid='{}' and (imp_width, imp_height) in ({})
            group by 1,2,3,4,5,6,7,8,9,10,11,12
            having sum(rt_uu) > 1 and imp_type!=2 and (sum(shows) > 20 or sum(uu) > 300) and sum(clicks)/sum(shows) > 0.00
            """.format(self.country_oid_rt_uu_table,
                       start_date,
                       oid,
                       ','.join(["({}, {})".format(size[0], size[1])
                                 for size in creative_size]))
        else:
            query = """
            select
            oid,
            app_id,
            partner_id,
            bundle_id,
            imp_is_instl,
            imp_type,
            imp_width,
            imp_height,
            device_type,
            app_type,
            tagid,
            inv_hash,
            sum(uu) as uu,
            sum(rt_uu) as rt_uu,
            sum(cost) as cost,
            sum(bids) as bids,
            sum(shows) as shows,
            sum(clicks) as clicks,
            sum(country_shows_rt_uu) as country_shows_rt_uu,
            sum(country_clicks_rt_uu) as country_clicks_rt_uu,
            sum(shows)/sum(bids) as bid2show,
            sum(cost)/10000000/sum(shows)*1000 as cpm
            from {}
            where date>='{}' and oid='{}'
            group by 1,2,3,4,5,6,7,8,9,10,11,12
            having sum(rt_uu) > 1 and imp_type!=2 and (sum(shows) > 20 or sum(uu) > 300) and sum(clicks)/sum(shows) > 0.00
            """.format(self.country_oid_rt_uu_table,
                       start_date,
                       oid)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)

        click_rt_uu = 0.44
        show_rt_uu = 10
        for row in results:
            click_rt_uu += int(row['country_clicks_rt_uu'])
            show_rt_uu += int(row['country_shows_rt_uu'])

        for row in results:
            row['imp_size_group'] = lookupImpSizeGroup(row)
            row['exchange'] = row['partner_id'].split('_')[0]
            row['rt_ratio'] = float(row['rt_uu'])/float(row['uu'])
            if self.get_model_type(cid) == 'lobster':
                row['ctr'] = (float(row['country_clicks_rt_uu'])+(float(row['clicks'])/float(row['shows']))*100)/(float(row['country_shows_rt_uu'])+100)
                # if cid in ('sruua6hHT3KPHPufdcFwGA', 'uVLX7kz_RoK92mzE8rpmGg'):
                #     row['ctr'] = (float(row['country_clicks_rt_uu'])+(float(row['clicks'])/float(row['shows']))*100)/(float(row['country_shows_rt_uu'])+100)
                # else:
                #     row['ctr'] = (float(row['country_clicks_rt_uu'])+(click_rt_uu/show_rt_uu)*100)/(float(row['country_shows_rt_uu'])+100)
            else:
                if float(row['country_shows_rt_uu']) >= 15:
                    row['ctr'] = (float(row['country_clicks_rt_uu'])+float(row['clicks'])/float(row['shows'])*15)/(float(row['country_shows_rt_uu'])+15)
                else:
                    row['ctr'] = (float(row['country_clicks_rt_uu'])+float(row['clicks'])/float(row['shows'])*1)/(float(row['country_shows_rt_uu'])+1)
            row['score'] = row['ctr'] * row['rt_ratio'] * np.log(float(row['uu'])+1)
            row['est_cpi'] = float(row['cpm']) / row['score'] / 1000 if row['score'] != 0 and self.get_model_type(cid) == 'lobster' else 0
            # row['est_cvr'] = row['score'] / (float(row['clicks'])/float(row['shows']))
        filtered_results = []
        for row in results:
            if cid in ('uVLX7kz_RoK92mzE8rpmGg', 'nRSznur8QMCK4zaiQcOA5A'):
                if row['bundle_id'] not in ('com.cashwalk.cashwalk', 'com.easybrain.block.puzzle.games', 'com.funple.app.zzal.reward'):
                    filtered_results.append(row)
            else:
                filtered_results.append(row)

        return filtered_results

    def delete_invs(self, cid, oid):
        query = """
                DELETE from {} where cid='{}' and oid='{}'
                """.format(self.inv_table, cid, oid)
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def get_complete_inv(self, cid, oid, complete_reason=None):
        if complete_reason:
            query = """
                    select *
                    from {}
                    where cid='{}' AND oid='{}' and complete_reason in ('{}', 'early_stop')
                    ORDER BY score DESC
                    """.format(self.inv_complete_table, cid, oid, complete_reason)
        else:
            query = """
                SELECT * FROM {} WHERE cid = '{}' AND oid = '{}' ORDER BY score DESC
            """.format(self.inv_complete_table, cid, oid)
        # logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        return results

    def get_topn_non_sampling_inv(self, cid, oid, topn=10):

        if oid in ('8UqNB8jgRyWTGRPrXDHGlA', 'WqvPQ27vQaK3LHzeBLNZeA') and topn != -1:
            topn = 100

        if self.get_model_type(cid) == 'lobster':
            query = """
                    select *
                    from {}
                    where cid='{}' AND oid='{}' AND sampling=0
                    ORDER BY est_cpi ASC
                    """.format(self.inv_table, cid, oid)
        else:
            query = """
                    select *
                    from {}
                    where cid='{}' AND oid='{}' AND sampling=0
                    ORDER BY score DESC
                    """.format(self.inv_table, cid, oid)
        # logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for row in results:
            del row['update_time']

        if topn == -1:
            return results

        return results[:topn]

    def get_sampling_inv(self, cid, oid):
        query = """
                select *
                from {}
                where cid='{}' AND oid='{}' AND imp_type!=2 AND sampling=1
                ORDER BY score DESC
                """.format(self.inv_table, cid, oid)
        # logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        return results

    def get_topn_inv(self, cid, oid, topn=10):

        if oid in ('8UqNB8jgRyWTGRPrXDHGlA', 'WqvPQ27vQaK3LHzeBLNZeA') and topn != -1:
            topn = 50

        if self.get_model_type(cid) == 'lobster':
            query = """
                    select *
                    from {}
                    where cid='{}' AND oid='{}'
                    ORDER BY est_cpi ASC
                    """.format(self.inv_table, cid, oid)
        else:
            query = """
                    select *
                    from {}
                    where cid='{}' AND oid='{}'
                    ORDER BY score DESC
                    """.format(self.inv_table, cid, oid)
        # logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        for row in results:
            del row['update_time']

        if topn == -1:
            return results

        return results[:topn]

    def update_checked(self, oid, cid, inv):

        query = """
        UPDATE {} SET checked=1 WHERE oid='{}' AND cid='{}' AND inv_hash='{}'
        """.format(self.inv_table,
                   oid,
                   cid,
                   inv['inv_hash'])
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def update_sampling_status(self, oid, cid, inv, status, goal, goal_type, installs):
        query = """
        UPDATE {} SET status='{}', goal='{}', goal_type='{}', installs='{}' WHERE oid='{}' AND cid='{}' AND
        inv_hash='{}'
        """.format(self.inv_table,
                   status,
                   goal,
                   goal_type,
                   installs,
                   oid,
                   cid,
                   inv['inv_hash'])
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def update_complete_status(self, oid, cid, inv, status, goal, goal_type, installs):
        query = """
        UPDATE {} SET status='{}', goal='{}', goal_type='{}', installs='{}' WHERE oid='{}' AND cid='{}' AND
        inv_hash='{}'
        """.format(self.inv_complete_table,
                   status,
                   goal,
                   goal_type,
                   installs,
                   oid,
                   cid,
                   inv['inv_hash'])
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def update_adjust_weight(self, oid, cid, inv, weight, use_hash=False):
        if use_hash:
            query = """
            UPDATE {} SET adjust_weight={} WHERE sampling=1 AND oid='{}' AND cid='{}' AND inv_hash='{}'
            """.format(self.inv_table,
                       weight,
                       oid,
                       cid,
                       inv['inv_hash'])
        else:
            query = """
            UPDATE {} SET adjust_weight={} WHERE sampling=1 AND oid='{}' AND cid='{}' AND {}
            """.format(self.inv_table,
                       weight,
                       oid,
                       cid,
                       ' AND '.join(["{}='{}'".format(k, v) for k, v in inv.items()]))
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def update_sampling_inv(self, oid, cid, inv, use_hash=False):

        if use_hash:
            query = """
            UPDATE {} SET sampling=1 WHERE oid='{}' AND cid='{}' AND inv_hash='{}'
            """.format(self.inv_table,
                       oid,
                       cid,
                       inv['inv_hash'])
        else:
            query = """
            UPDATE {} SET sampling=1 WHERE oid='{}' AND cid='{}' AND {}
            """.format(self.inv_table,
                       oid,
                       cid,
                       ' AND '.join(["{}='{}'".format(k, v) for k, v in inv.items()]))
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def collect_appanni_cate_bundle(self):
        query = """
        select bundle_id
        from {}
        group by 1
        """.format(self.appanni_cate)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        return results

    def collect_appanni_cate_pid(self):
        query = """
        select product_id
        from {}
        group by 1
        """.format(self.appanni_cate)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        return results

    def collect_appanni_bundle(self):
        query = """
        select bundle_id
        from {}
        group by 1
        """.format(self.appanni)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        return results

    def insert_appanni_cate(self, bundles):
        for row in bundles:
            keys = list(row.keys())
            query = """
            INSERT INTO `{}`
            ({})
            VALUES ({})
            ON DUPLICATE KEY UPDATE
            `unified_category_path`=values(`unified_category_path`)
            """.format(self.appanni_cate,
                       ','.join(["`{}`".format(col) for col in keys]),
                       ','.join(["'{}'".format(row[col]) for col in keys]))
            # logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)

    def insert_appanni(self, bundles):
        for row in bundles:
            keys = list(row.keys())
            query = """
            INSERT INTO `{}`
            ({})
            VALUES ({})
            ON DUPLICATE KEY UPDATE
            `usage_penetration`=values(`usage_penetration`),
            `install_penetration`=values(`install_penetration`),
            `open_rate`=values(`open_rate`),
            `avg_sessions_per_user`=values(`avg_sessions_per_user`),
            `avg_session_duration`=values(`avg_session_duration`),
            `average`=values(`average`),
            `star_5_count`=values(`star_5_count`),
            `star_4_count`=values(`star_4_count`),
            `star_3_count`=values(`star_3_count`),
            `star_2_count`=values(`star_2_count`),
            `star_1_count`=values(`star_1_count`)
            """.format(self.appanni,
                       ','.join(["`{}`".format(col) for col in keys]),
                       ','.join(["'{}'".format(row[col]) for col in keys]))
            # logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)

    def insert_sampling_inv(self, invs):
        if not isinstance(invs, list):
            invs = [invs]
        df = pd.DataFrame(invs)
        try:
            df.drop(["complete_reason"], inplace=True, axis=1)
        except:
            logger.error("Cannot drop columns of ['complete_reason']")
        finally:
            df['complete_sampling'] = 0
        for _, inv in df.iterrows():
            keys = list(inv.keys())
            query = """
            INSERT INTO `{}`
            ({})
            VALUES ({})
            ON DUPLICATE KEY UPDATE
            `status`=values(`status`),
            `installs`=values(`installs`)
            """.format(self.inv_table,
                       ','.join(["`{}`".format(col) for col in keys]),
                       ','.join(["'{}'".format(inv[col]) for col in keys]))
            # logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)

    def remove_out_of_rank(self, oid, cid, invs):
        for inv in invs:
            query = """
            DELETE from {} WHERE oid='{}' AND cid='{}' AND inv_hash='{}' AND complete_reason!='reach_goal' AND complete_reason!='early_stop'
            """.format(self.inv_complete_table,
                       oid,
                       cid,
                       inv['inv_hash'])
            # logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)

    def insert_complete_inv(self, inv, complete_reason):

        tmp_inv = inv.copy()
        for col in ['update_time', 'complete_sampling', 'sampling', 'est_cpi', 'checked']:
            try:
                del tmp_inv[col]
            except:
                continue
        tmp_inv['complete_reason'] = complete_reason
        keys = list(tmp_inv.keys())

        query = """
        INSERT INTO `{}`
        ({})
        VALUES ({})
        ON DUPLICATE KEY UPDATE
        `status`=values(`status`)
        """.format(self.inv_complete_table,
                   ','.join(["`{}`".format(col) for col in keys]),
                   ','.join(["'{}'".format(tmp_inv[col]) for col in keys]))
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def update_complete_inv(self, oid, cid, inv):

        query = """
        UPDATE {} SET complete_sampling=1, sampling=0 WHERE oid='{}' AND cid='{}' AND inv_hash='{}'
        """.format(self.inv_table,
                   oid,
                   cid,
                   inv['inv_hash'])
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def delete_sampling_inv(self, oid, cid, inv):
        query = """
        DELETE from {} WHERE oid='{}' AND cid='{}' AND inv_hash='{}'
        """.format(self.inv_table,
                   oid,
                   cid,
                   inv['inv_hash'])
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def delete_complete_inv(self, oid, cid, inv):
        query = """
        DELETE from {} WHERE oid='{}' AND cid='{}' AND inv_hash='{}'
        """.format(self.inv_complete_table,
                   oid,
                   cid,
                   inv['inv_hash'])
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)

    def gen_enhance_rules(self, cid, invs, use_tag_id=True):
        whitelist = set()
        tagid = list()
        for inv in invs:
            if inv['dealid'] != "" and inv['deal_count'] == 1:
                whitelist.add(inv['dealid'])
            else:
                whitelist.add('{}:{}'.format(inv['partner_id'], inv['app_id']))
                tagid.append([inv['partner_id'], inv['app_id'], inv['tagid']])

        enhance_rules = self.idash_endpoint.get_enhance_rules_status(cid)
        if len(enhance_rules):
            checked = set()
            for rule_dict in enhance_rules:
                if rule_dict.get('enhancement_type') == 'white' and \
                        rule_dict.get('source') == 'publisher_id_or_deal_id' and \
                        rule_dict.get('operator') == 'in':
                    checked.add('publisher_id_or_deal_id')
                    rule_dict['value'] = list(whitelist)
                    # rule_dict['value'] = list(set(rule_dict['value']) | set(self.df_adgroup_trees[root_cid]['whitelist']))

                if rule_dict.get('enhancement_type') == 'white' and \
                        rule_dict.get('source') == 'tag_id' and \
                        rule_dict.get('operator') == 'in':
                    if use_tag_id:
                        checked.add('tag_id')
                        rule_dict['value'] = list(tagid)
                    else:
                        # remove tagid if exists
                        enhance_rules.remove(rule_dict)

            if 'publisher_id_or_deal_id' not in checked:
                whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
                whitelist_update['value'] = list(whitelist)
                enhance_rules.append(whitelist_update)
            if 'tag_id' not in checked and use_tag_id:
                tagid_update = c.TAGID_UPDATE_TEMPLATE.copy()
                tagid_update['value'] = list(tagid)
                enhance_rules.append(tagid_update)

            update_dict = {'enhance_rules': enhance_rules}
        else:
            whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
            whitelist_update['value'] = list(whitelist)
            if use_tag_id:
                tagid_update = c.TAGID_UPDATE_TEMPLATE.copy()
                tagid_update['value'] = list(tagid)
                update_dict = {"enhance_rules": [whitelist_update, tagid_update]}
            else:
                update_dict = {"enhance_rules": [whitelist_update]}

        return update_dict

    def adjust_weight(self, cid, adjust_rules):
        if len(adjust_rules) == 0:
            return True
        feature_keys = c.INV_FEATURE_KEYS['fix_price']
        adjust_db_fields = feature_keys + ['adjust_val', 'adjust_by_robot']
        adjust_rule_tuple = []
        for rule in adjust_rules:
            adjust_rule_tuple.append(
                '(\'%s\',' % cid + ','.join(['\'' + str(rule[field]) + '\'' for field in adjust_db_fields]) + ')')
        query = 'INSERT INTO %s (cid, %s) VALUES %s ON DUPLICATE KEY UPDATE adjust_val = VALUES(adjust_val), adjust_by_robot=VALUES(adjust_by_robot)' \
                % (self.beethoven_adjust_table, ', '.join(adjust_db_fields), ','.join(adjust_rule_tuple))
        # logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_rtb_opt_client)

        data = get_all_campaign_data.get_beethoven_all_params(cid)
        success = self.idash_endpoint.send_bidder_aggregator(data, cid)

        return success


class UserListTableCenter(object):
    def __init__(self, idash_endpoint, query_db, listctrl_cfg):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.cid_status = {}
        self.df_db_client = query_db.db_clients[MySQL_DB_AIBE_DEV]
        self.userlist_table = listctrl_cfg['userlist_table']

    def get_sample_list(self):
        query = """
        select
        cid,
        `inventory.partner_id` as partner_id,
        `inventory.app_id` as app_id,
        `inventory.tagid` as tagid
        from {}
        where cid is not null
        """.format(self.userlist_table)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        return results

    def gen_enhance_rules(self, cid, invs):

        whitelist = set()
        tagid = list()
        for inv in invs:
            whitelist.add('{}:{}'.format(inv['partner_id'], inv['app_id']))
            tagid.append([inv['partner_id'], inv['app_id'], inv['tagid']])

        enhance_rules = self.idash_endpoint.get_enhance_rules_status(cid)
        if len(enhance_rules):
            checked = set()
            for rule_dict in enhance_rules:
                if rule_dict.get('enhancement_type') == 'white' and \
                        rule_dict.get('source') == 'publisher_id_or_deal_id' and \
                        rule_dict.get('operator') == 'in':
                    checked.add('publisher_id_or_deal_id')
                    rule_dict['value'] = list(whitelist)
                    # rule_dict['value'] = list(set(rule_dict['value']) | set(self.df_adgroup_trees[root_cid]['whitelist']))

                if rule_dict.get('enhancement_type') == 'white' and \
                        rule_dict.get('source') == 'tag_id' and \
                        rule_dict.get('operator') == 'in':
                    checked.add('tag_id')
                    rule_dict['value'] = list(tagid)

            if 'publisher_id_or_deal_id' not in checked:
                whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
                whitelist_update['value'] = list(whitelist)
                enhance_rules.append(whitelist_update)
            if 'tag_id' not in checked:
                tagid_update = c.TAGID_UPDATE_TEMPLATE.copy()
                tagid_update['value'] = list(tagid)
                enhance_rules.append(tagid_update)

            update_dict = {'enhance_rules': enhance_rules}
        else:
            whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
            whitelist_update['value'] = list(whitelist)
            tagid_update = c.TAGID_UPDATE_TEMPLATE.copy()
            tagid_update['value'] = list(tagid)
            update_dict = {"enhance_rules": [whitelist_update, tagid_update]}

        return update_dict