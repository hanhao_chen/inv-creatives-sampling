"""Provides interface module for creating/retriving deep funnel adgroup trees """
# TODO: refactor this module
from __future__ import absolute_import
import json
import time
import datetime
import re
from collections import defaultdict
import numpy as np
import constants as c
import re

from app.utils.db import MySQL_DB_AI_DEEP_FUNNEL, MySQL_DB_CAMPAIGN, MySQL_DB_AIBE_DEV
from app.module.Const import ENUM_AI_RULE_OPTION
from app.module.CIdashJobQueue import IdashJobQueue
from app.utils.partner_utils import get_df_ready_partners
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.common_utils import AppUsage, get_external_capability, \
    in_scope, get_partner_source_list
from app.external_optimization.deep_funnel.utils.constants import APPIER_TO_USD, USD_TO_APPIER, HYDRA_MODELS

logger = init_logger(__name__)
CID_PENDING = 'pending'
CID_FAILURE = 'failed'
HYDRA_NAME_PREFIX = re.compile(r"(^\[Hydra_.+\])_(.+)$")


def get_hydra_raw_adgroup_name(cid_name):
    hydra_prefix = HYDRA_NAME_PREFIX.match(cid_name)
    return cid_name if hydra_prefix is None else hydra_prefix.group(2)


class ExploreAdgroupTrees(IdashJobQueue):
    """Class for creating and retriving DF adgroup trees

    Usage:
        1. calls update_df_adgroup_trees() to load current DF adgroup trees into the instance
        2. calls register_new_df_cids() to get registered cids
        3. calls get_df_adgroup_trees() to retrive current DF adgroup trees
        4. calls get_active_root_cids_status() to retrive active root cids and it's adgroups' status
        5. calls check_warning_messages() to check iDash job queue status
    """
    STATUS_PENDING_LONG = 'wait more than a hour'
    STATUS_PENDING_TOO_LONG = 'wait more than half day'
    STATUS_FAILURE = 'failed'

    def __init__(self, listctrl_cfg, idash_endpoint, query_db):
        super(ExploreAdgroupTrees, self).__init__(
            idash_endpoint, query_db.db_clients[MySQL_DB_AIBE_DEV],
            listctrl_cfg['idash_job_queue_table'], 'explore_adgroup_trees')
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AIBE_DEV]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        """
        TODO change to real sampling df client, and sampling table
        """
        self.sampling_db_client = query_db.db_clients[MySQL_DB_AIBE_DEV]
        self.df_adgroup_trees_table = listctrl_cfg['df_adgroup_trees_table']
        self.campaign_table = listctrl_cfg['campaign_table']
        self.order_info = listctrl_cfg['order_info_table']
        self.aggregated_perf_table = listctrl_cfg['aggregated_performance_table']
        self.daily_perf_table = listctrl_cfg['oid_daily_performance_table']
        self.quantum_cids = {}
        self.df_adgroup_trees = {}
        self.registered_cids = set()
        self.new_opened_root_cids = set()
        self.warning_messages = defaultdict(list)

    def gen_quantum_oid_whitelist(self):

        quantum_whitelists = defaultdict(set)
        result = self.load_oid_perf()

        for row in result:
            oid = row['oid']
            sample_cpa = row['cost'] / row['actions'] if row['actions'] else np.inf
            # sample_cpc = row['cost'] / row['clicks'] if row['clicks'] else np.inf
            for cid, v in self.quantum_cids.items():
                if v['oid'] == oid:
                    if sample_cpa <= self.quantum_cids[cid]['optimization_goal_cpa'] and row['shows'] > 30:
                        quantum_whitelists[cid].add("{}:{}".format(row['partner_id'], row['app_id']))
                    # elif sample_cpc <= self.quantum_cids[cid]['optimization_goal_cpc'] and row['shows'] > 50:
                    #     quantum_whitelists[cid].add("{}:{}".format(row['partner_id'], row['app_id']))

        return quantum_whitelists

    def gen_quantum_cid_whitelist(self):

        quantum_whitelists = defaultdict(set)
        result = self.load_quantum_cid_perf()

        for row in result:
            cid = row['cid']
            sample_cpa = row['cost'] / row['actions'] if row['actions'] else np.inf
            sample_cpc = row['cost'] / row['clicks'] if row['clicks'] else np.inf
            if sample_cpa <= self.quantum_cids[cid]['optimization_goal_cpa'] and row['shows'] > 50:
                quantum_whitelists[cid].add("{}:{}".format(row['partner_id'], row['app_id']))
            elif sample_cpc <= self.quantum_cids[cid]['optimization_goal_cpc'] and row['shows'] > 50:
                quantum_whitelists[cid].add("{}:{}".format(row['partner_id'], row['app_id']))
        return quantum_whitelists

    def load_oid_perf(self):
        print(self.quantum_cids)
        oids = set([v['oid'] for _, v in self.quantum_cids.items()])

        query = """SELECT oid, partner_id, app_id, 
                   sum(shows) as shows, 
                   sum(clicks) as clicks, 
                   sum(actions) as actions, 
                   sum(cost) as cost
                   FROM {} WHERE oid in ({}) GROUP BY 1,2,3""".format(
                self.daily_perf_table,
                ','.join(['\'{}\''.format(oid) for oid in oids]))

        # query = """SELECT * FROM {} WHERE cid in ({})""".format(
        #         self.aggregated_perf_table,
        #         ','.join(['\'{}\''.format(cid) for cid in quantum_cids]))
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.sampling_db_client)

        return result

    def load_quantum_cid_perf(self):

        # return [
        #     {'partner_id':'1st_p', 'app_id': '1st_app', 'cid': 'Znd_6uztQJytl-BQ3oHDjQ', 'cid_cost': 10, 'cid_action': 4, 'cid_click': 9},
        #     {'partner_id':'2st_p', 'app_id': '2st_app', 'cid': 'Znd_6uztQJytl-BQ3oHDjQ', 'cid_cost': 10, 'cid_action': 1, 'cid_click': 10},
        #     {'partner_id':'3st_p', 'app_id': '3st_app', 'cid': 'Znd_6uztQJytl-BQ3oHDjQ', 'cid_cost': 10, 'cid_action': 1, 'cid_click': 10}
        # ]
        #
        query = """SELECT cid, partner_id, app_id, 
                   sum(shows) as shows, 
                   sum(clicks) as clicks, 
                   sum(actions) as actions, 
                   sum(cost) as cost
                   FROM {} WHERE cid in ({}) GROUP BY 1,2,3""".format(
                self.daily_perf_table,
                ','.join(['\'{}\''.format(cid) for cid in self.quantum_cids.keys()]))

        # query = """SELECT * FROM {} WHERE cid in ({})""".format(
        #         self.aggregated_perf_table,
        #         ','.join(['\'{}\''.format(cid) for cid in quantum_cids]))
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.sampling_db_client)

        return result

    def load_quantum_cid_v2(self):

        query = """SELECT cid, oid, name FROM {} WHERE running=1 AND algorithm='quantum'""".format(self.campaign_table)
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        exclude_cids = self.get_exclude_cids()
        logger.info("exclude cids: %s", exclude_cids)

        oids = set([row['oid'] for row in result if row['cid'] not in exclude_cids])
        query = """SELECT oid, goalcpa FROM {} where oid in ({})""".format(self.order_info, ','.join(['\'{}\''.format(oid) for oid in oids]))
        logger.info(query)
        goal_result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        oid2goal = {row['oid']:row['goalcpa'] for row in goal_result}

        for row in result:
            if row['cid'] in exclude_cids:
                continue
            goal_cpa = oid2goal.get(row['oid'], 0) * 5
            self.quantum_cids[row['cid']] = {
                'oid': row['oid'],
                'optimization_goal_cpa': goal_cpa
            }

    def load_quantum_cid(self):
        # self.quantum_cids['Znd_6uztQJytl-BQ3oHDjQ'] = {'optimization_goal_cpa': 2.5, 'optimization_goal_cpc': 1}
        # return

        query = """SELECT cid, oid, name FROM {} WHERE running=1 AND algorithm='quantum'""".format(self.campaign_table)
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        exclude_cids = self.get_exclude_cids()
        logger.info("exclude cids: %s", exclude_cids)
        for row in result:
            if row['cid'] in exclude_cids:
                continue
            status = self.idash_endpoint.get_cid_data_request(row['cid']).json()
            goal_cpa = goal_cpc = 0
            if len(status):
                status = status[0]
                if isinstance(status, dict):
                    goal_cpa = status.get('optimization_goal_cpa', 0)
                    goal_cpc = status.get('optimization_goal_cpc', 0)
                else:
                    logger.info('Cannot get cid goal %s', row['cid'])
            else:
                logger.warning('Cannot get cid status %s', row['cid'])

            self.quantum_cids[row['cid']] = {
                'oid': row['oid'],
                'optimization_goal_cpa': goal_cpa,
                'optimization_goal_cpc': goal_cpc
            }

    def load_df_adgroup_trees(self):
        """Loads the current deep funnel adgroup trees from MySQL table """
        self.df_adgroup_trees = {}
        self.registered_cids = set()
        query = 'SELECT * FROM {}'.format(self.df_adgroup_trees_table)
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.df_db_client)

        for row in result:
            self.df_adgroup_trees[row['root_cid']] = {
                'dup_campaign_cids': json.loads(row['dup_campaign_cids']) if row['dup_campaign_cids'] else [],
                'pending_tasks': set(json.loads(row['pending_tasks'])) if row['pending_tasks'] else set(),
                'whitelist': json.loads(row['whitelist']) if row['whitelist'] else [],
                'dup_status': json.loads(row['dup_status']) if row['dup_status'] else {},
                'update_status': json.loads(row['update_status']) if row['update_status'] else {}
            }
            self.registered_cids |= self.get_tree_cids_set(row['root_cid'])

        logger.info('%s root cids loaded, %s cids registered', len(self.df_adgroup_trees), len(self.registered_cids))

    def get_tree_cids_set(self, root_cid):
        """Gets the cids set in a tree
        :param root_cid:
        :return cids: set of cids
        """
        cids = set()
        for field in ('dup_campaign_cids',):
            value = self.df_adgroup_trees[root_cid][field]
            if not value:
                continue
            if isinstance(value, list):
                cids |= set(value)
            elif value:
                cids.add(value)

        return cids

    def get_enhance_rules_status(self, new_cid):

        status = self.idash_endpoint.get_cid_data_request(new_cid).json()
        if len(status):
            status = status[0]
            if isinstance(status, dict):
                enhance_rules = status.get('enhance_rules')
                return enhance_rules
            else:
                logger.info('Cannot get enhance rules %s', new_cid)
                return []
        else:
            logger.warning('Cannot get cid status %s', new_cid)
            return []

    def gen_enhance_rules(self, root_cid, new_cid, init_budget=False):

        enhance_rules = self.get_enhance_rules_status(new_cid)
        if len(enhance_rules):
            checked = False
            for rule_dict in enhance_rules:
                if rule_dict.get('enhancement_type') == 'white' and \
                        rule_dict.get('source') == 'publisher_id_or_deal_id' and \
                        rule_dict.get('operator') == 'in':
                    checked = True
                    # rule_dict['value'] = list(set(rule_dict['value']) | set(self.df_adgroup_trees[root_cid]['whitelist']))
                    rule_dict['value'] = self.df_adgroup_trees[root_cid]['whitelist']
            if not checked:
                whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
                whitelist_update['value'] = self.df_adgroup_trees[root_cid]['whitelist']
                enhance_rules.append(whitelist_update)
            update_dict = {'enhance_rules': enhance_rules}
        else:
            whitelist_update = c.WHITELIST_UPDATE_TEMPLATE.copy()
            whitelist_update['value'] = self.df_adgroup_trees[root_cid]['whitelist']
            update_dict = {"enhance_rules": [whitelist_update]}

        if init_budget:
            update_dict.update({"daily_max_budget": "100000000", "optimization_type": "cpa"})

        return update_dict

    def update_df_adgroup_trees(self):
        """Updates the deep funnel adgroup trees to MySQL table from iDash job queue status,
           enable sucessful created sampling adgroups, then load the newest adgroup trees
        """
        self.load_df_adgroup_trees()
        self.warning_messages = defaultdict(list)

        self.polling_jobs_status()
        results = self.get_jobs_result()
        current_datetime = datetime.datetime.utcnow()
        update_root_cids = set()
        processesed_job_ids = []
        fail_retry_job_ids = []

        for row in results:
            if not row['command'] or not row['application_params']:
                logger.warning('unknown job %s', row)
                continue
            command = json.loads(row['command'])
            origin_cid = command['cid']
            application_params = json.loads(row['application_params'])
            root_cid = application_params['root_cid']

            if row['job_status'] == 'success':
                # campaign whitelist cid to sampling whitelist cid
                r_result = re.sub(r'"errorMsg".*?"]\']",', '', row['result'])
                result = json.loads(r_result)

                new_cid = result['data']['cid']

                config = self.idash_endpoint.read_campaign_fields(
                    new_cid, ('campaign_other_type', 'daily_max_budget', 'ext_bid_price'))
                copy_type = application_params['copy_type']

                # update model
                if copy_type == 'cell':
                    update_model = c.CELL_UPDATE
                elif copy_type == 'lobster':
                    update_model = c.LOBSTER_UPDATE
                elif copy_type == 'alpaca':
                    update_model = c.ALPACA_UPDATE
                else:
                    update_model = None
                    logger.info('Unkown copy type %s', copy_type)

                if update_model:
                    self.idash_endpoint.send_update_data_request(
                        new_cid, update_model)

                # update whitelist
                enhance_rules = self.gen_enhance_rules(root_cid, new_cid, init_budget=True)
                self.idash_endpoint.send_update_data_request(new_cid, enhance_rules)
                self.df_adgroup_trees[root_cid]['update_status'][copy_type] = "checked"

                # remove from adgroup tree pending tasks
                try:
                    self.df_adgroup_trees[root_cid]['pending_tasks'].remove(row['job_id'])
                except Exception as _:
                    pass

                self.idash_endpoint.send_cid_enable_request(new_cid, config['campaign_other_type'], True)
                processesed_job_ids.append(row['job_id'])
            else:
                copy_type = application_params['copy_type']
                message = None
                retry = False
                if row['job_status'] != 'pending':
                    new_cid = CID_FAILURE
                    message = self.STATUS_FAILURE
                    retry = True
                else:
                    pending_ts = (current_datetime - row['created_at']).total_seconds()
                    new_cid = CID_PENDING
                    if pending_ts > 3600 * 6:
                        message = self.STATUS_PENDING_TOO_LONG
                        self.warning_messages[self.STATUS_PENDING_TOO_LONG].append(origin_cid)
                        retry = True
                    elif pending_ts > 3600:
                        message = self.STATUS_PENDING_LONG

                if message:
                    self.warning_messages[message].append(origin_cid)

                if retry:  # let hydra can retry the copy event
                    fail_retry_job_ids.append(row['job_id'])
                    job_id = self.idash_endpoint.copy_adgroup(
                        origin_cid, command['params']['begin_at'], command['params']['end_at'],
                        command['params']['name'])
                    self.record_job_id(job_id, application_params=row['application_params'])
                    self.df_adgroup_trees[root_cid]['dup_status'][copy_type] = job_id
                else:
                    self.df_adgroup_trees[root_cid]['pending_tasks'].add(row['job_id'])
                update_root_cids.add(root_cid)
            if new_cid not in (CID_PENDING, CID_FAILURE):
                update_root_cid = self._update_df_adgroup_tree(application_params, new_cid)
                if update_root_cid:
                    update_root_cids.add(update_root_cid)

        if processesed_job_ids:
            self.update_application_status(processesed_job_ids, 'checked')
        if fail_retry_job_ids:
            self.update_application_status(processesed_job_ids, 'fail')

        self._update_df_adgroup_trees_table(update_root_cids, self.df_adgroup_trees)

    def update_whitelist(self):
        """
        update whitelist update only root_cid (existing root cid)
        :return:
        """
        update_root_cids = set()
        results = self.get_jobs_status_result()
        jobid2cid = {row['job_id']: json.loads(re.sub(r'"errorMsg".*?"]\']",', '', row['result']))['data']['cid'] for row in results}
        for root_cid in self.df_adgroup_trees.keys():
            jobid_mp = self.df_adgroup_trees[root_cid]['dup_status']
            for copy_type, value in self.df_adgroup_trees[root_cid]['update_status'].items():
                if value == 'pending' and jobid_mp.get(copy_type, "None") in jobid2cid.keys():
                    update_root_cids.add(root_cid)
                    enhance_rules = self.gen_enhance_rules(root_cid, jobid2cid[jobid_mp[copy_type]])
                    self.idash_endpoint.send_update_data_request(jobid2cid[jobid_mp[copy_type]], enhance_rules)
                    self.df_adgroup_trees[root_cid]['update_status'][copy_type] = "checked"

                    # enable / disable this duplicate cid
                    new_cid = jobid2cid[jobid_mp[copy_type]]
                    config = self.idash_endpoint.read_campaign_fields(new_cid, ('campaign_other_type', 'daily_max_budget', 'ext_bid_price'))
                    if len(self.df_adgroup_trees[root_cid]['whitelist']) > 0:
                        self.idash_endpoint.send_cid_enable_request(new_cid, config['campaign_other_type'], True)
                    else:
                        self.idash_endpoint.send_cid_enable_request(new_cid, config['campaign_other_type'], False)

                elif value == 'pending' and len(self.df_adgroup_trees[root_cid]['whitelist']) == 0:
                    update_root_cids.add(root_cid)
                    self.df_adgroup_trees[root_cid]['update_status'][copy_type] = "checked"

        self._update_df_adgroup_trees_table(update_root_cids, self.df_adgroup_trees)

    def _update_df_adgroup_tree(self, params, new_cid):
        """Updates an adgroup tree from a job queue result
        :param params: dict contains copy_type and root_cid
        :param new_cid:
        :return update_root_cid: root_cid need to be updated, None when already updated to MySQL table
        """
        root_cid = params['root_cid']

        new_tree_data = {
            'dup_campaign_cids': list(set(self.df_adgroup_trees[root_cid]['dup_campaign_cids']) | set([new_cid]))
        }

        update_root_cid = None
        for key, value in new_tree_data.items():
            if isinstance(value, list) and set(value) != set(self.df_adgroup_trees[root_cid][key]):
                update_root_cid = root_cid
                self.df_adgroup_trees[root_cid][key] = value
            elif not isinstance(value, list) and value != self.df_adgroup_trees[root_cid][key]:
                update_root_cid = root_cid
                self.df_adgroup_trees[root_cid][key] = value

        return update_root_cid

    def _update_df_adgroup_trees_table(self, update_root_cids, df_adgroup_trees):
        """Updates a batch root_cids into MySQL table
        :param update_root_cids:
        """
        for root_cid in update_root_cids:
            query = """
            UPDATE {}
            SET
                dup_campaign_cids = '{}',
                pending_tasks = '{}',
                whitelist = '{}',
                dup_status = '{}',
                update_status = '{}'
            WHERE
                root_cid = '{}'
            """.format(self.df_adgroup_trees_table,
                       json.dumps(df_adgroup_trees[root_cid]['dup_campaign_cids']),
                       json.dumps(list(set(df_adgroup_trees[root_cid]['pending_tasks']))),
                       json.dumps(list(set(df_adgroup_trees[root_cid]['whitelist']))),
                       json.dumps(df_adgroup_trees[root_cid]['dup_status']),
                       json.dumps(df_adgroup_trees[root_cid]['update_status']),
                       root_cid)
            logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)

    def get_df_adgroup_trees(self):
        """Gets the deep funnel adgroup trees
        :return df_adgroup_trees: dict of root_cid to it's adgroup leaves
        """
        if not self.df_adgroup_trees:
            self.load_df_adgroup_trees()
        return self.df_adgroup_trees

    def register_root_cid(self, root_cid):
        """Registers a new tree
        :param root_cid:
        """
        if root_cid in self.df_adgroup_trees:
            logger.warning('already registered root cid: %s', root_cid)
            return False
        else:
            query = """
            INSERT INTO {}
            (root_cid)
            VALUES ('{}')
            """.format(self.df_adgroup_trees_table, root_cid)
            logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)

            return True

    def register_root_cids(self, root_cids):

        self.load_df_adgroup_trees()
        for root_cid in root_cids:
            self.register_root_cid(root_cid)
        self.load_df_adgroup_trees()

        return True

    def get_exclude_cids(self):

        results = self.get_jobs_status_all()
        cids = set([json.loads(re.sub(r'"errorMsg".*?"]\']",', '', row['result']))['data']['cid'] for row in results])
        return cids

    def get_dup_root_cids(self):

        results = self.get_jobs_status_all()
        root_cids = set([json.loads(row['application_params'])['root_cid'] for row in results])
        return root_cids

    def pull_cid_whitelists(self, root_cids):
        """
        pull cids whitelist result and check if whitelist change
        :return:
        """
        """
        TODO get pull result from db
        """
        self.load_quantum_cid_v2()
        # quantum_cid_whitelist = self.gen_quantum_cid_whitelist()
        quantum_cid_whitelist = self.gen_quantum_oid_whitelist()

        update_cids = set()
        self.register_root_cids(quantum_cid_whitelist.keys())
        self.load_df_adgroup_trees()
        for cid in quantum_cid_whitelist.keys():
            if root_cids and cid not in root_cids:
                continue
            new_adgroup = set(quantum_cid_whitelist[cid])# - set(self.df_adgroup_trees[cid]['whitelist'])
            if len(new_adgroup) > 0:
                update_cids.add(cid)
            self.df_adgroup_trees[cid]['whitelist'] = list(new_adgroup) #| set(self.df_adgroup_trees[cid]['whitelist']))
            for copy_type in ('cell', 'alpaca', 'lobster'):
                self.df_adgroup_trees[cid]['update_status'][copy_type] = "pending"

        return update_cids


class ExploreAdgroupTree(IdashJobQueue):
    CELL_ADGROUP_NAME = '[Cell]_{}_{}'
    AlPACA_ADGROUP_NAME = '[Alpaca]_{}_{}'
    LOBSTER_ADGROUP_NAME = '[Lobster]_{}_{}'

    def __init__(self, listctrl_cfg, idash_endpoint, query_db, root_cid, adgroup_tree):
        super(ExploreAdgroupTree, self).__init__(
            idash_endpoint, query_db.db_clients[MySQL_DB_AIBE_DEV],
            listctrl_cfg['idash_job_queue_table'], 'explore_adgroup_trees')
        self.df_adgroup_trees_table = listctrl_cfg['df_adgroup_trees_table']
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AIBE_DEV]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.root_cid = root_cid
        self.adgroup_tree = adgroup_tree
        self.cids_config = self.get_cids_config()

    def get_idash_pending_tasks(self):
        """Gets idash pending task names in job queue
        :return pending_tasks: list of pending task names
        """
        return self.adgroup_tree['pending_tasks']

    def trigger_copy_event(self, copy_type, goal_type='CPI'):
        """Triggers an adgroup copying event, send a job to idash job queue
        :param copy_type:
        """
        target_name = self._get_adgroup_copying_params(copy_type)

        job_id = self.idash_endpoint.copy_adgroup(
            self.root_cid, self.cids_config[self.root_cid]['begin_at'], self.cids_config[self.root_cid]['end_at'], target_name)
        self.record_job_id(job_id, application_params=json.dumps({'copy_type': copy_type, 'root_cid': self.root_cid, 'goal_type': goal_type}))

        self.adgroup_tree['dup_status'].update({copy_type: job_id})

    def _get_adgroup_copying_params(self, copy_type):

        root_cid_name = get_hydra_raw_adgroup_name(
            self.idash_endpoint.read_campaign_fields(self.root_cid, ['name'])['name'])
        if copy_type == 'cell':
            target_name = self.CELL_ADGROUP_NAME.format(c.DUPNAME, root_cid_name)
        elif copy_type == 'alpaca':
            target_name = self.AlPACA_ADGROUP_NAME.format(c.DUPNAME, root_cid_name)
        elif copy_type == 'lobster':
            target_name = self.LOBSTER_ADGROUP_NAME.format(c.DUPNAME, root_cid_name)

        return target_name

    def get_cids_config(self):
        """Gets the cids campaign config for all adgroups in the tree
        :return cids_config: dict of cid to (field, value) dict
        """
        cids_config = {}
        query = """
        SELECT
            cid, begin_at, end_at, ext_channel, oid, country, top_category, sub_category,
            name AS cid_name, optimization_model
        FROM
            campaign
        WHERE
            cid IN ({})
        """.format(','.join(['\'{}\''.format(self.root_cid)]))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)

        uncached_cids = list(set(self.root_cid) - set(self.root_cid))
        if uncached_cids:
            logger.warning('querying idash for uncached_cids: %s', uncached_cids)
            uncached_result = self.idash_endpoint.read_campaigns_by_cids(
                uncached_cids, fields=['cid', 'campaign_other_type', 'begin_at', 'end_at',
                                       'oid', 'name', 'optimization_model'])
            for row in uncached_result:
                row['ext_channel'] = row['campaign_other_type']
                row['cid_name'] = row['name']
                result.append(row)

        oid_partner = set()
        for row in result:
            top_category = row.get('top_category', '')  # not in campaign table
            sub_category = row.get('sub_category', '')
            category = (top_category + '_' + sub_category) if sub_category else top_category
            cids_config[row['cid']] = {
                'cid_name': row['cid_name'],
                'begin_at': int(row['begin_at']),
                'end_at': int(row['end_at']),
                'ext_channel': row['ext_channel'],
                'oid': row['oid'],
                'category': category,
                'top_category': top_category,
                'country': row.get('country', ''),
                'optimization_model': row['optimization_model']
            }
            oid_partner.add((row['oid'], row['ext_channel']))
        assert len(oid_partner) == 1

        (oid, partner) = list(oid_partner)[0]
        # oids = [oid]
        # if self.oid_data_reference and oid in self.oid_data_reference:
        #     oids.extend(self.oid_data_reference[oid])
        # target_partners = self.partner_source_list.get(partner, [partner])
        # query = "SELECT DISTINCT cid FROM campaign WHERE oid IN ({}) AND ext_channel IN ({})".format(
        #     in_scope(oids), in_scope(target_partners))
        # logger.debug(query)
        # result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        # partner_cids = [row['cid'] for row in result]
        #
        # for cid, config in cids_config.items():
        #     config['partner_cids'] = partner_cids

        query = """
        SELECT
            goalcpa, primary_goal, vertical, name AS oid_name
        FROM
            order_info
        WHERE
            oid = '{}'
        """.format(oid)
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)[0]
        goalcpa = APPIER_TO_USD * result['goalcpa']
        for cid in cids_config:
            cids_config[cid]['goalcpa'] = goalcpa
            cids_config[cid]['primary_goal'] = result['primary_goal']
            cids_config[cid]['vertical'] = result['vertical']
            cids_config[cid]['oid_name'] = result['oid_name']

        return cids_config


class DeepFunnelAdgroupTrees(IdashJobQueue):
    """Class for creating and retriving DF adgroup trees

    Usage:
        1. calls update_df_adgroup_trees() to load current DF adgroup trees into the instance
        2. calls register_new_df_cids() to get registered cids
        3. calls get_df_adgroup_trees() to retrive current DF adgroup trees
        4. calls get_active_root_cids_status() to retrive active root cids and it's adgroups' status
        5. calls check_warning_messages() to check iDash job queue status
    """
    STATUS_PENDING_LONG = 'wait more than a hour'
    STATUS_PENDING_TOO_LONG = 'wait more than half day'
    STATUS_FAILURE = 'failed'

    def __init__(self, listctrl_cfg, idash_endpoint, query_db):
        super(DeepFunnelAdgroupTrees, self).__init__(
            idash_endpoint, query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL],
            listctrl_cfg['idash_job_queue_table'], 'deep_funnel_adgroup_trees')
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.df_adgroup_trees_table = listctrl_cfg['df_adgroup_trees_table']
        self.garbage_cid_table = listctrl_cfg['explore_garbage_cid_table']

        self.df_adgroup_trees = {}
        self.registered_cids = set()
        self.new_opened_root_cids = set()
        self.warning_messages = defaultdict(list)
        self.df_adgroups = DeepFunnelAdgroups(listctrl_cfg, idash_endpoint, query_db)
        self.partner_capabilities = get_external_capability(self.query_db)
        self.cid2partner = {
            row['cid']: row['ext_channel']
            for row in self.query_db.get_query_result(
                'SELECT cid, ext_channel FROM campaign WHERE ext_channel in ({})'.format(
                    ','.join('\'{}\''.format(partner) for partner in get_df_ready_partners(self.query_db))),
                db_client=self.campaign_db_client)
        }

    def load_df_adgroup_trees(self):
        """Loads the current deep funnel adgroup trees from MySQL table """
        self.df_adgroup_trees = {}
        self.registered_cids = set()
        query = 'SELECT * FROM {}'.format(self.df_adgroup_trees_table)
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.df_db_client)

        for row in result:
            self.df_adgroup_trees[row['root_cid']] = {
                'dup_campaign_cids': json.loads(row['dup_campaign_cids']) if row['dup_campaign_cids'] else []
            }
            self.registered_cids |= self.get_tree_cids_set(row['root_cid'])

        logger.info('%s root cids loaded, %s cids registered', len(self.df_adgroup_trees), len(self.registered_cids))

    def get_tree_cids_set(self, root_cid):
        """Gets the cids set in a tree
        :param root_cid:
        :return cids: set of cids
        """
        cids = set()
        for field in ('campaign_cid', 'blacklist_cid', 'global_cid',
                      'stable_campaign_cids', 'stable_global_cids', 'garbage_cids'):
            value = self.df_adgroup_trees[root_cid][field]
            if not value:
                continue
            if isinstance(value, list):
                cids |= set(value)
            elif value:
                cids.add(value)

        return cids

    def get_tree_cids_label(self, root_cid):
        """Gets cid to it's hydra label from a tree
        :param root_cid:
        :return cids_label: dict of cid to hydra label
        """
        cids_label = {}
        for field in ('campaign_cid', 'blacklist_cid', 'global_cid',
                      'stable_campaign_cids', 'stable_global_cids', 'garbage_cids'):
            value = self.df_adgroup_trees[root_cid][field]
            if not value or value in (CID_PENDING, CID_FAILURE):
                continue
            if field == 'campaign_cid':
                label = 'White OID'
            elif field == 'blacklist_cid':
                label = 'Black Sampling'
            elif field == 'global_cid' and value in self.cid2partner:
                is_blacklist = AppUsage.get_pub_sub_usage(
                    self.partner_capabilities[self.cid2partner[value]]).is_black_usage
                label = 'Black Sampling' \
                    if is_blacklist or self.df_adgroup_trees[root_cid]['is_transparent'] else 'White Sampling'
            elif field in ('stable_campaign_cids', 'stable_global_cids'):
                label = 'White Stable'
            else:
                label = 'Garbage'

            if isinstance(value, list):
                cids_label.update(
                    {cid: label for cid in value
                     if cid not in (CID_PENDING, CID_FAILURE) and cid in self.cid2partner}
                )
            elif value in self.cid2partner:
                cids_label[value] = label

        return cids_label

    def get_hydra_sm_adgroup_name(self, root_cid, app_usage, is_transparent, cid_config=None):
        if cid_config is None:
            cid_config = self.idash_endpoint.read_campaign_fields(root_cid, ['name'])
        cid_name = get_hydra_raw_adgroup_name(cid_config['name'])
        if is_transparent:
            return DeepFunnelAdgroupTree.TRANSPARENT_ADGROUP_NAME.format(cid_name)
        elif app_usage.is_black_usage:
            return DeepFunnelAdgroupTree.BLACKLIST_ADGROUP_NAME.format(cid_name)
        return DeepFunnelAdgroupTree.SAMPLING_ADGROUP_NAME.format(cid_name)

    def register_root_cid(self, root_cid):
        """Registers a new tree
        :param root_cid:
        """
        if root_cid in self.df_adgroup_trees:
            logger.warning('already registered root cid: %s', root_cid)
            return False
        else:
            try:
                app_usage = AppUsage.get_pub_sub_usage(self.partner_capabilities[self.cid2partner[root_cid]])
            except Exception as exception:
                logger.error('register root cid %s failed for partner %s: %s',
                             root_cid, self.cid2partner[root_cid], exception)
                self.idash_endpoint.send_cid_enable_request(root_cid, self.cid2partner[root_cid], False)
                return False

            query_fields = ['created_by', 'created_at', 'copy_from', 'cid', 'name',
                            'enabled', 'traffic_name_transparency']
            cid_config = self.idash_endpoint.read_campaign_fields(root_cid, query_fields)
            if not cid_config or (cid_config['copy_from'] != ''
                                  and cid_config['created_by'] == self.idash_endpoint.idash_cfg['username']):
                new_name = '_' + DeepFunnelAdgroupTree.COPY_FAILED_ADGROUP_NAME.format(
                    get_hydra_raw_adgroup_name(cid_config['name']))
                logger.error('register root cid %s failed for self-created cid, %s', root_cid, new_name)
                self.idash_endpoint.send_cid_enable_request(root_cid, self.cid2partner[root_cid], False)
                self.idash_endpoint.send_update_data_request(root_cid, {'optimization_model': '', 'name': new_name})
                return False

            is_transparent = int(cid_config.get('traffic_name_transparency') or 0)
            query = "INSERT INTO {0} (root_cid, global_cid, is_transparent) VALUES ('{1}', '{1}', '{2}')".format(
                self.df_adgroup_trees_table, root_cid, is_transparent)
            logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)
            new_name = self.get_hydra_sm_adgroup_name(root_cid, app_usage, is_transparent, cid_config=cid_config)
            self.idash_endpoint.send_update_data_request(root_cid, {'name': new_name})
            self.clear_cm_lists(root_cid)

            # re-open new registered root_cid when
            # 1. it was closed by Hydra
            # 2. Created after 2019/12/02 and not closed by any user (AI-8047)
            if cid_config.get('enabled') == 'false':
                result = self.idash_endpoint.get_campaign_last_action_info(root_cid, 'enabled')
                user = result['updated_by'] if result and result['to'] == 'false' else None
                if user and user == self.idash_endpoint.idash_cfg['username']:
                    self.new_opened_root_cids.add(root_cid)
                if not user and int(cid_config.get('created_at', 0)) > 1575216000:
                    self.new_opened_root_cids.add(root_cid)

            return True

    def clear_cm_lists(self, cid):
        """Clear CM's black/white lists
        :param cid:
        """
        cm_lists = self.idash_endpoint.read_campaign_fields(
            cid, fields=['require_publisher', 'require_sub_publisher', 'reject_publisher', 'reject_sub_publisher'])
        if cm_lists.get('require_publisher') or cm_lists.get('require_sub_publisher') \
                or cm_lists.get('reject_publisher') or cm_lists.get('reject_sub_publisher'):
            logger.info('clean cm list: %s', json.dumps(cm_lists))
            data = {
                'require_publisher': [],
                'require_sub_publisher': [],
                'reject_publisher': [],
                'reject_sub_publisher': []
            }
            self.idash_endpoint.send_update_data_request(cid, data)
            self.idash_endpoint.deploy_campaign(cid)

    def update_df_adgroup_trees(self):
        """Updates the deep funnel adgroup trees to MySQL table from iDash job queue status,
           enable sucessful created sampling adgroups, then load the newest adgroup trees
        """
        self.load_df_adgroup_trees()
        self.warning_messages = defaultdict(list)

        auto_bl_root_cids = self.get_auto_bl_root_cids()
        self.polling_jobs_status()
        results = self.get_jobs_result()
        current_datetime = datetime.datetime.utcnow()
        update_root_cids = set()
        processesed_job_ids = []
        for row in results:
            if not row['command'] or not row['application_params']:
                logger.warning('unknown job %s', row)
                continue
            command = json.loads(row['command'])
            origin_cid = command['cid']
            application_params = json.loads(row['application_params'])
            root_cid = application_params['root_cid']

            if row['job_status'] == 'success':
                # campaign whitelist cid to sampling whitelist cid
                result = json.loads(row['result'])
                new_cid = result['data']['cid']

                config = self.idash_endpoint.read_campaign_fields(
                    new_cid, ('campaign_other_type', 'daily_max_budget', 'ext_bid_price'))
                copy_type = application_params['copy_type']
                if copy_type in ('stable_campaign_white', 'stable_sampling_white', 'stable_sampling_black'):
                    origin_price = int(float(config['ext_bid_price']))
                    new_bid_price = self._get_stable_adgroup_new_price(origin_cid, origin_price)
                    if new_bid_price != origin_price:
                        new_budget = max(int(config['daily_max_budget']), new_bid_price * 50)
                        logger.info('increase bid price from %s to %s', config['ext_bid_price'], new_bid_price)
                        self.idash_endpoint.send_update_data_request(
                            origin_cid, {'ext_bid_price': str(new_bid_price), 'daily_max_budget': str(new_budget)})

                # enable adgroups except `blacklist_cid`
                if copy_type != 'add_blacklist_field' or root_cid in auto_bl_root_cids:
                    self.idash_endpoint.send_cid_enable_request(new_cid, config['campaign_other_type'], True)
                processesed_job_ids.append(row['job_id'])
            else:
                message = None
                retry = False
                if row['job_status'] != 'pending':
                    new_cid = CID_FAILURE
                    message = self.STATUS_FAILURE
                    retry = True
                else:
                    pending_ts = (current_datetime - row['created_at']).total_seconds()
                    new_cid = CID_PENDING
                    if pending_ts > 3600 * 6:
                        message = self.STATUS_PENDING_TOO_LONG
                        self.warning_messages[self.STATUS_PENDING_TOO_LONG].append(origin_cid)
                        retry = True
                    elif pending_ts > 3600:
                        message = self.STATUS_PENDING_LONG

                if message:
                    self.warning_messages[message].append(origin_cid)

                if retry:  # let hydra can retry the copy event
                    processesed_job_ids.append(row['job_id'])
                    try:
                        # if a cid is already created, rename and throw it to garbage cids
                        copy_failed_cid = json.loads(row['result'])['new_cid']
                        if copy_failed_cid:
                            self.df_adgroup_trees[root_cid]['garbage_cids'].append(copy_failed_cid)
                            origin_name = self.idash_endpoint.read_campaign_fields(copy_failed_cid, ['name'])['name']
                            cid_name = get_hydra_raw_adgroup_name(origin_name)
                            new_name = DeepFunnelAdgroupTree.COPY_FAILED_ADGROUP_NAME.format(cid_name)
                            self.idash_endpoint.send_update_data_request(copy_failed_cid, {'name': new_name})
                    except Exception as exception:
                        logger.warning('retry job_id %s without failed cid: %s', row['job_id'], exception)

                    # resumit copy job for `stable_sampling_white`, `stable_campaign_white` and `stable_sampling_black`
                    # since hydra won't trigger these events again
                    if application_params['copy_type'] in ('stable_sampling_white',
                                                           'stable_campaign_white',
                                                           'stable_sampling_black'):
                        job_id = self.idash_endpoint.copy_adgroup(
                            origin_cid, command['params']['begin_at'], command['params']['end_at'],
                            command['params']['name'])
                        self.record_job_id(job_id, application_params=row['application_params'])
                    elif application_params['copy_type'] == 'campaign_whitelist':
                        # recover HydraSM to it's name
                        app_usage = AppUsage.get_pub_sub_usage(self.partner_capabilities[self.cid2partner[origin_cid]])
                        new_name = self.get_hydra_sm_adgroup_name(origin_cid, app_usage, is_transparent=False)
                        self.idash_endpoint.send_update_data_request(origin_cid, {'name': new_name})
                else:  # keep hydra waiting for the copy event
                    self.df_adgroup_trees[root_cid]['pending_tasks'].append(application_params['copy_type'])
                update_root_cids.add(root_cid)

            update_root_cid = self._update_df_adgroup_tree(application_params, origin_cid, new_cid)
            if update_root_cid:
                update_root_cids.add(update_root_cid)

        if processesed_job_ids:
            self.update_application_status(processesed_job_ids, 'checked')
        self._update_df_adgroup_trees_table(update_root_cids)

    def _get_stable_adgroup_new_price(self, cid, origin_price):
        """Gets new price of stable cid AI-4954
        :param cid:
        :param origin_price:
        :return new_price:
        """
        query = """
        SELECT oid, rebate, goalcpa
        FROM order_info
        WHERE oid=(SELECT oid FROM campaign WHERE cid='{}')
        """.format(cid)
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)[0]
        new_price = min(1.1 * origin_price,
                        origin_price + 0.5 * USD_TO_APPIER,
                        (0.8 - result['rebate']) * result['goalcpa'])
        return max(int(new_price), origin_price)

    def _update_df_adgroup_tree(self, params, origin_cid, new_cid):
        """Updates an adgroup tree from a job queue result
        :param params: dict contains copy_type and root_cid
        :param origin_cid:
        :param new_cid:
        :return update_root_cid: root_cid need to be updated, None when already updated to MySQL table
        """
        copy_type = params['copy_type']
        root_cid = params['root_cid']

        new_tree_data = {}
        if new_cid not in (CID_PENDING, CID_FAILURE) or copy_type in ('stable_campaign_white', 'stable_sampling_white'):
            if copy_type == 'campaign_whitelist':
                new_tree_data = {
                    'campaign_cid': origin_cid,
                    'global_cid': new_cid
                }
            elif copy_type == 'stable_campaign_white':
                new_tree_data = {
                    'campaign_cid': new_cid,
                    'stable_campaign_cids': list(set(self.df_adgroup_trees[root_cid]['stable_campaign_cids'])
                                                 | set([origin_cid]))
                }
            elif copy_type == 'stable_sampling_white':
                new_tree_data = {
                    'global_cid': new_cid,
                    'stable_global_cids': list(set(self.df_adgroup_trees[root_cid]['stable_global_cids'])
                                               | set([origin_cid]))
                }
            elif copy_type == 'stable_sampling_black':
                new_tree_data = {
                    'blacklist_cid': new_cid,
                    'stable_global_cids': list(set(self.df_adgroup_trees[root_cid]['stable_global_cids'])
                                               | set([origin_cid]))
                }
            elif copy_type == 'add_blacklist_field':
                new_tree_data = {
                    'blacklist_cid': new_cid
                }
            elif copy_type == 'reconstruct_campaign_white':
                new_tree_data = {
                    'campaign_cid': new_cid,
                    'garbage_cids': list(set(self.df_adgroup_trees[root_cid]['garbage_cids']) | set([origin_cid]))
                }
            elif copy_type == 'reconstruct_sampling_white':
                new_tree_data = {
                    'global_cid': new_cid,
                    'garbage_cids': list(set(self.df_adgroup_trees[root_cid]['garbage_cids']) | set([origin_cid]))
                }
            elif copy_type == 'reconstruct_global_cid':
                new_tree_data = {'global_cid': new_cid}

        update_root_cid = None
        for key, value in new_tree_data.items():
            if isinstance(value, list) and set(value) != set(self.df_adgroup_trees[root_cid][key]):
                update_root_cid = root_cid
                self.df_adgroup_trees[root_cid][key] = value
            elif not isinstance(value, list) and value != self.df_adgroup_trees[root_cid][key]:
                update_root_cid = root_cid
                self.df_adgroup_trees[root_cid][key] = value

        return update_root_cid

    def _update_df_adgroup_trees_table(self, update_root_cids):
        """Updates a batch root_cids into MySQL table
        :param update_root_cids:
        """
        for root_cid in update_root_cids:
            query = """
            UPDATE {}
            SET
                campaign_cid = '{}',
                blacklist_cid = '{}',
                global_cid = '{}',
                stable_campaign_cids = '{}',
                stable_global_cids = '{}',
                garbage_cids = '{}',
                pending_tasks = '{}'
            WHERE
                root_cid = '{}'
            """.format(self.df_adgroup_trees_table,
                       self.df_adgroup_trees[root_cid]['campaign_cid'],
                       self.df_adgroup_trees[root_cid]['blacklist_cid'],
                       self.df_adgroup_trees[root_cid]['global_cid'],
                       json.dumps(self.df_adgroup_trees[root_cid]['stable_campaign_cids']),
                       json.dumps(self.df_adgroup_trees[root_cid]['stable_global_cids']),
                       json.dumps(self.df_adgroup_trees[root_cid]['garbage_cids']),
                       json.dumps(self.df_adgroup_trees[root_cid]['pending_tasks']),
                       root_cid)
            logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)

    def check_warning_messages(self):
        """Checks idash job queue status, raise Exception when job failed or pending more than half day """
        if self.warning_messages[self.STATUS_PENDING_LONG]:
            logger.warning('%s cids: %s', self.STATUS_PENDING_LONG, self.warning_messages[self.STATUS_PENDING_LONG])
        elif self.warning_messages[self.STATUS_PENDING_TOO_LONG] or self.warning_messages[self.STATUS_FAILURE]:
            raise RuntimeError('{} cids: {}, {} cids: {}'.format(
                self.STATUS_PENDING_TOO_LONG, self.warning_messages[self.STATUS_PENDING_TOO_LONG],
                self.STATUS_FAILURE, self.warning_messages[self.STATUS_FAILURE]))

    def get_registered_cids(self):
        """Gets registered cids in deep funnel adgroup trees
        :return registered_cids: set of cids
        """
        return self.registered_cids

    def get_df_adgroup_trees(self):
        """Gets the deep funnel adgroup trees
        :return df_adgroup_trees: dict of root_cid to it's adgroup leaves
        """
        if not self.df_adgroup_trees:
            self.load_df_adgroup_trees()
        return self.df_adgroup_trees

    def register_new_df_cids(self):
        """Registers new deep funnel cids to root_cid """
        self.load_df_adgroup_trees()

        new_root_cids = []
        failed_root_cids = []
        for cid in self.df_adgroups.get_active_df_adgroups():
            if cid not in self.registered_cids:
                success = self.register_root_cid(cid)
                if success:
                    new_root_cids.append(cid)
                else:
                    failed_root_cids.append(cid)

        self.load_df_adgroup_trees()
        return new_root_cids, failed_root_cids

    def get_active_root_cids_status(self):
        """Gets the cids status for active root cids
        :return active_root_cids_status: dict of root_cid to cid_status,
            cid_status is dict of cid to it's `cid_enabled`, `is_deep_funnel`, and `deep_funnel_enabled`
        """
        active_root_cids_status = {}
        for root_cid in self.df_adgroup_trees:
            cids = self.get_tree_cids_set(root_cid)
            cids_status = {cid: self.df_adgroups.get_cid_status(cid) for cid in cids}
            if not all([cids_status[cid]['cid_enabled'] is None for cid in cids_status]):
                active_root_cids_status[root_cid] = cids_status

        return active_root_cids_status

    def get_root_cids(self):
        """Gets list of root cids
        :return root_cids: list of root_cid
        """
        return list(self.df_adgroup_trees.keys())

    def get_auto_bl_root_cids(self):
        query = """
        SELECT
            result, application_params
        FROM
            idash_job_queue_botpoc
        WHERE
            job_status = 'success'
            and application = 'hydra-bot-poc'
            and application_status = 'checked'
            and created_at >= '{}'
        """.format((datetime.datetime.utcnow() - datetime.timedelta(hours=48)).strftime('%Y-%m-%d %H:%M:%S'))
        logger.info(query)
        auto_bl_root_cids = set()
        for row in self.query_db.get_query_result(query, db_client=self.df_db_client):
            try:
                cid = json.loads(row['result'])['data']['ad_groups'][0]['cid']
            except Exception as exception:
                logger.error('%s: %s', exception, row)
                continue
            if json.loads(row['application_params']).get('auto_hydra_bl') == 'true':
                auto_bl_root_cids.add(cid)

        return auto_bl_root_cids

    def get_new_opened_root_cids(self):
        return self.new_opened_root_cids


class DeepFunnelAdgroupTree(IdashJobQueue):
    """Class to interact with a DF adgroup tree """
    SAMPLING_ADGROUP_NAME = u'[Hydra_SM]_{}'
    CAMPAIGN_ADGROUP_NAME = u'[Hydra_OID]_{}'
    BLACKLIST_ADGROUP_NAME = u'[Hydra_BL]_{}'
    TRANSPARENT_ADGROUP_NAME = u'[Hydra_BL_trans]_{}'
    STABLE_ADGROUP_NAME = u'[Hydra_WL_{}]_{}'
    GARBAGE_ADGROUP_NAME = u'[Hydra_X_{}]_{}'
    COPY_FAILED_ADGROUP_NAME = u'[Hydra_F]_{}'
    CELL_ADGROUP_NAME = '[Cell]_{}'
    AlPACA_ADGROUP_NAME = '[Alpaca]_{}'
    LOBSTER_ADGROUP_NAME = '[Lobster]_{}'

    def __init__(self, listctrl_cfg, idash_endpoint, query_db, root_cid, adgroup_tree, cids_status, data_reference,
                 is_new_opened_root_cid):
        super(DeepFunnelAdgroupTree, self).__init__(
            idash_endpoint, query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL],
            listctrl_cfg['idash_job_queue_table'], 'deep_funnel_adgroup_trees')
        self.df_adgroup_trees_table = listctrl_cfg['df_adgroup_trees_table']
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.root_cid = root_cid
        self.adgroup_tree = adgroup_tree
        self.cids_status = cids_status
        self.oid_data_reference = []#data_reference['oid']
        self.partner_source_list = []#get_partner_source_list(data_reference)
        self.is_new_opened_root_cid = is_new_opened_root_cid
        self.cids_config = self.get_cids_config()

    @staticmethod
    def get_cid_index_str(cid):
        return cid[:3]

    def recover_garbage_cid(self, cid, turn_on, debug=False):
        if cid not in self.adgroup_tree['garbage_cids']:
            return
        self.adgroup_tree['garbage_cids'].remove(cid)
        self.adgroup_tree['stable_campaign_cids'].append(cid)
        query = 'UPDATE {} SET stable_campaign_cids=\'{}\', garbage_cids=\'{}\' WHERE root_cid=\'{}\''.format(
            self.df_adgroup_trees_table, json.dumps(self.adgroup_tree['stable_campaign_cids']),
            json.dumps(self.adgroup_tree['garbage_cids']), self.root_cid)
        logger.info(query)
        if not debug:
            self.query_db.execute_query(query, db_client=self.df_db_client)
        origin_name = self.idash_endpoint.read_campaign_fields(cid, ['name'])['name']
        cid_name = get_hydra_raw_adgroup_name(origin_name)
        new_name = self.STABLE_ADGROUP_NAME.format(DeepFunnelAdgroupTree.get_cid_index_str(cid), cid_name)
        logger.info('change cid %s name from %s to %s', cid, origin_name, new_name)
        if not debug:
            self.idash_endpoint.send_update_data_request(cid, {'name': new_name})
        if turn_on and not debug:
            self.idash_endpoint.send_cid_enable_request(cid, self.cids_config[cid]['ext_channel'], True)

    def recycle_stable_cid(self, cid):
        if cid in self.adgroup_tree['stable_campaign_cids']:
            self.adgroup_tree['stable_campaign_cids'].remove(cid)
            self.adgroup_tree['garbage_cids'].append(cid)
            query = 'UPDATE {} SET stable_campaign_cids=\'{}\', garbage_cids=\'{}\' WHERE root_cid=\'{}\''.format(
                self.df_adgroup_trees_table, json.dumps(self.adgroup_tree['stable_campaign_cids']),
                json.dumps(self.adgroup_tree['garbage_cids']), self.root_cid)
        elif cid in self.adgroup_tree['stable_global_cids']:
            self.adgroup_tree['stable_global_cids'].remove(cid)
            self.adgroup_tree['garbage_cids'].append(cid)
            query = 'UPDATE {} SET stable_global_cids=\'{}\', garbage_cids=\'{}\' WHERE root_cid=\'{}\''.format(
                self.df_adgroup_trees_table, json.dumps(self.adgroup_tree['stable_global_cids']),
                json.dumps(self.adgroup_tree['garbage_cids']), self.root_cid)
        else:
            return

        logger.info(query)
        self.query_db.execute_query(query, db_client=self.df_db_client)
        cid_name = get_hydra_raw_adgroup_name(self.idash_endpoint.read_campaign_fields(cid, ['name'])['name'])
        new_name = self.GARBAGE_ADGROUP_NAME.format(DeepFunnelAdgroupTree.get_cid_index_str(cid), cid_name)
        self.idash_endpoint.send_update_data_request(cid, {'name': new_name})
        self.idash_endpoint.send_cid_enable_request(cid, self.cids_config[cid]['ext_channel'], False)

    def get_cids_config(self):
        """Gets the cids campaign config for all adgroups in the tree
        :return cids_config: dict of cid to (field, value) dict
        """
        cids_config = {}
        query = """
        SELECT
            cid, begin_at, end_at, ext_channel, oid, country, top_category, sub_category,
            name AS cid_name, optimization_model
        FROM
            campaign
        WHERE
            cid IN ({})
        """.format(','.join(['\'{}\''.format(cid) for cid in self.cids_status]))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)

        uncached_cids = list(set(cid for cid in self.cids_status) - set(row['cid'] for row in result))
        if uncached_cids:
            logger.warning('querying idash for uncached_cids: %s', uncached_cids)
            uncached_result = self.idash_endpoint.read_campaigns_by_cids(
                uncached_cids, fields=['cid', 'campaign_other_type', 'begin_at', 'end_at',
                                       'oid', 'name', 'optimization_model'])
            for row in uncached_result:
                row['ext_channel'] = row['campaign_other_type']
                row['cid_name'] = row['name']
                result.append(row)

        oid_partner = set()
        for row in result:
            top_category = row.get('top_category', '')  # not in campaign table
            sub_category = row.get('sub_category', '')
            category = (top_category + '_' + sub_category) if sub_category else top_category
            cids_config[row['cid']] = {
                'cid_name': row['cid_name'],
                'begin_at': int(row['begin_at']),
                'end_at': int(row['end_at']),
                'ext_channel': row['ext_channel'],
                'oid': row['oid'],
                'category': category,
                'top_category': top_category,
                'country': row.get('country', ''),
                'optimization_model': row['optimization_model']
            }
            oid_partner.add((row['oid'], row['ext_channel']))
        assert len(oid_partner) == 1

        (oid, partner) = list(oid_partner)[0]
        oids = [oid]
        if self.oid_data_reference and oid in self.oid_data_reference:
            oids.extend(self.oid_data_reference[oid])
        target_partners = self.partner_source_list.get(partner, [partner])
        query = "SELECT DISTINCT cid FROM campaign WHERE oid IN ({}) AND ext_channel IN ({})".format(
            in_scope(oids), in_scope(target_partners))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        partner_cids = [row['cid'] for row in result]

        for cid, config in cids_config.items():
            config['partner_cids'] = partner_cids

        query = """
        SELECT
            goalcpa, primary_goal, vertical, name AS oid_name
        FROM
            order_info
        WHERE
            oid = '{}'
        """.format(oid)
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)[0]
        goalcpa = APPIER_TO_USD * result['goalcpa']
        for cid in cids_config:
            cids_config[cid]['goalcpa'] = goalcpa
            cids_config[cid]['primary_goal'] = result['primary_goal']
            cids_config[cid]['vertical'] = result['vertical']
            cids_config[cid]['oid_name'] = result['oid_name']

        return cids_config

    def get_unstable_df_cids(self):
        """Gets the unstabled campaign_cid, blacklist_cid, and global_cid
        :return campaign_cid:
        :return blacklist_cid:
        :return global_cid:
        """
        return self.adgroup_tree['campaign_cid'], self.adgroup_tree['blacklist_cid'], self.adgroup_tree['global_cid']

    def get_stabled_df_cids(self):
        """Gets the stabled df cids
        :return cids: list of cids
        """
        return [cid for cid in self.adgroup_tree['stable_campaign_cids'] + self.adgroup_tree['stable_global_cids']
                if self.able_to_update(cid)]

    def get_garbage_df_cids(self):
        return self.adgroup_tree['garbage_cids']

    def get_idash_pending_tasks(self):
        """Gets idash pending task names in job queue
        :return pending_tasks: list of pending task names
        """
        return self.adgroup_tree['pending_tasks']

    def is_stable_cid(self, cid):
        """Whether a cid is a stabel adgroup
        :param cid:
        :return is_stable_cid: <boolean>
        """
        return cid in self.adgroup_tree['stable_campaign_cids'] or cid in self.adgroup_tree['stable_global_cids']

    def is_transparent(self):
        return self.adgroup_tree['is_transparent']

    @staticmethod
    def is_valid_cid(cid):
        """Whether a cid valid to control
        :param cid:
        :return is_valid: <boolean>
        """
        return cid and cid not in (CID_PENDING, CID_FAILURE)

    def able_to_update(self, cid):
        """Whether a cid need to update it's whitelist or not
        :param cid:
        :return do_update: <boolean>
        """
        return self.is_valid_cid(cid) and self.cids_status[cid]['is_deep_funnel'] \
            and (self.cids_status[cid]['cid_enabled'] == 1
                 or self.cids_status[cid]['deep_funnel_enabled'] == 0
                 or self.is_new_opened_cid(cid))

    def is_new_opened_cid(self, cid):
        return cid == self.root_cid and self.is_new_opened_root_cid


    def trigger_copy_event(self, copy_type):
        """Triggers an adgroup copying event, send a job to idash job queue
        :param copy_type:
        """
        target_name = self._get_adgroup_copying_params(copy_type)

        job_id = self.idash_endpoint.copy_adgroup(
            self.root_cid, self.cids_config[self.root_cid]['begin_at'], self.cids_config[self.root_cid]['end_at'], target_name)
        self.record_job_id(job_id, application_params=json.dumps({'copy_type': copy_type, 'root_cid': self.root_cid}))

    # def trigger_copy_event_oo(self, copy_type):
    #     """Triggers an adgroup copying event, send a job to idash job queue
    #     :param copy_type:
    #     """
    #     source_cid, source_name, target_name = self._get_adgroup_copying_params(copy_type)
    #     if source_name:
    #         self.idash_endpoint.send_update_data_request(source_cid, {'name': source_name})
    #
    #     job_id = self.idash_endpoint.copy_adgroup(
    #         source_cid, self.cids_config[source_cid]['begin_at'], self.cids_config[source_cid]['end_at'], target_name)
    #     self.record_job_id(job_id, application_params=json.dumps({'copy_type': copy_type, 'root_cid': self.root_cid}))

    def _get_adgroup_copying_params(self, copy_type):

        root_cid_name = get_hydra_raw_adgroup_name(
            self.idash_endpoint.read_campaign_fields(self.root_cid, ['name'])['name'])
        if copy_type == 'cell':
            target_name = self.CELL_ADGROUP_NAME.format(root_cid_name)
        elif copy_type == 'alpaca':
            target_name = self.AlPACA_ADGROUP_NAME.format(root_cid_name)
        elif copy_type == 'lobster':
            target_name = self.LOBSTER_ADGROUP_NAME.format(root_cid_name)

        return target_name

    # def _get_adgroup_copying_params_oo(self, copy_type):
    #     """Gets the paramters for adgroup copying event
    #     :param copy_type:
    #     :return source_cid: source adgroup to copy
    #     :return source_name: source adgroup's new name
    #     :return target_name: target adgroup's new name
    #     """
    #     if copy_type in ('campaign_whitelist', 'stable_sampling_white', 'add_blacklist_field',
    #                      'reconstruct_sampling_white', 'reconstruct_global_cid'):
    #         source_cid = self.adgroup_tree['global_cid']
    #     elif copy_type in ('stable_campaign_white', 'reconstruct_campaign_white'):
    #         source_cid = self.adgroup_tree['campaign_cid']
    #     elif copy_type in ('stable_sampling_black'):
    #         source_cid = self.adgroup_tree['blacklist_cid']
    #
    #     root_cid_name = get_hydra_raw_adgroup_name(
    #         self.idash_endpoint.read_campaign_fields(source_cid, ['name'])['name'])
    #     cid_index_str = DeepFunnelAdgroupTree.get_cid_index_str(source_cid)
    #
    #     if copy_type == 'campaign_whitelist':
    #         source_name = self.CAMPAIGN_ADGROUP_NAME.format(root_cid_name)
    #         target_name = self.SAMPLING_ADGROUP_NAME.format(root_cid_name)
    #     elif copy_type == 'stable_campaign_white':
    #         source_name = self.STABLE_ADGROUP_NAME.format(cid_index_str, root_cid_name)
    #         target_name = self.CAMPAIGN_ADGROUP_NAME.format(root_cid_name)
    #     elif copy_type == 'stable_sampling_white':
    #         source_name = self.STABLE_ADGROUP_NAME.format(cid_index_str, root_cid_name)
    #         target_name = self.SAMPLING_ADGROUP_NAME.format(root_cid_name)
    #     elif copy_type == 'stable_sampling_black':
    #         source_name = self.STABLE_ADGROUP_NAME.format(cid_index_str, root_cid_name)
    #         target_name = self.BLACKLIST_ADGROUP_NAME.format(root_cid_name)
    #     elif copy_type == 'add_blacklist_field':
    #         source_name = None
    #         target_name = self.BLACKLIST_ADGROUP_NAME.format(root_cid_name)
    #     elif copy_type == 'reconstruct_campaign_white':
    #         source_name = self.GARBAGE_ADGROUP_NAME.format(cid_index_str, root_cid_name)
    #         target_name = self.CAMPAIGN_ADGROUP_NAME.format(root_cid_name)
    #     elif copy_type == 'reconstruct_sampling_white':
    #         source_name = self.GARBAGE_ADGROUP_NAME.format(cid_index_str, root_cid_name)
    #         target_name = self.SAMPLING_ADGROUP_NAME.format(root_cid_name)
    #     elif copy_type == 'reconstruct_global_cid':
    #         source_name = None
    #         target_name = self.SAMPLING_ADGROUP_NAME.format(root_cid_name)
    #
    #     return source_cid, source_name, target_name


class DeepFunnelAdgroups(object):
    """Class for store adgroup status for DF

    Usage:
        * calls get_active_df_adgroups() to get all DF cids need to be updated
        * call get_cid_status() to retrive an adgroup's status
            `cid_enabled`: <int> `campaign`.enabled, default None
            `is_deep_funnel`: <boolean> optimizer option is DF or not
            `deep_funnel_enabled`: <int> enabled flag from DF auto switch, `deep_funnel_adgroup_status`, default None
    """
    def __init__(self, listctrl_cfg, idash_endpoint, query_db):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.cid_status_table = listctrl_cfg['df_cid_status_table']
        self.cache_data = {}
        self.load_df_adgroup_status()

    def get_cid_status(self, cid):
        """Gets a cid's adgroup status
        :param cid:
        :return adgroup_status: dict contains `cid_enabled`, `is_deep_funnel`, `deep_funnel_enabled`
        """
        return {
            'cid_enabled': self.cache_data['cid_enabled_status'].get(cid, None),
            'is_deep_funnel': self.cache_data['cid_model_status'].get(cid) in HYDRA_MODELS,
            'deep_funnel_enabled': self.cache_data['df_enabled_status'].get(cid, None)
        }

    def load_df_adgroup_status(self):
        """Loads adgroup status for all DF adgroups """
        # get cid enabled and model status for DF partners on unfinished campaigns
        query = """
        SELECT
            a.cid, a.enabled, a.optimization_model, a.begin_at, a.end_at
        FROM
        (
            SELECT cid, enabled, oid, optimization_model, begin_at, end_at
            FROM campaign WHERE ext_channel IN ({})
        ) AS a
        LEFT OUTER JOIN
        (SELECT oid, status_code FROM order_info) AS b
        ON a.oid = b.oid
        WHERE b.status_code NOT IN ('finished', 'new')
        """.format(','.join(['\'{}\''.format(partner) for partner in get_df_ready_partners(self.query_db)]))
        logger.info(query)
        result = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        current_timestamp = int(time.time())
        self.cache_data['cid_enabled_status'] = {
            row['cid']: row['enabled'] if row['end_at'] > current_timestamp else 0 for row in result
        }
        self.cache_data['cid_model_status'] = {row['cid']: row['optimization_model'] for row in result}
        self.cache_data['cid_begin_at'] = {row['cid']: row['begin_at'] for row in result}
        finished_cid_set = set(row['cid'] for row in result if row['end_at'] <= current_timestamp)

        # read auto rule cids
        optimizer_auto_rule_cids = set([
            entry.get('cid', '') for entry in self.idash_endpoint.read_optimizer_ai_rule(enabled=True)
            if entry.get('rule_type') in (ENUM_AI_RULE_OPTION.AUTO, ENUM_AI_RULE_OPTION.BLACK,
                                          ENUM_AI_RULE_OPTION.WHITE)])
        for cid in optimizer_auto_rule_cids:
            if self.cache_data['cid_model_status'].get(cid) in HYDRA_MODELS:
                logger.warning('cid %s use auto rule and hydra', cid)
                self.idash_endpoint.save_optimizer_ai_rule(cid, {'enabled': False})

        # get disabled fox cids by AI-4653
        '''
        query = """
        SELECT
            b.cid, b.enabled
        FROM
            (SELECT cid, max(date) AS last_date FROM {0} GROUP BY cid) AS a,
            {0} AS b
        WHERE
            a.cid = b.cid
            AND a.last_date = b.date
        """.format(self.cid_status_table)
        logger.info(query)
        self.cache_data['df_enabled_status'] = {
            row['cid']: row['enabled'] for row in self.query_db.get_query_result(query, db_client=self.df_db_client)
            if row['cid'] not in finished_cid_set
        }
        '''

    def get_active_df_adgroups(self):
        """Gets the currently active DF cids
        :return df_cids: set of cids
        """
        # AI-5970 hydra cid created after 2019/02/18 will be registered, no matter it's enable status
        force_enabled_timestamp = 1550419200

        df_cids = set([cid for cid, enabled in self.cache_data['cid_enabled_status'].items()
                       if enabled == 1 or self.cache_data['cid_begin_at'][cid] >= force_enabled_timestamp])
        df_cids &= set([cid for cid, model in self.cache_data['cid_model_status'].items() if model in HYDRA_MODELS])
        df_cids |= set(self.cache_data['df_enabled_status'].keys())
        return df_cids
