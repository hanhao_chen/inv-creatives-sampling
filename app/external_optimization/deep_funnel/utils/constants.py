APPIER_TO_USD = 0.0000001
USD_TO_APPIER = 10000000.0

HYDRA_MODELS = ('hydra', 'hydra,squirrel', 'hydra-roas', 'hydra-roas,squirrel')
HYDRA_ROAS_MODELS = ('hydra-roas', 'hydra-roas,squirrel')

# AI-6630 default oid name for those coldstart oids; should be the same as in minor_goal_optimization
DEFAULT_PREFIX = 'DEFAULT_'
DEFAULT_CATEGORY = DEFAULT_PREFIX + 'CATEGORY_'
DEFAULT_COUNTRY = DEFAULT_PREFIX + 'COUNTRY'

# invalid/valid reasons for Hydra list control
INVALID_MULTIPLE_MAJORS = 'multiple_major_types'
INVALID_MAJOR_TYPE = 'major_type'
INVALID_MINOR_GOAL = 'minor_goal'
INVALID_EMPTY_LIST = 'empty_list'
INVALID_PARTNER = 'invalid_partner'
INVALID_GOAL_VALUE = 'invalid_goal_value'
INVALID_PRIMARY_GOAL = 'primary_goal is not cpa'
INVALID_VERTICAL = 'vertical is not app_install'
INVALID_NO_INSTALL_EVENT = 'no app_install event'
INVALID_CPA_PARTNER = 'partner_id does not support CPA'
VALID_REOPEN = 'cid has whitelist to buy'

HYDRA_CREDENTAIL_FILE = '/etc/external-optimization/hydra_credential.json'
