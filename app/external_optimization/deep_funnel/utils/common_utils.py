from __future__ import absolute_import
from __future__ import division
import datetime
import enum
import hashlib
import pytz
import traceback
import requests
from collections import namedtuple
from functools import wraps
import cProfile
import pstats

from app.utils.db import MySQL_DB_AI_HYDRA, MySQL_DB_AI_DEEP_FUNNEL
from app.module.CQueryDB import CQueryDB
from app.utils.init_logger import init_logger

logger = init_logger(__name__)
gmt0_timezone = pytz.timezone('Etc/GMT+0')
ExternalCapability = namedtuple('ExternalCapability', ['white_publisher', 'white_subpublisher',
                                                       'black_publisher', 'black_subpublisher'])


def send_post_request(url, data=None, headers=None, timeout=30, max_retry=3):
    for retry in range(max_retry):
        try:
            response = requests.post(url, data=data, timeout=timeout, headers=headers)
        except requests.exceptions.ReadTimeout as timeout_exception:
            logger.warning('timeout %s', retry)
            if retry == max_retry - 1:
                raise timeout_exception
            continue
        if response.status_code == 200:
            return response
        if response.status_code in range(400, 500):
            response.raise_for_status()
        logger.warning('status_code: %s', response.status_code)
        if retry == max_retry - 1:
            response.raise_for_status()

    return response


def send_get_request(url, params=None, headers=None, timeout=30, max_retry=3):
    for retry in range(max_retry):
        try:
            response = requests.get(url, params=params, timeout=timeout, headers=headers)
        except requests.exceptions.ReadTimeout as timeout_exception:
            logger.warning('timeout %s', retry)
            if retry == max_retry - 1:
                raise timeout_exception
            continue
        if response.status_code == 200:
            return response
        if response.status_code in range(400, 500):
            response.raise_for_status()
        logger.warning('status_code: %s', response.status_code)
        if retry == max_retry - 1:
            response.raise_for_status()

    return response


def patch_metric_row(row, goal_type, min_major_count, revenue_cpa):
    if revenue_cpa and goal_type.startswith('ROAS') and row.get('installs') and not row.get('major_count'):
        logger.warning('ROAS major_count missing: %s', row)
        row['major_count'] = row['installs'] * revenue_cpa

    if goal_type in ('ROAS', 'PRAT'):
        if float(row['minor_count']) > 0:
            if 'installs' in row and float(row['installs']) <= 0.0:
                row['installs'] = 1
            if float(row['major_count']) <= 0.0:
                row['major_count'] = min_major_count


def safe_div(a, b, default=0.0):
    return a / b if b else default


def string_hash(source, length=12):
    # str.encode(): Return an encoded version of the string as a bytes object.
    return hashlib.sha1(source.encode()).hexdigest()[:length]


def trace_multiprocess_exceptions(func):
    @wraps(func)
    def wrapped_func(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as exception:
            logger.exception('exception %s from func: %s, args: %s, kwargs: %s', exception, func.__name__, args, kwargs)
            if kwargs and kwargs.get('_raise_exception', False):
                raise RuntimeError(traceback.print_exc())
            else:
                traceback.print_exc()

    return wrapped_func


def time_profiler(func):
    @wraps(func)
    def wrapped_func(*args, **kwargs):
        try:
            pr = cProfile.Profile()
            pr.enable()
            result = func(*args, **kwargs)
            pr.disable()
            ps = pstats.Stats(pr).sort_stats('cumtime')
            ps.print_stats(500)
            return result
        except Exception as exception:
            logger.exception('exception %s from func: %s, args: %s, kwargs: %s',
                             exception, func.__name__, args, kwargs)
            traceback.print_exc()

    return wrapped_func


def in_scope(values):
    return ','.join(["'{}'".format(v) for v in values])


def get_external_capability(query_db):
    query = 'select * from {}'.format(CQueryDB.TABLE_HYDRA_EXT_CAPABILITY)

    rows = query_db.get_query_result(
        query, db_client=query_db.get_db_client_by_table(CQueryDB.TABLE_HYDRA_EXT_CAPABILITY))
    partner_capabilities = {}
    for row in rows:
        partner_id = row['partner_id']
        # FIXME: AI-5418 temporary hardcode external capability for appnext and newborntown
        if partner_id == 'appnext':
            row.update({'white_publisher': 1, 'white_subpublisher': 0, 'black_publisher': 1, 'black_subpublisher': 0})
        elif partner_id == 'newborntown':
            row.update({'white_publisher': 0, 'white_subpublisher': 0, 'black_publisher': 1, 'black_subpublisher': 1})

        capability = ExternalCapability(
            row['white_publisher'], row['white_subpublisher'], row['black_publisher'], row['black_subpublisher'])
        partner_capabilities[partner_id] = capability

    logger.debug(partner_capabilities)
    return partner_capabilities


def get_cpa_enabled_partner_ids(query_db):
    query = 'select partner_id from cpa_enabled_partners'
    return set([row['partner_id'] for row in query_db.get_query_result(
        query, db_client=query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL])])


def get_pub_sub_partners(partner_capabilities):
    # read pub/sub rules from capabilities
    subpublisher_partners = []
    publisher_partners = []
    for partner, external_usage in partner_capabilities.items():
        if external_usage.black_subpublisher or external_usage.white_subpublisher:
            subpublisher_partners.append(partner)
        if external_usage.black_publisher or external_usage.white_publisher:
            publisher_partners.append(partner)

    return publisher_partners, subpublisher_partners


def get_partner2split_by(idash_endpoint):
    return {
        partner: capability.get('split_by', None)
        for partner, capability in idash_endpoint.get_ext_capabilities().items()
    }


class AppUsage(enum.Enum):
    INVALID = (ExternalCapability(white_publisher=0, white_subpublisher=0, black_publisher=0, black_subpublisher=0), )
    SUB = (ExternalCapability(
        white_publisher=1, white_subpublisher=1, black_publisher=1, black_subpublisher=1), )
    SUB_BLACK_PUB = (ExternalCapability(  # dotc
        white_publisher=0, white_subpublisher=1, black_publisher=1, black_subpublisher=1), )
    SUB_ONLY = (ExternalCapability(  # appthis, batmobi, leadbolt_api, mobrand
        white_publisher=0, white_subpublisher=1, black_publisher=0, black_subpublisher=1), )
    BOTH = (ExternalCapability(  # personaly
        white_publisher=1, white_subpublisher=0, black_publisher=1, black_subpublisher=1), )
    PUB = (ExternalCapability(  # (empty)
        white_publisher=1, white_subpublisher=0, black_publisher=1, black_subpublisher=0), )
    BLACK_SUB = (ExternalCapability(  # newborntown, manhuaren, minimob, doglobal_api
        white_publisher=0, white_subpublisher=0, black_publisher=1, black_subpublisher=1), )
    BLACK_PUB = (ExternalCapability(  # appave, cavemusic, fyber, unity
        white_publisher=0, white_subpublisher=0, black_publisher=1, black_subpublisher=0), )
    BLACK_SUB_ONLY = (ExternalCapability(  # adsfast, mobair, mobisense, mobvault
        white_publisher=0, white_subpublisher=0, black_publisher=0, black_subpublisher=1), )

    def __init__(self, capability):
        self.capability = capability
        self.white_publisher = capability.white_publisher
        self.black_publisher = capability.black_publisher
        self.white_subpublisher = capability.white_subpublisher
        self.black_subpublisher = capability.black_subpublisher
        # derivative properties
        self.black_sub_writable = self.black_subpublisher
        self.white_sub_writable = self.white_subpublisher
        self.black_pub_writable = self.black_publisher
        self.white_pub_writable = self.white_publisher and not self.white_subpublisher
        # legacy: BLACK_SUB, BLACK_PUB, BLACK_SUB_ONLY
        self.is_black_usage = not self.white_subpublisher and not self.white_publisher
        # legacy: PUB, BLACK_PUB, BOTH
        self.needs_pub_performance = ((self.black_publisher and not self.black_subpublisher) or
                                      (self.white_publisher and not self.white_subpublisher))
        # legacy: SUB, BLACK_SUB, SUB_ONLY, BOTH, BLACK_SUB_ONLY
        self.needs_sub_performance = self.black_subpublisher or self.white_subpublisher

    @staticmethod
    def get_pub_sub_usage(external_usage):
        for app_usage in AppUsage:
            if app_usage.capability == external_usage:
                return app_usage
        raise RuntimeError('invalid external_usage {}'.format(external_usage))

    def __repr__(self):  # str(params)
        return self.name.lower()

    def __str__(self):  # json.dumps(params, default=str)
        return self.name.lower()


def get_enabled_oids_info(query_db, start_ts, end_ts, partners, oid_data_reference):
    query = """
    SELECT
        oid, cid, country, ext_channel, top_category, sub_category, enabled
    FROM
        {}
    WHERE
        end_at >= {}
        and begin_at <= {}
        and is_rtb = 0
        and type = 'cpa'
        and ext_channel in ({})
    GROUP BY
        oid, cid, country, ext_channel, top_category, sub_category, enabled
    """.format(CQueryDB.TABLE_CAMPAIGN, start_ts, end_ts, in_scope(partners))
    logger.info(query)
    reply = query_db.get_query_result(query, db_client=query_db.get_db_client_by_table(CQueryDB.TABLE_CAMPAIGN))
    active_oids = set()
    oid2country = {}
    oid2cids = {}
    oid2channels = {}
    oid2category = {}
    for r in reply:
        oid, cid, country, channel = r['oid'], r['cid'], r['country'], r['ext_channel']
        top_category, sub_category = r['top_category'], r['sub_category']
        enabled = r['enabled']

        if enabled == 1:
            active_oids.add(oid)

        if oid not in oid2cids:
            oid2cids[oid] = []
        if oid not in oid2channels:
            oid2channels[oid] = set()
        oid2country[oid] = country
        oid2cids[oid].append(cid)
        if channel not in oid2channels[oid]:
            oid2channels[oid].add(channel)
        oid2category[oid] = (top_category, sub_category)

    oid_info = {
        'active_oids': list(active_oids),
        'country': oid2country,
        'cids': oid2cids,
        'channels': oid2channels,
        'category': oid2category
    }

    patch_oids = []
    for oid, refer_oids in oid_data_reference.items():
        patch_oids.append(oid)
        patch_oids.extend(refer_oids)
    patch_oids = set(patch_oids) - set(oid_info['country'].keys())
    if patch_oids:
        query = 'select oid, country from order_info where oid in ({})'.format(
            ','.join('\'{}\''.format(oid) for oid in patch_oids))
        for row in query_db.get_query_result(query, db_client=query_db.get_db_client_by_table('order_info')):
            oid_info['country'][row['oid']] = row['country']

    return oid_info


def get_cid2channel(query_db):
    query = "SELECT cid, ext_channel as channel FROM {} GROUP BY cid, ext_channel".format(CQueryDB.TABLE_CAMPAIGN)
    logger.info(query)
    reply = query_db.get_query_result(query, db_client=query_db.get_db_client_by_table(CQueryDB.TABLE_CAMPAIGN))
    cid2channel = {}
    for r in reply:
        cid, channel = r['cid'], r['channel']
        cid2channel[cid] = channel

    return cid2channel


def get_partner_source_list(data_reference):
    # AI-7293: doglobal_api_wl = doglobal_api_wl + doglobal_api = doglobal_api_union
    # There is no doglobal_api_union in some pipelines, so we need to merge them manually.
    partner_source_list = {}
    for original_partner, new_partner in data_reference['partner_replaced_by'].items():
        source_list = partner_source_list.setdefault(original_partner, [])
        source_list.extend(data_reference['partner_merged_from'].get(new_partner, [original_partner]))
    return partner_source_list


def get_job_latest_datetime(query_db, job_name, target_datetime, tolerance=6):
    """Get job's latest date with tolerance.
    :param query_db:
    :param job_name:
    :param target_datetime: <datetime>
    :param tolerance: days for fallback
    :return date_obj: <datetime>
    :raise KeyError: if no record is found
    """
    date_start = (target_datetime - datetime.timedelta(days=tolerance)).strftime('%Y%m%d')
    date_end = target_datetime.strftime('%Y%m%d')
    query = """
        SELECT
            date
        FROM
            job_update_status
        WHERE
            job_name = '{}'
            AND date >= {}
            AND date <= {}
        ORDER BY
            date DESC
    """.format(job_name, date_start, date_end)
    logger.info(query)
    result = query_db.get_query_result(query, db_client=query_db.db_clients[MySQL_DB_AI_HYDRA])
    if not result:
        raise KeyError('No {} record in {} ~ {}.'.format(job_name, date_start, date_end))
    return result[0]['date']  # <datetime.date>


class AppList(object):

    def __init__(self, app_id_set, is_subpublisher, able_to_write=True):
        self.app_id_set = set(app_id_set)
        self.is_subpublisher = is_subpublisher
        self.able_to_write = able_to_write

    def clear(self, is_subpublisher=None):
        self.app_id_set = set()
        if is_subpublisher is not None:
            self.is_subpublisher = is_subpublisher

    def remove(self, app_id):
        self.app_id_set.remove(app_id)

    def get_app_id_list(self):
        return list(self.app_id_set) if self.able_to_write else []

    def get_app_id_set(self):
        return set(self.app_id_set) if self.able_to_write else set()

    def set_able_to_write(self, able_to_write):
        self.able_to_write = able_to_write

    def __bool__(self):
        return self.__len__()

    def __contains__(self, app_id):
        return app_id in self.app_id_set

    def __len__(self):
        return len(self.app_id_set)

    def __sub__(self, app_list):
        if app_list.is_subpublisher == self.is_subpublisher:
            return AppList(self.app_id_set - app_list.app_id_set, self.is_subpublisher, self.able_to_write)
        return AppList(self.app_id_set, self.is_subpublisher, self.able_to_write)

    def __isub__(self, app_list):
        if app_list.is_subpublisher == self.is_subpublisher:
            self.app_id_set -= app_list.app_id_set
        return self

    def __or__(self, app_list):
        if app_list.is_subpublisher == self.is_subpublisher:
            return AppList(self.app_id_set | app_list.app_id_set, self.is_subpublisher, self.able_to_write)
        return AppList(self.app_id_set, self.is_subpublisher, self.able_to_write)

    def __ior__(self, app_list):
        if app_list.is_subpublisher == self.is_subpublisher:
            self.app_id_set |= app_list.app_id_set
        return self

    def __and__(self, app_list):
        if app_list.is_subpublisher == self.is_subpublisher:
            return AppList(self.app_id_set & app_list.app_id_set, self.is_subpublisher, self.able_to_write)
        return AppList(self.app_id_set, self.is_subpublisher, self.able_to_write)

    def __iand__(self, app_list):
        if app_list.is_subpublisher == self.is_subpublisher:
            self.app_id_set &= app_list.app_id_set
        return self

    def __str__(self):
        return 'is_subpublisher: {}, app_id: {}'.format(self.is_subpublisher, list(self.app_id_set))
