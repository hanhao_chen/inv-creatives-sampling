"""Define classes for querying cid/oid major/minor goal settings. """
# TODO: unify cid/oid event setting queries under external_optimization/deep_funnel/
from collections import namedtuple

from app.utils.db import MySQL_DB_CAMPAIGN
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.constants import APPIER_TO_USD
from app.external_optimization.deep_funnel.utils.goal_transformer import CidTransformer, OidTransformer

logger = init_logger(__name__)


class GoalQueryAgent(object):
    """Provides utilities for processing optimization goal setting. """
    GOAL = namedtuple('goal', ['event_type', 'event_ids', 'goal_type', 'goal_value'])
    EVENT = namedtuple('event', ['event_type', 'event_id'])

    def __init__(self, query_db):
        self.query_db = query_db
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]

    @staticmethod
    def get_goal_value_in_usd(goal_type, goal_value):
        """Get the goal value in USD dollar.
        :param goal_type:
        :param goal_value: the origin goal value.
        :return goal_value_in_usd: goal_value in USD dollar instead of APPIER dollar.
        """
        if goal_type in ('CPA', 'CPC', 'CPM', 'ROAA') or goal_type.startswith('ROAS'):
            return goal_value * APPIER_TO_USD
        return goal_value

    def _get_minor_goals_query(self, id_type, id_name):
        """Get the SQL query for querying optimization minor goals.
        :param id_type: either 'cid' or 'oid'
        :param id_name: the cid or oid
        :return query: the query string for minor goals
        """
        assert id_type in ('cid', 'oid')
        query = """
        select
            a.goal_type, a.event_type, a.goal_value, b.event_ids
        from
        (
            select
                event_ids_sha1, type as goal_type, event_type, goal_value
            from
                {}
            where
                {} = '{}'
                and is_major = 0
            order by
                order_seq asc
        ) as a
        left outer join
        (
            select event_ids_sha1, event_ids from optimization_goal_eids_hash
        ) as b
        on a.event_ids_sha1 = b.event_ids_sha1
        """.format('optimization_goal' if id_type == 'cid' else 'optimization_goal_order', id_type, id_name)
        return query

    def _parse_minor_goal_query_row(self, row):
        """Parse a row from query into a <GOAL> instance.
        :param row: dict contains `event_type`, `event_ids`, `goal_type` and `goal_value`
        :return goal: an instance of <GOAL>
        """
        event_ids = tuple(row['event_ids'].split(';')) if row['event_ids'] else None
        goal_value = GoalQueryAgent.get_goal_value_in_usd(row['goal_type'], row['goal_value'])
        return self.GOAL(row['event_type'], event_ids, row['goal_type'], goal_value)

    def _adapt_df_supported_minor_goal(self, id_type, id_name, minor_goal):
        """Get the adapted minor goal that supported by deep funnel prediction.
        goal_type: PRAT-XD -> PRAT-30D, ROAS-XD -> PRAT-30D
        goal_value: times by roas2prat_ratio
        :param id_type: either 'cid' or 'oid'
        :param id_name: the cid or oid
        :param minor_goal: the origin <GOAL> of minor goal
        :return adapted_minor_goal: the adapted <GOAL>
        """
        roas2prat_ratio = 1.0
        if minor_goal.goal_type.startswith('ROAS'):
            transformer = OidTransformer(self.query_db, id_name) if \
                id_type == 'oid' else CidTransformer(self.query_db, id_name)
            roas2prat_ratio = transformer.get_roas2prat_ratio()

        goal_value = max(0.0, min(minor_goal.goal_value * roas2prat_ratio, 1.0))
        goal_type = 'PRAT-30D' if minor_goal.goal_type.startswith('PRAT') or minor_goal.goal_type.startswith('ROAS') \
            else minor_goal.goal_type

        return self.GOAL(minor_goal.event_type, minor_goal.event_ids, goal_type, goal_value)


class OidGoalQueryAgent(GoalQueryAgent):
    """Provides interfaces for query oid goal settings. """
    def get_oid_minor_goals(self, oid):
        """Get oid's all optimization minor goals.
        :param oid:
        :return minor_goals: list of <GOAL>, order by order_seq
        """
        query = self._get_minor_goals_query('oid', oid)
        logger.debug(query)
        minor_goals = [
            self._parse_minor_goal_query_row(row)
            for row in self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        ]
        return minor_goals

    def adapt_df_supported_oid_minor_goal(self, oid, minor_goal):
        """Get the adapted oid minor goal that supported by deep funnel prediction.
        goal_type: PRAT-XD -> PRAT-30D, ROAS-XD -> PRAT-30D
        goal_value: times by roas2prat_ratio
        :param oid:
        :param minor_goal: the origin <GOAL> of minor goal
        :return adapted_minor_goal: the adapted <GOAL>
        """
        return self._adapt_df_supported_minor_goal('oid', oid, minor_goal)


class CidGoalQueryAgent(GoalQueryAgent):
    """Provides interfaces for query cid goal settings. """
    def get_cid_minor_goals(self, cid):
        """Get cid's all optimization minor goals.
        :param cid:
        :return minor_goals: list of <GOAL>, order by order_seq
        """
        query = self._get_minor_goals_query('cid', cid)
        logger.debug(query)
        minor_goals = [
            self._parse_minor_goal_query_row(row)
            for row in self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        ]
        return minor_goals

    def get_cid_major_goal(self, cid, goal_type):
        """Get cid's optimization major goal.
        :param cid:
        :param goal_type: CPA/CPA/CPM
        :return major_goal: an instance of <GOAL>, the event_ids field is always None
        """
        query = """
        select
            event_type, goal_value
        from
            optimization_goal
        where
            cid = '{}'
            and type = '{}'
            and is_major = 1
        """.format(cid, goal_type)
        logger.debug(query)
        results = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        assert len(results) <= 1

        if not results:
            return None
        goal_value = GoalQueryAgent.get_goal_value_in_usd(goal_type, results[0]['goal_value'])
        return self.GOAL(results[0]['event_type'], None, goal_type, goal_value)

    def get_cid_major_events(self, cid):
        """Get cid's all major events.
        :param cid:
        :return major_events: set of <EVENT>
        """
        query = """
        select
            event_type, event_id
        from
            event_id
        where
            cid = '{}'
            and major = 1
        """.format(cid)
        logger.debug(query)
        major_events = []
        for row in self.query_db.get_query_result(query, db_client=self.campaign_db_client):
            major_events.append(self.EVENT(row['event_type'], row['event_id']))

        return major_events

    def get_cid_event_types(self, cid):
        query = "select distinct event_type from event_id where cid = '{}'".format(cid)
        logger.debug(query)
        return set(
            [row['event_type']
             for row in self.query_db.get_query_result(query, db_client=self.campaign_db_client)]
        )

    def adapt_df_supported_cid_minor_goal(self, cid, minor_goal):
        """Get the adapted cid minor goal that supported by deep funnel prediction.
        goal_type: PRAT-XD -> PRAT-30D, ROAS-XD -> PRAT-30D
        goal_value: times by roas2prat_ratio
        :param cid:
        :param minor_goal: the origin <GOAL> of minor goal
        :return adapted_minor_goal: the adapted <GOAL>
        """
        return self._adapt_df_supported_minor_goal('cid', cid, minor_goal)
