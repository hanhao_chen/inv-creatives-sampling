import calendar
import datetime
from dateutil.relativedelta import relativedelta

from app.utils.init_logger import init_logger

logger = init_logger(__name__)


class MySQLTables(object):

    def __init__(self, query_db, db_client):
        self.query_db = query_db
        self.db_client = db_client

    def get_monthly_partitions(self):
        month = datetime.datetime.today().replace(day=1) - relativedelta(months=6)
        end_month = datetime.datetime.today().replace(day=1) + relativedelta(months=2)
        partitions = []
        while month <= end_month:
            partitions.append(
                ('p{}'.format(month.strftime('%Y%m')), month + relativedelta(months=1)))
            month += relativedelta(months=1)

        return partitions

    def get_partitioning_value(self, value_type, end):
        if value_type == 'ts':
            return calendar.timegm(end.timetuple())
        elif value_type == 'date':
            return "TO_DAYS('{}')".format(end.strftime('%Y-%m-%d'))

        raise ValueError("Not a valid value_type: {}, available value_type are ['ts', 'date']".format(value_type))

    def init_tables_from_config(self, table_config):
        raise NotImplementedError


class InvTables(MySQLTables):

    def init_tables_from_config(self, table_config):
        self.init_inv_creative_table_table(table_config['inv-creative-table'])

    def init_inv_sampling_info_table(self):
        query = """
            CREATE TABLE IF NOT EXISTS `inventory_sampling_info` (
                `oid` varchar(32) NOT NULL DEFAULT '',
                `cid` varchar(32) NOT NULL DEFAULT '',
                `country` varchar(32) NOT NULL DEFAULT '',
                `imp_type` int(11) NOT NULL DEFAULT '0',
                `is_rewarded` int(11) NOT NULL DEFAULT '0',
                `ssp` text CHARACTER SET utf8,
                `sampling_size` int(11) NOT NULL,
                `sampling_list` text CHARACTER SET utf8,
                `update_time` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                PRIMARY KEY (`oid`,`cid`, `update_time`),
                KEY `oid_cid` (`oid`, `cid`),
                KEY `oid_cid_imp_type_is_rewarded` (`oid`, `cid`, `imp_type`, `is_rewarded`),
                KEY `oid_cid_update_time` (`oid`, `cid`, `update_time`),
                KEY `time` (`update_time`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_inv_complete_table_table(self, table_name):

        query = """
                CREATE TABLE IF NOT EXISTS `{}` (
                    `oid` varchar(32) NOT NULL DEFAULT '',
                    `cid` varchar(32) NOT NULL DEFAULT '',
                    `dealid` varchar(32) NOT NULL DEFAULT '',
                    `deal_count` int(11) NOT NULL DEFAULT '0',
                    `app_id` varchar(255) NOT NULL DEFAULT '',
                    `partner_id` varchar(32) NOT NULL DEFAULT '',
                    `exchange` varchar(32) NOT NULL DEFAULT '',
                    `bundle_id` varchar(255) NOT NULL DEFAULT '',
                    `imp_size_group` varchar(32) NOT NULL DEFAULT '',
                    `imp_is_instl` int(11) NOT NULL DEFAULT '0',
                    `imp_type` int(11) NOT NULL DEFAULT '0',
                    `imp_width` int(11) NOT NULL DEFAULT '0',
                    `imp_height` int(11) NOT NULL DEFAULT '0',
                    `device_type` int(11) NOT NULL DEFAULT '0',
                    `app_type` int(11) NOT NULL,
                    `tagid` varchar(255) NOT NULL DEFAULT '',
                    `is_rewarded` int(11) NOT NULL DEFAULT '0',
                    `score_type` varchar(32) NOT NULL DEFAULT '',
                    `uu` bigint(20) NOT NULL DEFAULT '0',
                    `rt_uu` bigint(20) NOT NULL DEFAULT '0',
                    `cost` bigint(20) NOT NULL DEFAULT '0',
                    `bids` bigint(20) NOT NULL DEFAULT '0',
                    `shows` bigint(20) NOT NULL DEFAULT '0',
                    `clicks` bigint(20) NOT NULL DEFAULT '0',
                    `installs` bigint(20) NOT NULL DEFAULT '0',
                    `country_shows_rt_uu` bigint(20) NOT NULL DEFAULT '0',
                    `country_clicks_rt_uu` bigint(20) NOT NULL DEFAULT '0',
                    `rt_ratio` double NOT NULL DEFAULT '0',
                    `ctr` double NOT NULL DEFAULT '0',
                    `bid2show` double NOT NULL DEFAULT '0',
                    `cpm` double NOT NULL DEFAULT '0',
                    `score` double NOT NULL DEFAULT '0',
                    `adjust_weight` double NOT NULL,
                    `status` double NOT NULL,
                    `goal` double NOT NULL,
                    `goal_type` varchar(32) NOT NULL DEFAULT '',
                    `inv_hash` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
                    `complete_reason` varchar(255) NOT NULL DEFAULT '',
                    `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`oid`, `cid`, `inv_hash`),
                    KEY `hash` (`inv_hash`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_inv_sampling_table_table(self, table_name):

        query = """
                CREATE TABLE IF NOT EXISTS `{}` (
                    `oid` varchar(32) NOT NULL DEFAULT '',
                    `cid` varchar(32) NOT NULL DEFAULT '',
                    `dealid` varchar(32) NOT NULL DEFAULT '',
                    `deal_count` int(11) NOT NULL DEFAULT '0',
                    `app_id` varchar(255) NOT NULL DEFAULT '',
                    `partner_id` varchar(32) NOT NULL DEFAULT '',
                    `exchange` varchar(32) NOT NULL DEFAULT '',
                    `bundle_id` varchar(255) NOT NULL DEFAULT '',
                    `imp_size_group` varchar(32) NOT NULL DEFAULT '',
                    `imp_is_instl` int(11) NOT NULL DEFAULT '0',
                    `imp_type` int(11) NOT NULL DEFAULT '0',
                    `imp_width` int(11) NOT NULL DEFAULT '0',
                    `imp_height` int(11) NOT NULL DEFAULT '0',
                    `device_type` int(11) NOT NULL DEFAULT '0',
                    `app_type` int(11) NOT NULL,
                    `tagid` varchar(255) NOT NULL DEFAULT '',
                    `is_rewarded` int(11) NOT NULL DEFAULT '0',
                    `score_type` varchar(32) NOT NULL DEFAULT '',
                    `uu` bigint(20) NOT NULL DEFAULT '0',
                    `rt_uu` bigint(20) NOT NULL DEFAULT '0',
                    `cost` bigint(20) NOT NULL DEFAULT '0',
                    `bids` bigint(20) NOT NULL DEFAULT '0',
                    `shows` bigint(20) NOT NULL DEFAULT '0',
                    `clicks` bigint(20) NOT NULL DEFAULT '0',
                    `installs` bigint(20) NOT NULL DEFAULT '0',
                    `country_shows_rt_uu` bigint(20) NOT NULL DEFAULT '0',
                    `country_clicks_rt_uu` bigint(20) NOT NULL DEFAULT '0',
                    `rt_ratio` double NOT NULL DEFAULT '0',
                    `ctr` double NOT NULL DEFAULT '0',
                    `bid2show` double NOT NULL DEFAULT '0',
                    `cpm` double NOT NULL DEFAULT '0',
                    `score` double NOT NULL DEFAULT '0',
                    `est_cpi` double NOT NULL DEFAULT '0',
                    `complete_sampling` tinyint NOT NULL DEFAULT '0',
                    `sampling` tinyint NOT NULL DEFAULT '0',
                    `checked` tinyint NOT NULL DEFAULT '0',
                    `adjust_weight` double NOT NULL,
                    `status` double NOT NULL,
                    `goal` double NOT NULL,
                    `goal_type` varchar(32) NOT NULL DEFAULT '',
                    `inv_hash` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT '',
                    `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`oid`, `cid`, `inv_hash`),
                    KEY `hash` (`inv_hash`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_inv_creative_table_table(self, table_name):

        query = """
                CREATE TABLE IF NOT EXISTS `{}` (
                    `start_date` date NOT NULL,
                    `end_date` date NOT NULL,
                    `oid` varchar(32) NOT NULL DEFAULT '',
                    `cid` varchar(32) NOT NULL DEFAULT '',
                    `dealid` varchar(32) NOT NULL DEFAULT '',
                    `deal_count` int(11) NOT NULL DEFAULT '0',
                    `app_id` varchar(255) NOT NULL DEFAULT '',
                    `partner_id` varchar(32) NOT NULL DEFAULT '',
                    `exchange` varchar(32) NOT NULL DEFAULT '',
                    `bundle_id` varchar(255) NOT NULL DEFAULT '',
                    `imp_size_group` varchar(32) NOT NULL DEFAULT '',
                    `imp_is_instl` int(11) NOT NULL DEFAULT '0',
                    `imp_type` int(11) NOT NULL DEFAULT '0',
                    `imp_width` int(11) NOT NULL DEFAULT '0',
                    `imp_height` int(11) NOT NULL DEFAULT '0',
                    `device_type` int(11) NOT NULL DEFAULT '0',
                    `app_type` int(11) NOT NULL,
                    `tagid` varchar(255) NOT NULL DEFAULT '',
                    `score_type` varchar(32) NOT NULL DEFAULT '',
                    `traffic_count` float DEFAULT NULL DEFAULT '0',
                    `uu` bigint(20) NOT NULL DEFAULT '0',
                    `rt_uu` bigint(20) NOT NULL DEFAULT '0',
                    `cost` bigint(20) NOT NULL DEFAULT '0',
                    `bids` bigint(20) NOT NULL DEFAULT '0',
                    `shows` bigint(20) NOT NULL DEFAULT '0',
                    `clicks` bigint(20) NOT NULL DEFAULT '0',
                    `actions` bigint(20) NOT NULL DEFAULT '0',
                    `rt_ratio` double NOT NULL DEFAULT '0',
                    `ctr` double NOT NULL DEFAULT '0',
                    `bid2show` double NOT NULL DEFAULT '0',
                    `cpm` double NOT NULL DEFAULT '0',
                    `score` double NOT NULL DEFAULT '0',
                    `complete_sampling` tinyint NOT NULL DEFAULT '0',
                    `sampling` tinyint NOT NULL DEFAULT '0',
                    `adjust_weight` double NOT NULL,
                    `status` double NOT NULL,
                    `goal_price` double NOT NULL,
                    `est_cpi` double DEFAULT NULL,
                    `target_cpm` double DEFAULT NULL,
                    `est_win_rate` double DEFAULT NULL,
                    `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    `inv_hash` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
                    PRIMARY KEY (`end_date`, `oid`, `score_type`, `app_id`, `partner_id`, `exchange`, 
                    `bundle_id`, `imp_size_group`, `imp_width`, `imp_height`, `imp_is_instl`, 
                    `imp_type`, `device_type`, `app_type`, `tagid`, `dealid`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_inv_goal_value_table(self, table_name):
        query = """
                CREATE TABLE IF NOT EXISTS `{}` (
                    `cid` varchar(32) NOT NULL DEFAULT '',
                    `goal_cpm` double NOT NULL DEFAULT '0',
                    `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`cid`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_daily_agg_performance(self, table_name):
        query = """
                CREATE TABLE IF NOT EXISTS `{}` (
                    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                    `date` date NOT NULL,
                    `oid` varchar(32) NOT NULL DEFAULT '',
                    `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
                    `app_type` int(11) NOT NULL DEFAULT '0',
                    `device_type` int(11) NOT NULL DEFAULT '0',
                    `exchange` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
                    `tagid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
                    `imp_size_group` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
                    `costs` bigint(20) NOT NULL DEFAULT '0',
                    `bids` bigint(20) NOT NULL DEFAULT '0',
                    `shows` bigint(20) NOT NULL DEFAULT '0',
                    `clicks` bigint(20) NOT NULL DEFAULT '0',
                    `actions` bigint(20) NOT NULL DEFAULT '0',
                    PRIMARY KEY (`id`, `date`),
                    UNIQUE KEY `unique` (`oid`, `date`)
                ) ENGINE=InnoDB AUTO_INCREMENT=708838376 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def drop_table(self, table_name):
        query = """
        Drop Table If Exists {}
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_agg_oid_performance(self, table_name):
        query = """
                CREATE TABLE IF NOT EXISTS `{}` (
                    `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                    `date` datetime NOT NULL,
                    `time` bigint(20) unsigned NOT NULL,
                    `oid` varchar(32) NOT NULL DEFAULT '',
                    `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
                    `bundle_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
                    `imp_is_instl` int(11) NOT NULL DEFAULT '0',
                    `imp_type` int(11) NOT NULL DEFAULT '0',
                    `imp_width` int(11) NOT NULL DEFAULT '0',
                    `imp_height` int(11) NOT NULL DEFAULT '0',
                    `app_type` int(11) NOT NULL DEFAULT '0',
                    `device_type` int(11) NOT NULL DEFAULT '0',
                    `inv_hash` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
                    `partner_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
                    `tagid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
                    `imp_size_group` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
                    `cost` bigint(20) NOT NULL DEFAULT '0',
                    `bids` bigint(20) NOT NULL DEFAULT '0',
                    `shows` bigint(20) NOT NULL DEFAULT '0',
                    `clicks` bigint(20) NOT NULL DEFAULT '0',
                    `actions` bigint(20) NOT NULL DEFAULT '0',
                    PRIMARY KEY (`id`, `date`, `oid`, `time`),
                    UNIQUE KEY `unique` (`time`, `oid`, `app_id`, `inv_hash`, `partner_id`, `imp_is_instl`, `imp_type`, `imp_width`, `imp_height`,
                                         `app_type`, `device_type`, `tagid`),
                    KEY `date` (`date`, `oid`),
                    KEY `hash` (`inv_hash`)
                ) ENGINE=InnoDB AUTO_INCREMENT=708838376 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
                /*!50100 PARTITION BY RANGE (`time`)
                (PARTITION p20200130 VALUES LESS THAN (1580428800) ENGINE = InnoDB,
                 PARTITION p20200131 VALUES LESS THAN (1580515200) ENGINE = InnoDB,
                 PARTITION p20200201 VALUES LESS THAN (1580601600) ENGINE = InnoDB,
                 PARTITION p20200202 VALUES LESS THAN (1580688000) ENGINE = InnoDB,
                 PARTITION p20200203 VALUES LESS THAN (1580774400) ENGINE = InnoDB,
                 PARTITION p20200204 VALUES LESS THAN (1580860800) ENGINE = InnoDB,
                 PARTITION p20200205 VALUES LESS THAN (1580947200) ENGINE = InnoDB,
                 PARTITION p20200206 VALUES LESS THAN (1581033600) ENGINE = InnoDB,
                 PARTITION p20200207 VALUES LESS THAN (1581120000) ENGINE = InnoDB,
                 PARTITION p20200208 VALUES LESS THAN (1581206400) ENGINE = InnoDB,
                 PARTITION p20200209 VALUES LESS THAN (1581292800) ENGINE = InnoDB,
                 PARTITION p20200210 VALUES LESS THAN (1581379200) ENGINE = InnoDB,
                 PARTITION p20200211 VALUES LESS THAN (1581465600) ENGINE = InnoDB,
                 PARTITION p20200212 VALUES LESS THAN (1581552000) ENGINE = InnoDB,
                 PARTITION p20200213 VALUES LESS THAN (1581638400) ENGINE = InnoDB,
                 PARTITION p20200214 VALUES LESS THAN (1581724800) ENGINE = InnoDB,
                 PARTITION p20200215 VALUES LESS THAN (1581811200) ENGINE = InnoDB,
                 PARTITION p20200216 VALUES LESS THAN (1581897600) ENGINE = InnoDB,
                 PARTITION p20200217 VALUES LESS THAN (1581984000) ENGINE = InnoDB,
                 PARTITION p20200218 VALUES LESS THAN (1582070400) ENGINE = InnoDB,
                 PARTITION p20200219 VALUES LESS THAN (1582156800) ENGINE = InnoDB,
                 PARTITION p20200220 VALUES LESS THAN (1582243200) ENGINE = InnoDB,
                 PARTITION p20200221 VALUES LESS THAN (1582329600) ENGINE = InnoDB,
                 PARTITION p20200222 VALUES LESS THAN (1582416000) ENGINE = InnoDB,
                 PARTITION p20200223 VALUES LESS THAN (1582502400) ENGINE = InnoDB,
                 PARTITION p20200224 VALUES LESS THAN (1582588800) ENGINE = InnoDB,
                 PARTITION p20200225 VALUES LESS THAN (1582675200) ENGINE = InnoDB,
                 PARTITION p20200226 VALUES LESS THAN (1582761600) ENGINE = InnoDB,
                 PARTITION p20200227 VALUES LESS THAN (1582848000) ENGINE = InnoDB,
                 PARTITION p20200228 VALUES LESS THAN (1582934400) ENGINE = InnoDB,
                 PARTITION p20200229 VALUES LESS THAN (1583020800) ENGINE = InnoDB,
                 PARTITION p20200301 VALUES LESS THAN (1583107200) ENGINE = InnoDB) */;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_country_uu_table(self, table_name):
        query = """
            CREATE TABLE IF NOT EXISTS `{}` (
              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              `date` datetime NOT NULL,
              `time` bigint(20) unsigned NOT NULL,
              `oid` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `bundle_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `imp_is_instl` int(11) NOT NULL DEFAULT '0',
              `imp_type` int(11) NOT NULL DEFAULT '0',
              `imp_width` int(11) NOT NULL DEFAULT '0',
              `imp_height` int(11) NOT NULL DEFAULT '0',
              `app_type` int(11) NOT NULL DEFAULT '0',
              `device_type` int(11) NOT NULL DEFAULT '0',
              `inv_hash` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `partner_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `tagid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `cost` bigint(20) NOT NULL DEFAULT '0',
              `bids` bigint(20) NOT NULL DEFAULT '0',
              `wins` bigint(20) NOT NULL DEFAULT '0',
              `shows` bigint(20) NOT NULL DEFAULT '0',
              `clicks` bigint(20) NOT NULL DEFAULT '0',
              `actions` bigint(20) NOT NULL DEFAULT '0',
              `uu` bigint(20) NOT NULL DEFAULT '0',
              `rt_uu` bigint(20) NOT NULL DEFAULT '0',
              `clicks_uu` bigint(20) NOT NULL DEFAULT '0',
              `shows_uu` bigint(20) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`,`date`,`oid`,`time`),
              UNIQUE KEY `unique` (`time`,`oid`,`app_id`, `inv_hash`, `partner_id`,`imp_is_instl`,`imp_type`,`imp_width`,`imp_height`,`app_type`,`device_type`,`tagid`),
              KEY `date` (`date`,`oid`),
              KEY `hash` (`inv_hash`)
            ) ENGINE=InnoDB AUTO_INCREMENT=708921205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
            /*!50100 PARTITION BY RANGE (`time`)
            (PARTITION p20200130 VALUES LESS THAN (1580428800) ENGINE = InnoDB,
             PARTITION p20200131 VALUES LESS THAN (1580515200) ENGINE = InnoDB,
             PARTITION p20200201 VALUES LESS THAN (1580601600) ENGINE = InnoDB,
             PARTITION p20200202 VALUES LESS THAN (1580688000) ENGINE = InnoDB,
             PARTITION p20200203 VALUES LESS THAN (1580774400) ENGINE = InnoDB,
             PARTITION p20200204 VALUES LESS THAN (1580860800) ENGINE = InnoDB,
             PARTITION p20200205 VALUES LESS THAN (1580947200) ENGINE = InnoDB,
             PARTITION p20200206 VALUES LESS THAN (1581033600) ENGINE = InnoDB,
             PARTITION p20200207 VALUES LESS THAN (1581120000) ENGINE = InnoDB,
             PARTITION p20200208 VALUES LESS THAN (1581206400) ENGINE = InnoDB,
             PARTITION p20200209 VALUES LESS THAN (1581292800) ENGINE = InnoDB,
             PARTITION p20200210 VALUES LESS THAN (1581379200) ENGINE = InnoDB,
             PARTITION p20200211 VALUES LESS THAN (1581465600) ENGINE = InnoDB,
             PARTITION p20200212 VALUES LESS THAN (1581552000) ENGINE = InnoDB,
             PARTITION p20200213 VALUES LESS THAN (1581638400) ENGINE = InnoDB,
             PARTITION p20200214 VALUES LESS THAN (1581724800) ENGINE = InnoDB,
             PARTITION p20200215 VALUES LESS THAN (1581811200) ENGINE = InnoDB,
             PARTITION p20200216 VALUES LESS THAN (1581897600) ENGINE = InnoDB,
             PARTITION p20200217 VALUES LESS THAN (1581984000) ENGINE = InnoDB,
             PARTITION p20200218 VALUES LESS THAN (1582070400) ENGINE = InnoDB,
             PARTITION p20200219 VALUES LESS THAN (1582156800) ENGINE = InnoDB,
             PARTITION p20200220 VALUES LESS THAN (1582243200) ENGINE = InnoDB,
             PARTITION p20200221 VALUES LESS THAN (1582329600) ENGINE = InnoDB,
             PARTITION p20200222 VALUES LESS THAN (1582416000) ENGINE = InnoDB,
             PARTITION p20200223 VALUES LESS THAN (1582502400) ENGINE = InnoDB,
             PARTITION p20200224 VALUES LESS THAN (1582588800) ENGINE = InnoDB,
             PARTITION p20200225 VALUES LESS THAN (1582675200) ENGINE = InnoDB,
             PARTITION p20200226 VALUES LESS THAN (1582761600) ENGINE = InnoDB,
             PARTITION p20200227 VALUES LESS THAN (1582848000) ENGINE = InnoDB,
             PARTITION p20200228 VALUES LESS THAN (1582934400) ENGINE = InnoDB,
             PARTITION p20200229 VALUES LESS THAN (1583020800) ENGINE = InnoDB,
             PARTITION p20200301 VALUES LESS THAN (1583107200) ENGINE = InnoDB) */;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_imp_ctcv_table(self, table_name):
        query = """
            CREATE TABLE IF NOT EXISTS `{}` (
              `date` datetime NOT NULL,
              `oid` varchar(32) NOT NULL DEFAULT '',
              `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `bundle_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `imp_is_instl` int(11) NOT NULL DEFAULT '0',
              `imp_type` int(11) NOT NULL DEFAULT '0',
              `imp_width` int(11) NOT NULL DEFAULT '0',
              `imp_height` int(11) NOT NULL DEFAULT '0',
              `app_type` int(11) NOT NULL DEFAULT '0',
              `device_type` int(11) NOT NULL DEFAULT '0',
              `is_rewarded` int(11) NOT NULL DEFAULT '0',
              `inv_hash` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `partner_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `tagid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `uu` bigint(20) NOT NULL DEFAULT '0',
              `rt_uu` bigint(20) NOT NULL DEFAULT '0',
              `install_rt_uu` bigint(20) NOT NULL DEFAULT '0',
              `bid_count` bigint(20) NOT NULL DEFAULT '0',
              PRIMARY KEY (`date`,`oid`,`inv_hash`),
              UNIQUE KEY `unique` (`oid`, `inv_hash`),
              KEY `date` (`date`,`oid`),
              KEY `hash` (`inv_hash`)
            ) ENGINE=InnoDB AUTO_INCREMENT=708921205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_country_oid_uu_table(self, table_name):
        query = """
            CREATE TABLE IF NOT EXISTS `{}` (
              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              `date` datetime NOT NULL,
              `time` bigint(20) unsigned NOT NULL,
              `oid` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `bundle_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `imp_is_instl` int(11) NOT NULL DEFAULT '0',
              `imp_type` int(11) NOT NULL DEFAULT '0',
              `imp_width` int(11) NOT NULL DEFAULT '0',
              `imp_height` int(11) NOT NULL DEFAULT '0',
              `app_type` int(11) NOT NULL DEFAULT '0',
              `device_type` int(11) NOT NULL DEFAULT '0',
              `inv_hash` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `partner_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `tagid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `cost` bigint(20) NOT NULL DEFAULT '0',
              `bids` bigint(20) NOT NULL DEFAULT '0',
              `wins` bigint(20) NOT NULL DEFAULT '0',
              `shows` bigint(20) NOT NULL DEFAULT '0',
              `clicks` bigint(20) NOT NULL DEFAULT '0',
              `actions` bigint(20) NOT NULL DEFAULT '0',
              `_cost` bigint(20) NOT NULL DEFAULT '0',
              `_bids` bigint(20) NOT NULL DEFAULT '0',
              `_wins` bigint(20) NOT NULL DEFAULT '0',
              `_shows` bigint(20) NOT NULL DEFAULT '0',
              `_clicks` bigint(20) NOT NULL DEFAULT '0',
              `_actions` bigint(20) NOT NULL DEFAULT '0',
              `uu` bigint(20) NOT NULL DEFAULT '0',
              `rt_uu` bigint(20) NOT NULL DEFAULT '0',
              `country_shows_rt_uu` bigint(20) NOT NULL DEFAULT '0',
              `country_clicks_rt_uu` bigint(20) NOT NULL DEFAULT '0',
              `shows_uu` bigint(20) NOT NULL DEFAULT '0',
              `clicks_uu` bigint(20) NOT NULL DEFAULT '0',
              `shows_rt_uu` bigint(20) NOT NULL DEFAULT '0',
              `clicks_rt_uu` bigint(20) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`,`date`,`oid`,`time`),
              UNIQUE KEY `unique` (`time`,`oid`,`app_id`, `inv_hash`, `partner_id`,`imp_is_instl`,`imp_type`,`imp_width`,`imp_height`,`app_type`,`device_type`,`tagid`),
              KEY `date` (`date`,`oid`),
              KEY `hash` (`inv_hash`),
              KEY `oid` (`oid`)
            ) ENGINE=InnoDB AUTO_INCREMENT=708921205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin
            /*!50100 PARTITION BY RANGE (`time`)
            (PARTITION p20200708 VALUES LESS THAN (1594166400) ENGINE = InnoDB,
             PARTITION p20200709 VALUES LESS THAN (1594252800) ENGINE = InnoDB,
             PARTITION p20200710 VALUES LESS THAN (1594339200) ENGINE = InnoDB) */;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_imp_join_cost_table(self, table_name):
        query = """
            CREATE TABLE IF NOT EXISTS `{}` (
              `date` datetime NOT NULL,
              `time` bigint(20) unsigned NOT NULL,
              `oid` varchar(32) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `app_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `bundle_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `imp_is_instl` int(11) NOT NULL DEFAULT '0',
              `imp_type` int(11) NOT NULL DEFAULT '0',
              `imp_width` int(11) NOT NULL DEFAULT '0',
              `imp_height` int(11) NOT NULL DEFAULT '0',
              `app_type` int(11) NOT NULL DEFAULT '0',
              `device_type` int(11) NOT NULL DEFAULT '0',
              `inv_hash` varchar(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `partner_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `tagid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `cost` bigint(20) NOT NULL DEFAULT '0',
              `shows` bigint(20) NOT NULL DEFAULT '0',
              `clicks` bigint(20) NOT NULL DEFAULT '0',
              `actions` bigint(20) NOT NULL DEFAULT '0',
              PRIMARY KEY (`date`,`oid`,`time`, `inv_hash`),
              UNIQUE KEY `unique` (`time`,`oid`,`app_id`, `inv_hash`, `tagid`),
              KEY `date` (`date`,`oid`),
              KEY `hash` (`inv_hash`),
              KEY `oid` (`oid`)
            ) ENGINE=InnoDB AUTO_INCREMENT=708921205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
            """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_appanni_cate(self, table_name):
        query = """
            CREATE TABLE IF NOT EXISTS `{}` (
              `bundle_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `os` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `country` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `product_id` varchar(15) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `unified_category_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              PRIMARY KEY (`bundle_id`,`country`, `os`)
            ) ENGINE=InnoDB AUTO_INCREMENT=708921205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;            
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_appanni_info_table(self, table_name):
        query = """
            CREATE TABLE IF NOT EXISTS `{}` (
              `bundle_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
              `os` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `country` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `product_id` varchar(15) COLLATE utf8mb4_bin NOT NULL DEFAULT '',
              `usage_penetration` double NOT NULL DEFAULT '0',
              `install_penetration` double NOT NULL DEFAULT '0',
              `open_rate` double NOT NULL DEFAULT '0',
              `avg_sessions_per_user` double NOT NULL DEFAULT '0',
              `avg_session_duration` bigint(20) NOT NULL DEFAULT '0',
              `avg_time_per_user` bigint(20) NOT NULL DEFAULT '0',
              `average` double NOT NULL DEFAULT '0',
              `star_5_count` bigint(20) NOT NULL DEFAULT '0',
              `star_4_count` bigint(20) NOT NULL DEFAULT '0',
              `star_3_count` bigint(20) NOT NULL DEFAULT '0',
              `star_2_count` bigint(20) NOT NULL DEFAULT '0',
              `star_1_count` bigint(20) NOT NULL DEFAULT '0',
              PRIMARY KEY (`bundle_id`,`country`, `os`)
            ) ENGINE=InnoDB AUTO_INCREMENT=708921205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;            
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

class HydraTables(MySQLTables):

    def init_tables_from_config(self, table_config):
        self.init_hydra_list_control_table(table_config['hydra-list-control'])
        self.init_hydra_adgroup_trees_table(table_config['hydra-adgroup-trees'])
        self.init_hydra_garbage_cid_table(table_config['hydra-garbage-cid-status'])

    def init_hydra_list_control_table(self, table_name):
        """Initializes MySQL table for recording hydra list control results """
        query = """
        CREATE TABLE IF NOT EXISTS `{}` (
            `date_hour` datetime NOT NULL,
            `cid` varchar(32) NOT NULL DEFAULT '',
            `partner_id` varchar(32) NOT NULL DEFAULT '',
            `event_type` varchar(64) NOT NULL DEFAULT '',
            `hydra_label` varchar(64) NOT NULL DEFAULT '',
            `require_publisher` text NOT NULL,
            `require_sub_publisher` text NOT NULL,
            `reject_publisher` text NOT NULL,
            `reject_sub_publisher` text NOT NULL,
            `note` text NOT NULL,
            PRIMARY KEY (`date_hour`, `cid`),
            KEY `cid` (`cid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        PARTITION BY RANGE (TO_DAYS(`date_hour`)) (
            {}
        );
        """.format(table_name, ',\n\t\t\t'.join(
            'PARTITION {} VALUES LESS THAN ({})'.format(p_name, self.get_partitioning_value('date', p_boundary))
            for (p_name, p_boundary) in self.get_monthly_partitions()))
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_hydra_adgroup_trees_table(self, table_name):
        """Initializes MySQL table for recording hydra adgroup trees """
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `root_cid` varchar(32) NOT NULL,
        `campaign_cid` varchar(32) NOT NULL DEFAULT '',
        `blacklist_cid` varchar(32) NOT NULL DEFAULT '',
        `global_cid` varchar(32) NOT NULL,
        `stable_campaign_cids` text NOT NULL,
        `stable_global_cids` text NOT NULL,
        `garbage_cids` text NOT NULL,
        `pending_tasks` text NOT NULL,
        `is_transparent` tinyint(1) NOT NULL DEFAULT '0',
        `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`root_cid`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_hydra_garbage_cid_table(self, table_name):
        """Initializes MySQL table for recording hydra garbage cid status """
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `cid` varchar(32) NOT NULL DEFAULT '',
        `recycle_time` int(20) NOT NULL,
        `recovered` tinyint(1) NOT NULL DEFAULT '0',
        `note` text NOT NULL DEFAULT '',
        PRIMARY KEY (`cid`, `recycle_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)


class HydraDryRunTables(MySQLTables):

    def init_tables_from_config(self, table_config):
        self.init_dry_run_result_table(table_config['dry_run_result_table'])

    def init_dry_run_result_table(self, table_name):
        query = """
        CREATE TABLE IF NOT EXISTS `{}` (
            `date` date NOT NULL,
            `oid` varchar(32) NOT NULL,
            `partner_id` varchar(32) NOT NULL,
            `country` varchar(32) NOT NULL,
            `top_category` varchar(32) NOT NULL,
            `sub_category` varchar(32) NOT NULL,
            `event_type` varchar(32) NOT NULL DEFAULT '',
            `goal_type` varchar(32) NOT NULL DEFAULT '',
            `goal_value` float NOT NULL DEFAULT '0',
            `df_goal_value` float NOT NULL DEFAULT '0',
            `sm_list_size` int(10) unsigned NOT NULL DEFAULT '0',
            `sm_global_installs` float NOT NULL DEFAULT '0',
            `oid_list_size` int(10) unsigned NOT NULL DEFAULT '0',
            `oid_campaign_installs` float NOT NULL DEFAULT '0',
            `bl_list_size` int(10) unsigned NOT NULL DEFAULT '0',
            `bl_global_installs` float NOT NULL DEFAULT '0',
            `bl_campaign_installs` float NOT NULL DEFAULT '0',
            `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            PRIMARY KEY (`date`, `oid`, `partner_id`),
            KEY `oid` (`oid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        PARTITION BY RANGE (TO_DAYS(`date`)) (
            {}
        );
        """.format(table_name, ',\n\t\t\t'.join(
            'PARTITION {} VALUES LESS THAN ({})'.format(p_name, self.get_partitioning_value('date', p_boundary))
            for (p_name, p_boundary) in self.get_monthly_partitions()))
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)


class CidMinorGoalHistoryTables(MySQLTables):

    def init_tables_from_config(self, table_config):
        self.init_cid_minor_goal_history_table(table_config.cid_minor_goal_history_table)
        self.init_event_ids_hash_table(table_config.event_ids_hash_table)
        self.init_job_status_table(table_config.job_status_table)

    def init_cid_minor_goal_history_table(self, table_name):
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `cid` varchar(32) NOT NULL DEFAULT '',
        `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `event_type` varchar(32) NOT NULL DEFAULT '',
        `event_ids_sha1` varchar(8) NOT NULL DEFAULT '',
        `goal_type` varchar(32) NOT NULL DEFAULT '',
        `goal_value` float NOT NULL DEFAULT '0',
        PRIMARY KEY (`cid`, `timestamp`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_event_ids_hash_table(self, table_name):
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `event_ids_sha1` varchar(8) NOT NULL DEFAULT '',
        `event_ids` text NOT NULL,
        `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`event_ids_sha1`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_job_status_table(self, table_name):
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `job_name` varchar(256) NOT NULL DEFAULT '',
        `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`job_name`, `timestamp`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)


class CidConfigHistoryTables(MySQLTables):

    def init_tables_from_config(self, table_config):
        self.init_cid_config_history_table(table_config.cid_config_history_table)
        self.init_job_status_table(table_config.job_status_table)

    def init_cid_config_history_table(self, table_name):
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `field_name` varchar(64) NOT NULL DEFAULT '',
        `cid` varchar(32) NOT NULL DEFAULT '',
        `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `field_value` text NOT NULL,
        PRIMARY KEY (`field_name`, `cid`, `timestamp`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def init_job_status_table(self, table_name):
        query = """
    CREATE TABLE IF NOT EXISTS `{}` (
        `job_name` varchar(256) NOT NULL DEFAULT '',
        `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`job_name`, `timestamp`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)
