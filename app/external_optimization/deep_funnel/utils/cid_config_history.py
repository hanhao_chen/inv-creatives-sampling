"""Define class for updating cid config and getting cid daily config. """
import calendar
import datetime
import math
import time
from collections import defaultdict
import numpy as np

from app.utils.init_logger import init_logger

logger = init_logger(__name__)


class CidConfigHistory(object):
    """Provides interfaces for updating cid config history and getting cid daily config. """

    def __init__(self, idash_endpoint, query_db, db_client, cid_config_table, job_status_table, job_name):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.db_client = db_client
        self.cid_config_table = cid_config_table
        self.job_status_table = job_status_table
        self.job_name = job_name

    def _get_field_job_name(self, field_name):
        return '{}-{}'.format(self.job_name, field_name)

    def get_recorded_cids(self, field_name):
        """Gets current recorded cids.
        :param field_name: the field_name to update
        :return recorded_cids: set of cids
        """
        query = "select distinct cid from {} where field_name = '{}'".format(self.cid_config_table, field_name)
        logger.info(query)
        recorded_cids = set(row['cid'] for row in self.query_db.get_query_result(query, db_client=self.db_client))
        return recorded_cids

    def get_checkpoint_timestamp(self, field_name):
        """Get the timestamp of last update.
        :param field_name: the field_name to update
        :return timestamp: integer timestamp
        """
        query = "select max(timestamp) as timestamp from {} where job_name = '{}'".format(
            self.job_status_table, self._get_field_job_name(field_name))
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.db_client)
        return calendar.timegm(results[0]['timestamp'].timetuple()) if results[0]['timestamp'] else 0

    def _parse_one_idash_history_log(self, history, field_name, idash_field_name):
        for change in history['changes']:
            if change['key'] == idash_field_name and 'errorMsg' not in change:
                if 'origin' in change and 'updated' in change:
                    return (self._parse_idash_field(change['origin'], field_name),
                            self._parse_idash_field(change['updated'], field_name))
                else:
                    return change['from'], change['to']

        return None, None

    def _parse_idash_field(self, cid_config, field_name):
        if field_name == 'optimization_model':
            return str(cid_config.get('optimization_model', ''))
        raise RuntimeError('unknown filed_name {}'.format(field_name))

    def _get_new_cids_history(self, cids, field_name, idash_field_name, batch_size=1000):
        history_records = []
        query_fields = ['history', 'begin_at', idash_field_name]
        logger.info('query %s new cids history', len(cids))
        for batch_index in range(int(math.ceil(float(len(cids)) / batch_size))):
            query_cids = cids[batch_index * batch_size: (batch_index + 1) * batch_size]
            logger.info('query new cids batch %s', batch_index)
            results = self.idash_endpoint.read_campaigns_by_cids(query_cids, fields=query_fields)
            for cid_result in results:
                begin_at = int(float(cid_result['begin_at']))  # some cid has float begin_at
                cid_history_records = []
                if 'history' in cid_result:
                    cid_result['history'].sort(key=lambda item: item['updated_at'])
                    for history in cid_result['history']:
                        origin_value, updated_value = self._parse_one_idash_history_log(
                            history, field_name, idash_field_name)
                        if origin_value == updated_value:
                            continue
                        if not cid_history_records:
                            cid_history_records.append((cid_result['cid'], begin_at, origin_value))
                        cid_history_records.append((cid_result['cid'], history['updated_at'], updated_value))

                if not cid_history_records:
                    cid_history_records.append(
                        (cid_result['cid'], begin_at, self._parse_idash_field(cid_result, field_name)))
                history_records.extend(cid_history_records)

        return history_records

    def _get_old_cids_history(self, cids, field_name, idash_field_name, modified_at_after, batch_size=1000):
        history_records = []
        logger.info('query %s old cids history', len(cids))
        batch_size = batch_size if modified_at_after < int(time.time()) - 3 * 86400 else len(cids) + 1
        for batch_index in range(int(math.ceil(float(len(cids)) / batch_size))):
            logger.info('query old cids batch %s', batch_index)
            query_cids = cids[batch_index * batch_size: (batch_index + 1) * batch_size]
            results = self.idash_endpoint.read_campaigns_by_cids(
                query_cids, fields=['history'], modified_at_after=modified_at_after)
            for cid_result in results:
                if 'history' in cid_result:
                    cid_result['history'].sort(key=lambda item: item['updated_at'])
                    for history in cid_result['history']:
                        origin_value, updated_value = self._parse_one_idash_history_log(
                            history, field_name, idash_field_name)
                        if origin_value == updated_value or history['updated_at'] < modified_at_after:
                            continue
                        history_records.append((cid_result['cid'], history['updated_at'], updated_value))

        return history_records

    def _update_history_records(self, history_records, field_name):
        rows = [
            [field_name, record[0], datetime.datetime.fromtimestamp(record[1]).strftime('%Y-%m-%d %H:%M:%S'), record[2]]
            for record in history_records
        ]
        query = """
        REPLACE INTO {}
        (field_name, cid, timestamp, field_value)
        VALUES (%s, %s, %s, %s)
        """.format(self.cid_config_table)
        logger.info(query)
        self.query_db.execute_query_many(query, rows, db_client=self.db_client)

    def _update_job_status_table(self, update_timestamp, field_name):
        query = """
        INSERT INTO {}
        (job_name, timestamp)
        VALUES ('{}', '{}')
        """.format(self.job_status_table, self._get_field_job_name(field_name), update_timestamp)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def update_cids_optimization_model(self, cids):
        self.update_cids_config_history(cids, 'optimization_model', 'optimization_model')

    def update_cids_config_history(self, cids, field_name, idash_field_name):
        """Update optimization_model for input cids.
        * recorded cids will only query changes since last update.
        * new cids will query in batch.
        * job status timestamp will be updated in the `job_status_table`.

        :param cids: list of cids to be updated
        """
        update_timestamp = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        modified_at_after = self.get_checkpoint_timestamp(field_name)
        recorded_cids = self.get_recorded_cids(field_name)
        history_records = []
        history_records.extend(self._get_new_cids_history(
            list(set(cids) - recorded_cids), field_name, idash_field_name))
        history_records.extend(self._get_old_cids_history(
            list(set(cids) & recorded_cids), field_name, idash_field_name, modified_at_after))
        if history_records:
            logger.info('update %s rows', len(history_records))
            self._update_history_records(history_records, field_name)
        else:
            logger.info('no campaign setting changed since last update')
        self._update_job_status_table(update_timestamp, field_name)

    def _get_cids_config_history(self, field_name, tz):
        query = "select * from {} where field_name = '{}'".format(self.cid_config_table, field_name)
        logger.info(query)
        results = self.query_db.get_query_result(query, db_client=self.db_client)
        cids_config_history = defaultdict(dict)
        for row in results:
            cids_config_history[row['cid']][row['timestamp'] + datetime.timedelta(hours=tz)] = row['field_value']

        return cids_config_history

    def _get_cid_daily_config(self, cid, cid_data, start_date, end_date, field_name):
        daily_config = {}
        matched_index = 0
        update_datetimes = sorted(cid_data.keys())
        logger.debug(update_datetimes)
        last_index = len(update_datetimes) - 1

        the_date = start_date
        while the_date <= end_date:
            # get to the last change before the date
            the_date_indices = []
            while matched_index != last_index and update_datetimes[matched_index + 1].date() < the_date:
                matched_index += 1
            if update_datetimes[matched_index].date() > the_date:
                logger.debug('cid %s on %s does not have %s', cid, the_date, field_name)
                the_date = the_date + datetime.timedelta(days=1)
                continue
            the_date_indices.append(matched_index)

            # append same date updates
            while matched_index != last_index and update_datetimes[matched_index + 1].date() == the_date:
                matched_index += 1
                the_date_indices.append(matched_index)

            if len(the_date_indices) <= 1:
                daily_config[the_date] = cid_data[update_datetimes[matched_index]]
            else:
                the_datetime = datetime.datetime.combine(the_date, datetime.time())
                time_ranges = [update_datetimes[the_date_indices[1]] - the_datetime]
                for index in xrange(1, len(the_date_indices) - 1):
                    time_ranges.append(update_datetimes[index + 1] - update_datetimes[index])
                time_ranges.append(the_datetime + datetime.timedelta(days=1) - update_datetimes[the_date_indices[-1]])
                logger.debug(the_date_indices)
                logger.debug(time_ranges)
                # use the change that uses longest in the date
                longest_index = the_date_indices[np.argmax(time_ranges)]
                daily_config[the_date] = cid_data[update_datetimes[longest_index]]

            logger.debug('cid %s use %s=%s on %s (%s)',
                         cid, field_name, daily_config[the_date], the_date, len(the_date_indices))
            the_date = the_date + datetime.timedelta(days=1)

        return daily_config

    def get_cids_daily_config(self, start_date, end_date, field_name, cids=None, tz=0, raise_exception=True):
        """Get cid's daily minor goal from start date to end date.
        :param start_datetime: start date, <datetime.date>.
        :param end_datetime: end date, <datetime.date>.
        :param field_name: the field_name to query
        :param cids: list of query cids, raise exception if any query cid does not has minor goal history.
        :param tz: timezone
        :return cid_daily_minor_goal: dict of [cid][datetime] to <MINOR_GOAL>.
        """
        cid_daily_config = defaultdict(dict)
        for cid, cid_data in self._get_cids_config_history(field_name, tz).items():
            if cids and cid not in cids:
                continue
            cid_daily_config[cid] = self._get_cid_daily_config(cid, cid_data, start_date, end_date, field_name)

        if cids and set(cid_daily_config.keys()) != set(cids):
            msg = 'missing cids: {}'.format(set(cids) - set(cid_daily_config.keys()))
            if raise_exception:
                raise RuntimeError(msg)
            logger.warning(msg)
        return cid_daily_config
