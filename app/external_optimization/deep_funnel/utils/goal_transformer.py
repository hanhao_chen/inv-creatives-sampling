from __future__ import absolute_import
from __future__ import division
import time

from app.module.CQueryDB import CQueryDB
from app.utils.partner_utils import get_df_ready_partners
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.common_utils import in_scope
from app.external_optimization.deep_funnel.utils.constants import APPIER_TO_USD

logger = init_logger(__name__)


class Transformer(object):
    LOWER_BOUND = 0.0
    UPPER_BOUND = 5.0

    def __init__(self, query_db, id_type, id_name):
        self.query_db = query_db
        self.id_type = id_type
        self.id_name = id_name
        self.avg_pamount = None
        self.goalcpa = None

    def _get_purchase_event_id(self, cids):
        query = """
        SELECT
            cid, event_id
        FROM
            {}
        WHERE
            cid in ({})
            and event_type in ('app_purchase_with_revenue', 'app_purchase')
        """.format(CQueryDB.TABLE_EVENT_ID, in_scope(cids))
        logger.debug(query)
        reply = self.query_db.get_query_result(
            query, db_client=self.query_db.get_db_client_by_table(CQueryDB.TABLE_EVENT_ID))
        cid2event_ids = {}
        for row in reply:
            cid = row['cid']
            if cid not in cid2event_ids:
                cid2event_ids[cid] = []
            cid2event_ids[cid].append(row['event_id'])

        return cid2event_ids

    def _get_last_timestamp(self, cids):
        query = """
        SELECT
            max(time) as time
        FROM
            {}
        WHERE
            cid in ({})
            and value > 0
            and time > {}
        """.format(CQueryDB.TABLE_EVENT_METRIC, in_scope(cids), time.time() - 7 * 24 * 60 * 60)
        logger.debug(query)
        reply = self.query_db.get_query_result(
            query, db_client=self.query_db.get_db_client_by_table(CQueryDB.TABLE_EVENT_METRIC))
        for row in reply:
            last_timestamp = row['time']

        return last_timestamp

    def _get_total_count_and_amount(self, cids, cid2event_ids, last_timestamp):
        query = """
        SELECT
            cid, event_id, metric, sum(value) as value
        FROM
            {}
        WHERE
            cid in ({})
            and metric in ('rng30_event_p_amount', 'rng30_event_count')
            and time >= {} - 30 * 24 * 60 * 60
        GROUP BY
            cid, event_id, metric
        """.format(CQueryDB.TABLE_EVENT_METRIC, in_scope(cids), last_timestamp)
        logger.debug(query)
        total_count, total_amount = 0, 0
        reply = self.query_db.get_query_result(
            query, db_client=self.query_db.get_db_client_by_table(CQueryDB.TABLE_EVENT_METRIC))
        for row in reply:
            cid, event_id, metric, value = row['cid'], row['event_id'], row['metric'], row['value']
            if cid not in cid2event_ids or event_id not in cid2event_ids[cid]:
                continue
            if metric == 'rng30_event_p_amount':
                total_amount += value
            elif metric == 'rng30_event_count':
                total_count += value

        return total_count, total_amount

    def _compute_avg_pamount_30d(self):
        if self.avg_pamount is not None:
            return self.avg_pamount

        cids = self._get_cids()
        if not cids:
            return None

        cid2event_ids = self._get_purchase_event_id(cids)
        last_timestamp = self._get_last_timestamp(cids)
        if last_timestamp is not None:
            total_count, total_amount = self._get_total_count_and_amount(cids, cid2event_ids, last_timestamp)
            logger.info('total_count %s total_amount %s', total_count, total_amount)
            self.avg_pamount = float(total_amount) / float(total_count) if total_count != 0 else 0
        else:
            logger.info('last_timestamp is None, cids %s', cids)
        return self.avg_pamount

    def get_roas2prat_ratio(self):
        goalcpa = self._get_goalcpa()
        avg_pamount = self._compute_avg_pamount_30d()
        if not avg_pamount or not goalcpa:
            # no data: default value 0.3 and continue to sample
            return 0.3
        ratio = min(self.UPPER_BOUND, (max(self.LOWER_BOUND, goalcpa / avg_pamount)))
        logger.info('%s %s goalcpa %s avg_pamount %s ratio %s', self.id_type, self.id_name, goalcpa, avg_pamount, ratio)
        return ratio

    def _get_cids(self):
        raise NotImplementedError

    def _get_goalcpa(self):
        raise NotImplementedError


class CidTransformer(Transformer):

    def __init__(self, query_db, cid):
        super(CidTransformer, self).__init__(query_db, 'cid', cid)
        self.cid = cid

    def _get_goalcpa(self):
        goalcpa = None
        query = """
        SELECT
            cid, goal_value
        FROM
            {}
        WHERE
            cid = '{}'
            and event_type = 'app_install'
            and type = 'CPA'
            and is_major = 1
        """.format(CQueryDB.TABLE_OPT_GOAL, self.cid)
        logger.debug(query)
        reply = self.query_db.get_query_result(
            query, db_client=self.query_db.get_db_client_by_table(CQueryDB.TABLE_OPT_GOAL))
        for row in reply:
            goalcpa = row['goal_value'] * APPIER_TO_USD

        return goalcpa

    def _get_cids(self):
        query = """
        SELECT
            cid
        FROM
            {0}
        WHERE
            ext_channel in ({1})
            and oid in (SELECT oid FROM {0} WHERE cid = '{2}')
        """.format(CQueryDB.TABLE_CAMPAIGN, in_scope(get_df_ready_partners(self.query_db)), self.cid)
        logger.debug(query)
        reply = self.query_db.get_query_result(
            query, db_client=self.query_db.get_db_client_by_table(CQueryDB.TABLE_CAMPAIGN))

        return [row['cid'] for row in reply]


class OidTransformer(Transformer):

    def __init__(self, query_db, oid):
        super(OidTransformer, self).__init__(query_db, 'oid', oid)
        self.oid = oid

    def _get_goalcpa(self):
        goalcpa = None
        query = """
        SELECT
            goalcpa
        FROM
            {}
        WHERE
            oid = '{}'
        """.format(CQueryDB.TABLE_ORDER_INFO, self.oid)
        logger.debug(query)
        reply = self.query_db.get_query_result(
            query, db_client=self.query_db.get_db_client_by_table(CQueryDB.TABLE_ORDER_INFO))
        for row in reply:
            goalcpa = row['goalcpa'] * APPIER_TO_USD

        return goalcpa

    def _get_cids(self):
        query = """
        SELECT
            cid
        FROM
            {}
        WHERE
            ext_channel in ({})
            and oid = '{}'
        """.format(CQueryDB.TABLE_CAMPAIGN, in_scope(get_df_ready_partners(self.query_db)), self.oid)
        logger.debug(query)
        reply = self.query_db.get_query_result(
            query, db_client=self.query_db.get_db_client_by_table(CQueryDB.TABLE_CAMPAIGN))
        return [row['cid'] for row in reply]
