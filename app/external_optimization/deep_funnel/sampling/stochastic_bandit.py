from __future__ import absolute_import
from __future__ import division
import datetime
import json
import calendar
import numpy as np

from app.utils.db import MySQL_DB_AI_DEEP_FUNNEL
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.sampling.mysql_queries import read_df_sampling_params_db_query, \
    update_sampling_params_db_query
from app.external_optimization.deep_funnel.utils.common_utils import gmt0_timezone


logger = init_logger(__name__)


class StochasticBandit(object):

    def __init__(self, sampling_cfg, query_db, goal_type, is_subpublisher):
        self.sampling_cfg = sampling_cfg
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.goal_type = goal_type
        self.is_subpublisher = is_subpublisher

    def read_distribution(self):
        query = read_df_sampling_params_db_query.format(
            self.goal_type, self.is_subpublisher, self.sampling_cfg['bandit_variables_table'])
        logger.info(query)
        reply = self.query_db.get_query_result(query, db_client=self.df_db_client)
        mean, cov = None, None
        for r in reply:
            variable, value = r['variable'], json.loads(r['value'])
            if variable == 'bandit_mean':
                mean = value
            elif variable == 'bandit_covariance':
                cov = value

        assert mean is not None and value is not None, 'mean {} cov {}'.format(mean, cov)
        return np.mat(mean), np.mat(cov)

    def update_one(self, sigma, r, f, mean, cov):
        sub_K = np.matmul(np.matmul(f.T, cov), f) + sigma ** 2
        K = np.matmul(cov, f) / sub_K
        H = f.T
        identity = np.identity(mean.shape[0])

        mean = (np.matmul(identity - np.matmul(K, H), mean) + K * r)
        cov = (cov - np.matmul(K, H) * cov)

        return mean.T, cov

    def update_theta(self, sigma, rewards, features, date):
        mean, cov = self.read_distribution()

        rs = np.array(rewards)
        fs = np.array(features)

        for r, f in zip(rs, fs):
            mean, cov = self.update_one(sigma, r, np.mat(f).T, mean.T, cov)

        date_ts = calendar.timegm(datetime.datetime.now(gmt0_timezone).timetuple())

        records = []
        for variable in ('bandit_mean', 'bandit_covariance'):
            if variable == 'bandit_mean':
                mean = mean.tolist()[0]
                mean = [float('%e' % m) for m in mean]
                value = str(mean)
            elif variable == 'bandit_covariance':
                cov = cov.tolist()
                for index, rc in enumerate(cov):
                    cov[index] = [float('%e' % c) for c in rc]
                value = str(cov)
            else:
                assert False, 'variable name is not defined {}'.format(variable)
            record = (date, date_ts, self.goal_type, self.is_subpublisher, variable, value)
            records.append(record)

        query = update_sampling_params_db_query.format(self.sampling_cfg['bandit_variables_table'])
        logger.info(query)
        self.query_db.execute_query_many(query, records, db_client=self.df_db_client)
        return mean, cov

    def sample_theta(self):
        mean, cov = self.read_distribution()
        return np.random.multivariate_normal(np.array(mean.tolist()[0]), cov, 1).T, mean.T
