from __future__ import absolute_import

from app.utils.init_logger import init_logger

logger = init_logger(__name__)


def combine_partition2performance(a_partition2performance, b_partition2performance, balance=0.80):
    c_partition2performance = Partition2Performance()
    for local_partition in a_partition2performance.get_local_partitions():
        install, event = a_partition2performance.get_performance(local_partition)
        c_partition2performance.add_performance(local_partition, install, event)

    for local_partition in b_partition2performance.get_local_partitions():
        install, event = b_partition2performance.get_performance(local_partition)
        c_partition2performance.add_performance(local_partition, install * balance, event * balance)

    return c_partition2performance


class Partition2Performance(object):

    INSTALL_INDEX = 0
    EVENT_INDEX = 1
    EMPTY_PERFORMANCE = (0, 0)

    def __init__(self):
        self.local_partition2performance = {}
        self.global_partition2performance = {}
        self.smooth_partition2performance = {}

    def _init_partition(self, local_partition):
        global_partition = local_partition[:-1]
        smooth_partition = local_partition[:-2]
        if local_partition not in self.local_partition2performance:
            self.local_partition2performance[local_partition] = list(self.EMPTY_PERFORMANCE)
        if global_partition not in self.global_partition2performance:
            self.global_partition2performance[global_partition] = list(self.EMPTY_PERFORMANCE)
        if smooth_partition not in self.smooth_partition2performance:
            self.smooth_partition2performance[smooth_partition] = list(self.EMPTY_PERFORMANCE)

    def _add_performance(self, local_partition, install, event):
        global_partition = local_partition[:-1]
        smooth_partition = local_partition[:-2]
        self.local_partition2performance[local_partition][self.INSTALL_INDEX] += install
        self.local_partition2performance[local_partition][self.EVENT_INDEX] += event
        self.global_partition2performance[global_partition][self.INSTALL_INDEX] += install
        self.global_partition2performance[global_partition][self.EVENT_INDEX] += event
        self.smooth_partition2performance[smooth_partition][self.INSTALL_INDEX] += install
        self.smooth_partition2performance[smooth_partition][self.EVENT_INDEX] += event

    def add_performance(self, local_partition, install, event):
        self._init_partition(local_partition)
        self._add_performance(local_partition, install, event)

    def add_local_partition_performance(self, target_local_partition, source_local_partition):
        if source_local_partition in self.local_partition2performance:
            if target_local_partition not in self.local_partition2performance:
                self.local_partition2performance[target_local_partition] = list(self.EMPTY_PERFORMANCE)
            self.local_partition2performance[target_local_partition][self.INSTALL_INDEX] \
                += self.local_partition2performance[source_local_partition][self.INSTALL_INDEX]
            self.local_partition2performance[target_local_partition][self.EVENT_INDEX] \
                += self.local_partition2performance[source_local_partition][self.EVENT_INDEX]

    def get_performance(self, partition):
        assert 3 <= len(partition) <= 5, 'The length of partition is wrong: {}'.format(partition)
        if len(partition) == 5 and partition in self.local_partition2performance:
            return self.local_partition2performance[partition]
        elif len(partition) == 4 and partition in self.global_partition2performance:
            return self.global_partition2performance[partition]
        elif len(partition) == 3 and partition in self.smooth_partition2performance:
            return self.smooth_partition2performance[partition]
        else:
            return list(self.EMPTY_PERFORMANCE)

    def get_local_partitions(self):
        return list(self.local_partition2performance.keys())

    def get_global_partitions(self):
        return list(self.global_partition2performance.keys())

    def get_smooth_partitions(self):
        return list(self.smooth_partition2performance.keys())


class Partition2PredictionPerformance(Partition2Performance):

    def __init__(self):
        super(Partition2PredictionPerformance, self).__init__()
        self.local_partition2prediction = {}
        self.local_partition2db_global_performance = {}
        self.local_partition2db_smooth_performance = {}
        self.local_partition2db_pass_through_performance = {}

    def add_performance_with_db_batch(self, local_partition, install, event, db_batch):
        self.add_performance(local_partition, install, event)
        (prediction, db_global_prediction, db_category_install, db_category_event, db_smooth_install, db_smooth_event,
         db_d_cam_event, db_d_cam_install, db_d_cam_rate, db_d_cat_event, db_d_cat_install, db_d_cat_rate,
         db_d_cou_event, db_d_cou_install, db_d_cou_rate) = db_batch
        self.local_partition2prediction[local_partition] = (prediction, db_global_prediction)
        self.local_partition2db_global_performance[local_partition] = (db_category_install, db_category_event)
        self.local_partition2db_smooth_performance[local_partition] = (db_smooth_install, db_smooth_event)
        self.local_partition2db_pass_through_performance[local_partition] = \
            (db_d_cam_event, db_d_cam_install, db_d_cam_rate, db_d_cat_event, db_d_cat_install,
             db_d_cat_rate, db_d_cou_event, db_d_cou_install, db_d_cou_rate)

    def get_prediction(self, local_partition):
        return self.local_partition2prediction[local_partition] \
         if local_partition in self.local_partition2prediction else list(self.EMPTY_PERFORMANCE)

    def get_db_category_performance(self, local_partition):
        return self.local_partition2db_global_performance[local_partition] \
         if local_partition in self.local_partition2db_global_performance else list(self.EMPTY_PERFORMANCE)

    def get_db_smooth_performance(self, local_partition):
        return self.local_partition2db_smooth_performance[local_partition] \
         if local_partition in self.local_partition2db_smooth_performance else list(self.EMPTY_PERFORMANCE)

    def get_db_pass_through_performance(self, local_partition):
        return self.local_partition2db_pass_through_performance[local_partition] \
         if local_partition in self.local_partition2db_pass_through_performance else [0, 0, 0, 0, 0, 0, 0, 0, 0]
