df_sampling_performance_db_query = """
SELECT
    oid,
    partner_id,
    event_type,
    app_id,
    minor_count,
    major_count
FROM
    {table}
WHERE
    date = {date}
    AND period = {period}
    AND goal_type = '{goal_type}'
    AND is_subpublisher = {is_subpublisher}
"""

df_prediction_and_performance_db_query = """
SELECT
    event_type,
    oid,
    country,
    partner_id as channel,
    app_id,
    prediction,
    global_prediction,
    minor_count30D as event_30d,
    major_count30D as install_30d,
    category_minor_count30D as global_event_30d,
    category_major_count30D as global_install_30d,
    decay_minor_count30D as db_d_cam_event,
    decay_major_count30D as db_d_cam_install,
    decay_event_rate30D as db_d_cam_rate,
    decay_category_minor_count30D as db_d_cat_event,
    decay_category_major_count30D as db_d_cat_install,
    decay_category_event_rate30D as db_d_cat_rate,
    decay_country_minor_count30D as db_d_cou_event,
    decay_country_major_count30D as db_d_cou_install,
    decay_country_event_rate30D as db_d_cou_rate
FROM
    {4}
WHERE
    date = {0}
    and goal_type = '{3}'
    and partner_id in ({1})
    and is_subpublisher = {2}
    and (category_major_count30D > 0 or oid = '{5}')
    and global_prediction > 0
"""

latest_installs_db_query = """
SELECT
    cid,
    app_id,
    count(*) as latest_install
FROM
    {0}
WHERE
    action_time >= {1}
    and action_time < ({1} + 86400)
    and cid in ({2})
GROUP BY
    cid,
    app_id
"""

read_df_sampling_params_db_query = """
SELECT
    variable,
    value
FROM
    {2}
WHERE
    variable in ('bandit_mean', 'bandit_covariance')
    and time = (SELECT
                    max(time) as time
                FROM
                    {2}
                WHERE
                    variable in ('bandit_mean', 'bandit_covariance')
                    and goal_type = '{0}'
                    and is_subpublisher = {1})
    and goal_type = '{0}'
    and is_subpublisher = {1}
"""

update_sampling_prediction_db_query = """
REPLACE INTO {}
(date, event_type, goal_type, is_subpublisher, oid, country, partner_id, app_id, prediction,
 global_prediction, mean_value, sampled_value, latest_install, latest_global_install, decay_local_minor_count30D,
 decay_local_major_count30D, decay_global_minor_count30D, decay_global_major_count30D,
 minor_count30D, major_count30D, global_minor_count30D, global_major_count30D, smoothing_decay_event_rate,
 smoothing_decay_global_event_rate, smoothing_event_rate, smoothing_global_event_rate, decay_minor_count30D,
 decay_major_count30D, decay_event_rate30D, decay_category_minor_count30D, decay_category_major_count30D,
 decay_category_event_rate30D, decay_country_minor_count30D,
 decay_country_major_count30D, decay_country_event_rate30D)
 VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
         %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
"""

update_sampling_params_db_query = """
INSERT INTO {}
(date, time, goal_type, is_subpublisher, variable, value)
VALUES (%s, %s, %s, %s, %s, %s)
"""
