from __future__ import absolute_import
from __future__ import division
import datetime
import os
import calendar
import numpy as np
import pickle
from collections import defaultdict

from app.utils.db import MySQL_DB_AI_DEEP_FUNNEL, MySQL_DB_AI_HYDRA
from app.module.CQueryDB import CQueryDB
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.sampling.performance import Partition2Performance, \
    Partition2PredictionPerformance, combine_partition2performance
from app.external_optimization.deep_funnel.sampling.mysql_queries import df_prediction_and_performance_db_query, \
    latest_installs_db_query, update_sampling_prediction_db_query, df_sampling_performance_db_query
from app.external_optimization.deep_funnel.sampling.stochastic_bandit import StochasticBandit
from app.external_optimization.deep_funnel.utils.common_utils import get_external_capability, get_enabled_oids_info, \
    in_scope, get_cid2channel, get_pub_sub_partners, string_hash, get_job_latest_datetime
from app.external_optimization.deep_funnel.utils.constants import DEFAULT_PREFIX, DEFAULT_CATEGORY, DEFAULT_COUNTRY


logger = init_logger(__name__)


def smooth_value(value):
    return value ** 0.5 if value >= 0 else 0


def smooth_values(values):
    return [smooth_value(value) for value in values]


def get_smooth_term(high_priority_performance, low_priority_performance):
    hp_install, hp_event = high_priority_performance
    lp_install, lp_event = low_priority_performance
    if hp_event != 0:
        return hp_install / hp_event
    elif lp_event != 0:
        return lp_install / lp_event
    return 0


class DeepFunnelSampling(object):
    BANDIT_SIGMA = 0.1
    DEBUG = False

    def __init__(self, sampling_cfg, query_db, update_task, appid_ruler, data_reference):
        self.sampling_cfg = sampling_cfg
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.hydra_db_client = query_db.db_clients[MySQL_DB_AI_HYDRA]
        self.update_task = update_task
        self.appid_ruler = appid_ruler
        self.oid_data_reference = data_reference['oid']

        self.partner_capabilities = get_external_capability(query_db)
        # AI-7293: add pseudo partners into query list for list_control usage
        for original_partner, new_partner in data_reference['partner_replaced_by'].items():
            self.partner_capabilities[new_partner] = self.partner_capabilities[original_partner]
        self.publisher_partners, self.subpublisher_partners = get_pub_sub_partners(self.partner_capabilities)
        self.target_partners = self.publisher_partners \
            if update_task.is_subpublisher == 0 else self.subpublisher_partners

        self.valid_end_date = datetime.datetime.strptime(update_task.valid_end_date_str, '%Y%m%d')
        self.valid_start_date = self.valid_end_date + datetime.timedelta(days=-update_task.valid_days+1)
        self.train_end_date = self.valid_start_date + datetime.timedelta(days=-1)
        self.train_start_date = self.train_end_date + datetime.timedelta(days=-update_task.train_days)

        valid_end_ts = calendar.timegm(self.valid_end_date.timetuple())
        train_start_ts = calendar.timegm(self.train_start_date.timetuple())
        self.oid_info = get_enabled_oids_info(
            query_db, train_start_ts, valid_end_ts, self.target_partners, self.oid_data_reference)

    def get_reversed_oid_data_reference(self):
        reversed_oid_data_reference = defaultdict(set)
        if self.oid_data_reference:
            for target_oid, refer_oids in self.oid_data_reference.items():
                for source_oid in refer_oids:
                    reversed_oid_data_reference[source_oid].add(target_oid)

        return reversed_oid_data_reference

    def solve_bandit_and_update_prediction(self):
        logger.info('get performance data')
        train_partition2performance, valid_partition2performance = self._get_performance_data()

        logger.info('load prediction data')
        train_partition2db_pp, train_oid2event_types = self.get_prediction_and_performance(self.train_end_date)
        valid_partition2db_pp, valid_oid2event_types = self.get_prediction_and_performance(self.valid_end_date)

        oid2event_types = {
            oid: train_oid2event_types.get(oid, set()) | valid_oid2event_types.get(oid, set())
            for oid in set(list(train_oid2event_types.keys()) + list(valid_oid2event_types.keys()))
        }

        logger.info('get latest install')
        (meta2latest_install, global_meta2latest_install,
         ext_smooth_meta2app_ids) = self.get_latest_install(oid2event_types)

        logger.info('combine partitions')
        online_partition2performance = combine_partition2performance(
            valid_partition2performance, train_partition2performance, 0.70)

        logger.info('generate training instances')
        _train_metas, train_rewards, train_features = self.generate_instance(
            train_partition2db_pp, train_partition2performance, valid_partition2performance, {}, {})

        logger.info('update theta')
        stochastic_bandit = StochasticBandit(
            self.sampling_cfg, self.query_db, self.update_task.goal_type, self.update_task.is_subpublisher)
        stochastic_bandit.update_theta(
            self.BANDIT_SIGMA, train_rewards, train_features, self.update_task.valid_end_date_str)

        logger.info('sample theta')
        theta, theta_mean = stochastic_bandit.sample_theta()

        logger.info('generate online instances')
        online_metas, _, online_features = self.generate_instance(
            valid_partition2db_pp, online_partition2performance, Partition2Performance(), meta2latest_install,
            global_meta2latest_install)

        logger.info('add extention set to prediction')
        extention_metas, extention_features = self.add_latest_app_id_to_extention(
            set(online_metas), oid2event_types, online_partition2performance, meta2latest_install,
            global_meta2latest_install, ext_smooth_meta2app_ids)
        online_metas.extend(extention_metas)
        online_features.extend(extention_features)

        sampled_values = np.dot(np.array(online_features), theta).tolist()
        mean_values = np.dot(np.array(online_features), theta_mean).tolist()

        logger.info('get results to DB')
        rows = self.get_sampled_values_data(
            self.update_task.valid_end_date_str, online_metas, online_features, sampled_values, mean_values,
            valid_partition2db_pp, online_partition2performance, meta2latest_install, global_meta2latest_install)

        return rows

    def _get_performance_data(self):
        oid2country = self.oid_info['country']
        valid_date = get_job_latest_datetime(
            self.query_db, self.sampling_cfg['performance_data_table'], self.valid_end_date, tolerance=2)
        train_date = valid_date - datetime.timedelta(days=self.update_task.valid_days)

        train_partition2performance = Partition2Performance()
        valid_partition2performance = Partition2Performance()
        reversed_oid_data_reference = self.get_reversed_oid_data_reference()
        for query_date, period, p2p in [(train_date, self.update_task.train_days, train_partition2performance),
                                        (valid_date, self.update_task.valid_days, valid_partition2performance)]:
            query = df_sampling_performance_db_query.format(
                table=self.sampling_cfg['performance_data_table'], date=query_date.strftime('%Y%m%d'), period=period,
                goal_type=self.update_task.goal_type, is_subpublisher=self.update_task.is_subpublisher)
            logger.info(query)

            refer_metas = defaultdict(set)
            for row in self.query_db.get_query_result(query, db_client=self.hydra_db_client):
                if row['oid'] not in oid2country:
                    continue
                local_meta = (oid2country[row['oid']], row['partner_id'], row['event_type'], row['app_id'], row['oid'])
                p2p.add_performance(local_meta, row['major_count'], row['minor_count'])
                if row['oid'] in reversed_oid_data_reference:
                    for target_oid in reversed_oid_data_reference[row['oid']]:
                        target_local_meta = tuple(list(local_meta[:-1]) + [target_oid])
                        refer_metas[local_meta].add(target_local_meta)

            for source_local_meta, targets in refer_metas.items():
                for target_local_meta in targets:
                    p2p.add_local_partition_performance(target_local_meta, source_local_meta)

        return train_partition2performance, valid_partition2performance

    def get_prediction_and_performance(self, target_date):
        """Check and get data from minor_goal pickles
        :param target_date: target date of cache file <datetime>
        :return pickled_data: partition2db_pp and oid2event_types
        """
        cache_file = self._get_cachefile_name(target_date.strftime('%Y%m%d'))
        if not os.path.exists(cache_file):
            logger.info('Target cache file (%s) is not ready.', cache_file)
            date_obj = get_job_latest_datetime(
                self.query_db, self.sampling_cfg['quality_prediction_table'], target_date)
            cache_file = self._get_cachefile_name(date_obj.strftime('%Y%m%d'))
            if not os.path.exists(cache_file):
                self._pickle_prediction_and_performance(date_obj.strftime('%Y%m%d'))

        logger.info('Load from cache file %s.', cache_file)
        with open(cache_file, 'rb') as pf:
            return pickle.load(pf)

    def _pickle_prediction_and_performance(self, date_str):
        query = df_prediction_and_performance_db_query.format(
            date_str, in_scope(self.target_partners), self.update_task.is_subpublisher,
            self.update_task.goal_type, self.sampling_cfg['quality_prediction_table'], DEFAULT_COUNTRY)
        logger.info(query)
        reply = self.query_db.get_query_result(query, db_client=self.hydra_db_client)

        active_oids = self.oid_info['active_oids']
        partition2db_pp = Partition2PredictionPerformance()
        oid2event_types = {}
        for row in reply:
            oid = row['oid']
            if oid not in active_oids and not oid.startswith(DEFAULT_PREFIX):
                continue
            event_type = row['event_type']
            meta = (row['country'], row['channel'], event_type, row['app_id'], oid)
            db_batch = (row['prediction'], row['global_prediction'],
                        row['global_install_30d'], row['global_event_30d'], 0, 0,
                        row['db_d_cam_event'], row['db_d_cam_install'], row['db_d_cam_rate'],
                        row['db_d_cat_event'], row['db_d_cat_install'], row['db_d_cat_rate'],
                        row['db_d_cou_event'], row['db_d_cou_install'], row['db_d_cou_rate'])
            partition2db_pp.add_performance_with_db_batch(meta, row['install_30d'], row['event_30d'], db_batch)

            if oid not in oid2event_types:
                oid2event_types[oid] = set()
            oid2event_types[oid].add(event_type)

        if partition2db_pp.get_local_partitions():
            cache_file = self._get_cachefile_name(date_str)
            pickle.dump([partition2db_pp, oid2event_types], open(cache_file, 'wb'))
            logger.info('Dump minor_goal predictions to %s.', cache_file)
            query = "REPLACE INTO job_update_status (job_name, date) VALUES ('{}', {})".format(
                self.sampling_cfg['df_prediction_table'], date_str)
            logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)

    def _get_cachefile_name(self, date_str):
        return os.path.join(self.update_task.cache_data_path,
                            '{}_{}_{}_is_sub{}.pkl'.format(
                                self.sampling_cfg['bandit_cachefile_prefix'],
                                date_str, self.update_task.goal_type, self.update_task.is_subpublisher))

    def get_latest_install(self, oid2event_types):
        # TODO: should we use oid_reference on latest installs ?
        oid2country, oid2cids, oid2category = self.oid_info['country'], self.oid_info['cids'], self.oid_info['category']
        cid2oid = {cid: oid for oid in oid2cids for cid in oid2cids[oid]}
        cid2channel = get_cid2channel(self.query_db)
        date_ts = calendar.timegm(self.valid_end_date.timetuple())

        query = latest_installs_db_query.format(
            CQueryDB.TABLE_EXT_AI_PERDAY_ACTION_TIME, date_ts, in_scope(cid2oid.keys()))
        logger.debug(query)
        reply = self.query_db.get_query_result(
            query, db_client=self.query_db.get_db_client_by_table(CQueryDB.TABLE_EXT_AI_PERDAY_ACTION_TIME))

        meta2latest_install = defaultdict(float)
        global_meta2latest_install = defaultdict(float)
        coldstart_meta2latest_install = defaultdict(float)
        ext_smooth_meta2app_ids = {}
        for r in reply:
            cid, app_id, latest_install = r['cid'], r['app_id'], r['latest_install']
            oid = cid2oid[cid]
            country = oid2country[oid]
            channel = cid2channel[cid]
            if channel in self.publisher_partners and self.update_task.is_subpublisher == 0:
                bid_pub_id, _bid_sub_id = self.appid_ruler.get_pubid_subid_from_appid(
                    channel, app_id)
                app_id = bid_pub_id
            if not app_id:
                continue

            latest_install_meta = (country, channel, app_id, oid)
            meta2latest_install[latest_install_meta] += latest_install
            # for prat-30d training, the campaign only involves rr-1d (app_open) would not be considered
            if oid not in oid2event_types:
                continue
            category = oid2category[oid]
            for event_type in oid2event_types[oid]:
                global_meta = (country, channel, event_type, app_id)
                global_meta2latest_install[global_meta] += latest_install
                # add to latest install extention set
                ext_smooth_meta = (category, country, channel, event_type)
                ext_smooth_meta2app_ids.setdefault(ext_smooth_meta, set()).add(app_id)

            top_category, sub_category = category
            concatenate_category = (top_category + '_' + sub_category) if sub_category else top_category
            for category in {top_category, concatenate_category}:
                category_meta = (country, channel, app_id, DEFAULT_CATEGORY + string_hash(category))
                coldstart_meta2latest_install[category_meta] += latest_install

            country_meta = (country, channel, app_id, DEFAULT_COUNTRY)
            coldstart_meta2latest_install[country_meta] += latest_install

        meta2latest_install.update(coldstart_meta2latest_install)

        return meta2latest_install, global_meta2latest_install, ext_smooth_meta2app_ids

    def generate_instance(self, train_partition2db_pp, train_partition2performance, valid_partition2performance,
                          meta2latest_install, global_meta2latest_install):
        metas = []
        rewards = []
        features = []
        global_ratio = 0.7
        smooth_ratio = 1 - global_ratio
        valid_local_partitions = len(valid_partition2performance.get_local_partitions())
        for meta in train_partition2db_pp.get_local_partitions():
            # reward: key by train, label by local + add 1 global + add 0.5 smooth
            valid_install, valid_event = valid_partition2performance.get_performance(meta)

            # training will stop if no validation part; valid will always pass this condition
            if valid_local_partitions != 0 and valid_install == 0:
                continue

            # smooth term
            global_meta, smooth_meta = meta[:-1], meta[:-2]

            add_one_global_install = get_smooth_term(train_partition2performance.get_performance(global_meta),
                                                     valid_partition2performance.get_performance(global_meta))
            add_one_smooth_install = get_smooth_term(train_partition2performance.get_performance(smooth_meta),
                                                     valid_partition2performance.get_performance(smooth_meta))

            db_add_one_global_install = get_smooth_term(train_partition2db_pp.get_performance(global_meta),
                                                        list(Partition2Performance.EMPTY_PERFORMANCE))
            db_add_one_smooth_install = get_smooth_term(train_partition2db_pp.get_performance(smooth_meta),
                                                        list(Partition2Performance.EMPTY_PERFORMANCE))

            # smooth for valid reward
            if add_one_global_install != 0:
                valid_install += global_ratio * add_one_global_install
                valid_event += global_ratio
            if add_one_smooth_install != 0:
                valid_install += smooth_ratio * add_one_smooth_install
                valid_event += smooth_ratio

            reward = valid_event / valid_install if valid_install != 0 else 0
            rewards.append(smooth_value(reward))
            metas.append(meta)

            # feature
            _prediction, global_prediction = train_partition2db_pp.get_prediction(meta)
            train_install, train_event = train_partition2performance.get_performance(meta)
            train_global_install, train_global_event = train_partition2performance.get_performance(global_meta)
            nd_train_install, nd_train_event = train_partition2db_pp.get_performance(meta)
            nd_train_category_install, nd_train_category_event = train_partition2db_pp.get_db_category_performance(meta)

            # for local install
            country, channel, _event_type, app_id, oid = meta
            latest_install_meta = (country, channel, app_id, oid)

            # smooth for train feature
            if add_one_global_install != 0:
                train_install += global_ratio * add_one_global_install
                train_event += global_ratio
            if db_add_one_global_install != 0:
                nd_train_install += global_ratio * db_add_one_global_install
                nd_train_event += global_ratio

            if add_one_smooth_install != 0:
                train_install += smooth_ratio * add_one_smooth_install
                train_event += smooth_ratio
                train_global_install += smooth_ratio * add_one_smooth_install
                train_global_event += smooth_ratio
            if db_add_one_smooth_install != 0:
                nd_train_install += smooth_ratio * db_add_one_smooth_install
                nd_train_event += smooth_ratio
                nd_train_category_install += smooth_ratio * db_add_one_smooth_install
                nd_train_category_event += smooth_ratio

            # for install burst
            if latest_install_meta in meta2latest_install:
                latest_install = meta2latest_install[latest_install_meta]
                train_install += latest_install
                nd_train_install += latest_install

            if global_meta in global_meta2latest_install:
                latest_global_install = global_meta2latest_install[global_meta]
                train_global_install += latest_global_install
                nd_train_category_install += latest_global_install

            train_rate = train_event / train_install if train_install != 0 else 0
            train_global_rate = train_global_event / train_global_install if train_global_install != 0 else 0
            nd_train_rate = nd_train_event / nd_train_install if nd_train_install != 0 else 0
            nd_train_category_rate = nd_train_category_event / nd_train_category_install \
                if nd_train_category_install != 0 else 0

            feature = smooth_values([global_prediction, train_rate, train_global_rate, nd_train_rate,
                                     nd_train_category_rate])
            features.append(feature)

        return metas, rewards, features

    def add_latest_app_id_to_extention(self, online_metas_set, oid2event_types, online_partition2performance,
                                       meta2latest_install, global_meta2latest_install, ext_smooth_meta2app_ids):
        metas = []
        features = []
        oid2country = self.oid_info['country']
        oid2channels = self.oid_info['channels']
        oid2category = self.oid_info['category']

        # AI-7609: Don't create rows for apps not belonging to this group. Let list_control query default entries.
        online_campaign_group = {(country, channel, event_type, oid)
                                 for (country, channel, event_type, _app_id, oid) in online_metas_set}
        for oid in oid2event_types:
            # run, run and paused
            if oid not in oid2country:
                logger.warning('[PAUSE] oid %s', oid)
                continue
            logger.info('add oid %s', oid)
            country = oid2country[oid]
            channels = oid2channels[oid]
            category = oid2category[oid]
            cnt = 0
            for channel in channels:
                for event_type in oid2event_types[oid]:
                    if (country, channel, event_type, oid) not in online_campaign_group:
                        continue
                    smooth_meta = (country, channel, event_type)
                    ext_smooth_meta = (category, country, channel, event_type)
                    if ext_smooth_meta not in ext_smooth_meta2app_ids:
                        continue
                    smooth_install, smooth_event = online_partition2performance.get_performance(smooth_meta)
                    for app_id in ext_smooth_meta2app_ids[ext_smooth_meta]:
                        meta = (country, channel, event_type, app_id, oid)
                        if meta in online_metas_set:
                            continue
                        cnt += 1
                        # generate meta
                        global_meta = meta[:-1]
                        latest_install_meta = (country, channel, app_id, oid)
                        # find features
                        prediction = 0
                        latest_install = meta2latest_install[latest_install_meta] \
                            if latest_install_meta in meta2latest_install else 0
                        latest_global_install = global_meta2latest_install[global_meta] \
                            if global_meta in global_meta2latest_install else 0
                        event, global_event = 0, 0
                        if smooth_event != 0:
                            latest_install += smooth_install / smooth_event
                            latest_global_install += smooth_install / smooth_event
                            event += 1
                            global_event += 1
                        rate = event / latest_install if latest_install != 0 else 0
                        global_rate = global_event / latest_global_install
                        # the same performance for decay/non-decay features
                        feature = [prediction, rate, global_rate, rate, global_rate]
                        metas.append(meta)
                        features.append(feature)
            logger.info('oid %s process %s instances', oid, cnt)

        return metas, features

    def get_sampled_values_data(self, date_str, metas, features, sampled_values, mean_values, online_partition2db_pp,
                                decay_partition2performance, meta2latest_install, global_meta2latest_install):
        rows = []
        for meta, feature, sampled_value, mean_value in zip(metas, features, sampled_values, mean_values):
            global_meta = meta[:-1]
            country, channel, event_type, app_id, oid = meta
            latest_install_meta = (country, channel, app_id, oid)

            latest_install = meta2latest_install[latest_install_meta] \
                if latest_install_meta in meta2latest_install else 0
            latest_global_install = global_meta2latest_install[global_meta] \
                if global_meta in global_meta2latest_install else 0

            decay_local_install, decay_local_event = decay_partition2performance.get_performance(meta)
            decay_global_install, decay_global_event = decay_partition2performance.get_performance(global_meta)

            prediction, global_prediction = online_partition2db_pp.get_prediction(meta)
            local_install, local_event = online_partition2db_pp.get_performance(meta)
            category_install, category_event = online_partition2db_pp.get_db_category_performance(meta)
            (db_d_cam_event, db_d_cam_install, db_d_cam_rate, db_d_cat_event,
             db_d_cat_install, db_d_cat_rate, db_d_cou_event, db_d_cou_install,
             db_d_cou_rate) = online_partition2db_pp.get_db_pass_through_performance(meta)

            _, f_local_rate, f_global_rate, f_nd_local_rate, f_nd_global_rate = feature

            row = [
                date_str, event_type, self.update_task.goal_type, self.update_task.is_subpublisher,
                oid, country, channel, app_id, prediction,
                global_prediction, mean_value, sampled_value, latest_install, latest_global_install,
                decay_local_event, decay_local_install, decay_global_event, decay_global_install,
                local_event, local_install, category_event, category_install, f_local_rate, f_global_rate,
                f_nd_local_rate, f_nd_global_rate, db_d_cam_event, db_d_cam_install, db_d_cam_rate,
                db_d_cat_event, db_d_cat_install, db_d_cat_rate, db_d_cou_event, db_d_cou_install, db_d_cou_rate
            ]
            rows.append(row)

        return rows

    def sync_sampled_values_to_db(self, rows):
        query = update_sampling_prediction_db_query.format(self.sampling_cfg['df_prediction_table'])
        logger.info(query)
        self.query_db.execute_query_many(query, rows, db_client=self.df_db_client)
