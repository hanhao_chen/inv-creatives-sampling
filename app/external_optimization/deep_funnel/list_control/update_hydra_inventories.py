# TODO: refactor this module
from __future__ import absolute_import, division
import json
import time
from collections import namedtuple, defaultdict

from app.utils.db import MySQL_DB_AI_DEEP_FUNNEL, MySQL_DB_CAMPAIGN
from app.module.CCampaignInfo import CCampaignInfo
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.list_control.hydra_slack_alerts_agent import ALERT, HydraSlackAlertsAgent
from app.external_optimization.deep_funnel.utils.constants import APPIER_TO_USD, INVALID_MULTIPLE_MAJORS, \
    INVALID_MAJOR_TYPE, INVALID_MINOR_GOAL, INVALID_EMPTY_LIST, INVALID_PARTNER, INVALID_GOAL_VALUE, \
    INVALID_PRIMARY_GOAL, INVALID_VERTICAL, INVALID_NO_INSTALL_EVENT, INVALID_CPA_PARTNER, \
    VALID_REOPEN, HYDRA_ROAS_MODELS
from app.external_optimization.deep_funnel.utils.common_utils import AppUsage, get_external_capability, \
    get_cpa_enabled_partner_ids
from app.external_optimization.deep_funnel.utils.optimization_goal_query_agent import CidGoalQueryAgent, OidGoalQueryAgent

logger = init_logger(__name__)


def is_valid_minor_goal_type(goal_type):
    return goal_type.startswith('PRAT') or goal_type.startswith('RR') or goal_type.startswith('ROAS')


class Hydra(object):
    # TODO: avoid access variables in self.hydra_tree directly
    DEBUG = False
    DF_ADGROUP_PARAMS = namedtuple(
        'deep_funnel_cid_params',
        ['cid', 'oid', 'use_global_data', 'partner', 'event_type', 'goal_type', 'event_goal', 'app_usage',
         'do_cid_update', 'raw_goal_type', 'raw_event_goal', 'event_ids', 'partner_cids', 'min_major_count',
         'category_tuple', 'country', 'is_blacklist_cid', 'use_roas_filter', 'roas_goal_type', 'roas_event_ids']
    )

    def __init__(self, listctrl_cfg, idash_endpoint, query_db, hydra_tree, hydra_list_controller, cid2minor_goal=None):
        self.idash_endpoint = idash_endpoint
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.hydra_tree = hydra_tree
        self.hydra_list_controller = hydra_list_controller
        self.cid_status_table = listctrl_cfg['df_cid_status_table']
        self.garbage_cid_table = listctrl_cfg['hydra_garbage_cid_table']
        self.cid_goal_query_agent = CidGoalQueryAgent(self.query_db)

        self.partner_capabilities = get_external_capability(self.query_db)
        self.cpa_enabled_partners = get_cpa_enabled_partner_ids(self.query_db)
        self.goal_params_cache = {}
        # For experiment: if not None, cid params will use historical goal setting in it.
        self.cid2minor_goal = cid2minor_goal

    def cid_is_finished(self, cid):
        end_at = self.hydra_tree.cids_config.get(cid, {}).get('end_at') or 0
        return end_at < int(time.time())

    def update_df_adgroup_status(self, cid, enabled, reason=''):
        assert enabled in (0, 1)
        success = self.idash_endpoint.send_cid_enable_request(
            cid, self.hydra_tree.cids_config[cid]['ext_channel'], enabled == 1) \
            if not Hydra.DEBUG else True
        if success:
            query = 'insert into {0} (cid, enabled, reason) values (\'{1}\', {2}, \'{3}\')'.format(
                self.cid_status_table, cid, enabled, reason)
            logger.info(query)
            self.query_db.execute_query(query, db_client=self.df_db_client)

    def check_adgroup_copying(self, cid, copy_type, params):
        n_day = 30 if params.raw_goal_type in ('PRAT', 'ROAS') else 7
        performance = self.hydra_list_controller.get_hydra_performance_cache().get_cid_performance_data(
            params, n_day=n_day, decay_rate=1.0, install_decayed=False)
        do_cid_update = True
        if copy_type in ('stable_campaign_white', 'stable_sampling_white', 'stable_sampling_black') \
                and not params.app_usage.is_black_usage and performance['installs'] > 100 \
                and performance['event_rate'] > params.raw_event_goal * 1.05:
            logger.info('cid %s triggers %s since %s %s', cid, copy_type, performance, params)
            if not self.DEBUG and not self.cid_is_finished(cid):
                self.hydra_tree.trigger_copy_event(copy_type)
                do_cid_update = False
        # TODO: AI-4697 implement performance condition for cid reconstruction
        # elif copy_type in ('reconstruct_campaign_white', 'reconstruct_sampling_white'):
        #    if not self.DEBUG and not self.cid_is_finished(cid):
        #        self.hydra_tree.trigger_copy_event(copy_type)
        #        do_cid_update = False

        return do_cid_update

    def check_unstable_adgroup(self, cid, copy_types, is_exp, is_blacklist_cid, hydra_label, slack_alerts):
        do_cid_update = self.hydra_tree.able_to_update(cid)
        initial_params = performance_params = None
        if self.hydra_tree.is_valid_cid(cid):
            do_cid_update |= is_exp
            if do_cid_update:
                initial_params, invalid_reason = self.get_deep_funnel_list_control_params(
                    cid, 'global', is_blacklist_cid)
                performance_params, invalid_reason = self.get_deep_funnel_list_control_params(
                    cid, 'campaign', is_blacklist_cid)
                if not initial_params:
                    logger.warning('cid: %s has unexpected goal_type', cid)
                    slack_alerts.append(self.make_slack_alert(
                        'invalid', cid, hydra_label, is_blacklist_cid, invalid_reason.split(',')))
                    self.update_df_adgroup_status(cid, enabled=0, reason=invalid_reason)
                    do_cid_update = False
                elif not self.hydra_tree.is_transparent():
                    for copy_type in copy_types:
                        if do_cid_update:
                            do_cid_update &= self.check_adgroup_copying(cid, copy_type, initial_params)
            elif self.hydra_tree.cids_status[cid]['cid_enabled'] is None \
                    and self.hydra_tree.cids_status[cid]['is_deep_funnel']:
                logger.warning('cid: %s not in DF partners %s', cid, self.hydra_tree.cids_config[cid]['ext_channel'])
                slack_alerts.append(self.make_slack_alert(
                    'invalid', cid, hydra_label, is_blacklist_cid, [INVALID_PARTNER]))
                self.update_df_adgroup_status(cid, enabled=0, reason=INVALID_PARTNER)

        return do_cid_update, initial_params, performance_params

    def get_stable_adgroups_params(self, slack_alerts):
        stable_params = {}
        stabled_df_cids = self.hydra_tree.get_stabled_df_cids()
        for cid in stabled_df_cids:
            performance_params, invalid_reason = self.get_deep_funnel_list_control_params(cid, 'campaign', False)
            if not performance_params:
                logger.warning('stable cid: %s had unexpected goal_type', cid)
                slack_alerts.append(self.make_slack_alert(
                    'invalid', cid, 'HydraWL', False, invalid_reason.split(',')))
                self.update_df_adgroup_status(cid, enabled=0, reason=invalid_reason)
            else:
                stable_params[cid] = performance_params

        return stable_params

    def get_garbage_adgroups_params(self, slack_alerts):
        """AI-5957 check garbage cids in 7/30 days has whitelist or not """
        garbage_cids = self.hydra_tree.get_garbage_df_cids()
        if not garbage_cids:
            return {}

        query = """
        select
            data.cid, data.recycle_time
        from
            (select cid, max(recycle_time) as max_time from {0} where cid in ({1}) group by cid) as filter,
            (select * from {0} where cid in ({1})) as data
        where
            data.recycle_time = filter.max_time
            and data.cid = filter.cid
            and data.recovered = 0
        """.format(self.garbage_cid_table, ', '.join('\'{}\''.format(cid) for cid in garbage_cids))
        logger.debug(query)
        result = self.query_db.get_query_result(query, db_client=self.df_db_client)
        recoverable_cids = {row['cid']: row['recycle_time'] for row in result}

        garbage_params = {}
        for cid, recycle_time in recoverable_cids.items():
            performance_params, invalid_reason = self.get_deep_funnel_list_control_params(cid, 'campaign', False)
            if not performance_params:
                logger.warning('garbage cid: %s had unexpected goal_type', cid)
                slack_alerts.append(self.make_slack_alert(
                    'invalid', cid, 'HydraX', False, invalid_reason.split(',')))
            elif int(time.time()) - recycle_time < (30 * 86400
                                                    if performance_params.raw_goal_type in ('ROAS', 'PRAT')
                                                    else 7 * 86400):
                garbage_params[cid] = performance_params
        logger.info('root_cid: %s get %s recoverable garbage cids: %s',
                    self.hydra_tree.root_cid, len(garbage_params), garbage_params.keys())
        return garbage_params

    def _get_last_cid_disabled_user(self, cid):
        # returns None if no record or currently enabled
        result = self.idash_endpoint.get_campaign_last_action_info(cid, 'enabled')
        return result['updated_by'] if result and result['to'] == 'false' else None

    def update_adgroup_tree_inventories(self, date_str):
        logger.info('cids status: %s', self.hydra_tree.cids_status)
        slack_alerts = []
        is_exp = date_str is not None
        campaign_cid, blacklist_cid, global_cid = self.hydra_tree.get_unstable_df_cids()
        app_usage = AppUsage.get_pub_sub_usage(self.partner_capabilities.get(
            self.hydra_tree.cids_config[self.hydra_tree.root_cid]['ext_channel']))

        global_is_black_usage = app_usage.is_black_usage or self.hydra_tree.is_transparent()
        do_global_cid_update, global_initial_params, global_performance_params = self.check_unstable_adgroup(
            global_cid, ['stable_sampling_white', 'reconstruct_sampling_white'], is_exp, global_is_black_usage,
            'HydraBL' if global_is_black_usage else 'HydraSM', slack_alerts)
        do_campaign_cid_update, campaign_initial_params, campaign_performance_params = self.check_unstable_adgroup(
            campaign_cid, ['stable_campaign_white', 'reconstruct_campaign_white'], is_exp, app_usage.is_black_usage,
            'HydraOID', slack_alerts)

        if self.hydra_tree.adgroup_tree.setdefault('blacklist_cid', '') == '' \
                and not app_usage.is_black_usage and not self.hydra_tree.is_transparent():
            if 'add_blacklist_field' not in self.hydra_tree.get_idash_pending_tasks():
                logger.info('root_cid %s triggers add-blacklist-field copy event', self.hydra_tree.root_cid)
                if not self.DEBUG and not self.cid_is_finished(self.hydra_tree.root_cid):
                    self.hydra_tree.trigger_copy_event('add_blacklist_field')
            do_blacklist_cid_update, blacklist_initial_params, blacklist_performance_params = False, None, None
        else:
            (do_blacklist_cid_update, blacklist_initial_params,
             blacklist_performance_params) = self.check_unstable_adgroup(
                 blacklist_cid, ['stable_sampling_black'], is_exp, True, 'HydraBL', slack_alerts)

        stable_params = self.get_stable_adgroups_params(slack_alerts)
        garbage_params = self.get_garbage_adgroups_params(slack_alerts)
        if not do_global_cid_update and not do_campaign_cid_update \
                and not do_blacklist_cid_update and not stable_params and not garbage_params:
            logger.warning('skip root_cid: %s update, %s', self.hydra_tree.root_cid, self.hydra_tree.adgroup_tree)
            return slack_alerts

        # add third OR condition since only `do_blacklist_cid_update=True' might happen
        global_params = global_initial_params or campaign_initial_params or blacklist_initial_params \
            or self.get_no_update_cid_params(global_cid)
        global_params = global_params._replace(do_cid_update=do_global_cid_update)
        local_params = campaign_performance_params or global_performance_params or blacklist_performance_params \
            or self.get_no_update_cid_params(campaign_cid)
        local_params = local_params._replace(do_cid_update=do_campaign_cid_update)
        blacklist_params = blacklist_performance_params or blacklist_initial_params \
            or self.get_no_update_cid_params(blacklist_cid)
        blacklist_params = blacklist_params._replace(do_cid_update=do_blacklist_cid_update)

        cid2lists, trigger_sampling_cid_copy, recovered_garbage_cids = self.hydra_list_controller.dispatch_list_control(
            global_params, local_params, blacklist_params, stable_params, garbage_params,
            use_cid_blacklist=True, date_str=date_str)

        for cid, lists in cid2lists.iteritems():
            if self.hydra_tree.is_stable_cid(cid) \
                    and not lists.get('require_publisher') and not lists.get('require_sub_publisher'):
                logger.info('recycle stable cid %s', cid)
                query = "INSERT INTO {} (cid, recycle_time, note) VALUES ('{}', {}, '{}')".format(
                    self.garbage_cid_table, cid, int(time.time()),
                    json.dumps(lists['note']) if lists.get('note') else '')
                logger.info(query)
                slack_alerts.append(self.make_slack_alert(
                    'empty_list', cid, lists['hydra_label'], False, [INVALID_EMPTY_LIST],
                    suggest_goal_rate=lists['note'].get('max_event_rate', -1)))
                if not self.DEBUG:
                    self.query_db.execute_query(query, db_client=self.df_db_client)
                    self.hydra_tree.recycle_stable_cid(cid)
            elif self.hydra_tree.is_valid_cid(cid):
                self.apply_black_white_lists(cid, lists, slack_alerts)

        for cid in recovered_garbage_cids:
            if self.cid_is_finished(cid):
                continue
            user = self._get_last_cid_disabled_user(cid)
            if user and user != self.idash_endpoint.idash_cfg['username']:
                logger.info('cid %s will not be enabled since it has been disabled by %s.', cid, user)
                note = 'Closed By {}'.format(user)
            else:
                self.hydra_tree.recover_garbage_cid(cid, True, debug=Hydra.DEBUG)
                note = ''
            query = "INSERT INTO {} (cid, recycle_time, recovered, note) VALUES ('{}', {}, 1, '{}')".format(
                self.garbage_cid_table, cid, int(time.time()), note)
            logger.info(query)
            slack_alerts.append(self.make_slack_alert(
                'reopen', cid, 'HydraX', False, [VALID_REOPEN]))
            if not self.DEBUG:
                self.query_db.execute_query(query, db_client=self.df_db_client)

        if trigger_sampling_cid_copy and 'campaign_whitelist' not in self.hydra_tree.get_idash_pending_tasks():
            logger.info('global cid %s triggers campaign-whitelist copy event', global_cid)
            if not self.DEBUG and not self.cid_is_finished(global_cid):
                self.hydra_tree.trigger_copy_event('campaign_whitelist')

        return slack_alerts

    def make_slack_alert(self, alert_type, cid, hydra_label, is_blacklist_cid, reasons, suggest_goal_rate=None):
        df_params, invalid_reason = self.get_deep_funnel_list_control_params(cid, 'campaign', is_blacklist_cid)
        assert alert_type == 'invalid' or not invalid_reason
        yesterday_performance = self.hydra_list_controller.get_hydra_performance_cache().get_cid_performance_data(
            df_params, n_day=1, decay_rate=1.0, install_decayed=False) if alert_type != 'invalid' else None
        last_week_performance = self.hydra_list_controller.get_hydra_performance_cache().get_cid_performance_data(
            df_params, n_day=7, decay_rate=1.0, install_decayed=False) if alert_type != 'invalid' else None
        suggestion_data = {'goal_type': df_params.raw_goal_type, 'suggest_goal_rate': suggest_goal_rate} \
            if alert_type == 'empty_list' else None
        if alert_type == 'empty_list':
            color = 'warning'
        elif alert_type == 'invalid':
            color = 'danger'
        elif alert_type == 'reopen':
            color = 'good'
        else:
            color = '#439FE0'
        return ALERT(
            color, cid, self.hydra_tree.cids_config[cid]['oid'],
            self.hydra_tree.cids_config[cid]['cid_name'], self.hydra_tree.cids_config[cid]['oid_name'],
            HydraSlackAlertsAgent.get_alert_type(hydra_label, reasons),
            HydraSlackAlertsAgent.get_impact(yesterday_performance, last_week_performance),
            HydraSlackAlertsAgent.get_suggestions(reasons, suggestion_data))

    def apply_black_white_lists(self, cid, df_result_dict, slack_alerts):
        pub_whitelist = df_result_dict.setdefault('require_publisher', [])
        sub_whitelist = df_result_dict.setdefault('require_sub_publisher', [])
        pub_blacklist = df_result_dict.setdefault('reject_publisher', [])
        sub_blacklist = df_result_dict.setdefault('reject_sub_publisher', [])

        app_usage = df_result_dict['params'].app_usage
        is_blacklist_cid = df_result_dict['params'].is_blacklist_cid
        if is_blacklist_cid:
            pub_whitelist[:] = []
            sub_whitelist[:] = []
        elif app_usage != AppUsage.BOTH and (pub_whitelist or sub_whitelist):
            pub_blacklist[:] = []
            sub_blacklist[:] = []

        is_open = self.hydra_tree.cids_status[cid]['cid_enabled'] == 1
        is_new_opened = self.hydra_tree.is_new_opened_cid(cid)
        if (is_open or is_new_opened) and not pub_whitelist and not sub_whitelist and not pub_blacklist \
                and not sub_blacklist and not is_blacklist_cid:
            slack_alerts.append(self.make_slack_alert(
                'empty_list', cid, df_result_dict['hydra_label'], is_blacklist_cid, [INVALID_EMPTY_LIST],
                suggest_goal_rate=df_result_dict['note'].get('max_event_rate', -1)))
            return self.update_df_adgroup_status(cid, enabled=0, reason=INVALID_EMPTY_LIST)

        current_status = self.hydra_tree.cids_status[cid]['deep_funnel_enabled']
        if (current_status == 0 or is_new_opened) and (pub_whitelist or sub_whitelist or is_blacklist_cid):
            user = self._get_last_cid_disabled_user(cid) if current_status == 0 else None
            if user and user != self.idash_endpoint.idash_cfg['username']:
                logger.info('cid %s will not be enabled since it has been disabled by %s.', cid, user)
                self.update_df_adgroup_status(cid, enabled=0, reason='Closed By {}'.format(user))
            else:
                # re-open when adgroup has whitelist or it is blacklist cid
                slack_alerts.append(self.make_slack_alert(
                    'reopen', cid, df_result_dict['hydra_label'], False, [VALID_REOPEN]))
                self.update_df_adgroup_status(
                    cid, enabled=1, reason='BlacklistBuy' if is_blacklist_cid else 'WhitelistBuy')
                is_open = True
        elif (current_status != 0 or self.hydra_tree.cids_status[cid]['cid_enabled'] == 1 or is_new_opened) \
                and not pub_whitelist and not sub_whitelist and not is_blacklist_cid:
            # close whitelist adgroup when is uses blacklist to buy
            slack_alerts.append(self.make_slack_alert(
                'empty_list', cid, df_result_dict['hydra_label'], is_blacklist_cid, [INVALID_EMPTY_LIST],
                suggest_goal_rate=df_result_dict['note'].get('max_event_rate', -1)))
            self.update_df_adgroup_status(cid, enabled=0, reason=INVALID_EMPTY_LIST)

        # FIXME: CAMD-1087
        # /disable_and_set/final_ai_black_white_rule/{cid} failed when sending empty list
        # temporary skip updating to empty list now
        if not Hydra.DEBUG and is_open \
                and (pub_whitelist or sub_whitelist or pub_blacklist or sub_blacklist):
            CCampaignInfo(self.idash_endpoint, self.query_db, cid).update_hydra_app_list(df_result_dict)

    def get_deep_funnel_list_control_params(self, cid, params_type, is_blacklist_cid):
        assert params_type in ('global', 'campaign')
        adapted_minor_goal, raw_minor_goal, roas_goal, invalid_reason = self.get_cid_goal_params(cid)
        partner = self.hydra_tree.cids_config[cid]['ext_channel']
        external_usage = self.partner_capabilities.get(partner)
        min_major_count = None
        if raw_minor_goal and raw_minor_goal.goal_type in ('PRAT', 'ROAS'):
            min_major_count = 1 if raw_minor_goal.goal_type == 'PRAT' else self.hydra_tree.cids_config[cid]['goalcpa']
        use_roas_filter = self.hydra_tree.cids_config[cid]['optimization_model'] in HYDRA_ROAS_MODELS
        if use_roas_filter:
            roas_goal_type = roas_goal.goal_type if roas_goal else 'ROAS-30D'
            roas_event_ids = roas_goal.event_ids if roas_goal else None
        else:
            roas_goal_type = roas_event_ids = None

        # TODO: construct DF_ADGROUP_PARAMS with adapted_minor_goal, raw_minor_goal and roas_goal
        df_params = self.DF_ADGROUP_PARAMS(
            cid, self.hydra_tree.cids_config[cid]['oid'], params_type == 'global', partner,
            adapted_minor_goal.event_type, adapted_minor_goal.goal_type, adapted_minor_goal.goal_value,
            AppUsage.get_pub_sub_usage(external_usage), False,
            raw_minor_goal.goal_type, raw_minor_goal.goal_value, raw_minor_goal.event_ids,
            self.hydra_tree.cids_config[cid]['partner_cids'], min_major_count,
            (self.hydra_tree.cids_config[cid]['category'], self.hydra_tree.cids_config[cid]['top_category']),
            self.hydra_tree.cids_config[cid]['country'], is_blacklist_cid, use_roas_filter,
            roas_goal_type, roas_event_ids) if adapted_minor_goal else None
        return df_params, invalid_reason

    def get_no_update_cid_params(self, cid):
        return self.DF_ADGROUP_PARAMS(
            cid, '', False, '', '', '', 0.0, AppUsage.INVALID,
            False, '', 0.0, [], [], None, ('', ''), '', False, False, None, None)

    def get_cid_goal_params(self, cid):
        if cid not in self.goal_params_cache:
            self.goal_params_cache[cid] = self._get_cid_goal_params(cid)

        return self.goal_params_cache[cid]

    def _get_cid_goal_params(self, cid):
        invalid_reasons = []

        if self.hydra_tree.cids_config[cid]['primary_goal'] != 'cpa':
            invalid_reasons.append(INVALID_PRIMARY_GOAL)
        if self.hydra_tree.cids_config[cid]['vertical'] != 'app_install':
            invalid_reasons.append(INVALID_VERTICAL)

        major_goal = self.cid_goal_query_agent.get_cid_major_goal(cid, 'CPA')
        if not major_goal or not major_goal.event_type.startswith('app_'):
            invalid_reasons.append(INVALID_MAJOR_TYPE)

        if 'app_install' not in self.cid_goal_query_agent.get_cid_event_types(cid):
            invalid_reasons.append(INVALID_NO_INSTALL_EVENT)

        major_events = self.cid_goal_query_agent.get_cid_major_events(cid)
        if len(set(event.event_type for event in major_events)) != 1:
            invalid_reasons.append(INVALID_MULTIPLE_MAJORS)

        if 'app_install' not in set([event.event_type for event in major_events]) \
                and self.hydra_tree.cids_config[cid]['ext_channel'] not in self.cpa_enabled_partners:
            invalid_reasons.append(INVALID_CPA_PARTNER)

        if self.cid2minor_goal and cid in self.cid2minor_goal:
            minor_goals = [CidGoalQueryAgent.GOAL(*self.cid2minor_goal[cid])]
        else:
            minor_goals = [minor_goal for minor_goal in self.cid_goal_query_agent.get_cid_minor_goals(cid)
                           if is_valid_minor_goal_type(minor_goal.goal_type) and minor_goal.event_type != 'app_install']

        roas_goal = None
        if not minor_goals:
            invalid_reasons.append(INVALID_MINOR_GOAL)
        elif not minor_goals[0].goal_value:
            invalid_reasons.append(INVALID_GOAL_VALUE)
        else:
            raw_minor_goal = minor_goals[0]
            adapted_minor_goal = self.cid_goal_query_agent.adapt_df_supported_cid_minor_goal(cid, raw_minor_goal)
            for minor_goal in minor_goals:
                if minor_goal.goal_type.startswith('ROAS') and minor_goal.event_type == 'app_purchase_with_revenue':
                    roas_goal = minor_goal
                    break

        if invalid_reasons:
            logger.warning('cid: %s is invalid %s', cid, invalid_reasons)
            return None, None, roas_goal, ','.join(invalid_reasons)
        logger.info('cid: %s, adapted minor goal: %s, raw minor goal: %s', cid, adapted_minor_goal, raw_minor_goal)
        return adapted_minor_goal, raw_minor_goal, roas_goal, None


class HydraDryRun(object):
    CAMPAIGN_PARAMS = namedtuple(
        'campaign_params',
        ['oid', 'partner_id', 'app_usage', 'partner_cids', 'category_tuple', 'country', 'goalcpa']
    )

    def __init__(self, query_db, hydra_list_controller, current_oid_data_reference):
        self.query_db = query_db
        self.hydra_list_controller = hydra_list_controller
        self.current_oid_data_reference = current_oid_data_reference

        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.oid_goal_query_agent = OidGoalQueryAgent(self.query_db)
        self.partner_capabilities = get_external_capability(self.query_db)

    def get_campaign_minor_goal(self, oid):
        minor_goals = [minor_goal for minor_goal in self.oid_goal_query_agent.get_oid_minor_goals(oid)
                       if is_valid_minor_goal_type(minor_goal.goal_type)]
        if not minor_goals or not minor_goals[0].goal_value:
            return None, None

        raw_minor_goal = minor_goals[0]
        adapted_minor_goal = self.oid_goal_query_agent.adapt_df_supported_oid_minor_goal(oid, raw_minor_goal)
        return adapted_minor_goal, raw_minor_goal

    def get_partner_params(self, oid, partner_ids):
        query = """
        select
            country, goalcpa
        from
            order_info
        where
            oid = '{}'
        """.format(oid)
        results = self.query_db.get_query_result(query, db_client=self.campaign_db_client)
        if not results:
            return None
        country, goalcpa = results[0]['country'], results[0]['goalcpa'] * APPIER_TO_USD

        oids = [oid] + self.current_oid_data_reference
        query = """
        select
            cid, ext_channel as partner_id, sub_category, top_category
        from
            campaign
        where
            oid in ({})
            and ext_channel in ({})
        """.format(','.join("'" + tmp_oid + "'" for tmp_oid in oids),
                   ','.join("'" + partner_id + "'" for partner_id in partner_ids))
        partner_cids = defaultdict(list)
        category_tuple = None
        for row in self.query_db.get_query_result(query, db_client=self.campaign_db_client):
            partner_cids[row['partner_id']].append(row['cid'])
            top_category = row.get('top_category', '')
            sub_category = row.get('sub_category', '')
            category = (top_category + '_' + sub_category) if sub_category else top_category
            category_tuple = (category, top_category)

        if category_tuple is None or not partner_cids:
            return None

        partner_params = {}
        for partner_id in partner_ids:
            partner_params[partner_id] = self.CAMPAIGN_PARAMS(
                oid, partner_id, AppUsage.get_pub_sub_usage(self.partner_capabilities.get(partner_id)),
                partner_cids[partner_id], category_tuple, country, goalcpa)

        return partner_params

    def get_fake_adgroup_tree(self, oid, partner_id):
        return {
            'campaign_cid': '_'.join(['HydraOID', oid, partner_id]),
            'global_cid': '_'.join(['HydraSM', oid, partner_id]),
            'blacklist_cid': '_'.join(['HydraBL', oid, partner_id]),
            'stable_campaign_cids': [],
            'stable_global_cids': [],
            'garbage_cids': [],
            'pending_tasks': [],
            'is_transparent': 0
        }

    def get_hydra_adgroup_params(self, adapted_minor_goal, raw_minor_goal, campaign_params, fake_adgroup_tree):
        min_major_count = None
        if raw_minor_goal and raw_minor_goal.goal_type in ('PRAT', 'ROAS'):
            min_major_count = 1 if raw_minor_goal.goal_type == 'PRAT' else campaign_params.goalcpa
        params = {}
        for cid, tag, use_global_data in [(fake_adgroup_tree['global_cid'], 'global', True),
                                          (fake_adgroup_tree['campaign_cid'], 'local', False),
                                          (fake_adgroup_tree['blacklist_cid'], 'blacklist', False)]:
            is_blacklist_cid = (cid == fake_adgroup_tree['blacklist_cid'] or campaign_params.app_usage.is_black_usage)
            params[tag] = Hydra.DF_ADGROUP_PARAMS(
                cid, campaign_params.oid, use_global_data, campaign_params.partner_id,
                adapted_minor_goal.event_type, adapted_minor_goal.goal_type, adapted_minor_goal.goal_value,
                campaign_params.app_usage, True, raw_minor_goal.goal_type, raw_minor_goal.goal_value,
                raw_minor_goal.event_ids, campaign_params.partner_cids, min_major_count,
                campaign_params.category_tuple, campaign_params.country, is_blacklist_cid, False, None, None)

        return params['global'], params['local'], params['blacklist']

    def update_campaign_inventories(self, oid, partner_ids):
        adapted_minor_goal, raw_minor_goal = self.get_campaign_minor_goal(oid)
        if not adapted_minor_goal or not raw_minor_goal:
            logger.warning('oid %s skip dry run for invalid goal setting', oid)
            return

        partner_params = self.get_partner_params(oid, partner_ids)
        if not partner_params:
            logger.warning('oid %s skip dry run for non-existed config', oid)
            return

        for partner_id, campaign_params in partner_params.iteritems():
            fake_adgroup_tree = self.get_fake_adgroup_tree(oid, partner_id)
            global_params, local_params, blacklist_params = self.get_hydra_adgroup_params(
                adapted_minor_goal, raw_minor_goal, campaign_params, fake_adgroup_tree)
            self.hydra_list_controller.dispatch_list_control(
                global_params, local_params, blacklist_params, fake_adgroup_tree)
