"""Define classes for generating black/white lists for HydraSM, HydraOID and HydraWL,
as well as class for generating pre-filter blacklists, like constraint, hydra cid, fraud, cm, auto rule, ..., etc.
"""
from __future__ import absolute_import
from __future__ import division
import time
from collections import defaultdict
from scipy.stats import beta

from app.module.Const import RULE_ID_DEEPFUNNEL
from app.utils.util import date_to_timestamp
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.list_control.performance import DeepFunnelPerformanceCache, Performance
from app.external_optimization.deep_funnel.list_control.mysql_queries import minor_rr_filter_target_date_query, \
    minor_prat_filter_target_date_query
from app.external_optimization.deep_funnel.utils.common_utils import AppList, AppUsage

logger = init_logger(__name__)


def get_publisher_blacklist(pub_blacklist, sub_blacklist, pub2sub_set, pub_performance_data, sub_performance_data,
                            install_threshold, goal_value):
    """Get the publisher blacklist.
    publisher must satisfy the following 2 conditions to be added to blacklist:
        1. has enough sub (>=10)
        2. non-sub-blacklist performance is poor (<goal_value), and confident (>=install_threshold).
    :param pub_blacklist: list/set of publisher id
    :param sub_blacklist: list/set of sub-publisher id
    :param pub2sub_set: dict of publisher id to set of sub-publisher ids
    :param pub_performance_data: dict of publisher id to <AppPerformance>
    :param sub_performance_data: dict of sub-publisher id to <AppPerformance>
    :param install_threshold:
    :param goal_value:
    :return final_pub_blacklist: set of output publisher blacklist
    """
    final_pub_blacklist = set()
    for pub in pub_blacklist:
        if len(pub2sub_set[pub]) < 10:
            continue
        pub_performance = pub_performance_data[pub].get_local_performance()
        for sub in pub2sub_set[pub]:
            if sub in sub_blacklist:
                pub_performance -= sub_performance_data[sub].get_local_performance()

        if pub_performance.installs >= install_threshold \
                and pub_performance.get_event_rate_with_today_installs() < goal_value:
            final_pub_blacklist.add(pub)

    return final_pub_blacklist


def apply_today_installs_capping(confidence, performance):
    """Cap the today_install such that the event_rate_with_today_installs will not lower than the min event rate
    from a beta distribution at given confidence level.
    :param confidence:
    :param performance: an instance of <Performance>
    :return performance: the <Performance> instance which `today_installs` is capped
    """
    assert confidence < 1.0 and confidence > 0.0
    if not performance.installs or not performance.get_event_rate():
        return performance
    min_rate = beta.ppf(1.0 - confidence, 0.5 * performance.installs, 0.5 * performance.installs) \
        * performance.get_event_rate() / 0.5
    min_rate_installs = performance.minor_count / min_rate - performance.major_count
    assert min_rate_installs >= 0
    performance.today_installs = min(performance.today_installs, min_rate_installs)
    return performance


class OPTIMIZED_LISTS(object):
    """Storage class for record `sub_white`, `sub_black`, `pub_white`, `pub_black` 4 variables for optimized lists. """
    def __init__(self, sub_white, sub_black, pub_white, pub_black):
        self.sub_white = sub_white
        self.sub_black = sub_black
        self.pub_white = pub_white
        self.pub_black = pub_black


class STABLE_OPTIMIZED_LISTS(OPTIMIZED_LISTS):
    """Storage class for record `sub_white`, `sub_black`, `pub_white`, `pub_black`,
    and `starvelist` 5 variables for HydraWL optimized lists. """
    def __init__(self, sub_white, sub_black, pub_white, pub_black, starvelist):
        super(STABLE_OPTIMIZED_LISTS, self).__init__(sub_white, sub_black, pub_white, pub_black)
        self.starvelist = starvelist


class HARD_BLACKLISTS(object):
    """Storage class for record `all_pub`, `all_sub`, `hydra_pub`, `hydra_sub`, `non_hydra_pub`, `non_hydra_sub`
    4 variables for hard black lists. """
    def __init__(self, all_pub, all_sub, hydra_pub, hydra_sub, non_hydra_pub, non_hydra_sub):
        self.all_pub = all_pub
        self.all_sub = all_sub
        self.hydra_pub = hydra_pub
        self.hydra_sub = hydra_sub
        self.non_hydra_pub = non_hydra_pub
        self.non_hydra_sub = non_hydra_sub


class HARD_WHITELISTS(object):
    """Storage class for record `pub`, `sub` 2 variables for hard white lists. """
    def __init__(self, pub, sub):
        self.pub = pub
        self.sub = sub


class BaseSingleOptimizedLists(object):
    """Provide access to pub/sub blacklist/whitelist and method to build single type bw lists. """

    def __init__(self, df_params, appid_ruler, prefix):
        self.df_params = df_params
        self.appid_ruler = appid_ruler
        self.prefix = prefix

        self.sub_whitelist = []
        self.sub_blacklist = []
        self.pub_whitelist = []
        self.pub_blacklist = []
        self.max_event_rate = None
        self.total_local_events = 0

    def __str__(self):
        return """{} cid: {} has {} events and get solution with
        {} sub_white: {}
        {} sub_black: {}
        {} pub_white: {}
        {} pub_black: {}""".format(self.prefix, self.df_params.cid, self.total_local_events,
                                   len(self.sub_whitelist), [app_id for (app_id, _installs) in self.sub_whitelist],
                                   len(self.sub_blacklist), [app_id for (app_id, _installs) in self.sub_blacklist],
                                   len(self.pub_whitelist), [app_id for (app_id, _installs) in self.pub_whitelist],
                                   len(self.pub_blacklist), [app_id for (app_id, _installs) in self.pub_blacklist])

    @staticmethod
    def _get_app_list(raw_list, is_subpublisher, required_installs, able_to_write):
        return AppList([app_id for (app_id, installs) in raw_list if installs >= required_installs],
                       is_subpublisher,
                       able_to_write=able_to_write)

    def get_sub_black_app_list(self, required_installs=0):
        """Get subpublisher blacklist with required_installs.
        :param required_installs:
        :return sub_blacklist: an AppList object from subpublisher blacklist with required installs
        """
        return self._get_app_list(
            self.sub_blacklist, 1, required_installs, self.df_params.app_usage.black_sub_writable)

    def get_sub_white_app_list(self, required_installs=0):
        """Get subpublisher whitelist with required_installs.
        :param required_installs:
        :return sub_whitelist: an AppList object from subpublisher whitelist with required installs
        """
        return self._get_app_list(
            self.sub_whitelist, 1, required_installs, self.df_params.app_usage.white_sub_writable)

    def get_pub_black_app_list(self, required_installs=0):
        """Get publisher blacklist with required_installs.
        :param required_installs:
        :return pub_blacklist: an AppList object from publisher blacklist with required installs
        """
        return self._get_app_list(
            self.pub_blacklist, 0, required_installs, self.df_params.app_usage.black_pub_writable)

    def get_pub_white_app_list(self, required_installs=0):
        """Get publisher whitelist with required_installs.
        :param required_installs:
        :return pub_whitelist: an AppList object from publisher whitelist with required installs
        """
        return self._get_app_list(
            self.pub_whitelist, 0, required_installs, self.df_params.app_usage.white_pub_writable)

    def get_optimized_lists(self, required_installs=0):
        """Get optimized lists with required_installs.
        :param required_installs:
        :return optimized_lists: an instance of <OPTIMIZED_LISTS>
                                 with `sub_white`, `sub_black`, `pub_white`, `pub_black`
        """
        return OPTIMIZED_LISTS(
            self.get_sub_white_app_list(required_installs=required_installs),
            self.get_sub_black_app_list(required_installs=required_installs),
            self.get_pub_white_app_list(required_installs=required_installs),
            self.get_pub_black_app_list(required_installs=required_installs)
        )

    def get_max_event_rate(self):
        """Get the maximum accumulated event rate during simulation.
        :return max_event_rate
        """
        return self.max_event_rate

    @staticmethod
    def union_optimized_lists(target_lists, source_lists):
        """Union target optimized lists from source optimized lists.
        :param target_lists: <OPTIMIZED_LISTS>
        :param source_lists: <OPTIMIZED_LISTS>
        :return optimized_lists: <OPTIMIZED_LISTS> with union in `sub_white`, `sub_black`, `pub_white`, `pub_black`
        """
        target_lists.sub_white |= source_lists.sub_white
        target_lists.sub_black |= source_lists.sub_black
        target_lists.pub_white |= source_lists.pub_white
        target_lists.pub_black |= source_lists.pub_black
        return target_lists

    def _apply_app_filter(self, app_performance_data, install_threshold, excluded_app_set, use_event_filter,
                          use_goal_rate_filter):
        """Apply app_id filter to app_performance data.
        :param app_performance_data: the input app_performance data, dict of app_id to <AppPerformance>
        :param install_threshold: only app_id with installs larger than threshold can pass
        :param excluded_app_set: only app_id not in excluded set can pass
        :param use_event_filter: use minor event count filter or not,
            when using this filter only app_id with minor events can pass except it will have less than 5 app_id's
        :param use_goal_rate_filter: whether app_ids with event_rate lower than goal_rate will be filtered out
        :return filtered_app_performance_data: dict of app_id to <AppPerformance> that passes all filters
        """
        filtered_app_performance_data = {}
        sampling_app_performance_data = {}
        goal_rate = self.df_params.event_goal if self.df_params.use_global_data else self.df_params.raw_event_goal
        for app_id, app_performance in app_performance_data.iteritems():
            if app_id in excluded_app_set:
                continue
            performance = app_performance.get_decayed_performance(self.df_params.use_global_data)
            if performance.get_total_major_count() <= install_threshold or performance.minor_count < 0:
                continue
            if use_goal_rate_filter and performance.get_event_rate_with_today_installs() < goal_rate:
                continue
            if use_event_filter and performance.minor_count == 0:
                sampling_app_performance_data[app_id] = app_performance
            else:
                filtered_app_performance_data[app_id] = app_performance

        if use_event_filter and sampling_app_performance_data and len(filtered_app_performance_data) < 5:
            logger.warning('[EXTEND] the list size is not large enough, use the extended list %s', self.df_params.cid)
            filtered_app_performance_data.update(sampling_app_performance_data)

        return filtered_app_performance_data

    def _set_total_local_events(self, app_performance_data):
        """Set the total event counts to ensure model confidence.
        :param app_performance_data: dict of app_id to <AppPerformance>
        """
        self.total_local_events = sum(app_performance.get_local_performance().minor_count
                                      for app_performance in app_performance_data.values())

    def _sort_app_performance_data(self, app_performance_data):
        """Sort the app_performance_data according to prediction/global_event_rate.
        :param app_performance_data: dict of app_id to <AppPerformance>
        :return app_ranks: sorted list of <AppPerformance>,
            sort by model prediction when total event count larger than 100, otherwise by global event rate
        """
        if self.total_local_events <= 100:
            app_ranks = sorted(app_performance_data.values(),
                               key=lambda performance: performance.get_unconfident_sort_key(),
                               reverse=True)
        else:
            app_ranks = sorted(app_performance_data.values(),
                               key=lambda performance: performance.get_confident_sort_key(),
                               reverse=True)

        return app_ranks

    def _get_app_ranking(self, app_performance_data, oid_whitelist, use_accumulate_simulation):
        raise NotImplementedError

    def _get_reach_goal_position(self, app_ranks, accu_performance, use_raw_goal, max_position=None):
        """Get the maximum position that the accumulated event rate reaches goal.
        :param app_ranks: ranked list of <AppPerformance>
        :param accu_performance: already accumulated <Performance>
        :param use_raw_goal: use raw_event_goal or not
        :param max_position: upper bound of position
        :return solution_position: the solution index, ranks[:solution_position] reaches goal
        :return max_event_rate: the maximum accumulated event rate during simulation
        """
        goal_rate = self.df_params.raw_event_goal if use_raw_goal else self.df_params.event_goal
        solution_position = solution_installs = solution_rate = 0
        max_event_rate = None

        for position, app_performance in enumerate(app_ranks, start=1):
            performance = app_performance.get_decayed_performance(self.df_params.use_global_data)
            accu_performance += performance

            accu_event_rate = accu_performance.get_event_rate_with_today_installs()
            max_event_rate = max(max_event_rate, accu_event_rate) if max_event_rate is not None else accu_event_rate
            logger.debug('%s cid %s k %s num_install %s, accu_rate %s, app_rate %s, app_id %s',
                         self.prefix, self.df_params.cid, position, accu_performance.get_total_major_count(),
                         accu_event_rate, performance.get_event_rate_with_today_installs(), app_performance.app_id)
            if accu_event_rate >= goal_rate:
                solution_position = position
                solution_installs = accu_performance.get_total_major_count()
                solution_rate = accu_event_rate

        if max_position and max_position < solution_position:
            logger.info('constraint k from %s to %s', solution_position, max_position)
            solution_position = max_position

        logger.info('%s solution cid %s k %s n_install: %s/%s, rate %s', self.prefix, self.df_params.cid,
                    solution_position, solution_installs, accu_performance.get_total_major_count(), solution_rate)
        return solution_position, max_event_rate

    def _get_whitelist_blacklist(self, app_ranks, solution_position):
        """Get the whitelist and blacklist from ranks and solution position.
        :param ranks: ranked list of (app_id, <Performance>)
        :param solution_position:
        :return whitelist: list of (app_id, installs) above solution position, exclusive
        :return blacklist: list of (app_id, installs) below solution position, inclusive
        """
        whitelist = [
            (app_performance.app_id, app_performance.get_decayed_performance(
                self.df_params.use_global_data).get_total_major_count())
            for app_performance in app_ranks[:solution_position]]
        blacklist = [
            (app_performance.app_id, app_performance.get_decayed_performance(
                self.df_params.use_global_data).get_total_major_count())
            for app_performance in app_ranks[solution_position:]]
        return whitelist, blacklist

    def _build_single_bw_lists(self, app_performance_data, oid_whitelist, use_accumulate_simulation, use_raw_goal):
        """Build the single type white/black lists (all pub or all sub).
        :param app_performance_data: dict of app_id to <AppPerformance>
        :param oid_whitelist: oid whitelist that will be excluded for SamplingOptimizedLists
        :param use_accumulate_simulation: use oid accumulate installs/events for SamplingOptimizedLists or not
        :param use_raw_goal: use raw_event_goal or not
        :return whitelist: list of (app_id, installs) in whitelist
        :return blacklist: list of (app_id, installs) in blacklist
        :return max_event_rate: the maximum accumulated event rate during simulation
        """
        app_ranks, accu_performance = self._get_app_ranking(
            app_performance_data, oid_whitelist, use_accumulate_simulation)
        solution_position, max_event_rate = self._get_reach_goal_position(
            app_ranks, accu_performance, use_raw_goal)

        whitelist, blacklist = self._get_whitelist_blacklist(app_ranks, solution_position)
        return whitelist, blacklist, max_event_rate

    def build_optimized_lists(self, pub_performance_data, sub_performance_data, excluded_app_set, install_threshold,
                              oid_whitelist, use_accumulate_simulation, use_event_filter,
                              use_goal_rate_filter, use_raw_goal):
        raise NotImplementedError

    def get_whitelists_performance(self, sub_performance_data, require_pub, require_sub, use_global_data, goal_value):
        """Get the reach goal performance from white lists.
        :param sub_performance_data: dict of sub_publisher to <AppPerformance>
        :param require_pub: list of require publisher id
        :param require_sub: list of require sub-publisher id
        :param use_global_data: use decayed global or local data to simulate
        :param goal_value:
        :return accu_performance: aggregated <Performance> in whitelists
        """
        accu_performance = Performance(0, 0, 0, 0)
        goal_value = self.df_params.raw_event_goal
        for app_id in sorted(sub_performance_data,
                             key=lambda app: sub_performance_data[app].get_simulation_sort_key(use_global_data),
                             reverse=True):
            pub_id, sub_id = self.appid_ruler.get_pubid_subid_from_appid(self.df_params.partner, app_id)
            if sub_id in require_sub or pub_id in require_pub:
                performance = sub_performance_data[app_id].get_decayed_performance(use_global_data)
                if (accu_performance + performance).get_event_rate_with_today_installs() >= goal_value:
                    accu_performance += performance
                else:
                    break

        return accu_performance

    def get_blacklists_performance(self, sub_performance_data, reject_pub, reject_sub, use_global_data, goal_value):
        """Get the reach goal performance from black lists.
        :param sub_performance_data: dict of sub_publisher to <AppPerformance>
        :param reject_pub: list of reject publisher id
        :param reject_sub: list of reject sub-publisher id
        :param use_global_data: use decayed global or local data to simulate
        :param goal_value:
        :return performance: aggregated <Performance> not in blacklists
        """
        accu_performance = Performance(0, 0, 0, 0)
        goal_value = self.df_params.raw_event_goal
        for app_id in sorted(sub_performance_data,
                             key=lambda app: sub_performance_data[app].get_simulation_sort_key(use_global_data),
                             reverse=True):
            pub_id, sub_id = self.appid_ruler.get_pubid_subid_from_appid(self.df_params.partner, app_id)
            if sub_id not in reject_sub and pub_id not in reject_pub:
                performance = sub_performance_data[app_id].get_decayed_performance(use_global_data)
                if (accu_performance + performance).get_event_rate_with_today_installs() >= goal_value:
                    accu_performance += performance
                else:
                    break

        return accu_performance


class BaseMultipleOptimizedLists(BaseSingleOptimizedLists):
    """Inherit from BaseSingleOptimizedLists and provides method to build multiple type bw lists (white pub, black sub)
    """
    def _get_app_ranking(self, app_performance_data, oid_whitelist, use_accumulate_simulation):
        raise NotImplementedError

    def _get_pub_sub_mapping(self, sub_app_ids):
        """Get the publisher-subpublisher mappings.
        :param sub_app_ids: list of subpublisher ids
        :return pub_id2sub_id_set: dict of publisher id to subpublisher id set
        :return sub_id2pub_id: dict of subpublisher id to publisher id
        """
        # FIXME: publisher in pub_performance but it's subpublisher not in sub_performance
        pub_id2sub_id_set = defaultdict(set)
        sub_id2pub_id = {}

        for sub_app_id in sub_app_ids:
            pub_id, sub_id = self.appid_ruler.get_pubid_subid_from_appid(self.df_params.partner, sub_app_id)
            if not pub_id or not sub_id:
                continue
            pub_id2sub_id_set[pub_id].add(sub_id)
            sub_id2pub_id[sub_id] = pub_id

        return pub_id2sub_id_set, sub_id2pub_id

    def _get_replaceable_subpublishers(self, sub_performance_data, pub_id2sub_id_set, pub_whitelist,
                                       threshold, sub_blacklist_id):
        """Get the replaceable subpublisher with score lower than threshold.
        :param sub_performance_data: dict of subpublisher id to <AppPerformance>
        :param pub_id2sub_id_set: dict of publisher id to subpublisher id set
        :param pub_whitelist: publisher whitelist, list of (app_id, <Performance>)
        :param threshold: the threshold <AppPerformance> object
        :param sub_blacklist_id: already existing subpublisher blacklist, list of subpublisher id
        :return replaceable_sub_apps: set of replaceable subpublisher ids,
            publisher whitelist's subpublishers with score lower than threshold is replaceable,
            use model prediction as score when total events larger than 100, otherwise use global event rate
        """
        replaceable_sub_apps = set()
        if self.total_local_events <= 100:
            for (pub, _installs) in pub_whitelist:
                for sub in pub_id2sub_id_set[pub]:
                    if sub in sub_blacklist_id:
                        continue
                    if sub_performance_data[sub].get_decayed_global_performance().get_event_rate_with_today_installs() \
                            < threshold.get_decayed_global_performance().get_event_rate_with_today_installs():
                        replaceable_sub_apps.add(sub)
        else:
            for (pub, _installs) in pub_whitelist:
                for sub in pub_id2sub_id_set[pub]:
                    if sub not in sub_blacklist_id and sub_performance_data[sub].prediction < threshold.prediction:
                        replaceable_sub_apps.add(sub)

        return replaceable_sub_apps

    def _get_multiple_sub_bw_lists(self, sub_performance_data, pub_whitelist, pub_id2sub_id_set, sub_blacklist_id):
        """Get the subpublisher white/black lists for multiple type bw lists.
        :param sub_performance_data: dict of subpublisher id to <AppPerformance>
        :param pub_whitelist: publisher whitelist, list of (app_id, <Performance>)
        :param pub_id2sub_id_set: dict of publisher id to subpublisher id set
        :param sub_blacklist_id: already existing subpublisher blacklist, list of subpublisher id
        :return sub_whitelist: subpublisher whitelist, list of (app_id, installs)
        :return sub_blacklist: subpublisher blacklist, list of (app_id, installs)
        """
        sub_whitelist = []
        for (pub, _installs) in pub_whitelist:
            for sub in pub_id2sub_id_set[pub]:
                if sub in sub_blacklist_id:
                    continue
                installs = sub_performance_data[sub].get_decayed_performance(
                    self.df_params.use_global_data).get_total_major_count()
                sub_whitelist.append((sub, installs))

        sub_blacklist = []
        for sub in sub_blacklist_id:
            installs = sub_performance_data[sub].get_decayed_performance(
                self.df_params.use_global_data).get_total_major_count()
            sub_blacklist.append((sub, installs))

        return sub_whitelist, sub_blacklist

    def _build_multiple_bw_lists(self, pub_performance_data, sub_performance_data,
                                 oid_whitelist, use_accumulate_simulation, use_raw_goal):
        """Build the multiple type bw lists (white pub, black sub).
        :param pub_performance_data: dict of publisher id to <AppPerformance>
        :param sub_performance_data: dict of subpublisher id to <AppPerformance>
        :param oid_whitelist: oid whitelist that will be excluded for SamplingOptimizedLists
        :param use_accumulate_simulation: use oid accumulate installs/events for SamplingOptimizedLists or not
        :param use_raw_goal: use raw_event_goal or not
        :return sub_whitelist: subpublisher whitelist, list of (app_id, installs)
        :return sub_blacklist: subpublisher blacklist, list of (app_id, installs)
        :return pub_whitelist: publisher whitelist, list of (app_id, installs)
        :return pub_blacklist: publisher blacklist, list of (app_id, installs)
        :return max_event_rate: the maximum accumulated event rate during simulation
        """
        pub_id2sub_id_set, sub_id2pub_id = self._get_pub_sub_mapping(sub_performance_data.keys())
        pub_whitelist, pub_blacklist, max_event_rate = self._build_single_bw_lists(
            pub_performance_data, oid_whitelist, use_accumulate_simulation, use_raw_goal)

        n_iteration = 1
        sub_blacklist_id = set()
        pub_blacklist_id = set()
        while pub_blacklist:
            # replace subpublisher below threshold
            log_prefix = '{} cid {} iteration {}'.format(self.prefix, self.df_params.cid, n_iteration)
            replaceable_sub_apps = self._get_replaceable_subpublishers(
                sub_performance_data, pub_id2sub_id_set, pub_whitelist,
                pub_performance_data[pub_blacklist[0][0]], sub_blacklist_id)
            if not replaceable_sub_apps:
                logger.info('%s finish with no replace subpublisher', log_prefix)
                break

            # minus pub performance and replace pub from replaceable subpublishers
            logger.info('%s try to replace %s bad subpublishers: %s',
                        log_prefix, len(replaceable_sub_apps), list(replaceable_sub_apps))
            for sub in replaceable_sub_apps:
                pub_performance_data[sub_id2pub_id[sub]] -= sub_performance_data[sub]
            # check pub_id2sub_id_set[pub] is not empty since there are pub without sub in input performance
            replaceable_pub_apps = set(
                pub for pub in pub_performance_data
                if len(pub_id2sub_id_set[pub] - replaceable_sub_apps - sub_blacklist_id) == 0
                and len(pub_id2sub_id_set[pub]) != 0)

            # get new pub white/black lists
            new_pub_whitelist, new_pub_blacklist, max_event_rate = self._build_single_bw_lists(
                {pub: app_performance for (pub, app_performance) in pub_performance_data.iteritems()
                 if pub not in replaceable_pub_apps},
                oid_whitelist, use_accumulate_simulation, use_raw_goal)
            diff_pub = list(set(pub for (pub, _installs) in new_pub_whitelist)
                            - set(pub for (pub, _installs) in pub_whitelist))
            if not diff_pub or len(new_pub_whitelist) < len(pub_whitelist):
                logger.info('%s finish with no new publisher', log_prefix)
                break

            pub_whitelist = new_pub_whitelist
            pub_blacklist = new_pub_blacklist
            sub_blacklist_id |= replaceable_sub_apps
            pub_blacklist_id |= replaceable_pub_apps
            logger.info('%s increase %s publisher: %s', log_prefix, len(diff_pub), diff_pub)
            n_iteration += 1

        # compute sub white/black and extend pub black
        sub_whitelist, sub_blacklist = self._get_multiple_sub_bw_lists(
            sub_performance_data, pub_whitelist, pub_id2sub_id_set, sub_blacklist_id)
        for pub in pub_blacklist_id:
            installs = pub_performance_data[pub].get_decayed_performance(
                self.df_params.use_global_data).get_total_major_count()
            pub_blacklist.append((pub, installs))
        return sub_whitelist, sub_blacklist, pub_whitelist, pub_blacklist, max_event_rate

    def build_optimized_lists(self, pub_performance_data, sub_performance_data, excluded_app_set, install_threshold,
                              oid_whitelist, use_accumulate_simulation, use_event_filter,
                              use_goal_rate_filter, use_raw_goal):
        """Build optimized white/black lists according to `df_params.app_usage`.
        `pub` builds pub_whitelist and pub_blacklist.
        `sub` and `black_sub` builds sub_whitelsit and sub_blacklist.
        `both` builds pub_whitelist, pub_blacklist, sub_whitelsit and sub_blacklist.

        :param pub_performance_data: dict of publisher id to <AppPerformance>
        :param sub_performance_data: dict of subpublisher id to <AppPerformance>
        :param excluded_app_set: only app_id not in excluded set can pass
        :param install_threshold: only app_id with installs larger than threshold can pass
        :param oid_whitelist: oid whitelist that will be excluded for SamplingOptimizedLists
        :param use_accumulate_simulation: use oid accumulate installs/events for SamplingOptimizedLists or not
        :param use_event_filter: use minor event count filter for app_id or not
        :param use_goal_rate_filter: use goal-event_rate filter for app_id or not
        :param use_raw_goal: use raw_event_goal or not
        """
        # FIXME: should we set total_nd_event before app_id filters ?
        if self.df_params.app_usage == AppUsage.BOTH:
            self._set_total_local_events(pub_performance_data)
            filtered_pub_performance_data = self._apply_app_filter(
                pub_performance_data, install_threshold, excluded_app_set, use_event_filter, use_goal_rate_filter)
            filtered_sub_performance_data = self._apply_app_filter(
                sub_performance_data, 0.5 * install_threshold,
                excluded_app_set, use_event_filter, use_goal_rate_filter)
            (self.sub_whitelist, self.sub_blacklist,
             self.pub_whitelist, self.pub_blacklist, self.max_event_rate) = self._build_multiple_bw_lists(
                 filtered_pub_performance_data, filtered_sub_performance_data, oid_whitelist,
                 use_accumulate_simulation, use_raw_goal)
        elif self.df_params.app_usage.needs_pub_performance:
            self._set_total_local_events(pub_performance_data)
            filtered_app_performance_data = self._apply_app_filter(
                pub_performance_data, install_threshold, excluded_app_set, use_event_filter, use_goal_rate_filter)
            self.pub_whitelist, self.pub_blacklist, self.max_event_rate = self._build_single_bw_lists(
                filtered_app_performance_data, oid_whitelist, use_accumulate_simulation, use_raw_goal)
        elif self.df_params.app_usage.needs_sub_performance:
            self._set_total_local_events(sub_performance_data)
            filtered_sub_performance_data = self._apply_app_filter(
                sub_performance_data, install_threshold, excluded_app_set, use_event_filter, use_goal_rate_filter)
            self.sub_whitelist, self.sub_blacklist, self.max_event_rate = self._build_single_bw_lists(
                filtered_sub_performance_data, oid_whitelist, use_accumulate_simulation, use_raw_goal)
        logger.info(self)


class CampaignOptimizedLists(BaseMultipleOptimizedLists):
    """Interface to build HydraOID white/black lists. """
    def __init__(self, df_params, appid_ruler):
        super(CampaignOptimizedLists, self).__init__(df_params, appid_ruler, '[Hydra_OID]')

    def train_optimized_lists(self, pub_performance_data, sub_performance_data, excluded_app_set, install_threshold):
        """Train optimized white/black lists.
        :param pub_performance_data: dict of publisher id to <AppPerformance>
        :param sub_performance_data: dict of subpublisher id to <AppPerformance>
        :param excluded_app_set: only app_id not in excluded set can pass
        :param install_threshold: only app_id with installs larger than threshold can pass
        """
        self.build_optimized_lists(
            pub_performance_data, sub_performance_data, excluded_app_set, install_threshold,
            oid_whitelist=None, use_accumulate_simulation=False,
            use_event_filter=False, use_goal_rate_filter=False, use_raw_goal=True)

    def _get_app_ranking(self, app_performance_data, _oid_whitelist, _use_accumulate_simulation):
        """Get the app ranking for HydraOID simulation.
        :param app_performance_data: dict of app_id to <AppPerformance>
        :param _oid_whitelist:
        :param _use_accumulate_simulation:
        :return app_ranks: list of <AppPerformance> order by model prediction or global event rate
        :return accu_performance: <Performance> object with all zeros
        """
        accu_performance = Performance(0, 0, 0, 0)
        app_ranks = self._sort_app_performance_data(app_performance_data)
        return app_ranks, accu_performance


class SamplingOptimizedLists(BaseMultipleOptimizedLists):
    """Interface to build HydraSM white/black lists. """
    def __init__(self, df_params, appid_ruler):
        super(SamplingOptimizedLists, self).__init__(df_params, appid_ruler, '[Hydra_SM]')

    def train_optimized_lists(self, pub_performance_data, sub_performance_data, excluded_app_set, oid_whitelist,
                              use_accumulate_simulation, use_raw_goal, use_goal_rate_filter=False):
        """Train optimized white/black lists.
        :param pub_performance_data: dict of publisher id to <AppPerformance>
        :param sub_performance_data: dict of subpublisher id to <AppPerformance>
        :param excluded_app_set: only app_id not in excluded set can pass
        :param oid_whitelist: oid whitelist that will be excluded
        :param use_accumulate_simulation: use oid accumulate installs/events or not
        :param use_raw_goal: use raw_event_goal or not
        :param use_goal_rate_filter: use goal-event_rate filter for app_id or not
        """
        self.build_optimized_lists(
            pub_performance_data, sub_performance_data, excluded_app_set, 0, oid_whitelist,
            use_accumulate_simulation, use_event_filter=True,
            use_goal_rate_filter=use_goal_rate_filter, use_raw_goal=use_raw_goal)
        if self.max_event_rate and not use_raw_goal:
            self.max_event_rate *= (self.df_params.raw_event_goal / self.df_params.event_goal)

    def _get_app_ranking(self, app_performance_data, oid_whitelist, use_accumulate_simulation):
        """Get the app ranking for HydraSM simulation.
        :param app_performance_data: dict of app_id to <AppPerformance>
        :param oid_whitelist: oid whitelist that will be excluded
        :param use_accumulate_simulation: use oid accumulate installs/events or not
        :return app_ranks: list of <Performance> order by model prediction or global event rate
        :return accu_performance: accumulated oid whitelist <Performance>
        """
        raw_app_ranks = self._sort_app_performance_data(app_performance_data)
        accu_performance = Performance(0, 0, 0, 0)
        app_ranks = []
        for app_performance in raw_app_ranks:
            if app_performance.app_id in oid_whitelist:
                if use_accumulate_simulation:
                    accu_performance += app_performance.get_decayed_performance(self.df_params.use_global_data)
            else:
                app_ranks.append(app_performance)

        return app_ranks, accu_performance


class StableOptimizedLists(object):
    """Interface to build HydraWL white/black lists. """
    MAX_STARVE_LIST = 1000

    def __init__(self, df_params, is_subpublisher):
        self.df_params = df_params
        self.is_subpublisher = is_subpublisher

        self.sub_whitelist = []
        self.sub_blacklist = []
        self.pub_whitelist = []
        self.pub_blacklist = []
        self.starvelist = []
        self.max_event_rate = None

    def get_max_event_rate(self):
        """Get the maximum accumulated event rate during simulation.
        :return max_event_rate
        """
        return self.max_event_rate

    def train_optimized_lists(self, pub_performance_data, sub_performance_data, starving_install, pub2sub_set):
        """Train stable optimized lists for HydraWL.
        :param performance_data: dict of app_id to <AppPerformance>
        :param starving_install:
        """
        if self.is_subpublisher:
            self.pub_whitelist = []
            self.pub_blacklist = []
            self.sub_whitelist, self.sub_blacklist, self.starvelist, self.max_event_rate = self._list_simulation(
                sub_performance_data, starving_install)
            if self.df_params.app_usage.black_publisher and pub_performance_data:
                _, pub_blacklist, _, _ = self._list_simulation(pub_performance_data, starving_install)
                self.pub_blacklist = get_publisher_blacklist(
                    pub_blacklist, self.sub_blacklist, pub2sub_set, pub_performance_data, sub_performance_data,
                    starving_install, self.df_params.raw_event_goal)
        else:
            self.sub_whitelist = []
            self.sub_blacklist = []
            self.pub_whitelist, self.pub_blacklist, self.starvelist, self.max_event_rate = self._list_simulation(
                pub_performance_data, starving_install)

    def _list_simulation(self, performance_data, starving_install):
        starvelist = []
        blacklist = []
        whitelist = []
        max_event_rate = None

        accumulate_performance = Performance(0.0, 0.0, 0.0, 0.0)
        goal_value = self.df_params.raw_event_goal
        logger.info('cid: %s, goal: %s', self.df_params.cid, goal_value)
        for app_id in sorted(performance_data,
                             key=lambda app: performance_data[app].get_local_performance_sort_key(),
                             reverse=True):
            performance = performance_data[app_id].get_local_performance()
            if performance.get_total_installs() >= starving_install:
                performance = apply_today_installs_capping(0.99, performance)
            accumulate_performance += performance
            accu_event_rate = accumulate_performance.get_event_rate_with_today_installs()
            max_event_rate = max(max_event_rate, accu_event_rate) if max_event_rate is not None else accu_event_rate

            if performance.get_total_installs() < starving_install:
                starvelist.append(app_id)
                logger.debug('starve app: %s, perf: %s, accu perf: %s', app_id, performance, accumulate_performance)
            elif accu_event_rate < goal_value:
                blacklist.append(app_id)
                logger.info('%s: %s, perf: %s, accu perf: %s',
                            'black app' if performance.get_event_rate() < goal_value else 'black burst app',
                            app_id, performance, accumulate_performance)
            else:
                whitelist.append(app_id)
                logger.debug('white app: %s, perf: %s, accu perf: %s', app_id, performance, accumulate_performance)

        return whitelist, blacklist, starvelist[:StableOptimizedLists.MAX_STARVE_LIST], max_event_rate

    def get_optimized_lists(self):
        """Get the optimized lists.
        :return stable_optimized_lists: an instance of <STABLE_OPTIMIZED_LISTS>
                                        with `whitelist`, `blacklist`, `starvelist`
        """
        return STABLE_OPTIMIZED_LISTS(
            AppList(self.sub_whitelist, 1),
            AppList(self.sub_blacklist, 1),
            AppList(self.pub_whitelist, 0),
            AppList(self.pub_blacklist, 0),
            AppList(self.starvelist, self.is_subpublisher)
        )


class HydraHardlists(object):
    """Interface to build hard blacklists and whitelists. """
    def __init__(self, listctrl_cfg, appid_ruler, idash_endpoint, appier_rule_endpoint,
                 df_performance_cache, hydra_performance_cache):
        self.listctrl_cfg = listctrl_cfg
        self.appid_ruler = appid_ruler
        self.idash_endpoint = idash_endpoint
        self.appier_rule_endpoint = appier_rule_endpoint
        self.df_performance_cache = df_performance_cache
        self.hydra_performance_cache = hydra_performance_cache
        self.cid2non_hydra_pub_blacklist = None
        self.cid2non_hydra_sub_blacklist = None

    def _get_constraint_oid_blacklists(self, df_params, date_str):
        """Gets hard constraint blacklists with Install >= 40
        and (RR-1D <= 0.05 for app_open adgroup or PRAT = 0 for others).
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :param date_str:
        :return oid_pub_blacklist: set of publisher level app_id
        :return oid_sub_blacklist: set of sub-publisher level app_id
        """
        oid_pub_blacklist = set()
        oid_sub_blacklist = set()
        query_format = minor_rr_filter_target_date_query \
            if df_params.event_type == 'app_open' else minor_prat_filter_target_date_query

        for (is_subpublisher, blacklist) in [(0, oid_pub_blacklist), (1, oid_sub_blacklist)]:
            query_key = DeepFunnelPerformanceCache.QUERY_KEY(
                date_str, df_params.oid, is_subpublisher, df_params.partner,
                df_params.category_tuple, df_params.country, df_params.goal_type, df_params.event_type,
                self.listctrl_cfg['df_prediction_table'], query_format)
            blacklist |= set(self.df_performance_cache.get_df_query_app_ids(query_key))

        logger.info('Constraint oid %s-%s blacklist, publisher %d, subpublisher %d',
                    df_params.oid, df_params.partner, len(oid_pub_blacklist), len(oid_sub_blacklist))
        logger.debug('%s, %s', oid_pub_blacklist, oid_sub_blacklist)
        return oid_pub_blacklist, oid_sub_blacklist

    def cache_non_hydra_cid_bwlist(self, cid_list, date_str):
        """Cache non hydra blacklists from other sources, fraud, cm, and auto rule.
           This method should be called before accessing self.cid2non_hydra_xub_blacklist.
        :param cid_list:
        :param date_str:
        """
        self.cid2non_hydra_pub_blacklist = defaultdict(set)
        self.cid2non_hydra_sub_blacklist = defaultdict(set)
        created_before = date_to_timestamp(date_str) if date_str else None
        cid2black_white_rules = self.idash_endpoint.batch_read_cid_black_white_rules(
            cid_list, created_before=created_before)
        for cid, black_white_rules in cid2black_white_rules.iteritems():
            for (blacklist, field) in [(self.cid2non_hydra_sub_blacklist[cid], 'reject_sub_publisher'),
                                       (self.cid2non_hydra_pub_blacklist[cid], 'reject_publisher')]:
                for rule_id, app_id_set in black_white_rules[field].items():
                    if rule_id != RULE_ID_DEEPFUNNEL:
                        blacklist |= app_id_set
            logger.info('Non-Hydra cid blacklist %s, publisher %d, subpublisher %d',
                        cid, len(self.cid2non_hydra_pub_blacklist), len(self.cid2non_hydra_sub_blacklist))
            logger.debug('%s, %s', self.cid2non_hydra_pub_blacklist, self.cid2non_hydra_sub_blacklist)

    def _get_hydra_cid_blacklists(self, df_params, n_day, decay_rate, install_threshold, use_pub_blacklist):
        """Get cid level blacklist for HydraOID, HydraSM and HydraBL.
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :param n_day: KPI date range
        :param decay_rate: metrics decay rate
        :param install_threshold: app_id with installs less than install_threshold can not be in blacklist
        :param use_pub_blacklist: use publisher level blacklist or not, <bool>
        :return cid_pub_blacklist: set of publisher level app_id
        :return cid_sub_blacklist: set of sub-publisher level app_id
        """
        # `both` black sub for unstable cid
        is_subpublisher = df_params.app_usage.needs_sub_performance
        cid_app_performance_data = self.hydra_performance_cache.get_cid_app_performance_data(
            df_params, is_subpublisher, n_day=n_day, decay_rate=decay_rate, install_decayed=True)

        blacklist = self._get_blacklist_set(cid_app_performance_data, install_threshold, df_params.raw_event_goal)
        cid_pub_blacklist = blacklist if not is_subpublisher else set()
        cid_sub_blacklist = blacklist if is_subpublisher else set()

        # check publisher-level blacklist if partner supports blackpub and blacksub
        if use_pub_blacklist and df_params.app_usage.black_publisher and is_subpublisher:
            cid_pub_performance_data = self.hydra_performance_cache.get_cid_app_performance_data(
                df_params, False, n_day=n_day, decay_rate=decay_rate, install_decayed=True)
            pub_blacklist = self._get_blacklist_set(
                cid_pub_performance_data, install_threshold, df_params.raw_event_goal)
            pub2sub_set = self.hydra_performance_cache.get_pub_to_sub_set(
                df_params.partner, cid_app_performance_data.keys())
            cid_pub_blacklist = get_publisher_blacklist(
                pub_blacklist, cid_sub_blacklist | self.cid2non_hydra_sub_blacklist[df_params.cid], pub2sub_set,
                cid_pub_performance_data, cid_app_performance_data, install_threshold, df_params.raw_event_goal)

        logger.info('cid: %s has cid level blacklist pub: %s, sub: %s',
                    df_params.cid, cid_pub_blacklist, cid_sub_blacklist)
        return cid_pub_blacklist, cid_sub_blacklist

    def _get_blacklist_set(self, cid_app_performance_data, install_threshold, goal_value):
        blacklist = set()
        accu_performance = Performance(0.0, 0.0, 0.0, 0.0)
        for app_id in sorted(cid_app_performance_data,
                             key=lambda app: cid_app_performance_data[app].get_local_performance_sort_key(),
                             reverse=True):
            performance = cid_app_performance_data[app_id].get_local_performance()
            if performance.installs < install_threshold:
                continue

            accu_performance += performance
            if accu_performance.get_event_rate() < goal_value:
                blacklist.add(app_id)

        return blacklist

    def _get_appier_rule_blacklist(self, df_params):
        """Get Appier rule cid blacklist.
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :return appier_rule_pub_blacklist: set of publisher level app_id
        :return appier_rule_sub_blacklist: set of sub-publisher level app_id
        """
        appier_rule_pub_blacklist, appier_rule_sub_blacklist = set(), set()
        url = '{}/do/appier_rule/get_cid_blist_with_final_rule?cid={}'.format(
            self.appier_rule_endpoint.config['host'], df_params.cid)
        results = self.appier_rule_endpoint.get_request_response(url, {'_': int(time.time())}, 'GET')
        b_is_subpublisher = df_params.app_usage.black_subpublisher
        for app_id in results:
            pub_app_id, sub_app_id = self.appid_ruler.get_pubid_subid_from_appid(df_params.partner, app_id)
            if b_is_subpublisher:
                appier_rule_sub_blacklist.add(sub_app_id)
            else:
                appier_rule_pub_blacklist.add(pub_app_id)

        logger.info('Appier rule cid blacklist: %s, pub: %s, sub: %s',
                    df_params.cid, appier_rule_pub_blacklist, appier_rule_sub_blacklist)
        return appier_rule_pub_blacklist, appier_rule_sub_blacklist

    def get_hard_blacklists(self, df_params, date_str, use_cid_blacklist, cid_install_threshold,
                            use_appier_rule, use_pub_blacklist):
        """Gets adgroup level pre-defined hard blacklists.
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :param date_str:
        :param use_cid_blacklist: use hydra cid performance blacklist or not
        :param cid_install_threshold: app_id with installs less than install_threshold can not be in hydra cid blacklist
        :param use_appier_rule: add blacklist from appier rule or not, <bool>
        :param use_pub_blacklist: use publisher level blacklist or not, <bool>
        :return hard_blacklists: a instance of <HARD_BLACKLIST> with `all_pub`, `all_sub`, `hydra_pub`, `hydra_sub`
        """
        # Naive bad performance filter
        oid_pub_blacklist, oid_sub_blacklist = self._get_constraint_oid_blacklists(df_params, date_str)
        # Non-Hydra blacklists
        cid_pub_blacklist = self.cid2non_hydra_pub_blacklist[df_params.cid]
        cid_sub_blacklist = self.cid2non_hydra_sub_blacklist[df_params.cid]
        # Hydra cid blacklist
        if use_cid_blacklist:
            hydra_pub_blacklist, hydra_sub_blacklist = self._get_hydra_cid_blacklists(
                df_params, 30, 0.9, cid_install_threshold, use_pub_blacklist)
        else:
            hydra_pub_blacklist, hydra_sub_blacklist = set(), set()
        # Appier Rule blacklist
        if use_appier_rule:
            appier_rule_pub_blacklist, appier_rule_sub_blacklist = self._get_appier_rule_blacklist(df_params)
        else:
            appier_rule_pub_blacklist, appier_rule_sub_blacklist = set(), set()

        hard_pub_blacklist = oid_pub_blacklist | cid_pub_blacklist | hydra_pub_blacklist | appier_rule_pub_blacklist
        hard_sub_blacklist = oid_sub_blacklist | cid_sub_blacklist | hydra_sub_blacklist | appier_rule_sub_blacklist
        # for partner supports sub-publisher blacklist, we only writes cid-level publisher blacklist
        output_pub_blacklist = oid_pub_blacklist | hydra_pub_blacklist \
            if not df_params.app_usage.black_subpublisher else hydra_pub_blacklist
        output_sub_blacklist = oid_sub_blacklist | hydra_sub_blacklist
        non_hydra_pub_blacklist = cid_pub_blacklist | appier_rule_pub_blacklist
        non_hydra_sub_blacklist = cid_sub_blacklist | appier_rule_sub_blacklist
        logger.info('Hard blacklist %s, publisher %d, subpublisher %d',
                    df_params.cid, len(hard_pub_blacklist), len(hard_sub_blacklist))
        return HARD_BLACKLISTS(
            AppList(hard_pub_blacklist, 0), AppList(hard_sub_blacklist, 1),
            AppList(output_pub_blacklist, 0), AppList(output_sub_blacklist, 1),
            AppList(non_hydra_pub_blacklist, 0), AppList(non_hydra_sub_blacklist, 1))

    def get_dryrun_hard_blacklists(self, df_params, date_str):
        """Gets adgroup level pre-defined hard blacklists for dry run mode.
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :param date_str:
        :return hard_blacklists: a instance of <HARD_BLACKLIST> with `all_pub`, `all_sub`, `hydra_pub`, `hydra_sub`
        """
        # Naive bad performance filter
        oid_pub_blacklist, oid_sub_blacklist = self._get_constraint_oid_blacklists(df_params, date_str)
        logger.info('Hard blacklist %s, publisher %d, subpublisher %d',
                    df_params.cid, len(oid_pub_blacklist), len(oid_sub_blacklist))
        return HARD_BLACKLISTS(
            AppList(oid_pub_blacklist, 0), AppList(oid_sub_blacklist, 1),
            AppList(oid_pub_blacklist, 0), AppList(oid_sub_blacklist, 1),
            AppList([], 0), AppList([], 1))

    def get_hard_whitelists(self, df_params, date_str):
        """Get adgroup-level hard whitelists.
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :param date_str:
        :return hard_whitelists: a instance of <HARD_WHITELIST> with `pub`, `sub`
        """
        is_subpublisher = 1 if df_params.app_usage.needs_sub_performance else 0
        stable_optimized_lists = StableOptimizedLists(df_params, is_subpublisher)
        cid_performance_data = self.hydra_performance_cache.get_cid_app_performance_data(
            df_params, is_subpublisher, n_day=30, decay_rate=0.9, date_str=date_str, install_decayed=False)
        if is_subpublisher:
            stable_optimized_lists.train_optimized_lists(None, cid_performance_data, 20, None)
        else:
            stable_optimized_lists.train_optimized_lists(cid_performance_data, None, 20, None)
        optimized_lists = stable_optimized_lists.get_optimized_lists()

        logger.info('Hard whitelist %s: %s, %s', df_params.cid, optimized_lists.pub_white, optimized_lists.sub_white)
        return HARD_WHITELISTS(optimized_lists.pub_white, optimized_lists.sub_white)

    def get_dryrun_hard_whitelists(self):
        """Get adgroup-level hard whitelists for dry run.
        :return hard_whitelists: a instance of <HARD_WHITELIST> with `pub`, `sub`
        """
        return HARD_WHITELISTS(AppList([], 0), AppList([], 1))
