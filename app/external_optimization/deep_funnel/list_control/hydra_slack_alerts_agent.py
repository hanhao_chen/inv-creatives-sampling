"""Define classes for sending slack alert messages for hydra-list-control. """
import json
import math
import time
from collections import defaultdict, namedtuple

from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.utils.constants import INVALID_MULTIPLE_MAJORS, \
    INVALID_MAJOR_TYPE, INVALID_MINOR_GOAL, INVALID_EMPTY_LIST, INVALID_PARTNER, INVALID_GOAL_VALUE, \
    INVALID_PRIMARY_GOAL, INVALID_VERTICAL, INVALID_NO_INSTALL_EVENT, INVALID_CPA_PARTNER
from app.external_optimization.deep_funnel.utils.slack_alerts_agent import SlackAlertsAgent

logger = init_logger(__name__)
MESSAGE = namedtuple('message', ['alert_audience', 'alerts'])
ALERT = namedtuple('alert', ['color', 'cid', 'oid', 'cid_name', 'oid_name', 'alert_type', 'impact', 'suggestions'])


class HydraSlackAlertsAgent(SlackAlertsAgent):
    """Provide interfaces to send slack alert message for update-hydra-list-control. """
    IDASH_URL = 'https://idash.appier.org/irobot3-adgroup/#/list/'
    INVALID_REASONS = {
        INVALID_MULTIPLE_MAJORS: 'event manager has multiple major event_types',
        INVALID_MAJOR_TYPE: 'oid Action Type is not app_install',
        INVALID_MINOR_GOAL: 'no Hydra supported minor goal is found',
        INVALID_EMPTY_LIST: 'pub/sub lists are empty',
        INVALID_PARTNER: 'supplier does not enable API',
        INVALID_GOAL_VALUE: 'minor goal value <= 0',
        INVALID_PRIMARY_GOAL: 'oid Campaign Type is not CPA',
        INVALID_VERTICAL: 'oid Campaign Vertical is not app_install',
        INVALID_NO_INSTALL_EVENT: 'no install event in event manager',
        INVALID_CPA_PARTNER: 'partner_id dose not support CPA buying'
    }

    def __init__(self, webhooks_url, auth_token, query_db, db_client, logging_table_name, idash_endpoint):
        super(HydraSlackAlertsAgent, self).__init__(webhooks_url, auth_token, query_db, db_client, logging_table_name)
        self.idash_endpoint = idash_endpoint

    def init_mysql_logging_table(self):
        """Initialize the MySQL table for logging slack messages. """
        query = """
        CREATE TABLE IF NOT EXISTS `{}` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `alert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `cid` varchar(32) NOT NULL,
            `oid` varchar(32) NOT NULL,
            `alert_type` varchar(32) NOT NULL,
            `alert_audience` varchar(32) NOT NULL,
            `message` text NOT NULL,
            PRIMARY KEY (`id`, `alert_time`),
            UNIQUE KEY `unique_key` (`alert_time`, `cid`, `oid`, `alert_type`),
            KEY `alert_type_search` (`alert_time`, `alert_type`),
            KEY `cid_search` (`alert_time`, `cid`),
            KEY `oid_search` (`alert_time`, `oid`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(self.logging_table_name)
        logger.info(query)
        self.query_db.execute_query(query, db_client=self.db_client)

    def update_mysql_logging_table(self, message):
        """Update a slack message into logging table.
        :param message: an instance of <MESSAGE>
        """
        rows = []
        for alert in message.alerts:
            short_alert_type = alert.alert_type.split('because ')[0].strip()
            reasons = alert.alert_type.split('because ')[1].strip().split(', ')
            json_message = {
                'impact': alert.impact,
                'suggestions': alert.suggestions,
                'reasons': reasons
            }
            rows.append([alert.cid, alert.oid, short_alert_type, message.alert_audience, json.dumps(json_message)])

        query = """
        REPLACE INTO `{}`
        (cid, oid, alert_type, alert_audience, message)
        VALUES
        (%s, %s, %s, %s, %s)
        """.format(self.logging_table_name)
        logger.info(query)
        self.query_db.execute_query_many(query, rows, db_client=self.db_client)

    def get_skippable_cids_and_oids(self, alert_cids, alert_oids):
        """Get alert-skippable cids and oids.
        :param alert_cids: list of cids
        :param alert_oids: list of oids
        :return skippable_cids: set of cids
        :return skippable_oids: set of oids
        """
        skippable_cids = set()
        alertable_timestamp = int(time.time()) - 12600
        for row in self.idash_endpoint.read_campaigns_by_cids(alert_cids, fields=['created_at']):
            if int(row['created_at']) > alertable_timestamp:
                skippable_cids.add(row['cid'])

        skippable_oids = set()
        for row in self.idash_endpoint.read_orders(alert_oids, fields=['status_code', 'status_message']):
            if row['status_code'].lower() in ('finished', 'stopped', 'new') \
                    or row['status_message'].startswith('[CUSTOM PERIOD] paused'):
                skippable_oids.add(row['oid'])

        return skippable_cids, skippable_oids

    def aggregate_alerts_by_audience(self, alerts, message_max_alerts=20):
        """Aggregate the alerts by it's audience.
        :param alerts: list of <ALERT> instances
        :return messages: list of <MESSAGE> instances
        """
        cids = list(set(alert.cid for alert in alerts))
        oids = list(set(alert.oid for alert in alerts))

        query = 'select cid, manager from campaign where cid in ({})'.format(
            ','.join('\'{}\''.format(cid) for cid in cids))
        cid2manager = {
            row['cid']: row['manager']
            for row in self.query_db.get_query_result(query, db_client=self.query_db.get_db_client_by_table('campaign'))
        }

        skippable_cids, skippable_oids = self.get_skippable_cids_and_oids(cids, oids)
        audience_alerts = defaultdict(list)
        for alert in alerts:
            if alert.oid not in skippable_oids and alert.cid not in skippable_cids:
                audience_alerts[cid2manager[alert.cid]].append(alert)

        messages = []
        for audience, alerts in audience_alerts.items():
            for idx in range(int(math.ceil(float(len(alerts)) / message_max_alerts))):
                messages.append(MESSAGE(audience, alerts[idx * message_max_alerts: (idx + 1) * message_max_alerts]))

        return messages

    def get_post_body(self, message):
        """Get the post body of slack message for POST.
        :param message: an instance of <MESSAGE>
        :return post_body: a json string
        """
        return json.dumps({
            'text': 'Hi, <@{}> you get {} alert messages from `hydra-list-update-alert`.'.format(
                self.get_slack_userid_from_mail(message.alert_audience), len(message.alerts)),
            'attachments': [
                {
                    'color': alert.color,
                    'fields': [
                        HydraSlackAlertsAgent._get_adgroup_campaign_field(alert),
                        HydraSlackAlertsAgent._get_situation_field(alert),
                        HydraSlackAlertsAgent._get_impact_field(alert),
                        HydraSlackAlertsAgent._get_suggestions_field(alert)
                    ]
                } for alert in sorted(message.alerts, key=lambda item: item.oid)
            ]
        })

    @staticmethod
    def _get_adgroup_campaign_field(alert):
        return {
            'title': 'Adgroup/Campaign',
            'value': u'<{0}{1}/{2}|{3}>\n<{0}{1}|{4}>'.format(
                HydraSlackAlertsAgent.IDASH_URL, alert.oid, alert.cid, alert.cid_name, alert.oid_name),
            'short': True
        }

    @staticmethod
    def _get_situation_field(alert):
        return {
            'title': 'Situation',
            'value': alert.alert_type,
            'short': True
        }

    @staticmethod
    def _get_impact_field(alert):
        return {
            'title': 'Impact',
            'value': '\n'.join(alert.impact),
            'short': True
        }

    @staticmethod
    def _get_suggestions_field(alert):
        return {
            'title': 'Suggestions',
            'value': '\n'.join(alert.suggestions),
            'short': True
        }

    @staticmethod
    def get_alert_type(hydra_label, reasons):
        """Get the alert_type field for constructing an <ALERT> instance.
        :param hydra_label: <string>
        :param reasons: list of reason string
        :return alert_type: <string>
        """
        status = 're-opened'
        for reason in reasons:
            if reason in HydraSlackAlertsAgent.INVALID_REASONS:
                status = 'closed'

        reasons_str = ', '.join(HydraSlackAlertsAgent.INVALID_REASONS.get(reason, reason) for reason in reasons)
        return '{} is {} because {}'.format(hydra_label, status, reasons_str)

    @staticmethod
    def get_impact(yesterday_performance, last_week_performance):
        """Get the impact field for constructing an <ALERT> instance.
        :param yesterday_performance: dict cantains `installs`, `major_count`, `minor_count` and `event_rate` or None
        :param last_week_performance: dict cantains `installs`, `major_count`, `minor_count` and `event_rate` or None
        :return impact: list of impact string
        """
        return [
            '{} installs yesterday'.format(yesterday_performance['installs'] if yesterday_performance else 0.0),
            '{} installs last week'.format(last_week_performance['installs'] if last_week_performance else 0.0)
        ]

    @staticmethod
    def get_suggestions(reasons, data):
        """Get the suggestions field for constructing an <ALERT> instance.
        :param reasons: list of reason string
        :param data: dict contains `goal_type` and `suggest_goal_rate` for INVALID_EMPTY_LIST alert
        :return suggestions: list of suggestion string
        """
        suggestions = set()
        for reason in reasons:
            if reason == INVALID_EMPTY_LIST:
                if data['suggest_goal_rate'] is None:
                    suggestions.add('no available pub/sub for whitelist, try HydraBL')
                elif data['suggest_goal_rate'] == 0.0:
                    suggestions.add('no minor event happened, check goal setting then try HydraBL')
                else:
                    suggestions.add('lower {} goal value at least to {}'.format(
                        data['goal_type'], data['suggest_goal_rate']))
            elif reason in (INVALID_MINOR_GOAL, INVALID_GOAL_VALUE):
                suggestions.add('check cid minor goal setting')
            elif reason in (INVALID_MAJOR_TYPE, INVALID_PRIMARY_GOAL, INVALID_VERTICAL):
                suggestions.add('check oid setting')
            elif reason in (INVALID_MULTIPLE_MAJORS, INVALID_NO_INSTALL_EVENT):
                suggestions.add('check event manager')
            elif reason in (INVALID_PARTNER, INVALID_CPA_PARTNER):
                suggestions.add('do not use Hydra')

        return list(suggestions)
