"""Define classes for updating `require_publisher`, `require_sub_publisher`, `reject_publisher`,
and `reject_sub_publisher` for Hydra cids.
"""
# TODO: replace the hydra output dict (`lists`) by a storage class
from __future__ import absolute_import
from __future__ import division
import datetime
import json

from app.utils.db import MySQL_DB_AI_DEEP_FUNNEL
from app.utils.init_logger import init_logger

from app.external_optimization.deep_funnel.list_control.list_generator import StableOptimizedLists, \
    BaseSingleOptimizedLists, CampaignOptimizedLists, SamplingOptimizedLists, HydraHardlists
from app.external_optimization.deep_funnel.list_control.mysql_queries import minor_high_mean_target_date_db_query, \
    minor_sampled_target_date_db_query, minor_black_category_target_date_query, minor_black_country_target_date_query, \
    minor_sampled_country_date_db_query
from app.external_optimization.deep_funnel.list_control.performance import DeepFunnelPerformanceCache, HydraPerformanceCache
from app.external_optimization.deep_funnel.utils.constants import DEFAULT_COUNTRY
from app.external_optimization.deep_funnel.utils.common_utils import AppList, AppUsage
from app.external_optimization.deep_funnel.utils.deep_funnel_adgroups import DeepFunnelAdgroupTree


logger = init_logger(__name__)


class InventoryFilter(object):
    """Class for applying inventory filters for performance data or lists. """
    def __init__(self, appid_ruler):
        self.appid_ruler = appid_ruler

    def remove_performance_data_from_blacklists(self, pub_blacklist, sub_blacklist,
                                                pub_performance_data, sub_performance_data, partner_id):
        """Apply hard blacklists on pub/sub performance before simulation.
        :param pub_blacklist: an instance of <AppList>, publisher blacklist to be applied
        :param sub_blacklist: an instance of <AppList>, subpublisher blacklist to be applied
        :param pub_performance_data: dict of publisher_id to <AppPerformance>
        :param sub_performance_data: dict of sub_publisher_id to <AppPerformance>
        :param partner_id:
        """
        origin_pub_size = len(pub_performance_data)
        origin_sub_size = len(sub_performance_data)

        for sub_app_id in list(sub_performance_data.keys()):
            pub_app_id, sub_app_id = self.appid_ruler.get_pubid_subid_from_appid(partner_id, sub_app_id)
            if pub_app_id and pub_app_id in pub_blacklist:  # match pub blacklist
                pub_performance_data.pop(pub_app_id, None)
                sub_performance_data.pop(sub_app_id, None)
            elif sub_app_id in sub_blacklist:  # match sub blacklist
                if pub_app_id in pub_performance_data:
                    pub_performance_data[pub_app_id] -= sub_performance_data[sub_app_id]
                sub_performance_data.pop(sub_app_id, None)

        for pub_app_id in list(pub_performance_data.keys()):
            if pub_app_id in pub_blacklist:
                pub_performance_data.pop(pub_app_id, None)

        logger.info('publisher list from %s to %s, subpublisher list from %s to %s',
                    origin_pub_size, len(pub_performance_data), origin_sub_size, len(sub_performance_data))

    def dedupe_sub_blacklist(self, partner_id, pub_blacklist, sub_blacklist):
        """Remove app_id in sub blacklist if it's already in pub blacklist
        :param partner_id:
        :param pub_blacklist: list of publisher id
        :param sub_blacklist: list of subpublisher id
        :return sub_blacklist: deduped sub blacklist
        """
        return [sub_id for sub_id in sub_blacklist
                if self.appid_ruler.get_pubid_subid_from_appid(partner_id, sub_id)[0] not in pub_blacklist]


class HydraROASListController(InventoryFilter):
    """Interface to append ROAS > 0 lists for Hydra. """
    def __init__(self, appid_ruler, hydra_performance_cache):
        super(HydraROASListController, self).__init__(appid_ruler)
        self.hydra_performance_cache = hydra_performance_cache

    def get_performance_data(self, df_params, date_str, hard_blacklists):
        """Get the performance data for Hydra (ROAS > 0).
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :param date_str:
        :param hard_blacklists: an instance of <HARD_BLACKLISTS>
        :return pub_performance_data: dict of app_id to <AppPerformance>
        :return sub_performance_data: dict of app_id to <AppPerformance>
        """
        pub_performance_data = self.hydra_performance_cache.get_cid_app_performance_data(
            df_params, 0, n_day=30, decay_rate=1.0, date_str=date_str, install_decayed=False)
        sub_performance_data = self.hydra_performance_cache.get_cid_app_performance_data(
            df_params, 1, n_day=30, decay_rate=1.0, date_str=date_str, install_decayed=False)

        if hard_blacklists is not None:
            self.remove_performance_data_from_blacklists(
                hard_blacklists.non_hydra_pub, hard_blacklists.non_hydra_sub,
                pub_performance_data, sub_performance_data, df_params.partner)

        return pub_performance_data, sub_performance_data

    def get_positive_roas_lists(self, df_params, date_str, hard_blacklists):
        """Get the ROAS > 0 publisehr/subpublisher app_id set.
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :param date_str:
        :param hard_blacklists: an instance of <HARD_BLACKLISTS>
        :return pub_whiteset: set of publisehr ids
        :return sub_whiteset: set of subpublisher ids
        """
        query_params = df_params._replace(
            raw_goal_type=df_params.roas_goal_type, event_type='app_purchase_with_revenue',
            event_ids=df_params.roas_event_ids)
        pub_performance_data, sub_performance_data = self.get_performance_data(
            query_params, date_str, hard_blacklists)

        pub_whitelist = AppList(
            app_id_set=set(publisher for publisher in pub_performance_data
                           if pub_performance_data[publisher].get_local_performance().minor_count),
            is_subpublisher=0,
            able_to_write=query_params.app_usage.black_publisher)
        sub_whitelist = AppList(
            app_id_set=set(subpublisher for subpublisher in sub_performance_data
                           if sub_performance_data[subpublisher].get_local_performance().minor_count),
            is_subpublisher=1,
            able_to_write=query_params.app_usage.black_subpublisher)

        return pub_whitelist.get_app_id_set(), sub_whitelist.get_app_id_set()

    def apply_positive_roas_filter(self, lists, date_str, hard_blacklists):
        """Apply positive ROAS filter on the final hydra result lists.
        :param lists: the dict of hydra list control result.
        :param date_str:
        :param hard_blacklists: an instance of <HARD_BLACKLISTS>
        """
        if not lists['params'].use_roas_filter:
            return

        pub_whiteset, sub_whiteset = self.get_positive_roas_lists(lists['params'], date_str, hard_blacklists)
        removed_publishers = set(lists['reject_publisher']) & pub_whiteset
        if removed_publishers:
            logger.info('remove %s reject publishers: %s', len(removed_publishers), removed_publishers)
            lists['reject_publisher'] = list(set(lists['reject_publisher']) - pub_whiteset)
            lists['note']['roas_removed_pub'] = list(removed_publishers)
            lists['note']['roas_removed_pub_cnt'] = len(removed_publishers)

        removed_subpublishers = set(lists['reject_sub_publisher']) & sub_whiteset
        if removed_subpublishers:
            logger.info('remove %s reject subpublishers: %s', len(removed_subpublishers), removed_subpublishers)
            lists['reject_sub_publisher'] = list(set(lists['reject_sub_publisher']) - sub_whiteset)
            lists['note']['roas_removed_sub'] = list(removed_subpublishers)
            lists['note']['roas_removed_sub_cnt'] = len(removed_subpublishers)

        if lists['hydra_label'] != 'HydraBL':
            if lists['params'].app_usage.white_subpublisher:
                added_subpublishers = sub_whiteset - set(lists['require_sub_publisher'])
                if added_subpublishers:
                    logger.info('add %s require subpublishers %s', len(added_subpublishers), added_subpublishers)
                    lists['require_sub_publisher'] = list(set(lists['require_sub_publisher']) | sub_whiteset)
                    lists['note']['roas_added_sub'] = list(added_subpublishers)
                    lists['note']['roas_added_sub_cnt'] = len(added_subpublishers)
            elif lists['params'].app_usage.white_publisher:
                added_publishers = pub_whiteset - set(lists['require_publisher'])
                if added_publishers:
                    logger.info('add %s require publishers %s', len(added_publishers), added_publishers)
                    lists['require_publisher'] = list(set(lists['require_publisher']) | pub_whiteset)
                    lists['note']['roas_added_pub'] = list(added_publishers)
                    lists['note']['roas_added_pub_cnt'] = len(added_publishers)


class HydraStableListController(InventoryFilter):
    """Interface to update lists for HydraWL and HydraX. """
    def __init__(self, hydra_performance_cache, idash_endpoint, appid_ruler, cid_hard_blacklists):
        super(HydraStableListController, self).__init__(appid_ruler)
        self.hydra_performance_cache = hydra_performance_cache
        self.idash_endpoint = idash_endpoint
        self.cid_hard_blacklists = cid_hard_blacklists

    def get_performance_data(self, df_params, date_str, current_whitelist, hard_blacklists):
        """Get the performance data for HydraWL simulation.
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :param date_str:
        :param current_whitelist:
        :param hard_blacklists: an instance of <HARD_BLACKLISTS>
        :return cid_pub_performance_data: dict of app_id to <AppPerformance>
        :return cid_sub_performance_data: dict of app_id to <AppPerformance>
        """
        day_range = 30 if df_params.raw_goal_type in ('PRAT', 'ROAS') else 7
        cid_pub_performance_data = self.hydra_performance_cache.get_cid_app_performance_data(
            df_params, 0, n_day=day_range, decay_rate=1.0, date_str=date_str, install_decayed=False,
            app_ids=current_whitelist.get('require_publisher'))
        cid_sub_performance_data = self.hydra_performance_cache.get_cid_app_performance_data(
            df_params, 1, n_day=day_range, decay_rate=1.0, date_str=date_str, install_decayed=False,
            app_ids=current_whitelist.get('require_sub_publisher'))

        self.remove_performance_data_from_blacklists(
            hard_blacklists.all_pub, hard_blacklists.all_sub,
            cid_pub_performance_data, cid_sub_performance_data, df_params.partner)
        return cid_pub_performance_data, cid_sub_performance_data

    def get_optimized_lists(self, df_params, cid_pub_performance_data, cid_sub_performance_data, w_is_subpublisher):
        """Get stable optimized lists after training <StableOptimizedLists>, then apply the hard blacklist.
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS> for stable cid
        :param cid_pub_performance_data: dict of app_id to <AppPerformance>
        :param cid_sub_performance_data: dict of app_id to <AppPerformance>
        :param w_is_subpublisher: 1 or 0, whitelist is subpublisher or not
        :return optimized_lists: an instance of <STABLE_OPTIMIZED_LISTS>
        :return max_event_rate: the maximum accumulated event rate during simulation
        """
        pub2sub_set = self.hydra_performance_cache.get_pub_to_sub_set(
            df_params.partner, cid_sub_performance_data.keys())
        stable_optimized_lists = StableOptimizedLists(df_params, w_is_subpublisher)
        stable_optimized_lists.train_optimized_lists(
            cid_pub_performance_data, cid_sub_performance_data, 5, pub2sub_set)
        optimized_lists = stable_optimized_lists.get_optimized_lists()
        max_event_rate = stable_optimized_lists.get_max_event_rate()
        return optimized_lists, max_event_rate

    def update_stable_cids_lists(self, stable_params, date_str):
        """Update lists for all stable cids.
        :param stable_params: dict of stable_cid to it's df_params
        :param date_str:
        :return hydra_stable_lists_results: dict of stable cid to hydra lists result
        :return total_stable_whitelist: an instance of <AppList>, whitelist of all stable cids
        """
        hydra_stable_lists_results = {}
        # `both` partners white pub
        w_is_subpublisher = 1 if stable_params and stable_params.values()[0].app_usage.white_subpublisher else 0
        total_stable_whitelist = AppList([], w_is_subpublisher)
        for stable_cid, params in stable_params.iteritems():
            hard_blacklists = self.cid_hard_blacklists[stable_cid]
            current_whitelist, current_blacklist = self.idash_endpoint.read_ai_black_white_rules_df(stable_cid)
            cid_pub_performance_data, cid_sub_performance_data = self.get_performance_data(
                params, date_str, current_whitelist, hard_blacklists)
            optimized_lists, max_event_rate = self.get_optimized_lists(
                params, cid_pub_performance_data, cid_sub_performance_data, w_is_subpublisher)

            pub_blacklist = optimized_lists.pub_black | hard_blacklists.hydra_pub \
                | AppList(set(current_blacklist.get('reject_publisher', [])), 0)
            sub_blacklist = optimized_lists.sub_black | hard_blacklists.hydra_sub \
                | AppList(set(current_blacklist.get('reject_sub_publisher', [])), 1)

            if w_is_subpublisher:
                total_stable_whitelist |= optimized_lists.sub_white
                optimized_lists.sub_white |= optimized_lists.starvelist
            else:
                total_stable_whitelist |= optimized_lists.pub_white
                optimized_lists.pub_white |= optimized_lists.starvelist

            hydra_stable_lists_results[stable_cid] = {
                'require_publisher': optimized_lists.pub_white.get_app_id_list(),
                'require_sub_publisher': optimized_lists.sub_white.get_app_id_list(),
                'reject_publisher': pub_blacklist.get_app_id_list(),
                'reject_sub_publisher': self.dedupe_sub_blacklist(
                    params.partner, pub_blacklist.get_app_id_set(), sub_blacklist.get_app_id_list()),
                'hydra_label': 'HydraWL',
                'note': {
                    'starve_app_set': optimized_lists.starvelist.get_app_id_list(),
                    'performance_sub_black': optimized_lists.sub_black.get_app_id_list(),
                    'performance_pub_black': optimized_lists.pub_black.get_app_id_list(),
                    'others_pub_count': len(hard_blacklists.all_pub - hard_blacklists.hydra_pub),
                    'others_sub_count': len(hard_blacklists.all_sub - hard_blacklists.hydra_sub),
                    'max_event_rate': max_event_rate
                },
                'params': params
            }

        return hydra_stable_lists_results, total_stable_whitelist

    def check_garbage_cids_recovery(self, garbage_params, date_str):
        """Check garbage cids can be recover or not.
        :param garbage_params: dict of garbage cid to it's df_params
        :param date_str:
        :return hydra_garbage_lists_results: dict of garbage cid to hydra lists result
        :return total_garbage_whitelist: an instance of <AppList>, whitelist of all garbage cids
        :return recovered_cids: list of garbage cid that can be recovered
        """
        hydra_garbage_lists_results = {}
        recovered_cids = []
        hydra_lists_results, total_garbage_whitelist = self.update_stable_cids_lists(garbage_params, date_str)
        for cid, lists in hydra_lists_results.iteritems():
            if lists['require_publisher'] or lists['require_sub_publisher']:
                recovered_cids.append(cid)
                hydra_garbage_lists_results[cid] = lists

        if recovered_cids:
            logger.info('%s cid recovered: %s', len(recovered_cids), recovered_cids)
        return hydra_garbage_lists_results, total_garbage_whitelist, recovered_cids

    def update_hydra_lists_results(self, stable_params, garbage_params, date_str):
        """Update hydra lists for stable cids and garbage cids
        :param stable_params: dict of stable_cid to it's df_params
        :param garbage_params: dict of garbage cid to it's df_params
        :param date_str:
        :return hydra_lists_results: dict of cid to hydra lists result
        :return total_stable_whitelist: an instance of <AppList>, whitelist of all stable cids
        :return recovered_cids: list of garbage cid that can be recovered
        """
        hydra_lists_results = {}

        hydra_stable_lists_results, total_stable_whitelist = self.update_stable_cids_lists(stable_params, date_str)
        hydra_lists_results.update(hydra_stable_lists_results)

        hydra_garbage_lists_results, total_garbage_whitelist, recovered_cids = self.check_garbage_cids_recovery(
            garbage_params, date_str)
        hydra_lists_results.update(hydra_garbage_lists_results)

        total_stable_whitelist |= total_garbage_whitelist
        return hydra_lists_results, total_stable_whitelist, recovered_cids


class HydraSamplingListController(InventoryFilter):
    """Interface to update lists for HydraSM, HydraOID and HydraBL. """
    def __init__(self, listctrl_cfg, hydra_performance_cache, df_performance_cache, appid_ruler,
                 cid_hard_blacklists, cid_hard_whitelists, stable_whitelist, adgroup_tree):
        super(HydraSamplingListController, self).__init__(appid_ruler)
        self.listctrl_cfg = listctrl_cfg
        self.hydra_performance_cache = hydra_performance_cache
        self.df_performance_cache = df_performance_cache
        self.cid_hard_blacklists = cid_hard_blacklists
        self.cid_hard_whitelists = cid_hard_whitelists
        self.stable_whitelist = stable_whitelist
        self.adgroup_tree = adgroup_tree

    def enhance_blacklist(self, df_params, blacklist):
        """Add category and country level blacklist.
        :param df_params: instance of <Hydra.DF_ADGROUP_PARAMS>
        :param blacklist: instance of <AppList>
        :return blacklist: enhanced blacklist <AppList>
        """
        # remove category level bad performance
        category_query_key = DeepFunnelPerformanceCache.QUERY_KEY(
            None, df_params.oid, blacklist.is_subpublisher, df_params.partner,
            df_params.category_tuple, df_params.country, df_params.goal_type, df_params.event_type,
            self.listctrl_cfg['quality_prediction_table'], minor_black_category_target_date_query)
        category_black_set = AppList(
            self.df_performance_cache.get_df_query_app_ids(category_query_key, use_hydra_db=True),
            blacklist.is_subpublisher)

        # remove country level bad performance
        country_query_key = DeepFunnelPerformanceCache.QUERY_KEY(
            None, df_params.oid, blacklist.is_subpublisher, df_params.partner,
            df_params.category_tuple, df_params.country, df_params.goal_type, df_params.event_type,
            self.listctrl_cfg['quality_prediction_table'], minor_black_country_target_date_query)
        country_black_set = AppList(
            self.df_performance_cache.get_df_query_app_ids(country_query_key, use_hydra_db=True),
            blacklist.is_subpublisher)

        n_origin = len(blacklist)
        blacklist |= category_black_set
        blacklist |= country_black_set
        logger.info('add category, country blacklist from %s to %s: cid %s, partner %s, category %s, country %s',
                    n_origin, len(blacklist), df_params.cid, df_params.partner,
                    len(category_black_set), len(country_black_set))
        return blacklist

    def get_performance_data(self, global_params, local_params, date_str):
        """Get performance data for HydraSM and HydraOID simulation.
        :param global_params: HydraSM cid's df_params
        :param local_params: HydraOID cid's df_params
        :param date_str:
        :return local_pub_performance_data: oid performance, dict of publisher_id to <AppPerformance>
        :return local_sub_performance_data: oid performance, dict of sub_publisher_id to <AppPerformance>
        :return local_use_raw_goal: whether oid_performance can use raw_event_goal to simulate or not
        :return global_pub_performance_data: category performance, dict of publisher_id to <AppPerformance>
        :return global_sub_performance_data: category performance, dict of sub_publisher_id to <AppPerformance>
        :return global_use_raw_goal: whether global_performance can use raw_event_goal to simulate or not
        :return country_pub_performance_data: country performance, dict of publisher_id to <AppPerformance>
        :return country_sub_performance_data: country performance, dict of sub_publisher_id to <AppPerformance>
        :return country_use_raw_goal: whether country_performance can use raw_event_goal to simulate or not
        """
        local_pub_query_key = DeepFunnelPerformanceCache.QUERY_KEY(
            date_str, local_params.oid, 0, local_params.partner,
            local_params.category_tuple, local_params.country, local_params.goal_type, local_params.event_type,
            self.listctrl_cfg['df_prediction_table'], minor_high_mean_target_date_db_query)
        local_sub_query_key = local_pub_query_key._replace(is_subpublisher=1)
        global_pub_query_key = DeepFunnelPerformanceCache.QUERY_KEY(
            date_str, global_params.oid, 0, global_params.partner,
            global_params.category_tuple, global_params.country, global_params.goal_type, global_params.event_type,
            self.listctrl_cfg['df_prediction_table'], minor_sampled_target_date_db_query)
        global_sub_query_key = global_pub_query_key._replace(is_subpublisher=1)
        country_pub_query_key = global_pub_query_key._replace(oid=DEFAULT_COUNTRY,
                                                              query=minor_sampled_country_date_db_query)
        country_sub_query_key = country_pub_query_key._replace(is_subpublisher=1)

        local_sub_performance_data = self.df_performance_cache.get_df_performance_data(local_sub_query_key) \
            if local_params.app_usage.needs_sub_performance else {}
        local_pub_performance_data = self.df_performance_cache.get_df_performance_data(local_pub_query_key) \
            if local_params.app_usage.needs_pub_performance else {}
        global_sub_performance_data = self.df_performance_cache.get_df_performance_data(
            global_sub_query_key, coldstart=True) if global_params.app_usage.needs_sub_performance else {}
        global_pub_performance_data = self.df_performance_cache.get_df_performance_data(
            global_pub_query_key, coldstart=True) if global_params.app_usage.needs_pub_performance else {}
        country_sub_performance_data = self.df_performance_cache.get_df_performance_data(country_sub_query_key) \
            if global_params.app_usage.needs_sub_performance else {}
        country_pub_performance_data = self.df_performance_cache.get_df_performance_data(country_pub_query_key) \
            if global_params.app_usage.needs_pub_performance else {}

        (local_pub_performance_data,
         local_sub_performance_data, local_use_raw_goal) = self.hydra_performance_cache.update_df_simulation_data(
             local_params, 'oid_app_id', local_pub_performance_data, local_sub_performance_data)
        (global_pub_performance_data,
         global_sub_performance_data, global_use_raw_goal) = self.hydra_performance_cache.update_df_simulation_data(
             global_params, 'category_app_id', global_pub_performance_data, global_sub_performance_data)
        (country_pub_performance_data,
         country_sub_performance_data, country_use_raw_goal) = self.hydra_performance_cache.update_df_simulation_data(
             global_params, 'country_app_id', country_pub_performance_data, country_sub_performance_data)

        if local_params.cid in self.cid_hard_blacklists:
            self.remove_performance_data_from_blacklists(
                self.cid_hard_blacklists[local_params.cid].all_pub, self.cid_hard_blacklists[local_params.cid].all_sub,
                local_pub_performance_data, local_sub_performance_data, local_params.partner)
        if global_params.cid in self.cid_hard_blacklists:
            hard_blacklists = self.cid_hard_blacklists[global_params.cid]
            self.remove_performance_data_from_blacklists(
                hard_blacklists.all_pub, hard_blacklists.all_sub,
                global_pub_performance_data, global_sub_performance_data, global_params.partner)
            self.remove_performance_data_from_blacklists(
                hard_blacklists.all_pub, hard_blacklists.all_sub,
                country_pub_performance_data, country_sub_performance_data, global_params.partner)

        return (local_pub_performance_data, local_sub_performance_data, local_use_raw_goal,
                global_pub_performance_data, global_sub_performance_data, global_use_raw_goal,
                country_pub_performance_data, country_sub_performance_data, country_use_raw_goal)

    def get_optimized_lists(self, global_params, local_params, local_pub_performance_data, local_sub_performance_data,
                            global_pub_performance_data, global_sub_performance_data, global_use_raw_goal,
                            country_pub_performance_data, country_sub_performance_data, country_use_raw_goal):
        """Get sampling optimized lists after training <CampaignOptimizedLists> and <SamplingOptimizedLists>.
        :param global_params: HydraSM cid's df_params
        :param local_params: HydraOID cid's df_params
        :param local_pub_performance_data: oid performance, dict of publisher_id to <AppPerformance>
        :param local_sub_performance_data: oid performance, dict of sub_publisher_id to <AppPerformance>
        :param global_pub_performance_data: category performance, dict of publisher_id to <AppPerformance>
        :param global_sub_performance_data: category performance, dict of sub_publisher_id to <AppPerformance>
        :param global_use_raw_goal: whether global_performance can use raw_event_goal to simulate or not
        :param country_pub_performance_data: country performance, dict of publisher_id to <AppPerformance>
        :param country_sub_performance_data: country performance, dict of sub_publisher_id to <AppPerformance>
        :param country_use_raw_goal: whether country_performance can use raw_event_goal to simulate or not
        :return global_optimized_list: an instance of <SamplingOptimizedLists> been trained
        :return local_optimized_list: an instance of <CampaignOptimizedLists> been trained
        """
        local_optimized_list = CampaignOptimizedLists(local_params, self.appid_ruler)
        install_threshold = 10 if DeepFunnelAdgroupTree.is_valid_cid(self.adgroup_tree['campaign_cid']) else 0
        local_optimized_list.train_optimized_lists(
            local_pub_performance_data, local_sub_performance_data,
            self.stable_whitelist.get_app_id_set(), install_threshold)

        global_optimized_list = SamplingOptimizedLists(global_params, self.appid_ruler)
        country_optimized_list = SamplingOptimizedLists(global_params, self.appid_ruler)
        optimized_lists = local_optimized_list.get_optimized_lists()
        oid_whitelist = optimized_lists.sub_white.get_app_id_set() | optimized_lists.pub_white.get_app_id_set()
        global_cid = global_params.cid if global_params.do_cid_update else local_params.cid
        local_cid = local_params.cid if local_params.do_cid_update else global_params.cid
        accumulate_simulation = global_cid == local_cid
        global_optimized_list.train_optimized_lists(
            global_pub_performance_data, global_sub_performance_data, self.stable_whitelist.get_app_id_set(),
            oid_whitelist, accumulate_simulation, global_use_raw_goal)
        country_optimized_list.train_optimized_lists(
            country_pub_performance_data, country_sub_performance_data, self.stable_whitelist.get_app_id_set(),
            oid_whitelist, accumulate_simulation, country_use_raw_goal, use_goal_rate_filter=True)
        return global_optimized_list, local_optimized_list, country_optimized_list

    def get_black_sampling_cid_lists(self, df_params, hydra_oid_lists):
        """Gets lists for a black only partner's cid.
        :param df_params:
        :param hydra_oid_lists: an instance of <OPTIMIZED_LISTS>
        :result lists: hydra output result
        """
        hard_whitelists = self.cid_hard_whitelists[df_params.cid]
        hard_blacklists = self.cid_hard_blacklists[df_params.cid]

        if hydra_oid_lists.sub_black.able_to_write:
            sub_black = self.enhance_blacklist(df_params, hydra_oid_lists.sub_black)
            pub_black = hydra_oid_lists.pub_black
            # for partner supports sub-publisher blacklist, we won't write publisher blacklist from simulation
            pub_black.clear()
        else:
            sub_black = hydra_oid_lists.sub_black
            pub_black = self.enhance_blacklist(df_params, hydra_oid_lists.pub_black)

        sub_blacklist = (sub_black | hard_blacklists.hydra_sub) - hard_whitelists.sub
        pub_blacklist = (pub_black | hard_blacklists.hydra_pub) - hard_whitelists.pub
        lists = {
            'require_publisher': [],
            'require_sub_publisher': [],
            'reject_publisher': pub_blacklist.get_app_id_list(),
            'reject_sub_publisher': self.dedupe_sub_blacklist(
                df_params.partner, pub_blacklist.get_app_id_set(), sub_blacklist.get_app_id_list()),
            'hydra_label': 'HydraBL',
            'note': {
                'others_pub_count': len(hard_blacklists.all_pub - hard_blacklists.hydra_pub),
                'others_sub_count': len(hard_blacklists.all_sub - hard_blacklists.hydra_sub)
            },
            'params': df_params
        }
        return lists

    def get_white_sampling_cid_lists(self, df_params, hydra_lists, hydra_label, max_event_rate):
        """Get lists for a HydraOID/HydraSM cid.
        :param df_params:
        :param hydra_lists: an instance of <OPTIMIZED_LISTS>
        :param hydra_label: HydraSM or HydraOID
        :param max_event_rate: the maximum accumulated event rate during simulation
        :result lists: hydra output result
        """
        no_whitelist = not hydra_lists.sub_white and not hydra_lists.pub_white
        if hydra_lists.sub_black.able_to_write:
            sub_blacklist = self.enhance_blacklist(df_params, hydra_lists.sub_black) \
                if no_whitelist else hydra_lists.sub_black
            pub_blacklist = hydra_lists.pub_black
            # for partner supports sub-publisher blacklist, we won't write publisher blacklist from simulation
            pub_blacklist.clear()
        else:
            sub_blacklist = hydra_lists.sub_black
            pub_blacklist = self.enhance_blacklist(df_params, hydra_lists.pub_black) \
                if no_whitelist else hydra_lists.pub_black

        hard_blacklists = self.cid_hard_blacklists.get(df_params.cid)
        if hard_blacklists:
            hydra_lists.sub_white -= hard_blacklists.all_sub
            hydra_lists.pub_white -= hard_blacklists.all_pub
            sub_blacklist |= hard_blacklists.hydra_sub
            pub_blacklist |= hard_blacklists.hydra_pub

        if df_params.app_usage == AppUsage.BOTH and hydra_lists.pub_white:
            # record all sub blacklist in order to simulate list performance on oid level
            if hard_blacklists:
                sub_blacklist |= hard_blacklists.all_sub
            for app_id in sub_blacklist.get_app_id_list():
                publisher, subpublisher = self.appid_ruler.get_pubid_subid_from_appid(df_params.partner, app_id)
                # check sub for unexpected sub in hard blacklists
                if subpublisher and publisher not in hydra_lists.pub_white:
                    sub_blacklist.remove(subpublisher)

        assert not (hydra_lists.pub_white.able_to_write and hydra_lists.sub_white.able_to_write)
        lists = {
            'require_publisher': hydra_lists.pub_white.get_app_id_list(),
            'require_sub_publisher': hydra_lists.sub_white.get_app_id_list(),
            'reject_publisher': pub_blacklist.get_app_id_list(),
            'reject_sub_publisher': self.dedupe_sub_blacklist(
                df_params.partner, pub_blacklist.get_app_id_set(), sub_blacklist.get_app_id_list()),
            'hydra_label': hydra_label,
            'note': {
                'others_pub_count': len(hard_blacklists.all_pub - hard_blacklists.hydra_pub) if hard_blacklists else 0,
                'others_sub_count': len(hard_blacklists.all_sub - hard_blacklists.hydra_sub) if hard_blacklists else 0,
                'max_event_rate': max_event_rate
            },
            'params': df_params
        }
        return lists

    def get_white_sampling_cids_lists(self, global_params, local_params, hydra_sm_lists, hydra_oid_lists,
                                      hydra_oid_max_event_rate, hydra_sm_max_event_rate):
        """Get lists for white sampling cids, HydraSM and HydraOID.
        :param global_params: HydraSM cid's df_params
        :param local_params: HydraOID cid's df_params
        :param hydra_sm_lists: an instance of <OPTIMIZED_LISTS> for HydraSM
        :param hydra_oid_lists: and instance of <OPTIMIZED_LISTS> for HydraOID
        :param hydra_oid_max_event_rate: max_event_rate for HydraOID
        :param hydra_sm_max_event_rate: max_event_rate for HydraSM
        :return hydra_whitelist_lists_results: dict of cid to hydra lists result
        :return trigger_sampling_cid_copy: whether triggers HydraSM splitting to HydraOID
        """
        hydra_whitelist_lists_results = {}
        trigger_sampling_cid_copy = False
        if global_params.do_cid_update and local_params.do_cid_update:
            assert global_params.cid != local_params.cid and global_params.partner == local_params.partner
            hydra_whitelist_lists_results[local_params.cid] = self.get_white_sampling_cid_lists(
                local_params, hydra_oid_lists, 'HydraOID', hydra_oid_max_event_rate)
            hydra_whitelist_lists_results[global_params.cid] = self.get_white_sampling_cid_lists(
                global_params, hydra_sm_lists, 'HydraSM', hydra_sm_max_event_rate)
        elif global_params.do_cid_update:
            if not DeepFunnelAdgroupTree.is_valid_cid(self.adgroup_tree['campaign_cid']):
                if (hydra_oid_lists.sub_white and hydra_oid_lists.sub_white.able_to_write) \
                        or (hydra_oid_lists.pub_white and hydra_oid_lists.pub_white.able_to_write):
                    trigger_sampling_cid_copy = True
                hydra_whitelist_lists_results[global_params.cid] = self.get_white_sampling_cid_lists(
                    global_params, BaseSingleOptimizedLists.union_optimized_lists(hydra_oid_lists, hydra_sm_lists),
                    'HydraSM', max(hydra_sm_max_event_rate, hydra_oid_max_event_rate))
            else:
                hydra_whitelist_lists_results[global_params.cid] = self.get_white_sampling_cid_lists(
                    global_params, hydra_sm_lists, 'HydraSM', hydra_sm_max_event_rate)
        elif local_params.do_cid_update:
            hydra_whitelist_lists_results[local_params.cid] = self.get_white_sampling_cid_lists(
                local_params, hydra_oid_lists, 'HydraOID', hydra_oid_max_event_rate)

        return hydra_whitelist_lists_results, trigger_sampling_cid_copy

    def get_hydra_lists_from_optimized_lists(self, local_optimized_list, global_optimized_list,
                                             country_optimized_list):
        """Gets optimized lists for HydraSM, HydraOID and HydraBL.
        :param local_optimized_list: an instance of <CampaignOptimizedLists> been trained
        :param global_optimized_list: an instance of <SamplingOptimizedLists> been trained
        :param country_optimized_list: global_optimized_list at country level to extend global white
        :return hydra_oid_lists: and instance of <OPTIMIZED_LISTS> for HydraOID
        :return hydra_sm_lists: an instance of <OPTIMIZED_LISTS> for HydraSM
        :return hydra_bl_lists: an instance of <OPTIMIZED_LISTS> for HydraBL
        """
        hydra_oid_lists = local_optimized_list.get_optimized_lists()
        hydra_sm_lists = global_optimized_list.get_optimized_lists()

        # SM white excludes OID black
        hydra_sm_lists.pub_white -= hydra_oid_lists.pub_black
        hydra_sm_lists.sub_white -= hydra_oid_lists.sub_black

        # SM black excludes OID white
        hydra_sm_lists.pub_black -= hydra_oid_lists.pub_white
        hydra_sm_lists.sub_black -= hydra_oid_lists.sub_white
        # SM black union OID black
        hydra_sm_lists.pub_black |= hydra_oid_lists.pub_black
        hydra_sm_lists.sub_black |= hydra_oid_lists.sub_black

        # country white excludes OID black and SM black and is merged into SM white
        country_lists = country_optimized_list.get_optimized_lists()
        country_lists.pub_white -= (hydra_oid_lists.pub_black | hydra_sm_lists.pub_black)
        country_lists.sub_white -= (hydra_oid_lists.sub_black | hydra_sm_lists.sub_black)
        hydra_sm_lists.pub_white |= country_lists.pub_white
        hydra_sm_lists.sub_white |= country_lists.sub_white

        # BL requires 20 installs
        hydra_bl_lists = BaseSingleOptimizedLists.union_optimized_lists(
            local_optimized_list.get_optimized_lists(required_installs=20),
            global_optimized_list.get_optimized_lists(required_installs=20))
        # BL union WL white
        hydra_bl_lists.sub_black |= self.stable_whitelist
        hydra_bl_lists.pub_black |= self.stable_whitelist
        # BL excludes OID white
        hydra_bl_lists.sub_black -= hydra_oid_lists.sub_white
        hydra_bl_lists.pub_black -= hydra_oid_lists.pub_white

        return hydra_oid_lists, hydra_sm_lists, hydra_bl_lists

    def _fill_in_lists_performance(self, list_type, global_performance_data, local_performance_data,
                                   optimized_list, results_entry, goal_value):
        assert list_type in ('black', 'white')
        if not goal_value:
            return
        if list_type == 'black':
            if global_performance_data:
                results_entry['global_performance'] = optimized_list.get_blacklists_performance(
                    global_performance_data, results_entry['reject_publisher'],
                    results_entry['reject_sub_publisher'], True, goal_value).to_dict()
            if local_performance_data:
                results_entry['local_performance'] = optimized_list.get_blacklists_performance(
                    local_performance_data, results_entry['reject_publisher'],
                    results_entry['reject_sub_publisher'], False, goal_value).to_dict()
        else:
            if global_performance_data:
                results_entry['global_performance'] = optimized_list.get_whitelists_performance(
                    global_performance_data, results_entry['require_publisher'],
                    results_entry['require_sub_publisher'], True, goal_value).to_dict()
            if local_performance_data:
                results_entry['local_performance'] = optimized_list.get_whitelists_performance(
                    local_performance_data, results_entry['require_publisher'],
                    results_entry['require_sub_publisher'], False, goal_value).to_dict()

    def update_hydra_lists_results(self, global_params, local_params, blacklist_params, date_str,
                                   eval_in_list_performance):
        """Update hydra lists for sampling cids.
        :param global_params: HydraSM cid's df_params
        :param local_params: HydraOID cid's df_params
        :param blacklist_params: HydraBL cid's df_params
        :param date_str:
        :param eval_in_list_performance: evaluate in-lists performance or not, <bool>
        :return hydra_lists_results: dict of cid to hydra lists result
        :return trigger_sampling_cid_copy: whether triggers HydraSM splitting to HydraOID
        """
        hydra_lists_results = {}
        trigger_sampling_cid_copy = False
        global_simulation_data = local_simulation_data = None
        if local_params.do_cid_update or global_params.do_cid_update or blacklist_params.do_cid_update:
            # build global/local simulation optimized lists
            (local_pub_performance_data, local_sub_performance_data, local_use_raw_goal,
             global_pub_performance_data, global_sub_performance_data, global_use_raw_goal,
             country_pub_performance_data, country_sub_performance_data,
             country_use_raw_goal) = self.get_performance_data(global_params, local_params, date_str)

            # only use campaign raw goal for dryrun simulation
            if eval_in_list_performance and (global_use_raw_goal or country_use_raw_goal):
                global_simulation_data = global_sub_performance_data \
                    if global_use_raw_goal else country_sub_performance_data
            if eval_in_list_performance and local_use_raw_goal:
                local_simulation_data = local_sub_performance_data

            global_optimized_list, local_optimized_list, country_optimized_list = self.get_optimized_lists(
                global_params, local_params, local_pub_performance_data, local_sub_performance_data,
                global_pub_performance_data, global_sub_performance_data, global_use_raw_goal,
                country_pub_performance_data, country_sub_performance_data, country_use_raw_goal)

            if global_params.app_usage.is_black_usage or self.adgroup_tree['is_transparent']:
                # update HydraBL
                if global_params.do_cid_update:
                    hydra_lists_results[global_params.cid] = self.get_black_sampling_cid_lists(
                       global_params, local_optimized_list.get_optimized_lists())
                    # fill HydraBL in-lists performance
                    if eval_in_list_performance:
                        self._fill_in_lists_performance(
                            'black', global_simulation_data, local_simulation_data, local_optimized_list,
                            hydra_lists_results[global_params.cid], global_params.raw_event_goal)
            else:
                hydra_oid_lists, hydra_sm_lists, hydra_bl_lists = self.get_hydra_lists_from_optimized_lists(
                    local_optimized_list, global_optimized_list, country_optimized_list)
                hydra_oid_max_event_rate = local_optimized_list.get_max_event_rate()
                hydra_sm_max_event_rate = max(country_optimized_list.get_max_event_rate(),
                                              global_optimized_list.get_max_event_rate())

                # update HydraOID and HydraSM
                hydra_whitelist_lists_results, trigger_sampling_cid_copy = self.get_white_sampling_cids_lists(
                    global_params, local_params, hydra_sm_lists, hydra_oid_lists,
                    hydra_oid_max_event_rate, hydra_sm_max_event_rate)
                hydra_lists_results.update(hydra_whitelist_lists_results)

                # fill HydraOID & HydraSM in-lists performance
                if eval_in_list_performance:
                    self._fill_in_lists_performance(
                        'white', None, local_simulation_data, local_optimized_list,
                        hydra_lists_results[local_params.cid], local_params.raw_event_goal)
                    self._fill_in_lists_performance(
                        'white', global_simulation_data, None, global_optimized_list,
                        hydra_lists_results[global_params.cid], global_params.raw_event_goal)

                # update HydraBL
                if blacklist_params.do_cid_update:
                    hydra_lists_results[blacklist_params.cid] = self.get_black_sampling_cid_lists(
                        blacklist_params, hydra_bl_lists)
                    # fill HydraBL in-lists performance
                    if eval_in_list_performance:
                        self._fill_in_lists_performance(
                            'black', global_simulation_data, local_simulation_data, local_optimized_list,
                            hydra_lists_results[blacklist_params.cid], blacklist_params.raw_event_goal)

        return hydra_lists_results, trigger_sampling_cid_copy


class HydraListController(object):
    """Interface to update lists for a Hydra adgroup tree. """
    RESULT_TABLE_COLUMNS = ('date_hour', 'cid', 'partner_id', 'event_type', 'hydra_label', 'require_publisher',
                            'require_sub_publisher', 'reject_publisher', 'reject_sub_publisher', 'note')
    RESULT_TABLE_VALUES = ('hydra_label', 'require_publisher', 'require_sub_publisher',
                           'reject_publisher', 'reject_sub_publisher', 'note')
    DATE_HOUR_FORMAT = '%Y-%m-%d %H:00:00'

    def __init__(self, listctrl_cfg, appid_ruler, idash_endpoint, appier_rule_endpoint, query_db,
                 root_cid, adgroup_tree, data_reference):
        self.listctrl_cfg = listctrl_cfg
        self.appid_ruler = appid_ruler
        self.idash_endpoint = idash_endpoint
        self.appier_rule_endpoint = appier_rule_endpoint
        self.query_db = query_db
        self.root_cid = root_cid
        self.adgroup_tree = adgroup_tree
        self.hydra_list_control_table = listctrl_cfg['hydra_list_control_table']

        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.df_performance_cache = DeepFunnelPerformanceCache(self.query_db, data_reference)
        self.hydra_performance_cache = HydraPerformanceCache(
            listctrl_cfg, self.query_db, self.appid_ruler, data_reference)
        self.hydra_hardlists = HydraHardlists(
            self.listctrl_cfg, self.appid_ruler, self.idash_endpoint, self.appier_rule_endpoint,
            self.df_performance_cache, self.hydra_performance_cache)

    def get_hydra_performance_cache(self):
        """Get the hydra performance cache. """
        return self.hydra_performance_cache

    def dispatch_list_control(self, global_params, local_params, blacklist_params, stable_params, garbage_params,
                              date_str=None, use_cid_blacklist=True, cid_install_threshold=10,
                              bl_cid_install_threshold=20):
        """Dispatch lists for a Hydra adgroup tree.
        :param global_params: HydraSM cid's df_params
        :param local_params: HydraOID cid's df_params
        :param blacklist_params: HydraBL cid's df_params
        :param stable_params: dict of stable_cid to it's df_params
        :param garbage_params: dict of garbage cid to it's df_params
        :param date_str:
        :param use_cid_blacklist: apply cid-level simulation blacklist on hydra cids or not
        :param cid_install_threshold: minimun cid installs required for HydraOID/HydraSM/HydraWL cid-level blacklist
        :param bl_cid_install_threshold: minimun cid installs required for HydraBL cid-level blacklist
        :return hydra_lists_results: dict of cid to hydra lists result
        :return trigger_sampling_cid_copy: whether triggers HydraSM splitting to HydraOID
        :return recovered_cids: list of garbage cid that can be recovered
        """
        logger.info('start list control %s: %s', self.root_cid, self.adgroup_tree)
        logger.info('global_params: %s, local_params: %s, blacklist_params: %s, stable_params: %s',
                    global_params, local_params, blacklist_params, stable_params)
        hydra_lists_results = {}

        cid_hard_blacklists = self.get_cid_hard_blacklists(
            global_params, local_params, blacklist_params, stable_params, garbage_params,
            date_str, use_cid_blacklist, cid_install_threshold, bl_cid_install_threshold)
        cid_hard_whitelists = self.get_cid_hard_whitelists(global_params, blacklist_params, date_str)
        stable_cid_controller = HydraStableListController(
            self.hydra_performance_cache, self.idash_endpoint, self.appid_ruler, cid_hard_blacklists)
        (hydra_stable_lists_results, total_stable_whitelist,
         recovered_cids) = stable_cid_controller.update_hydra_lists_results(stable_params, garbage_params, date_str)
        hydra_lists_results.update(hydra_stable_lists_results)

        sampling_cid_controller = HydraSamplingListController(
            self.listctrl_cfg, self.hydra_performance_cache, self.df_performance_cache, self.appid_ruler,
            cid_hard_blacklists, cid_hard_whitelists, total_stable_whitelist, self.adgroup_tree)
        hydra_sampling_lists_result, trigger_sampling_cid_copy = sampling_cid_controller.update_hydra_lists_results(
            global_params, local_params, blacklist_params, date_str, False)
        hydra_lists_results.update(hydra_sampling_lists_result)

        hydra_roas_filter = HydraROASListController(self.appid_ruler, self.hydra_performance_cache)
        for cid, lists in hydra_lists_results.iteritems():
            hydra_roas_filter.apply_positive_roas_filter(lists, date_str, cid_hard_blacklists.get(cid))
            logger.debug('cid: %s, result: %s', cid, json.dumps(lists, default=str))
            has_empty_id = self.remove_empty_app_id(lists)
            if has_empty_id:
                logger.warning('cid: %s has empty string in black/white list', cid)
            self.update_hourly_list_control_records(cid, lists, date_str)

        return hydra_lists_results, trigger_sampling_cid_copy, recovered_cids

    def get_cid_hard_whitelists(self, global_params, blacklist_params, date_str):
        """Get cid's hard whitelists (good performance app should not be in HydraBL).
        :param global_params: HydraSM cid's df_params
        :param blacklist_params: HydraBL cid's df_params
        :param date_str:
        :return cid_hard_whitelists: dict of cid to <HARD_WHITELISTS>
        """
        params = []
        if global_params.do_cid_update and (global_params.app_usage.is_black_usage
                                            or self.adgroup_tree['is_transparent']):
            params.append(global_params)
        if blacklist_params.do_cid_update:
            params.append(blacklist_params)

        cid_hard_whitelists = {}
        for param in params:
            cid_hard_whitelists[param.cid] = self.hydra_hardlists.get_hard_whitelists(param, date_str)

        return cid_hard_whitelists

    def get_cid_hard_blacklists(self, global_params, local_params, blacklist_params, stable_params, garbage_params,
                                date_str, use_cid_blacklist, cid_install_threshold, bl_cid_install_threshold):
        """Get cid's hard blacklists.
        :param global_params: HydraSM cid's df_params
        :param local_params: HydraOID cid's df_params
        :param blacklist_params: HydraBL cid's df_params
        :param stable_params: dict of stable_cid to it's df_params
        :param garbage_params: dict of garbage cid to it's df_params
        :param date_str:
        :param use_cid_blacklist: apply cid-level simulation blacklist on hydra cids or not
        :param cid_install_threshold: minimun cid installs required for HydraOID/HydraSM/HydraWL cid-level blacklist
        :param bl_cid_install_threshold: minimun cid installs required for HydraBL cid-level blacklist
        :return cid_hard_blacklists: dict of cid to <HARD_BLACKLISTS>
        """
        param_list = ([params for params in (local_params, global_params, blacklist_params) if params.do_cid_update] +
                      stable_params.values() + garbage_params.values())
        self.hydra_hardlists.cache_non_hydra_cid_bwlist(list({params.cid for params in param_list}), date_str)
        cid_hard_blacklists = {}
        for (params, install_threshold) in [(local_params, cid_install_threshold),
                                            (global_params, cid_install_threshold),
                                            (blacklist_params, bl_cid_install_threshold)]:
            if params.do_cid_update:
                use_appier_rule = params.is_blacklist_cid
                use_pub_blacklist = params.is_blacklist_cid
                cid_hard_blacklists[params.cid] = self.hydra_hardlists.get_hard_blacklists(
                    params, date_str, use_cid_blacklist, install_threshold, use_appier_rule, use_pub_blacklist)
        for stable_cid, params in stable_params.iteritems():
            cid_hard_blacklists[stable_cid] = self.hydra_hardlists.get_hard_blacklists(
                params, date_str, False, 0, False, False)
        for garbage_cid, params in garbage_params.iteritems():
            cid_hard_blacklists[garbage_cid] = self.hydra_hardlists.get_hard_blacklists(
                params, date_str, False, 0, False, False)

        return cid_hard_blacklists

    def remove_empty_app_id(self, lists):
        """Removes empty app_id in hydra lists result.
        :param lists: hydra lists result, <dict>
        :return has_empty_id: has empty app_id or not, <boolean>
        """
        has_empty_id = False
        for field in ['require_publisher', 'require_sub_publisher', 'reject_publisher', 'reject_sub_publisher']:
            if '' in lists[field]:
                lists[field].remove('')
                has_empty_id = True

        return has_empty_id

    def update_hourly_list_control_records(self, cid, lists, date_str):
        """Update hydra lists result into MySQL tables.
        :param cid:
        :param lists: hydra lists result, <dict>
        :param date_str: None on production
        """
        if date_str:  # experiment: store more information for building params
            datetime_str = HydraListController.format_date_hour(date_str)
            lists['note']['params'] = list(lists['params'])
        else:
            datetime_str = datetime.datetime.utcnow().strftime(HydraListController.DATE_HOUR_FORMAT)
        record = u'({})'.format(','.join([u'\'{}\''.format(value.replace("'", "''")) for value in [
            datetime_str, cid, lists['params'].partner, lists['params'].event_type, lists['hydra_label'],
            ','.join(sorted(lists['require_publisher'])), ','.join(sorted(lists['require_sub_publisher'])),
            ','.join(sorted(lists['reject_publisher'])), ','.join(sorted(lists['reject_sub_publisher'])),
            json.dumps(lists['note'])
        ]]))

        query = u"INSERT INTO {} ({}) values {} ON DUPLICATE KEY UPDATE {}".format(
            self.hydra_list_control_table, ','.join(self.RESULT_TABLE_COLUMNS), record,
            ','.join([u'{0}=values({0})'.format(col) for col in self.RESULT_TABLE_VALUES])
        )
        self.query_db.execute_query(query, db_client=self.df_db_client)

    @staticmethod
    def format_date_hour(date_str, hour=23):
        return datetime.datetime.strptime(
            date_str + str(hour), '%Y%m%d%H').strftime(HydraListController.DATE_HOUR_FORMAT)


class HydraDryRunController(object):
    """Interface to get Hydra traffic estimation from dry run. """
    RESULT_TABLE_COLUMNS = (
        'date', 'oid', 'partner_id', 'country', 'top_category', 'sub_category', 'event_type', 'goal_type',
        'goal_value', 'df_goal_value', 'sm_list_size', 'sm_global_installs', 'oid_list_size', 'oid_campaign_installs',
        'bl_list_size', 'bl_global_installs', 'bl_campaign_installs')

    def __init__(self, listctrl_cfg, appid_ruler, idash_endpoint, appier_rule_endpoint, query_db, df_data_date,
                 data_reference):
        self.listctrl_cfg = listctrl_cfg
        self.appid_ruler = appid_ruler
        self.idash_endpoint = idash_endpoint
        self.appier_rule_endpoint = appier_rule_endpoint
        self.query_db = query_db
        self.df_data_date = df_data_date

        self.dry_run_result_table = listctrl_cfg['dry_run_result_table']
        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.df_performance_cache = DeepFunnelPerformanceCache(self.query_db, data_reference)
        self.hydra_performance_cache = HydraPerformanceCache(
            listctrl_cfg, self.query_db, self.appid_ruler, data_reference)
        self.hydra_hardlists = HydraHardlists(
            self.listctrl_cfg, self.appid_ruler, self.idash_endpoint, self.appier_rule_endpoint,
            self.df_performance_cache, self.hydra_performance_cache)

    def dispatch_list_control(self, global_params, local_params, blacklist_params, adgroup_tree):
        """Dispatch dry run lists result for a Hydra adgroup tree.
        :param global_params: HydraSM cid's df_params
        :param local_params: HydraOID cid's df_params
        :param blacklist_params: HydraBL cid's df_params
        :param adgroup_tree: dict of hydra adgroup tree
        """
        cid_hard_blacklists = self.get_cid_hard_blacklists(global_params, local_params, blacklist_params)
        cid_hard_whitelists = self.get_cid_hard_whitelists(global_params, blacklist_params)
        total_stable_whitelist = AppList([], 1 if global_params.app_usage.white_subpublisher else 0)
        sampling_cid_controller = HydraSamplingListController(
            self.listctrl_cfg, self.hydra_performance_cache, self.df_performance_cache, self.appid_ruler,
            cid_hard_blacklists, cid_hard_whitelists, total_stable_whitelist, adgroup_tree)
        hydra_sampling_lists_result, _ = sampling_cid_controller.update_hydra_lists_results(
            global_params, local_params, blacklist_params, None, True)
        self.update_dry_run_results(hydra_sampling_lists_result, global_params, local_params, blacklist_params)

    def update_dry_run_results(self, lists_result, global_params, local_params, blacklist_params):
        """Update Hydra dry run result into MySQL table.
        :param lists_result: `lists` result dict from HydraSamplingListController
        :param global_params: HydraSM cid's df_params
        :param local_params: HydraOID cid's df_params
        :param blacklist_params: HydraBL cid's df_params
        """
        def get_installs(data, use_global_data):
            return data.get('major_count' if use_global_data else 'installs', 0) + data.get('today_installs', 0)

        def get_wl_size(data):
            return len(data['require_publisher']) + len(data['require_sub_publisher'])

        def get_bl_size(data):
            return len(data['reject_publisher']) + len(data['reject_sub_publisher'])

        if global_params.app_usage.is_black_usage:
            sm_list_size = sm_global_installs = oid_list_size = oid_campaign_installs = 0
            bl_list_size = get_bl_size(lists_result.get(global_params.cid, {}))
            bl_global_installs = get_installs(
                lists_result.get(global_params.cid, {}).get('global_performance', {}), True)
            bl_campaign_installs = get_installs(
                lists_result.get(global_params.cid, {}).get('local_performance', {}), False)
        else:
            sm_list_size = get_wl_size(lists_result.get(global_params.cid, {}))
            sm_global_installs = get_installs(
                lists_result.get(global_params.cid, {}).get('global_performance', {}), True)
            oid_list_size = get_wl_size(lists_result.get(local_params.cid, {}))
            oid_campaign_installs = get_installs(
                lists_result.get(local_params.cid, {}).get('local_performance', {}), False)
            bl_list_size = get_bl_size(lists_result.get(blacklist_params.cid, {}))
            bl_global_installs = get_installs(
                lists_result.get(blacklist_params.cid, {}).get('global_performance', {}), True)
            bl_campaign_installs = get_installs(
                lists_result.get(blacklist_params.cid, {}).get('local_performance', {}), False)

        query = """
        REPLACE INTO {}
        ({})
        VALUES
        ({})
        """.format(self.dry_run_result_table,
                   ','.join(self.RESULT_TABLE_COLUMNS),
                   ','.join(['%s'] * len(self.RESULT_TABLE_COLUMNS)))
        rows = [[self.df_data_date, global_params.oid, global_params.partner, global_params.country,
                 global_params.category_tuple[1], global_params.category_tuple[0], global_params.event_type,
                 global_params.raw_goal_type, global_params.raw_event_goal, global_params.event_goal,
                 sm_list_size, sm_global_installs, oid_list_size, oid_campaign_installs,
                 bl_list_size, bl_global_installs, bl_campaign_installs]]
        self.query_db.execute_query_many(query, rows, db_client=self.df_db_client)

    def get_cid_hard_whitelists(self, global_params, blacklist_params):
        """Get cid's hard whitelists (good performance app should not be in HydraBL).
        :param global_params: HydraSM cid's df_params
        :param blacklist_params: HydraBL cid's df_params
        :return cid_hard_whitelists: dict of cid to <HARD_WHITELISTS>
        """
        params = []
        if global_params.do_cid_update and global_params.app_usage.is_black_usage:
            params.append(global_params)
        if blacklist_params.do_cid_update:
            params.append(blacklist_params)

        cid_hard_whitelists = {}
        for param in params:
            cid_hard_whitelists[param.cid] = self.hydra_hardlists.get_dryrun_hard_whitelists()

        return cid_hard_whitelists

    def get_cid_hard_blacklists(self, global_params, local_params, blacklist_params):
        """Get cid's hard blacklists.
        :param global_params: HydraSM cid's df_params
        :param local_params: HydraOID cid's df_params
        :param blacklist_params: HydraBL cid's df_params
        :return cid_hard_blacklists: dict of cid to <HARD_BLACKLISTS>
        """
        cid_hard_blacklists = {}
        for params in [local_params, global_params, blacklist_params]:
            cid_hard_blacklists[params.cid] = self.hydra_hardlists.get_dryrun_hard_blacklists(params, None)

        return cid_hard_blacklists
