from __future__ import absolute_import
from __future__ import division
import datetime
import json
import time
from collections import defaultdict, namedtuple

from app.utils.db import MySQL_DB_AI_DEEP_FUNNEL, MySQL_DB_AI_HYDRA
from app.module.CQueryDB import CQueryDB
from app.utils.init_logger import init_logger
from app.utils.util import date_to_timestamp

from app.external_optimization.deep_funnel.utils.constants import APPIER_TO_USD, DEFAULT_CATEGORY
from app.external_optimization.deep_funnel.utils.common_utils import gmt0_timezone, patch_metric_row, string_hash, \
    in_scope, get_partner_source_list
from app.external_optimization.deep_funnel.list_control.mysql_queries import hydra_event_metrics_query, \
    minor_coldstart_target_date_db_query

logger = init_logger(__name__)


class Performance(object):
    """Storage class for performance, including major_count, minor_count, installs and today_installs. """
    def __init__(self, major_count, minor_count, installs, today_installs):
        self.major_count = major_count
        self.minor_count = minor_count
        self.installs = installs
        self.today_installs = today_installs

    def __isub__(self, performance):
        self.major_count -= performance.major_count
        self.minor_count -= performance.minor_count
        self.installs -= performance.installs
        self.today_installs -= performance.today_installs
        return self

    def __sub__(self, performance):
        return Performance(
            self.major_count - performance.major_count,
            self.minor_count - performance.minor_count,
            self.installs - performance.installs,
            self.today_installs - performance.today_installs)

    def __iadd__(self, performance):
        self.major_count += performance.major_count
        self.minor_count += performance.minor_count
        self.installs += performance.installs
        self.today_installs += performance.today_installs
        return self

    def __add__(self, performance):
        return Performance(
            self.major_count + performance.major_count,
            self.minor_count + performance.minor_count,
            self.installs + performance.installs,
            self.today_installs + performance.today_installs)

    def get_event_rate(self):
        """Get the event rate.
        :return event_rate: minor_count / major_count
        """
        return float(self.minor_count) / self.major_count if self.major_count else 0.0

    def get_total_major_count(self):
        """Get total major count.
        :return total_major_count: major_count + today_installs
        """
        return self.major_count + self.today_installs

    def get_total_installs(self):
        """Get the total installs.
        :return total_installs: installs + today_installs
        """
        return self.installs + self.today_installs

    def get_today_installs(self):
        """Get today's installs.
        :return today_installs:
        """
        return self.today_installs

    def get_event_rate_with_today_installs(self):
        """Get the event rate with today's installs.
        :return event_rate: minor_count / (major_count + today_installs)
        """
        return float(self.minor_count) / self.get_total_major_count() if self.get_total_major_count() else 0.0

    def to_dict(self):
        """Return the performance dict.
        :return performance_dict: dict of instance variables
        """
        return {
            'major_count': self.major_count,
            'minor_count': self.minor_count,
            'installs': self.installs,
            'today_installs': self.today_installs
        }

    def __str__(self):
        return json.dumps(self.to_dict(), sort_keys=True)


class AppPerformance(object):
    """Storage class for app_id's performance, including decayed_local_performance, decayed_global_performance
    and local_performance.
    """
    def __init__(self, app_id, is_subpublisher):
        self.app_id = app_id
        self.is_subpublisher = is_subpublisher
        self.prediction = None
        self.local_decay_performance = None
        self.global_decay_performance = None
        self.local_performance = None

    def set_prediction(self, prediction):
        """Set the prediction value of instance.
        :param prediction:
        """
        self.prediction = prediction

    def set_decayed_local_performance(self, major_count, minor_count, installs, today_installs):
        """Set the decayed local performance for HydraOID simulation.
        :param major_count: major count from deep funnel pipeline
        :param minor_count: minor count from deep funnel pipeline
        :param installs: always 0
        :param today_installs: latest installs from deep funnel pipeline
        """
        self.local_decay_performance = Performance(major_count, minor_count, installs, today_installs)

    def set_decayed_global_performance(self, major_count, minor_count, installs, today_installs):
        """Set the decayed global performance for HydraSM simulation.
        :param major_count: major count from deep funnel pipeline
        :param minor_count: minor count from deep funnel pipeline
        :param installs: always 0
        :param today_installs: latest installs from deep funnel pipeline
        """
        self.global_decay_performance = Performance(major_count, minor_count, installs, today_installs)

    def set_decayed_performance(self, use_global_data, major_count, minor_count, installs, today_installs):
        """Set the decayed performance for Oid/Category/Country Simulation.
        :param use_global_data: set global or local data
        :param major_count: major count from deep funnel pipeline
        :param minor_count: minor count from deep funnel pipeline
        :param installs: always 0
        :param today_installs: latest installs from deep funnel pipeline
        """
        if use_global_data:
            self.set_decayed_global_performance(major_count, minor_count, installs, today_installs)
        else:
            self.set_decayed_local_performance(major_count, minor_count, installs, today_installs)

    def set_local_performance(self, major_count, minor_count, installs, today_installs):
        """Set the local performance for HydraWL simulation.
        :param major_count: major count from hydra metrics pipeline
        :param minor_count: minor count from hydra metrics pipeline
        :param installs: installs from hydra metrics pipeline
        :param today_installs: today's installs from ext_ai_perday_action_time
        """
        self.local_performance = Performance(major_count, minor_count, installs, today_installs)

    def __isub__(self, app_performance):
        self.local_decay_performance -= app_performance.local_decay_performance
        self.global_decay_performance -= app_performance.global_decay_performance
        self.local_performance -= app_performance.local_performance
        return self

    def get_decayed_local_performance(self):
        """Get the decayed local performance object.
        :return local_decay_performance: an instance of <Performance>
        """
        return self.local_decay_performance

    def get_decayed_global_performance(self):
        """Get the decayed global performance object.
        :return global_decay_performance: an instance of <Performance>
        """
        return self.global_decay_performance

    def get_decayed_performance(self, use_global_data):
        """Get the decayed performance object.
        :param use_global_data: use global data or not, <bool>
        :return decay_performance: an instance of <Performance>
        """
        return self.global_decay_performance if use_global_data else self.local_decay_performance

    def get_local_performance(self):
        """Get the local performance.
        :return local_performance: an instance of <Perforamnce>
        """
        return self.local_performance

    def get_local_performance_sort_key(self):
        """Get the compare key to sort local performance data.
        :return sort_key: local performance data is sorted by event_rate
        """
        return self.local_performance.get_event_rate()

    def get_unconfident_sort_key(self):
        """Get the compare key to sort decayed performance when confident is low.
        :return sort_key: a tuple of (event_rate, local_minor, -local_major, global_minor, -global_major)
        """
        decayed_local_performance = self.get_decayed_local_performance()
        decayed_global_performance = self.get_decayed_global_performance()
        return (
            decayed_global_performance.get_event_rate_with_today_installs(),
            decayed_local_performance.minor_count,
            -decayed_local_performance.get_total_major_count(),
            decayed_global_performance.minor_count,
            -decayed_global_performance.get_total_major_count())

    def get_confident_sort_key(self):
        """Get the compare key to sort decayed performance when confident is high.
        :return sort_key: a tuple of (model prediction, local_minor, -local_major, global_minor, -global_major)
        """
        decayed_local_performance = self.get_decayed_local_performance()
        decayed_global_performance = self.get_decayed_global_performance()
        return (
            self.prediction,
            decayed_local_performance.minor_count,
            -decayed_local_performance.get_total_major_count(),
            decayed_global_performance.minor_count,
            -decayed_global_performance.get_total_major_count())

    def get_simulation_sort_key(self, use_global_data):
        """Get the compare key to sort decayed performance for list-reach-goal simulation.
        :param use_global_data: use global data or not, <bool>
        :return sort_key: a tuple of (event_rate, minor, -major)
        """
        decayed_performance = self.get_decayed_global_performance() \
            if use_global_data else self.get_decayed_local_performance()
        return (
            decayed_performance.get_event_rate_with_today_installs(),
            decayed_performance.minor_count,
            -decayed_performance.get_total_major_count()
        )

    def to_dict(self):
        """Return the app performance dict.
        :return app_performance_dict: dict of instance variables
        """
        return {
            'app_id': self.app_id,
            'is_subpublisher': self.is_subpublisher,
            'prediction': self.prediction,
            'local_decay_performance': self.local_decay_performance.to_dict(),
            'global_decay_performance': self.global_decay_performance.to_dict(),
            'local_performance': self.local_performance.to_dict()
        }

    def __str__(self):
        return json.dumps(self.to_dict(), sort_keys=True)


class DeepFunnelPerformanceCache(object):
    """Provide query interfaces from deep funnel pipeline. """
    QUERY_KEY = namedtuple('query_key', ['date_str', 'oid', 'is_subpublisher', 'partner',
                                         'category_tuple', 'country', 'goal_type', 'event_type',
                                         'table_name', 'query'])

    def __init__(self, query_db, data_reference):
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.hydra_db_client = query_db.db_clients[MySQL_DB_AI_HYDRA]
        # AI-7293: doglobal_api_wl = doglobal_api_wl + doglobal_api = doglobal_api_union (minor_goal output)
        self.partner_replaced_by = data_reference['partner_replaced_by']
        self.cache = {}

    def get_df_performance_data(self, query_key, coldstart=False):
        """Get the decay performance from deep funnel pipeline for HydraSM/HydraOID simulation.
        :param query_key: an instance of <QUERY_KEY>
        :param coldstart: query category level performance for those coldstart oids
        :return app_id2performance: dict of app_id fo <AppPerformance>
        """
        cache_data = self._get_cache_data(query_key, coldstart=coldstart)
        app_id2performance = {}
        for row in cache_data:
            app_performance = self._get_app_performance(row)
            app_id2performance[app_performance.app_id] = app_performance

        return app_id2performance

    def get_df_query_app_ids(self, query_key, use_hydra_db=False):
        """Get the app_id list from deep funnel pipeline for HydraHardLists.
        :param query_key: an instance of <QUERY_KEY>
        :return app_ids: list of app_id
        """
        return [row['app_id'] for row in self._get_cache_data(query_key, use_hydra_db=use_hydra_db)]

    def _get_app_performance(self, row):
        app_performance = AppPerformance(row['app_id'], row['is_subpublisher'])
        if 'prediction' in row:
            app_performance.set_prediction(row['prediction'])
        elif 'sampled_value' in row:
            app_performance.set_prediction(row['sampled_value'])
        else:
            raise KeyError('no prediction column')

        app_performance.set_decayed_local_performance(
            row['decay_major_count30D'], row['decay_minor_count30D'], 0, row['latest_install'])
        app_performance.set_decayed_global_performance(
            row['decay_global_major_count30D'], row['decay_global_minor_count30D'], 0, row['latest_global_install'])
        app_performance.set_local_performance(row['major_count30D'], row['minor_count30D'], 0, row['latest_install'])

        return app_performance

    def _get_cache_data(self, query_key, coldstart=False, use_hydra_db=False):
        if query_key in self.cache:
            return self.cache[query_key]

        self.cache[query_key] = self._fallback_query(query_key, use_hydra_db=use_hydra_db)
        if coldstart and not self.cache.get(query_key, []):
            for category in query_key.category_tuple:  # (category, top_category)
                category_as_oid = DEFAULT_CATEGORY + string_hash(category)
                coldstart_key = query_key._replace(oid=category_as_oid,
                                                   query=minor_coldstart_target_date_db_query)
                if coldstart_key not in self.cache:
                    self.cache[coldstart_key] = self._fallback_query(coldstart_key, use_hydra_db=use_hydra_db)
                self.cache[query_key] = self.cache[coldstart_key]
                if self.cache[query_key]:
                    logger.info('coldstart query (%s, %s, %s, %s, %s = %s) gets result',
                                query_key.oid, query_key.partner, query_key.event_type,
                                query_key.country, category, category_as_oid)
                    break
                else:
                    logger.warning('coldstart query (%s, %s, %s, %s, %s = %s) still has no result',
                                   query_key.oid, query_key.partner, query_key.event_type,
                                   query_key.country, category, category_as_oid)
                    if category == query_key.category_tuple[-1]:
                        break

        return self.cache[query_key]

    def _fallback_query(self, query_key, use_hydra_db=False):
        if query_key.date_str:  # not production
            return self._get_df_prediction_results(query_key, query_key.date_str, use_hydra_db=use_hydra_db)

        curr_datetime = datetime.datetime.now(gmt0_timezone)
        last_datetime = curr_datetime - datetime.timedelta(days=14)
        while curr_datetime > last_datetime:
            results = self._get_df_prediction_results(
                query_key, curr_datetime.strftime('%Y%m%d'), use_hydra_db=use_hydra_db)
            if results:
                return results
            curr_datetime -= datetime.timedelta(days=1)

        return []

    def _get_df_prediction_results(self, query_key, date_str, use_hydra_db=False):
        pseudo_partner = self.partner_replaced_by.get(query_key.partner, query_key.partner)
        query = query_key.query.format(
            query_key.oid, query_key.is_subpublisher, pseudo_partner, query_key.goal_type, query_key.event_type,
            date_str, query_key.table_name, query_key.country)
        logger.debug(query)
        return self.query_db.get_query_result(
            query, db_client=self.df_db_client if not use_hydra_db else self.hydra_db_client)


class HydraPerformanceCache(object):
    """Provide query interfaces from hydra event metrics pipeline. """
    VALID_GROUP_LEVELS = ('cid', 'cid_app_id', 'oid', 'oid_app_id',
                          'category', 'category_app_id', 'country', 'country_app_id')

    def __init__(self, listctrl_cfg, query_db, appid_ruler, data_reference):
        self.listctrl_cfg = listctrl_cfg
        self.query_db = query_db
        self.df_db_client = query_db.db_clients[MySQL_DB_AI_DEEP_FUNNEL]
        self.appid_ruler = appid_ruler
        self.partner_source_list = get_partner_source_list(data_reference)
        self.cached_app_performance = {}
        self.cached_performance = {}
        self.cid_today_installs = {}
        self.cached_revenue_cpa = {}

    def get_aggregated_query_ids(self, partner_id, country, category):
        """Get the query cids for an aggregation tuple key.
        :param partner_id:
        :param country:
        :param category:
        :return cids: None if the aggregation tuple does not have historical data
        """
        partner_ids = self.partner_source_list.get(partner_id, [partner_id])
        query = """
        SELECT
            sha1_key, has_data_cache, cids
        FROM
            {}
        WHERE
            partner_id IN ({})
            and country = '{}'
            and category = '{}'
        """.format(self.listctrl_cfg['perf_default_keys_table'], in_scope(partner_ids), country, category)
        logger.debug(query)
        results = self.query_db.get_query_result(query, db_client=self.df_db_client)
        if not results:
            logger.warning('no data for (%s, %s, %s)', partner_ids, country, category)
            return None
        cid_list = []
        for row in results:
            if row['has_data_cache']:
                cid_list.append(row['sha1_key'])
            else:
                cid_list.extend(json.loads(row['cids']))
        return cid_list

    def get_pub_to_sub_set(self, partner_id, sub_ids):
        pub2sub_set = defaultdict(set)
        for sub_id in sub_ids:
            publisher, _subpublisher = self.appid_ruler.get_pubid_subid_from_appid(partner_id, sub_id)
            if publisher:
                pub2sub_set[publisher].add(sub_id)

        return pub2sub_set

    def get_revenue_cpa(self, oid):
        """Get the revenue CPA (goalcpa) of campaign.
        :param oid:
        :return revenue_cpa:
        """
        if oid not in self.cached_revenue_cpa:
            query = 'select goalcpa from order_info where oid = \'{}\''.format(oid)
            logger.debug(query)
            result = self.query_db.get_query_result(query, db_client=self.query_db.get_db_client_by_table('order_info'))
            if result:
                self.cached_revenue_cpa[oid] = result[0]['goalcpa'] * APPIER_TO_USD
                logger.debug('oid: %s get goalcpa %s', oid, self.cached_revenue_cpa[oid])

        return self.cached_revenue_cpa.get(oid, 1.0)

    def get_cid_app_performance_data(self, df_params, is_subpublisher, n_day=7, decay_rate=1.0, date_str=None,
                                     install_decayed=True, app_ids=None):
        """Get the cid app performance data for HydraWL.
        :param df_params: an instance of <DF_ADGROUP_PARAMS>
        :param is_subpublisher: is subpublisher or not, 1 or 0
        :param n_day: data day range, 1/7/14/30
        :param decay_rate: daily decay rate of event metrics, 0.9/1.0
        :param date_str: query date_str for today's installs
        :param install_decayed: whether the `installs` field in <Performance> is decayed, <bool>
        :param app_ids: set/list of app_id that must appears in return data
        :return app_id2performance: dict of app_id fo <AppPerformance>
        """
        app_tag = 'subpublisher' if is_subpublisher else 'publisher'
        cid_today_installs = self._get_cid_today_installs(df_params.cid, df_params.partner, date_str)[app_tag]
        cid_cached_performance_data = self._get_cached_app_performance_data(
            df_params, 'cid_app_id', n_day, decay_rate, install_decayed)[app_tag]
        revenue_cpa = self.get_revenue_cpa(df_params.oid) if df_params.raw_goal_type.startswith('ROAS') else 1.0

        app_id2performance = {}
        app_id_set = set(cid_cached_performance_data.keys() + cid_today_installs.keys())
        if app_ids:
            app_id_set |= set(app_ids)
        for app_id in app_id_set:
            app_performance = AppPerformance(app_id, is_subpublisher)
            app_performance.set_decayed_local_performance(0, 0, 0, 0)
            app_performance.set_decayed_global_performance(0, 0, 0, 0)
            app_performance.set_local_performance(
                cid_cached_performance_data[app_id]['major_count'],
                cid_cached_performance_data[app_id]['minor_count'],
                cid_cached_performance_data[app_id]['installs'],
                cid_today_installs[app_id] * revenue_cpa)

            app_id2performance[app_id] = app_performance

        return app_id2performance

    def get_cid_performance_data(self, df_params, n_day=7, decay_rate=1.0, install_decayed=True):
        """Get cid-level performance data.
        :param df_params: an instance of <DF_ADGROUP_PARAMS>
        :param n_day: data day range, 1/7/14/30
        :param decay_rate: daily decay rate of event metrics, 0.9/1.0
        :param install_decayed: whether the `installs` field in the return dict is decayed, <bool>
        :return cid_performance_data: dict contains `installs`, `major_count`, `minor_count` and `event_rate`
        """
        return self._get_cached_performance_data(df_params, 'cid', n_day, decay_rate, install_decayed)

    def get_oid_app_performance_data(self, df_params, is_subpublisher, n_day=1, decay_rate=1.0,
                                     install_decayed=True):
        """Get the oid app performance data for experiment evaluation.
        :param df_params: an instance of <DF_ADGROUP_PARAMS>
        :param is_subpublisher: is subpublisher or not, 1 or 0
        :param n_day: data day range, 1/7/30
        :param decay_rate: daily decay rate of event metrics, 0.9/1.0
        :param install_decayed: whether the `installs` field in <Performance> is decayed, <bool>
        :param app_ids: set/list of app_id that must appears in return data
        :return app_id2performance: dict of app_id fo <AppPerformance>
        """
        app_tag = 'subpublisher' if is_subpublisher else 'publisher'
        oid_cached_performance_data = self._get_cached_app_performance_data(
            df_params, 'oid_app_id', n_day, decay_rate, install_decayed)[app_tag]

        app_id2performance = {}
        for app_id, performance_data in oid_cached_performance_data.iteritems():
            app_performance = AppPerformance(app_id, is_subpublisher)
            app_performance.set_decayed_local_performance(0, 0, 0, 0)
            app_performance.set_decayed_global_performance(0, 0, 0, 0)
            app_performance.set_local_performance(
                performance_data['major_count'], performance_data['minor_count'], performance_data['installs'], 0)
            app_id2performance[app_id] = app_performance

        return app_id2performance

    def update_df_simulation_data(self, df_params, group_level, pub_performance_data, sub_performance_data):
        """Update the pub/sub performance from deep funnel pipeline with metrics from hydra metrics pipeline,
        which provides metrics that aligns with cid minor goal setting.
            AI-5019: HydraOID use cid's (event_id/event_type, goal_type) goal setting
            AI-6660: HydraSM use cid's (event_type, goal_type) goal setting
        :param df_params: an instance of <DF_ADGROUP_PARAMS>
        :param group_level: oid_app_id/category_app_id/country_app_id
        :param pub_performance_data: dict of publisher_id to <AppPerformance>
        :param sub_performance_data: dict of subpublisher_id to <AppPerformance>
        :return pub_performance: updated pub_performance
        :return sub_performance: updated sub_performance
        :return use_raw_goal: whether the returned performance can use raw_event_goal to simulate or not
        """
        assert (df_params.use_global_data and group_level in ('category_app_id', 'country_app_id')) \
            or (not df_params.use_global_data and group_level == 'oid_app_id')
        hydra_performance_data = self._get_cached_app_performance_data(df_params, group_level, 30, 0.9, True)
        if hydra_performance_data is None:
            logger.warning('no %s data from %s, use transfered goal rate', group_level, df_params.cid)
            return pub_performance_data, sub_performance_data, False

        revenue_cpa = self.get_revenue_cpa(df_params.oid) if df_params.raw_goal_type.startswith('ROAS') else 1.0
        for (app_tag, performance_data) in [('publisher', pub_performance_data),
                                            ('subpublisher', sub_performance_data)]:
            old_app_ids = set(performance_data.keys()) - set(hydra_performance_data[app_tag].keys())
            if old_app_ids:
                logger.warning('cid %s query %s data get old %s %s', df_params.cid, group_level, app_tag, old_app_ids)
                for app_id in old_app_ids:
                    del performance_data[app_id]

            for app_id, performance in performance_data.iteritems():
                origin_performance = performance.get_decayed_performance(df_params.use_global_data)
                metrics = hydra_performance_data[app_tag][app_id]
                performance.set_decayed_performance(
                    df_params.use_global_data, metrics['major_count'], metrics['minor_count'], metrics['installs'],
                    origin_performance.get_today_installs() * revenue_cpa
                )
                logger.debug('cid: %s change %s data %s %s from %s to %s', df_params.cid, group_level, app_tag, app_id,
                             origin_performance, performance.get_decayed_performance(df_params.use_global_data))

        return pub_performance_data, sub_performance_data, True

    def _get_cache_key(self, df_params, group_level, n_day, decay_rate, install_decayed):
        assert group_level in self.VALID_GROUP_LEVELS
        if group_level in ('cid', 'cid_app_id'):
            return (df_params.cid, df_params.raw_goal_type, df_params.event_type,
                    group_level, n_day, decay_rate, install_decayed)
        elif group_level in ('oid', 'oid_app_id'):
            return (df_params.oid, df_params.partner, df_params.raw_goal_type, df_params.event_type,
                    group_level, n_day, decay_rate, install_decayed)
        elif group_level in ('category', 'category_app_id'):
            # top_category + sub_category
            return (df_params.category_tuple[0], df_params.country, df_params.partner, df_params.raw_goal_type,
                    df_params.event_type, group_level, n_day, decay_rate, install_decayed)
        elif group_level in ('country', 'country_app_id'):
            return (df_params.country, df_params.partner, df_params.raw_goal_type, df_params.event_type,
                    group_level, n_day, decay_rate, install_decayed)

    def _get_cached_performance_data(self, df_params, group_level, n_day, decay_rate, install_decayed):
        cache_key = self._get_cache_key(df_params, group_level, n_day, decay_rate, install_decayed)
        if cache_key in self.cached_performance:
            return self.cached_performance[cache_key]

        major_counted_keys = set()
        performance_dict = defaultdict(float)
        query = self._get_mysql_query_string(df_params, group_level, n_day, decay_rate, install_decayed)
        if query is None:
            return None

        logger.debug(query)
        revenue_cpa = self.get_revenue_cpa(df_params.oid) if df_params.raw_goal_type.startswith('ROAS') else 1.0
        for row in self.query_db.get_query_result(query, db_client=self.df_db_client):
            patch_metric_row(row, df_params.raw_goal_type, df_params.min_major_count, revenue_cpa)
            if row['cid'] not in major_counted_keys:
                performance_dict['installs'] += float(row['installs'])
                performance_dict['major_count'] += float(row['major_count'])
                major_counted_keys.add(row['cid'])
            performance_dict['minor_count'] += float(row['minor_count'])

        performance_dict['event_rate'] = performance_dict['minor_count'] / performance_dict['major_count'] \
            if performance_dict['major_count'] else 0.0
        self.cached_performance[cache_key] = performance_dict
        return self.cached_performance[cache_key]

    def _get_cached_app_performance_data(self, df_params, group_level, n_day, decay_rate, install_decayed):
        cache_key = self._get_cache_key(df_params, group_level, n_day, decay_rate, install_decayed)
        if cache_key in self.cached_app_performance:
            return self.cached_app_performance[cache_key]

        major_counted_keys = set()
        app_performance_dict = {
            'publisher': defaultdict(lambda: defaultdict(float)),
            'subpublisher': defaultdict(lambda: defaultdict(float))
        }
        query = self._get_mysql_query_string(df_params, group_level, n_day, decay_rate, install_decayed)
        if query is None:
            return None

        logger.debug(query)
        revenue_cpa = self.get_revenue_cpa(df_params.oid) if df_params.raw_goal_type.startswith('ROAS') else 1.0
        for row in self.query_db.get_query_result(query, db_client=self.df_db_client):
            patch_metric_row(row, df_params.raw_goal_type, df_params.min_major_count, revenue_cpa)
            if (row['cid'], row['subpublisher']) not in major_counted_keys:
                for app_tag in ('publisher', 'subpublisher'):
                    app_performance_dict[app_tag][row[app_tag]]['installs'] += float(row['installs'])
                    app_performance_dict[app_tag][row[app_tag]]['major_count'] += float(row['major_count'])
                major_counted_keys.add((row['cid'], row['subpublisher']))
            for app_tag in ('publisher', 'subpublisher'):
                app_performance_dict[app_tag][row[app_tag]]['minor_count'] += float(row['minor_count'])

        self.cached_app_performance[cache_key] = app_performance_dict
        return self.cached_app_performance[cache_key]

    def _get_cid_today_installs(self, cid, partner_id, date_str):
        if cid in self.cid_today_installs:
            return self.cid_today_installs[cid]

        self.cid_today_installs[cid] = {'publisher': defaultdict(int), 'subpublisher': defaultdict(int)}
        if self.listctrl_cfg['exp_snapshot']:
            return self.cid_today_installs[cid]

        begin_ts = date_to_timestamp(date_str) if date_str else int(time.time()) // 86400 * 86400
        query = """
        select app_id, count(1) as installs
        from {}
        where cid = '{}' and action_time between {} and {}
        group by app_id
        """.format(CQueryDB.TABLE_EXT_AI_PERDAY_ACTION_TIME, cid, begin_ts, begin_ts + 86399)
        logger.debug(query)
        results = self.query_db.get_query_result(
            query, db_client=self.query_db.get_db_client_by_table(CQueryDB.TABLE_EXT_AI_PERDAY_ACTION_TIME))

        for row in results:
            publisher, subpublisher = self.appid_ruler.get_pubid_subid_from_appid(partner_id, row['app_id'])
            if publisher:
                self.cid_today_installs[cid]['publisher'][publisher] += int(row['installs'])
            if subpublisher:
                self.cid_today_installs[cid]['subpublisher'][subpublisher] += int(row['installs'])

        return self.cid_today_installs[cid]

    def _get_mysql_query_string(self, df_params, group_level, n_day, decay_rate, install_decayed):
        metrics_goal_type = df_params.raw_goal_type \
            if decay_rate == 1.0 else self._get_decayed_goal_type(df_params.raw_goal_type)
        query_app = group_level in ('cid_app_id', 'oid_app_id', 'category_app_id', 'country_app_id')
        if group_level in ('cid', 'cid_app_id'):
            cids = [df_params.cid]
        elif group_level in ('oid', 'oid_app_id'):
            cids = df_params.partner_cids
        elif group_level in ('category', 'category_app_id'):
            cids = self.get_aggregated_query_ids(df_params.partner, df_params.country, df_params.category_tuple[0])
        elif group_level in ('country', 'country_app_id'):
            cids = self.get_aggregated_query_ids(df_params.partner, df_params.country, '_all_')

        if df_params.event_ids is None or group_level.startswith('category') or group_level.startswith('country'):
            table_name = self.listctrl_cfg['event_type_app_perf_table' if query_app else 'event_type_perf_table']
            group_keys = ['cid', 'event_type', 'publisher', 'subpublisher'] if query_app else ['cid', 'event_type']
            event_cond = 'event_type = \'{}\''.format(df_params.event_type)
        else:
            table_name = self.listctrl_cfg['event_id_app_perf_table' if query_app else 'event_id_perf_table']
            group_keys = ['cid', 'event_id', 'publisher', 'subpublisher'] if query_app else ['cid', 'event_id']
            # event_id may contains `'`
            event_cond = 'event_id in ({})'.format(
                ','.join(['\'{}\''.format(event_id.replace("'", "\\'")) for event_id in df_params.event_ids]))

        timedelta = datetime.timedelta(days=60) if self.listctrl_cfg['exp_snapshot'] else datetime.timedelta(hours=36)
        valid_update_time = (datetime.datetime.utcnow() - timedelta).strftime('%Y-%m-%d %H:%M:%S')
        install_goal_type = self._get_decayed_goal_type(df_params.raw_goal_type) \
            if decay_rate != 1.0 and install_decayed else df_params.raw_goal_type
        install_goal_type = install_goal_type.replace('ROAS', 'PRAT').replace('RR', 'PRAT')

        return hydra_event_metrics_query.format(
            group_keys=', '.join(group_keys), table_name=table_name, event_cond=event_cond, n_day=n_day,
            cids=','.join(['\'{}\''.format(cid) for cid in cids]), valid_update_time=valid_update_time,
            metrics_goal_type=metrics_goal_type, install_goal_type=install_goal_type,
            join_cond=' AND '.join('quality.{0}=quantity.{0}'.format(key) for key in group_keys),
            query_keys=', '.join('quality.{}'.format(key) for key in group_keys)) if cids else None

    def _get_decayed_goal_type(self, goal_type):
        return '{}_DECAY'.format(goal_type) if goal_type in ('PRAT', 'ROAS') else goal_type.replace('-', '-DECAY-')
