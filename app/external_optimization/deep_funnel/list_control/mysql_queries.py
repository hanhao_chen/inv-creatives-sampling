minor_high_mean_target_date_db_query = """
SELECT
    oid, app_id, is_subpublisher, prediction, decay_minor_count30D, decay_major_count30D, latest_install,
    decay_category_minor_count30D as decay_global_minor_count30D,
    decay_category_major_count30D as decay_global_major_count30D,
    latest_global_install, minor_count30D, major_count30D
FROM
    {6}
WHERE
    ((major_count30D > 0 and prediction > 0) or latest_install > 10)
    and oid = '{0}'
    and goal_type = '{3}'
    and event_type = '{4}'
    and is_subpublisher = {1}
    and date = '{5}'
    and partner_id = '{2}'
"""

minor_sampled_target_date_db_query = """
SELECT
    oid, app_id, is_subpublisher, sampled_value, decay_minor_count30D, decay_major_count30D, latest_install,
    decay_category_minor_count30D as decay_global_minor_count30D,
    decay_category_major_count30D as decay_global_major_count30D,
    latest_global_install, minor_count30D, major_count30D
FROM
    {6}
WHERE
    ((global_major_count30D > 0 and global_prediction > 0 and global_minor_count30D > 0) or latest_global_install > 10)
    and oid = '{0}'
    and goal_type = '{3}'
    and event_type = '{4}'
    and is_subpublisher = {1}
    and date = '{5}'
    and partner_id = '{2}'
"""

minor_coldstart_target_date_db_query = minor_sampled_target_date_db_query + """
    and country = '{7}'
"""

minor_sampled_country_date_db_query = """
SELECT
    oid, app_id, is_subpublisher, sampled_value, decay_minor_count30D, decay_major_count30D, latest_install,
    decay_country_minor_count30D as decay_global_minor_count30D,
    decay_country_major_count30D as decay_global_major_count30D,
    latest_global_install, minor_count30D, major_count30D
FROM
    {6}
WHERE
    (global_prediction > 0 or latest_global_install > 10)
    and oid = '{0}'
    and goal_type = '{3}'
    and event_type = '{4}'
    and is_subpublisher = {1}
    and date = '{5}'
    and partner_id = '{2}'
    and country = '{7}'
"""

# hard constraint for RR-1D with install > 40 and rate < 0.05
minor_rr_filter_target_date_query = """
SELECT
    oid,
    app_id,
    major_count30D,
    latest_install,
    minor_count30D / (major_count30D + latest_install) as event_rate
FROM
    {6}
WHERE
    oid = '{0}'
    and goal_type = 'RR-1D'
    and is_subpublisher = {1}
    and date = '{5}'
    and partner_id = '{2}'
    and major_count30D >= 40
    and minor_count30D / (major_count30D + latest_install) <= 0.05
"""

# hard constraint for PRAT with install > 40 and minor events = 0 (AI-4973)
minor_prat_filter_target_date_query = """
SELECT
    oid,
    app_id,
    major_count30D,
    latest_install,
    minor_count30D / (major_count30D + latest_install) as event_rate
FROM
    {6}
WHERE
    oid = '{0}'
    and goal_type = 'PRAT-30D'
    and is_subpublisher = {1}
    and date = '{5}'
    and partner_id = '{2}'
    and event_type = '{4}'
    and major_count30D >= 40
    and minor_count30D = 0
"""

minor_black_category_target_date_query = """
SELECT
    app_id
FROM
    {6}
WHERE
    oid = '{0}'
    and partner_id = '{2}'
    and goal_type = '{3}'
    and event_type = '{4}'
    and category_major_count30D > 10
    and category_event_rate30D <= 0
    and is_subpublisher = {1}
    and date = '{5}'
"""

minor_black_country_target_date_query = """
SELECT
    app_id
FROM
    {6}
WHERE
    oid = '{0}'
    and partner_id = '{2}'
    and goal_type = '{3}'
    and event_type = '{4}'
    and country_major_count30D > 10
    and country_event_rate30D <= 0
    and is_subpublisher = {1}
    and date = '{5}'
"""

hydra_event_metrics_query = """
select
    {query_keys}, quality.major_count, quality.minor_count, quantity.installs
from
(
    select
        {group_keys}, major_count, minor_count
    from
        {table_name}
    where
        cid in ({cids})
        and {event_cond}
        and n_day = {n_day}
        and goal_type = '{metrics_goal_type}'
        and update_time >= '{valid_update_time}'
        and (major_count > 0 or minor_count > 0)
    group by
        {group_keys}
) as quality
left outer join
(
    select
        {group_keys}, major_count as installs
    from
        {table_name}
    where
        cid in ({cids})
        and {event_cond}
        and n_day = {n_day}
        and goal_type = '{install_goal_type}'
        and update_time >= '{valid_update_time}'
        and (major_count > 0 or minor_count > 0)
    group by
        {group_keys}
) as quantity
on {join_cond}
having quantity.installs is not NULL
"""
