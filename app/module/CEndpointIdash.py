from __future__ import print_function
import json
import math
import re
import time
import calendar
import datetime
import requests
from builtins import range
from cachetools.func import ttl_cache

from app.module.Const import AI_AUTO_BIDPRICE_OPTION, CONST_DOLLAR, APPID_LABEL_NATIVE, \
    APPID_LABEL_CREATIVE, RULE_ID_AUTORULE, RULE_ID_DEEPFUNNEL
from app.module.Const import RULE_WHITELIST, RULE_BLACKLIST
from app.module.Const import HOST_AGGREGATOR
from app.utils.init_logger import init_logger

logger = init_logger(__name__)


class CEndpointIdash:
    ENDPOINT_ISP = "/read/external_field_value_list?partner={partner}&field=carrier"
    ENDPOINT_CRWTSK_LINK = "/read/crawler_task_link"
    ENDPOINT_CRWTSK = "/read/crawler_tasks"

    def __init__(self, idash_cfg):
        self.idash_cfg = idash_cfg
        self.hour_performance = {}
        self.campaign_config = {}
        self.external_config = {}
        self.oid_status = {}
        self.crid_data = {}
        self._isp_dict = {}
        self.cookies = None
        self._update_time = None
        self.login_campaign_data_service()

    def get_enhance_rules_status(self, cid):

        status = self.get_cid_data_request(cid).json()
        if len(status):
            status = status[0]
            if isinstance(status, dict):
                enhance_rules = status.get('enhance_rules')
                return enhance_rules
            else:
                logger.info('Cannot get enhance rules %s', cid)
                return []
        else:
            logger.warning('Cannot get cid status %s', cid)
            return []

    def get_cid_status(self, cid):
        status = self.get_cid_data_request(cid).json()[0]
        return status

    def get_cid_goal_cpm(self, cid):

        status = self.get_cid_data_request(cid).json()
        goal_cpm = 0
        if len(status):
            status = status[0]
            if isinstance(status, dict):
                goal_cpm = status.get('optimization_goal_cpm', 0)
            else:
                logger.info('Cannot get cid goal %s', cid)
        else:
            logger.warning('Cannot get cid status %s', cid)

        return goal_cpm

    def get_cid_goal_cpa(self, cid):

        status = self.get_cid_data_request(cid).json()
        goal_cpa = 0
        if len(status):
            status = status[0]
            if isinstance(status, dict):
                goal_cpa = status.get('optimization_goal_cpa', 0)
            else:
                logger.info('Cannot get cid goal %s', cid)
        else:
            logger.warning('Cannot get cid status %s', cid)

        return goal_cpa

    def _request_get(self, url, timeout=10, max_retry=3, params=None, headers=None, backoff_multiplier=1.4):
        """
        get requests with backoff retry strategy
        :param url: request url
        :param timeout: integer type, set default to 10 seconds
        :param max_retry: integer type, set default to 3 times
        :param params: same as params in requests.get()
        :param headers: same as headers in requests.get()
        :param backoff_multiplier: float/integer type, A multiplier which is used to determine sleep seconds
        for every retry attempt. set default value to 1.4
        :return: response object
        """
        self.check_cookie_expired()
        sleep_seconds = 1
        for retry in range(max_retry):
            sleep_seconds = sleep_seconds * backoff_multiplier
            try:
                r = requests.get(url, params=params, cookies=self.cookies, timeout=timeout, headers=headers)
            except requests.exceptions.ReadTimeout as e:
                logger.warning("Endpoint Timeout: %s, retry: %s", url, retry)
                if retry == max_retry - 1:
                    raise e
                time.sleep(sleep_seconds)
                continue
            if r.status_code == 200:
                return r
            if r.status_code in range(400, 500):
                r.raise_for_status()
            # retry for 5xx status
            logger.warning("Endpoint: %s, status_code: %s", url, r.status_code)
            if retry == max_retry - 1:
                r.raise_for_status()
            time.sleep(sleep_seconds)
        return r

    def send_bidder_aggregator(self, data, cid):
        logger.warning('[%s] adjust len: %s', cid, len(data['data']['inv_adjust']))
        start = time.time()
        r = requests.post(HOST_AGGREGATOR, data=json.dumps(data), timeout=240)
        status_code = r.status_code
        if status_code != 200:
            logger.critical('[%s] post to aggregator fail, status code %s, took %s sec',
                            cid, status_code, time.time() - start)
            return False

        logger.warning('[%s] post to aggregator success, took %s sec.', cid, time.time() - start)
        return True

    def _request_post(self, url, timeout=10, max_retry=3, data=None, json=None, headers=None, backoff_multiplier=1.4):
        """
        post request with backoff retry strategy
        :param url: request url
        :param timeout: integer type, set default to 10 seconds
        :param max_retry: integer type, set default to 3 times
        :param data: same as data in requests.post()
        :param headers: same as headers in requests.post()
        :param backoff_multiplier: float/integer type, A multiplier which is used to determine sleep seconds
        for every retry attempt. set default value to 1.4
        :return: response object
        """
        # The login request doesnt need to provide cookies.
        if self._update_time:
            self.check_cookie_expired()
        sleep_seconds = 1
        for retry in range(max_retry):
            sleep_seconds = sleep_seconds * backoff_multiplier
            try:
                r = requests.post(url, data=data, json=json, cookies=self.cookies, timeout=timeout, headers=headers)
            except requests.exceptions.ReadTimeout as e:
                logger.warning("Endpoint Timeout: %s, retry: %s", url, retry)
                if retry == max_retry - 1:
                    raise e
                time.sleep(sleep_seconds)
                continue
            if r.status_code == 200:
                return r
            if r.status_code in range(400, 500):
                r.raise_for_status()
            # retry for 5xx status
            logger.warning("Endpoint: %s, status_code: %s", url, r.status_code)
            if retry == max_retry - 1:
                r.raise_for_status()
            time.sleep(sleep_seconds)
        return r

    def login_campaign_data_service(self, timeout=30, max_retry=10):
        data = {
            "_": int(time.time()),
            'username': self.idash_cfg['username'],
            'password': self.idash_cfg['password']
        }
        url = self.idash_cfg["host"] + '/login'
        headers = {'Connection': 'close'}
        self._update_time = None
        r = self._request_post(url, timeout, max_retry, data=data, headers=headers)
        self.cookies = r.cookies
        self._update_time = int(time.time())
        return True

    def check_cookie_expired(self):
        ts_now = int(time.time())
        logger.debug('[IDASH] check timestamp %s %s', self._update_time, ts_now)
        if ts_now - self._update_time > 3600 * 6:
            logger.debug('[IDASH] cookie expired, re-login')
            self.login_campaign_data_service()

    def read_cid_performance_direct(self, cid, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/performance?campaign_id=%s&customer_view=cm_view" % cid
        r = self._request_get(url, timeout, max_retry, params=params)
        record = {}
        for res in r.json():
            record.update(res)
        return record

    def read_cid_hour_performance_direct(self, cid, start_ts, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url_suffix = "/performance?campaign_id=%s&group=hour&customer_view=cm_view&start_ts=%s" % (cid, start_ts)
        url = self.idash_cfg["host"] + url_suffix
        r = self._request_get(url, timeout, max_retry, params=params)
        record = []
        for res in r.json():
            record.append(res)
        return record

    def read_cid_hour_performance(self, cid, ts=time.time(), past_hours=24):
        # read last 24hr idash data
        self.check_cookie_expired()

        if len(self.hour_performance) <= 0:
            start_ts = ts - 3600 * past_hours
            self.hour_performance = self.read_hour_performance(start_ts, past_hours)

        return self.hour_performance.get(cid, [])

    def read_hour_performance(self, start_ts, past_hours=24, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        end_ts = start_ts + 3600 * past_hours
        url_suffix = "/performance?group=campaign-hour&customer_view=cm_view&start_ts=%s&end_ts=%s" % (
            int(start_ts), int(end_ts)
        )
        url = self.idash_cfg["host"] + url_suffix
        r = self._request_get(url, timeout, max_retry, params=params)
        record = {}
        for res in r.json():
            cid = res['cid']
            if cid not in record:
                record[cid] = []
            record[cid].append(res)
        return record

    def read_oid_list_performance(self, oid_list, start_ts, past_hours=24, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        end_ts = start_ts + 3600 * past_hours
        oid_list_str = ','.join(["\"%s\"" % oid for oid in oid_list])
        url_suffix = "/performance?order_ids=[%s]&group=campaign-hour&customer_view=cm_view&start_ts=%s&end_ts=%s&timezone=8" % (
            oid_list_str, int(start_ts), int(end_ts)
        )
        url = self.idash_cfg["host"] + url_suffix
        r = self._request_get(url, timeout, max_retry, params=params)
        record = {}
        for res in r.json():
            cid = res['cid']
            if cid not in record:
                record[cid] = []
            record[cid].append(res)
        return record

    def read_cids_hour_bid_log(self, cids_str, updated_time_after, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url_suffix = "/read/campaign_bid_record?cids=[{}]&updated_at_after={}".format(
            cids_str, str(int(updated_time_after))
        )
        url = self.idash_cfg["host"] + url_suffix
        r = self._request_get(url, timeout, max_retry, params=params)
        return r.json()['data']

    def read_cid_campaign_config(self, cid):
        self.check_cookie_expired()

        if cid not in self.campaign_config:
            self.read_campaign_config([cid])

        return self.campaign_config.get(cid, {})

    def read_campaign(self, cid, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        cid_list = [cid]
        cid_list_str = ','.join(["\"%s\"" % cid for cid in cid_list])
        url = self.idash_cfg["host"] + "/read/campaign?cids=[%s]" % cid_list_str
        r = self._request_get(url, timeout, max_retry, params=params)
        mapping = r.json()
        return mapping[0] if len(mapping) > 0 else {}

    def read_order(self, oid, fields=None, timeout=60, max_retry=10):
        params = {"_": int(time.time())}
        path = '/read/order?oid=' + oid
        if fields is not None:
            path += '&fields=' + json.dumps(fields)
        url = self.idash_cfg["host"] + path
        r = self._request_get(url, timeout, max_retry, params=params)
        return r.json()[0]

    def read_orders(self, oids, fields=None, timeout=60, max_retry=3):
        params = {"_": int(time.time())}
        rounds = int(math.ceil(len(oids) / 40.0))

        result = []
        for i in range(rounds):
            path = '/read/order?oids={}'.format(json.dumps(oids[i * 40:i * 40 + 40]))
            if fields is not None:
                path += '&fields={}'.format(json.dumps(fields))
            url = self.idash_cfg["host"] + path
            r = self._request_get(url, timeout, max_retry, params=params)
            result.extend(r.json())
        return result

    def read_campaign_fields(self, cid, fields, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url_suffix = "/read/campaign?cid=%s&fields=[%s]" % (cid, ",".join(["\"%s\"" % f for f in fields]))
        url = self.idash_cfg["host"] + url_suffix
        r = self._request_get(url, timeout, max_retry, params=params)
        return r.json()[0] if len(r.json()) > 0 else {}

    def get_campaign_last_action_info(self, cid, key, timeout=30, max_retry=10):
        """Returns dict of changed field and updated info, or empty dict if no result.
        For example, if key == 'enabled', this method returns {
            'from': <original_value>, 'key': 'enabled', 'to': <new_value>,
            'updated_at': <timestamp>, 'updated_by': <user>, 'v': 1,
        }.
        """
        params = {
            "_": int(time.time())  # cache buster
        }
        url_suffix = '/read/campaign?cid=%s&fields=["history"]' % cid
        url = self.idash_cfg["host"] + url_suffix
        result = self._request_get(url, timeout, max_retry, params=params).json()
        if not result:
            return {}
        for row in result[0].get('history', [])[::-1]:
            for change in row.get('changes', []):
                if change.get('key') == key:
                    row.pop('changes')
                    row.update(change)
                    return row
        return {}

    def read_campaigns_by_cids(self, cids, fields=None, modified_at_after=None, timeout=30, max_retry=3):
        data = {
            "_": int(time.time()),  # cache buster
            "cids": json.dumps(cids)
        }
        url = self.idash_cfg["host"] + "/read/campaign"
        params = []
        if fields is not None:
            params.append('fields={}'.format(json.dumps(fields)))
        if modified_at_after is not None:
            params.append('modified_at_after={}'.format(modified_at_after))
        if params:
            url += '?{}'.format('&'.join(params))
        r = self._request_post(url, timeout, max_retry, data=data)
        return r.json()

    def read_campaign_direct(self, cid, timeout=30, max_retry=10):
        # TODO: merge this function with read_campaign_config
        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/read/campaign?cid=%s" % cid
        r = self._request_get(url, timeout, max_retry, params=params)

        cid_dict = {}
        for entry in r.json():
            cid_dict[entry['cid']] = {}
            for label in ['cid', 'oid', 'enabled']:
                cid_dict[entry['cid']][label] = entry[label]

            cid_dict[entry['cid']]['ext_channel'] = entry['campaign_other_type']
            cid_dict[entry['cid']]['country'] = entry['require_regions'][0] if len(entry['require_regions']) > 0 else ''
            cid_dict[entry['cid']]['require_device_os'] = entry['require_device_os'][0] if len(
                entry['require_device_os']) > 0 else ''
            cid_dict[entry['cid']]['is_rtb'] = 1 if entry['integration_type'] == 'rtb' else 0
            cid_dict[entry['cid']]['algorithm'] = entry.get('ctr_table_version', '')

            category_list = [x.lower() for x in entry.get('advertiser_categories', [])]
            category = 'NonGameCosmetics'
            for cat in category_list:
                if 'game' in cat:
                    category = 'Game'
                    break
                if 'shopping' in cat or 'cosmetics' in cat:
                    category = 'Cosmetics'
                    break
            cid_dict[entry['cid']]['category'] = category

        return cid_dict.get(cid, {})

    def read_cid_external_config(self, cid):
        if cid not in self.external_config:
            self.read_external_config([cid])

        return self.external_config.get(cid, {})

    def read_external_config(self, cid_list, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        cid_list_str = ','.join(["\"%s\"" % cid for cid in cid_list])
        url = self.idash_cfg["host"] + "/read/external_campaign?cids=[%s]" % cid_list_str
        r = self._request_get(url, timeout, max_retry, params=params)

        res_dict = {}
        for entry in r.json():
            cid = entry['cid']
            res_dict[cid] = entry
        self.external_config.update(res_dict)
        return self.external_config

    def read_campaign_config(self, cid_list, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        cid_list_str = ','.join(["\"%s\"" % cid for cid in cid_list])
        url = self.idash_cfg["host"] + "/read/campaign?cids=[%s]" % cid_list_str
        r = self._request_get(url, timeout, max_retry, params=params)

        PARM_CAP_DAILY_BUDGET = 0
        adj_dict = {}
        for entry in r.json():
            cid = entry['cid']
            oid = entry['oid']
            bid_price = float(entry.get('ext_bid_price', 0.0))
            try:
                daily_budget = float(entry.get('daily_max_budget', 0.0))
            except:
                daily_budget = 0.0
            goal_cpa = float(entry.get('optimization_goal_cpa', 0.0))
            try:
                ai_goalcpa = float(entry.get('optimizer_options', {}).get('ai_auto_cpagoal', 0.0))
            except:
                ai_goalcpa = 0.0
            try:
                ai_budgetcap = float(entry.get('optimizer_options', {}).get('ai_auto_bgt_cap', PARM_CAP_DAILY_BUDGET))
            except:
                ai_budgetcap = PARM_CAP_DAILY_BUDGET
            optimizer_options = entry.get('optimizer_options', {})
            ai_bgt_ratio = float(optimizer_options.get('ai_auto_bgt_ratio', 1.0))
            ai_cpa_ratio = float(optimizer_options.get('ai_auto_cpa_ratio', 1.0))
            try:
                ai_bid_min = float(optimizer_options.get('ai_auto_bid_min', 0.0)) * CONST_DOLLAR
            except:
                ai_bid_min = 0.0

            try:
                ai_bid_max = float(optimizer_options.get('ai_auto_bid_max', 1.0)) * CONST_DOLLAR
            except:
                ai_bid_max = CONST_DOLLAR

            ai_auto_bidprice = optimizer_options.get('ai_auto_bidprice', AI_AUTO_BIDPRICE_OPTION.CM)
            auto_bw = optimizer_options.get('auto_bw', False)

            partner = entry['campaign_other_type']
            enabled = entry['enabled']
            history = entry.get('history', [])
            # deprecated
            require_apps = entry.get('require_apps', [])
            reject_apps = entry.get('reject_apps', [])
            # TODO: migrate all API buying partners to these new fields
            require_publisher = entry.get('require_publisher', [])
            require_sub_publisher = entry.get('require_sub_publisher', [])
            reject_publisher = entry.get('reject_publisher', [])
            reject_sub_publisher = entry.get('reject_sub_publisher', [])

            country = entry['require_regions'][0] if len(entry['require_regions']) > 0 else ''
            require_device_os = entry['require_device_os'][0] if len(entry['require_device_os']) > 0 else ''
            category_list = [x.lower() for x in entry.get('advertiser_categories', [])]
            enhance_rules = entry.get('enhance_rules', [])

            category = 'NonGameCosmetics'
            for cat in category_list:
                if 'game' in cat:
                    category = 'Game'
                    break
                if 'shopping' in cat or 'cosmetics' in cat:
                    category = 'Cosmetics'
                    break

            bid_price_history = {}
            for h in history:
                utime = int(h['updated_at'])
                changes = h['changes']
                for c in changes:
                    key = c['key']
                    if key != 'ext_bid_price':
                        continue
                    price = float(c['to'])
                    bid_price_history[utime] = price

            isp = entry.get('ext_require_device_carriers', {}).get(country, [])

            adj_dict[cid] = {
                'bid_price': bid_price,
                'daily_budget': daily_budget,
                'partner': partner,
                'enabled': enabled,
                'goalcpa': goal_cpa,
                'history': bid_price_history,
                'ai_goalcpa': ai_goalcpa * CONST_DOLLAR,
                'ai_budgetcap': ai_budgetcap * CONST_DOLLAR,
                'ai_bgt_ratio': ai_bgt_ratio,
                'ai_cpa_ratio': ai_cpa_ratio,
                'ai_bid_min': ai_bid_min,
                'ai_bid_max': ai_bid_max,
                'ai_auto_bidprice': ai_auto_bidprice,
                'require_apps': require_apps,
                'reject_apps': reject_apps,
                'oid': oid,
                'country': country,
                'require_device_os': require_device_os,
                'category': category,
                'auto_bw': auto_bw,
                'enhance_rules': enhance_rules,
                'isp': isp,
                'require_publisher': require_publisher,
                'require_sub_publisher': require_sub_publisher,
                'reject_publisher': reject_publisher,
                'reject_sub_publisher': reject_sub_publisher,
                'raw_data': entry
            }

        self.campaign_config.update(adj_dict)
        return self.campaign_config

    def read_order_of_asap(self, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/read/order?source=ASAP&fields=[\"name\"]"
        r = self._request_get(url, timeout, max_retry, params=params)
        oid_list = []
        for c in r.json():
            oid_list.append(c['oid'])
        return oid_list

    def read_order_by_status(self, status, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/read/order?status_code=%s&fields=[\"oid\"]" % status
        r = self._request_get(url, timeout, max_retry, params=params)

        oid_list = []
        for c in r.json():
            oid_list.append(c['oid'])
        return oid_list

    def read_oid_status(self, oid, timeout=30, max_retry=10):
        if oid in self.oid_status:
            return self.oid_status[oid]

        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/read/order?oid=%s&fields=[\"status_code\"]" % oid
        r = self._request_get(url, timeout, max_retry, params=params)

        status = ""
        for entry in r.json():
            status = entry['status_code']
        self.oid_status[oid] = status
        return status

    def read_oid_goalcpa(self, oid, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/read/order?oid=%s&fields=[\"goal_cpa\"]" % oid
        r = self._request_get(url, timeout, max_retry, params=params)

        goal_cpa = 1.0 * CONST_DOLLAR
        for entry in r.json():
            goal_cpa = float(entry['goal_cpa'])
        return goal_cpa

    def read_crid_data(self, crid_list, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        new_crid_list = []
        for crid in crid_list:
            if len(crid) < 22 or crid in self.crid_data:
                continue
            new_crid_list.append(crid)
        if len(new_crid_list) <= 0:
            return self.crid_data
        crid_list_str = '/'.join(new_crid_list)
        url = self.idash_cfg["host"] + "/read/creative/%s" % crid_list_str
        r = self._request_get(url, timeout, max_retry, params=params)

        crid_dict = {}
        for entry in r.json():
            crid = entry['crid']
            crid_dict[crid] = entry
        self.crid_data.update(crid_dict)
        return self.crid_data

    def get_crid_type(self, crid_dict):
        new_crid_list = []
        for crid in crid_dict:
            if crid not in self.crid_data:
                new_crid_list.append(crid)

        crid_data = self.read_crid_data(new_crid_list)

        for crid in crid_dict:
            creative_type = crid_data.get(crid, {}).get('creative_type', '')
            ext_crid = crid_dict.get(crid, {}).get('ext_crid', '')
            creative_label = APPID_LABEL_CREATIVE
            if 'rich' in creative_type or 'native' in ext_crid:
                creative_label = APPID_LABEL_NATIVE

            crid_dict[crid]['creative_type'] = creative_label

        return crid_dict

    def read_startapp_min_bid(self, cid, country_list, require_device_os, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        country_list_str = json.dumps(country_list)
        url = self.idash_cfg["host"] + "/externalrobotdata/get/startapp_min_bid?countries=%s&os=%s&cid=%s" % (
            country_list_str, require_device_os, cid
        )
        r = self._request_get(url, timeout, max_retry, params=params)
        return float(r.text.strip('"')) * CONST_DOLLAR

    def filter_startapp_appid_rules(self, appid_list):
        filter_appid_list = [appid for appid in appid_list if re.match(r'^[\w-]+$', appid)]
        if '' in appid_list:
            filter_appid_list.extend(['unknown', ''])

        return filter_appid_list

    def send_update_request(self, cid, partner, reject_apps=None, require_apps=None, bid_price=None,
                            daily_max_budget=None, daily_max_bid=None, daily_budget_cap=None,
                            daily_min_bid=None, timeout=30, max_retry=10):
        data = {
            "_": int(time.time())  # cache buster
        }

        for apps in [reject_apps, require_apps]:
            if apps is None:
                continue
            for app_id in apps:
                if '{pub_subid}' in app_id or 'xapnt' in app_id:
                    apps.remove(app_id)
        if reject_apps is not None:
            if partner in ['startapp']:
                appid_list = self.filter_startapp_appid_rules(reject_apps)
                data['reject_apps'] = appid_list[:3000]
            else:
                data['reject_apps'] = reject_apps[:3000]
        if require_apps is not None:
            if partner in ['startapp']:
                appid_list = self.filter_startapp_appid_rules(require_apps)
                data['require_apps'] = appid_list[:1000]
            else:
                data['require_apps'] = require_apps
        if bid_price is not None:
            data["ext_bid_price"] = "%s" % int(bid_price)
        if daily_max_budget is not None:
            data["daily_max_budget"] = "%s" % int(daily_max_budget)
        if daily_max_bid or daily_min_bid:
            if "optimizer_options" not in data:
                data["optimizer_options"] = {}
            if daily_max_bid:
                data["optimizer_options"]["ai_auto_bid_max"] = "%s" % daily_max_bid
            if daily_min_bid:
                data["optimizer_options"]["ai_auto_bid_min"] = "%s" % daily_min_bid
        if daily_budget_cap:
            if "optimizer_options" not in data:
                data["optimizer_options"] = {}
            data["optimizer_options"]["ai_auto_bgt_cap"] = "%s" % max(int(daily_budget_cap / CONST_DOLLAR), 5)

        url_suffix = "/update/campaign/%s" % cid
        url = self.idash_cfg["host"] + url_suffix
        _ = self._request_post(url, timeout, max_retry, data=self.stringify_dict(data))
        if partner not in ['ironsource']:
            return self.send_job(cid, partner, 'update')
        return True

    def get_cid_data_request(self, cid, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }

        url = self.idash_cfg["host"] + "/read/campaign?cid=%s" % cid
        r = self._request_get(url, timeout, max_retry, params=params)
        return r

    def get_creatives(self, cid, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }

        url = self.idash_cfg["host"] + "/query/creative?cid=%s" % cid
        r = self._request_get(url, timeout, max_retry, params=params)
        return r

    def send_update_data_request(self, cid, data_dict, timeout=30, max_retry=10):
        data = {
            "_": int(time.time())  # cache buster
        }

        url = self.idash_cfg["host"] + "/update/campaign/%s" % cid
        data.update(data_dict)
        _ = self._request_post(url, timeout, max_retry, data=self.stringify_dict(data))
        return True

    def send_cid_enable_request(self, cid, partner, enabled, timeout=30, max_retry=10):
        data = {
            "_": int(time.time()),
            'enabled': str(enabled).lower()
        }
        url = self.idash_cfg["host"] + "/update_status/campaign/%s" % cid
        _ = self._request_post(url, timeout, max_retry, data=data)
        return self.send_job(cid, partner, 'switch')

    @ttl_cache(ttl=3600)
    def get_ext_capabilities(self, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg['host'] + '/externalrobotdata/v2.1/config'
        r = self._request_get(url, timeout, max_retry, params=params)

        tmp_external_capability = {}
        for capability in r.json():
            tmp_external_capability.setdefault(capability['partner'], {})[capability['partner_type']] = capability

        external_capability = {}
        for partner_id, partner_types in tmp_external_capability.items():
            for partner_type in ('feed', 'api', 's2s'):
                if partner_type in partner_types:
                    external_capability[partner_id] = partner_types[partner_type]
                    break

        return external_capability

    def send_job(self, cid, partner, action, timeout=30, max_retry=10):
        capability = self.get_ext_capabilities()
        logger.info(capability.get(partner))
        try:
            if capability[partner]['partner_type'] != 'api':
                return True
        except:
            logger.error('unexpected capability: %s', partner)
            return False

        params = {
            "_": int(time.time())  # cache buster
        }
        url_suffix = "/create/job_queue?type=external_robot&command={\"partner\":\"%s\",\"cid\":\"%s\",\"action\":\"%s\"}" % (
            partner, cid, action
        )
        url = self.idash_cfg["host"] + url_suffix
        r = self._request_get(url, timeout, max_retry, params=params)
        return r.json().get('success', False)

    def copy_adgroup(self, source_cid, begin_at, end_at, name,
                     wait_job=False, wait_job_timeout=3600, timeout=60, max_retry=10):
        """Copy an existing adgroup
        :param source_cid: source adgroup cid
        :param begin_at: copied adgroup start date (timestamp)
        :param end_at: copied adgroup's end date (timestamp)
        :param name: copied adgroup's name
        :param wait_job: whether waits idash job finish
        :param wait_job_timeout: maximum timestamp to wait idash job queue
        :return id:
            returns new cid when wait_job is True
            returns job queue when wait_job if False
            returns None if error occurs
        """
        data = {'begin_at': begin_at, 'end_at': end_at, 'name': name}
        url = self.idash_cfg['host'] + '/copy/campaign/' + source_cid
        r = self._request_post(url, timeout, max_retry, data=data)

        response = r.json()
        if response['success'] is False:
            return None
        job_id = response['job_id']
        if wait_job:
            start_ts = int(time.time())
            success = False
            while not success and int(time.time()) - start_ts <= wait_job_timeout:
                job_status = self.read_job_queue(job_id)
                try:
                    if (job_status['result']['success'] and
                            job_status['result']['data']['cid'] and
                            'errorMsg' not in job_status['result']['data']):
                        return job_status['result']['data']['cid']
                except:
                    logger.info('job %s not ready: %s', job_id, job_status)
                    time.sleep(120)
            logger.error('copy campaign failed, job_id: %s', job_id)
            return None

        return job_id

    def read_job_queue(self, job_id, timeout=30, max_retry=10):
        params = {'_': int(time.time())}
        url = self.idash_cfg['host'] + '/read/job_queue?query={{"_id":"{}"}}'.format(job_id)
        r = self._request_get(url, timeout, max_retry, params=params)

        response = r.json()
        if response.get('success', False) is False or len(response['data']) == 0:
            return {}
        return response['data'][0]

    def deploy_campaign(self, cid, timeout=30, max_retry=10):
        self.check_cookie_expired()
        data = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/deploy/campaign?cid=%s" % cid
        r = self._request_post(url, timeout, max_retry, data=data)
        return r.json().get("success", False)

    def get_ad_network_table_name(self, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/ano/read/ano_performance/temp_table_info"
        r = self._request_get(url, timeout, max_retry, params=params)

        result = r.json()
        table_ad_network_info = {
            'action_table_name': result['data']['action_base_table'],
            'click_table_name': result['data']['base_table'],
            'table_date': result['data']['action_base_table_end_date'],
            'update_at': result['data']['update_at'],
        }
        return table_ad_network_info

    def stringify_dict(self, json_dict):
        for k in json_dict:
            if isinstance(json_dict[k], list) or \
                    isinstance(json_dict[k], dict) or \
                    isinstance(json_dict[k], bool):
                json_dict[k] = json.dumps(json_dict[k])

        return json_dict

    def read_inv_lookalike(self, oid, ext_channel, country, list_size=None, T=None, timeout=30, max_retry=10):
        url = self.idash_cfg["jelly_host"] + '/gen_lookalike'
        data = {'oid': oid, 'channel': ext_channel, 'country': country}
        if list_size:
            data['output_list_size'] = list_size
        if T:
            data['T'] = T
        r = self._request_post(url, timeout, max_retry, data=json.dumps(self.stringify_dict(data)))
        return r.json().get('white_list', [])

    def read_ai_black_white_rules_df(self, cid):
        data = self.read_ai_black_white_rules(cid, rule_id=RULE_ID_DEEPFUNNEL, logger_level=10)
        whitelist = {}
        blacklist = {}
        for row in data:
            if row.get('reject_type') in ('reject_sub_publisher', 'reject_publisher'):
                if row['reject_type'] not in blacklist:
                    blacklist[row['reject_type']] = set()
                blacklist[row['reject_type']].add(row['channel_id'])
            elif row.get('require_type') in ('require_sub_publisher', 'require_publisher'):
                if row['require_type'] not in whitelist:
                    whitelist[row['require_type']] = set()
                whitelist[row['require_type']].add(row['channel_id'])

        return whitelist, blacklist

    def read_ai_black_white_rules_auto_rule(self, cid, rule_type, field_name):
        data = self.read_ai_black_white_rules(cid, rule_id=RULE_ID_AUTORULE)
        bw_list = set()
        if rule_type == RULE_BLACKLIST:
            for d in data:
                if ('reject_type' not in d) or d['reject_type'] != field_name:
                    continue
                bw_list.add(d['channel_id'])
        elif rule_type == RULE_WHITELIST:
            for d in data:
                if ('require_type' not in d) or d['require_type'] != field_name:
                    continue
                bw_list.add(d['channel_id'])
        else:
            raise ValueError

        return bw_list

    def read_ai_black_white_rules(self, cid, rule_id=None, logger_level=20, timeout=30, max_retry=10):
        url = self.idash_cfg["host"] + "/read/ai_black_white_rules?cid={}&enabled=true".format(cid)
        if rule_id:
            url += '&rule_id={}'.format(rule_id)
        headers = {'content-type': 'application/json'}
        r = self._request_get(url, timeout, max_retry, headers=headers)
        data = r.json()['data']
        logger.log(logger_level, '[%s] succeed, result of /read/ai_black_white_rules: %s', cid, data)
        return data

    def read_cid_black_white_rules(self, cid, created_before=None, timeout=120, max_retry=10):
        """Reads cid black/white lists by rule_id
        :param cid:
        :param created_before: <int> only returns rules which are created before given timestamp
        :return black_white_rules: dict of
            ['reject_publisher'/'reject_sub_publisher'/'require_publisher'/'require_sub_publisher'][rule_id <string>]
            to set of app_id
        """
        url = self.idash_cfg['host'] + '/v2/get/blacklist_whitelist/{}'.format(cid)
        headers = {'content-type': 'application/json'}
        r = self._request_get(url, timeout, max_retry, headers=headers)

        result = r.json()
        if not result.get('success', False):
            raise RuntimeError('[{}] failed, result of /v2/get/blacklist_whitelist: {}'.format(cid, result))

        data = result['result']
        black_white_rules = {}
        for field in ('reject_publisher', 'reject_sub_publisher', 'require_publisher', 'require_sub_publisher'):
            black_white_rules[field] = {}
            for inventory in data[field]:
                for rule in inventory['rules']:
                    # FIXME: column `rule_id` and `created_at` has different types between old and new type
                    # rules, old one's rule_id is int and created_at is string while the new one's rule_id is
                    # string and created_at is int timestamp
                    # read both type of rule_ids
                    create_at = rule['create_at']
                    if not isinstance(rule['create_at'], int):
                        try:
                            create_at = calendar.timegm(
                                datetime.datetime.strptime(rule['create_at'], '%Y-%m-%d %H:%M:%S').timetuple())
                        except:
                            logger.error('skip rule %s since unknown create_at column', rule)
                            continue

                    if created_before is None or create_at <= created_before:
                        if rule['rule_id'] not in black_white_rules[field]:
                            black_white_rules[field][rule['rule_id']] = set()
                        black_white_rules[field][rule['rule_id']].add(inventory['channel_id'])
        return black_white_rules

    def batch_read_cid_black_white_rules(self, cid_list, created_before=None, timeout=120, max_retry=10):
        """Batch reads cid black/white lists by rule_id
        :param cid_list:
        :param created_before: <int> only returns rules which are created before given timestamp
        :return cid2black_white_rules: dict of cid to dict of
            ['reject_publisher'/'reject_sub_publisher'/'require_publisher'/'require_sub_publisher'][rule_id <string>]
            to set of app_id
        """
        url = self.idash_cfg['host'] + '/v3/batch_blacklist_whitelist'
        query = {'cids': cid_list, 'formatted': True, 'merged': True}
        headers = {'content-type': 'application/json'}
        r = self._request_post(url, timeout, max_retry, json=query, headers=headers)

        result = r.json()
        if not result.get('success', False):
            raise RuntimeError('[{}] failed, result of /v3/batch_blacklist_whitelist: {}'.format(cid_list, result))

        # - success<boolean>
        # - result<list of dict> - cid<String>
        #                        - field<list of dict> - channel_id<String>
        #                                              - rules<list of dict> - reason<String>
        #                                                                    - rule_id<String>
        #                                                                    - create_at/created_at<int>
        #                                                                    - recoverable<boolean>
        cid2black_white_rules = {}
        for cid_data in result['result']:
            black_white_rules = cid2black_white_rules.setdefault(cid_data['cid'], {})
            for field in ('reject_publisher', 'reject_sub_publisher', 'require_publisher', 'require_sub_publisher'):
                black_white_rules[field] = {}
                for inventory in cid_data[field]:
                    for rule in inventory['rules']:
                        # FIXME: column `rule_id` and `created_at` has different types between old and new type
                        # rules, old one's rule_id is int and created_at is string while the new one's rule_id is
                        # string and created_at is int timestamp
                        # read both type of rule_ids
                        create_at = rule.get('create_at') or rule['created_at']
                        if not isinstance(create_at, int):
                            try:
                                create_at = calendar.timegm(
                                    datetime.datetime.strptime(create_at, '%Y-%m-%d %H:%M:%S').timetuple())
                            except:
                                logger.error('skip rule %s since unknown create_at column', rule)
                                continue

                        if created_before is None or create_at <= created_before:
                            black_white_rules[field].setdefault(rule['rule_id'], set()).add(inventory['channel_id'])
        return cid2black_white_rules

    def disable_and_set_final_ai_black_white_rule(self, cid, bw_list, logger_level=20, timeout=300, max_retry=10):
        """
        Write separated fields instead of CM's manual fields
        Give final black/white setting object under `cid`
        :param cid: bw_list: list of dict contains each inventory's setting.
        e.g. [{
            "channel_id": "test publisher",
            "reject_type": "reject_publisher"
        },
        {
            "channel_id": "test publisher",
            "require_type": "require_sub_publisher"
        }]
        :return: True if succeed else False
        """
        url = self.idash_cfg["host"] + "/v2/disable_and_set/final_ai_black_white_rule/{cid}".format(cid=cid)
        headers = {'content-type': 'application/json'}
        logger.log(logger_level, '[%s] trying to /v2/disable_and_set/final_ai_black_white_rule: %s', cid, bw_list)
        _ = self._request_post(url, timeout, max_retry, data=json.dumps(bw_list), headers=headers)
        return True

    def save_optimizer_ai_rule(self, cid, config, timeout=30, max_retry=10):
        url = self.idash_cfg["host"] + "/save/optimizer_ai_rule/%s" % cid
        config.update({"_": int(time.time())})
        _ = self._request_post(url, timeout, max_retry, data=self.stringify_dict(config))
        return True

    def read_optimizer_fraud_rule(self, cid_list, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url_tpl = self.idash_cfg["host"] + "/read/optimizer_ai_fraud_rule/cid?cids=[{cid_list}]"

        cid_fraud_dict = {}
        for i in range(0, len(cid_list), 40):
            cid_list_str = ",".join(["\"%s\"" % cid for cid in cid_list[i:i + 40]])
            url = url_tpl.format(cid_list=cid_list_str)
            r = self._request_get(url, timeout, max_retry, params=params)
            res = r.json().get('data', [])
            for c in res:
                cid_fraud_dict[c['cid']] = c
        return cid_fraud_dict

    def read_optimizer_ai_rule(self, oid=None, cid_list=None, enabled=True, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/read/optimizer_ai_rule?enabled=%s" % enabled

        if oid:
            url += "&oid=%s" % (oid)
        if cid_list and len(cid_list) > 0:
            cid_list_str = ",".join(["\"%s\"" % cid for cid in cid_list])
            url += "&cids=[%s]" % cid_list_str

        r = self._request_get(url, timeout, max_retry, params=params)
        return r.json().get('data', [])

    def read_robot_config(self, timeout=30, max_retry=10):
        """ read crawler partners """
        url = "{}/read/robot_config".format(self.idash_cfg["host"])
        params = {
            "_": int(time.time())  # cache buster
        }
        r = self._request_get(url, timeout, max_retry, params=params)
        return r.json()

    def read_crawler_task(self, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.idash_cfg["host"] + "/read/crawler_tasks"
        r = self._request_get(url, timeout, max_retry, params=params)
        mapping = r.json()
        return mapping[0] if len(mapping) > 0 else {}

    def get_api_json_array(self, query_string):
        return self.get_endpoint_json_array(self.idash_cfg["host"] + query_string)

    def get_endpoint_json_array(self, endpoint, timeout=30, max_retry=10):
        params = {
            "_": int(time.time())  # cache buster
        }
        r = self._request_get(endpoint, timeout, max_retry, params=params)
        return r.json()
