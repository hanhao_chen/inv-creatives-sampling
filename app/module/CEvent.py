import time

from app.module.Const import CONST_INF, CONST_UNDEF, FORMAT_RANGE_RETENTION, FORMAT_RETENTION
from app.utils.init_logger import init_logger

logger = init_logger(__name__)


class CEvent:
    """
    Store count and metrics value which belong to a event_id

    :param event_id: string, event_id
    :param value_dict: dictionary, key: metric, value: a list of (timestamp, value) tuple
    :param event_ptr: a set used to record all inventory of a cid; object: a tuple of strings from inventory dimensions

    """

    def __init__(self, event_id):

        self.event_id = event_id
        self.value_dict = {}
        self.event_ptr = {}

    def update_value_dict(self, val_dict):

        for f in val_dict:
            if f not in self.value_dict:
                self.value_dict[f] = []  # (ts, value) tuple

            self.value_dict[f].extend(val_dict[f])

    def update_event(self, e):

        if self.event_id != e.event_id:
            return

        self.update_value_dict(e.value_dict)

    def get_cost_event(self):

        logger.info('ptr: %s', self.event_ptr)

        if self.event_id in ['cost', 'shows', 'clicks', 'actions']:
            key = 'cost'
        else:
            key = 'raw_cost'

        return self.event_ptr.get(key, CEvent(key))

    def get_cvr_base_event(self):

        if self.event_id in ['clicks']:
            key = 'shows'
        else:
            key = 'clicks'

        return key

    def sum(self, field, timestamp=time.time(), decay=1.0, invalid_days_from_now=0):

        now = time.time()
        end_ts = int(now - now % 86400 - invalid_days_from_now * 86400) if invalid_days_from_now > 0 else now

        return sum(
            [c['val'] * pow(decay, int((timestamp - c['time']) / 86400)) for c in self.value_dict.get(field, []) if int(c['time']) < end_ts])

    def func(self, field1, field2, event_list, timestamp=time.time(), decay=1.0):

        val_dict = {
            field1: self.sum(field1, timestamp, decay),
            field2: sum([e.sum(field2, timestamp, decay) for e in event_list])
        }
        res = CEvent.f_a(val_dict[field1], val_dict[field2])

        logger.info('%s %s', field1, val_dict[field1])
        logger.info('%s %s', field2, val_dict[field2])

        return res

    @staticmethod
    def f_a(term1, term2):

        if term2 > 0:
            res = term1 / term2
        else:
            res = CONST_UNDEF

        return res

    @staticmethod
    def conv_metric_format(metric):
        return metric.replace('-', '_').lower()


class CEventList:
    @staticmethod
    def sum(event_list, field, timestamp=time.time(), decay=1.0, invalid_days_from_now=0):

        val = 0.0
        now = time.time()
        end_ts = int(now - now%86400 - invalid_days_from_now * 86400) if invalid_days_from_now > 0 else now

        for e in event_list:
            val += sum([float(c['val']) * pow(decay, int((timestamp - c['time']) / 86400)) for c in
                        e.value_dict.get(field, []) if int(c['time']) < end_ts])

        return val

    @staticmethod
    def func(event_list, event_field, major_event_list, major_event_field, event_weight=1.0, major_event_weight=1.0,
             timestamp=time.time(), decay=1.0, invalid_days_from_now=0):

        val_dict = {
            'event': CEventList.sum(event_list, event_field, timestamp, decay, invalid_days_from_now),
            'major': sum([e.sum(major_event_field, timestamp, decay, invalid_days_from_now) for e in major_event_list])
        }
        res = CEvent.f_a(val_dict['event'] * event_weight, val_dict['major'] * major_event_weight)

        return res, val_dict['event'] * event_weight, val_dict['major'] * major_event_weight

    '''
    cpa 
    '''

    @staticmethod
    def cpa(event_list, cinventory, timestamp=time.time(), decay=1.0):

        count = CEventList.sum(event_list, 'count', timestamp, decay)

        c_event = [cinventory.get_event('cost')]
        cost = CEventList.sum(c_event, 'count', timestamp, decay)

        if count > 0:
            cpa = cost / count
        elif cost > 0:
            cpa = CONST_INF
        else:
            cpa = CONST_UNDEF

        return cpa, cost, count

    @staticmethod
    def smooth_cpa(event_list, major_event_list, cinventory, revenue_cpx=1.0, smooth_term=1.0, timestamp=time.time(),
                   decay=1.0):

        count = CEventList.sum(event_list, 'count', timestamp, decay)

        c_event = [cinventory.get_event(f) for f in major_event_list]
        revenue = CEventList.sum(c_event, 'count', timestamp, decay) * revenue_cpx

        if count > 0 or revenue > 0:
            cpa = (revenue + smooth_term) / (count + 1)
        else:
            cpa = CONST_UNDEF

        return cpa, (revenue + smooth_term), (count + 1)

    @staticmethod
    def cpm(event_list, cinventory, timestamp=time.time(), decay=1.0):

        cpa, cost, count = CEventList.cpa(event_list, cinventory, timestamp, decay)
        if cpa not in [CONST_INF, CONST_UNDEF]:
            cpa *= 1000.0
        return cpa, cost, count

    @staticmethod
    def cvr(event_list, cinventory, timestamp=time.time(), decay=1.0):

        base_key_list = [e.get_cvr_base_event() for e in event_list]
        base_key = base_key_list[0] if len(base_key_list) > 0 else ''
        base_event = [cinventory.get_event(base_key)]

        count = CEventList.sum(event_list, 'count', timestamp, decay)
        base = CEventList.sum(base_event, 'count', timestamp, decay)

        if base > 0:
            cvr = count / base
        elif count > 0:
            cvr = 1.0
        else:
            cvr = CONST_UNDEF

        return cvr, count, base

    '''
    rr_nd
    '''

    @staticmethod
    def rr_1d(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.rr_nd(event_list, 1, major_event_list, timestamp=timestamp, decay=decay)

    @staticmethod
    def rr_3d(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.rr_nd(event_list, 3, major_event_list, timestamp=timestamp, decay=decay)

    @staticmethod
    def rr_7d(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.rr_nd(event_list, 7, major_event_list, timestamp=timestamp, decay=decay)

    @staticmethod
    def rr_nd(event_list, days, major_event_list, timestamp=time.time(), decay=1.0):

        ev_field = FORMAT_RETENTION['event'].format(days=days)
        ma_field = FORMAT_RETENTION['major']
        return CEventList.func(event_list, ev_field, major_event_list, ma_field, timestamp=timestamp, decay=decay, invalid_days_from_now=days)

    '''
    prat
    '''

    @staticmethod
    def prat(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.func(event_list, 'count', major_event_list, 'count', timestamp=timestamp, decay=decay)

    '''
    prat_nd
    '''

    @staticmethod
    def prat_1d(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.prat_nd(event_list, 1, major_event_list, timestamp=timestamp, decay=decay)

    @staticmethod
    def prat_3d(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.prat_nd(event_list, 3, major_event_list, timestamp=timestamp, decay=decay)

    @staticmethod
    def prat_7d(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.prat_nd(event_list, 7, major_event_list, timestamp=timestamp, decay=decay)

    @staticmethod
    def prat_14d(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.prat_nd(event_list, 14, major_event_list, timestamp=timestamp, decay=decay)

    @staticmethod
    def prat_21d(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.prat_nd(event_list, 21, major_event_list, timestamp=timestamp, decay=decay)

    @staticmethod
    def prat_30d(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.prat_nd(event_list, 30, major_event_list, timestamp=timestamp, decay=decay)

    @staticmethod
    def prat_nd(event_list, days, major_event_list, timestamp=time.time(), decay=1.0):

        ev_field = FORMAT_RANGE_RETENTION['prat'].format(days=days)
        ma_field = 'count'

        return CEventList.func(event_list, ev_field, major_event_list, ma_field, timestamp=timestamp, decay=decay)

    '''
    roas
    '''

    @staticmethod
    def roas(event_list, major_event_list, goalcpa=1.0, timestamp=time.time(), decay=1.0):
        return CEventList.func(event_list, 'p_amount', major_event_list, 'count', major_event_weight=goalcpa,
                               timestamp=timestamp, decay=decay)

    '''
    roaa
    '''

    @staticmethod
    def roaa(event_list, major_event_list, timestamp=time.time(), decay=1.0):
        return CEventList.func(event_list, 'p_amount', major_event_list, 'count', timestamp=timestamp, decay=decay)

    '''
    roas_nd
    '''

    @staticmethod
    def roas_1d(event_list, major_event_list, goalcpa=1.0, timestamp=time.time(), decay=1.0):
        return CEventList.roas_nd(event_list, 1, major_event_list, goalcpa=goalcpa, timestamp=timestamp, decay=decay)

    @staticmethod
    def roas_3d(event_list, major_event_list, goalcpa=1.0, timestamp=time.time(), decay=1.0):
        return CEventList.roas_nd(event_list, 3, major_event_list, goalcpa=goalcpa, timestamp=timestamp, decay=decay)

    @staticmethod
    def roas_7d(event_list, major_event_list, goalcpa=1.0, timestamp=time.time(), decay=1.0):
        return CEventList.roas_nd(event_list, 7, major_event_list, goalcpa=goalcpa, timestamp=timestamp, decay=decay)

    @staticmethod
    def roas_14d(event_list, major_event_list, goalcpa=1.0, timestamp=time.time(), decay=1.0):
        return CEventList.roas_nd(event_list, 14, major_event_list, goalcpa=goalcpa, timestamp=timestamp, decay=decay)

    @staticmethod
    def roas_21d(event_list, major_event_list, goalcpa=1.0, timestamp=time.time(), decay=1.0):
        return CEventList.roas_nd(event_list, 21, major_event_list, goalcpa=goalcpa, timestamp=timestamp, decay=decay)

    @staticmethod
    def roas_30d(event_list, major_event_list, goalcpa=1.0, timestamp=time.time(), decay=1.0):
        return CEventList.roas_nd(event_list, 30, major_event_list, goalcpa=goalcpa, timestamp=timestamp, decay=decay)

    @staticmethod
    def roas_nd(event_list, days, major_event_list, goalcpa=1.0, timestamp=time.time(), decay=1.0):

        ev_field = FORMAT_RANGE_RETENTION['roas'].format(days=days)
        ma_field = 'count'

        return CEventList.func(event_list, ev_field, major_event_list, ma_field, major_event_weight=goalcpa,
                               timestamp=timestamp, decay=decay)
