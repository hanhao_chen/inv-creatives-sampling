import time
import requests
from app.utils.init_logger import init_logger

logger = init_logger(__name__)

class CEndpointLogService:
    def __init__(self, logservice_cfg):
        self.logservice_cfg = logservice_cfg
        self._update_time = None
        self.cookies = None
        self.login_campaign_data_service()

    def get_prd_id(self, os, bundle_ids, timeout=30, max_retry=10):
        if os == 'ios':
            market = 'ios'
        else:
            market = 'google-play'

        params = {
            "_": int(time.time())  # cache buster
        }
        url = self.logservice_cfg["host"] + \
        """/do/visibility_app_external_data/get_appannie_product_id?os={market}&bundle_ids=%5B{bundle_ids}%5D""".format(
            market=market,
            bundle_ids=",".join(['"{}"'.format(bundle_id) for bundle_id in bundle_ids])
        )
        response = self._request_get(url, timeout, max_retry, params=params)

        return response.json()

    def check_cookie_expired(self):
        ts_now = int(time.time())
        logger.debug('[logservice_cfg] check timestamp %s %s', self._update_time, ts_now)
        if ts_now - self._update_time > 3600 * 6:
            logger.debug('[logservice_cfg] cookie expired, re-login')
            self.login_campaign_data_service()

    def _request_get(self, url, timeout=10, max_retry=3, params=None, headers=None, backoff_multiplier=1.4):
        """
        get requests with backoff retry strategy
        :param url: request url
        :param timeout: integer type, set default to 10 seconds
        :param max_retry: integer type, set default to 3 times
        :param params: same as params in requests.get()
        :param headers: same as headers in requests.get()
        :param backoff_multiplier: float/integer type, A multiplier which is used to determine sleep seconds
        for every retry attempt. set default value to 1.4
        :return: response object
        """
        self.check_cookie_expired()
        sleep_seconds = 1
        for retry in range(max_retry):
            sleep_seconds = sleep_seconds * backoff_multiplier
            try:
                r = requests.get(url, params=params, cookies=self.cookies, timeout=timeout, headers=headers)
            except requests.exceptions.ReadTimeout as e:
                logger.warning("Endpoint Timeout: %s, retry: %s", url, retry)
                if retry == max_retry - 1:
                    raise e
                time.sleep(sleep_seconds)
                continue
            if r.status_code == 200:
                return r
            if r.status_code in range(400, 500):
                r.raise_for_status()
            # retry for 5xx status
            logger.warning("Endpoint: %s, status_code: %s", url, r.status_code)
            if retry == max_retry - 1:
                r.raise_for_status()
            time.sleep(sleep_seconds)
        return r

    def _request_post(self, url, timeout=10, max_retry=3, data=None, json=None, headers=None, backoff_multiplier=1.4):
        """
        post request with backoff retry strategy
        :param url: request url
        :param timeout: integer type, set default to 10 seconds
        :param max_retry: integer type, set default to 3 times
        :param data: same as data in requests.post()
        :param headers: same as headers in requests.post()
        :param backoff_multiplier: float/integer type, A multiplier which is used to determine sleep seconds
        for every retry attempt. set default value to 1.4
        :return: response object
        """
        # The login request doesnt need to provide cookies.
        if self._update_time:
            self.check_cookie_expired()
        sleep_seconds = 1
        for retry in range(max_retry):
            sleep_seconds = sleep_seconds * backoff_multiplier
            try:
                r = requests.post(url, data=data, json=json, cookies=self.cookies, timeout=timeout, headers=headers)
            except requests.exceptions.ReadTimeout as e:
                logger.warning("Endpoint Timeout: %s, retry: %s", url, retry)
                if retry == max_retry - 1:
                    raise e
                time.sleep(sleep_seconds)
                continue
            if r.status_code == 200:
                return r
            if r.status_code in range(400, 500):
                r.raise_for_status()
            # retry for 5xx status
            logger.warning("Endpoint: %s, status_code: %s", url, r.status_code)
            if retry == max_retry - 1:
                r.raise_for_status()
            time.sleep(sleep_seconds)
        return r

    def login_campaign_data_service(self, timeout=30, max_retry=10):
        data = {
            "_": int(time.time()),
            'username': self.logservice_cfg['username'],
            'password': self.logservice_cfg['password']
        }
        url = self.logservice_cfg["host"] + '/login'
        headers = {'Connection': 'close'}
        r = self._request_post(url, timeout, max_retry, data=data, headers=headers)
        self.cookies = r.cookies
        self._update_time = int(time.time())
        return True