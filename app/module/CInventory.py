import time

from app.module.CEvent import CEvent, CEventList
from app.module.Const import CONST_DOLLAR, APP_KEY_DICT


class CInventory:
    """
    Store metrics which belong to this dimension (key)

    :param key: a dictionary, key: dimension name like (app_id, imp_size_group, exchange, ...)
    :param event_dict: a dictionary, key: event_id, value: CEvent Object
    """
    KEY_FIELDS = ['app_id', 'imp_size_group', 'exchange', 'app_type', 'tagid', 'deal_id', 'device_type',
                  'partner_id', 'imp_type', 'imp_is_instl', 'imp_height', 'imp_width']

    BASE_DICT = {'clicks': 'shows', 'actions': 'clicks'}

    def __init__(self, key_dict):

        self.key = {}
        self.key.update({f: key_dict[f] for f in key_dict if f in CInventory.KEY_FIELDS})

        self.event_dict = {}

    def get_event(self, event_id):

        return self.event_dict.get(event_id, CEvent(event_id))

    def update_inventory(self, inventory):

        self.update_event_dict(inventory.event_dict)

    def update_event_dict(self, event_dict):

        for event_id in event_dict:
            if event_id not in self.event_dict:
                self.event_dict[event_id] = CEvent(event_id)
            self.event_dict[event_id].update_event(event_dict[event_id])

    def event_sum(self, event_id, field='count', timestamp=time.time(), decay=1.0):
        # print([self.event_dict[eid].value_dict for eid in self.event_dict if eid == event_id])
        return CEventList.sum([self.event_dict[eid] for eid in self.event_dict if eid == event_id], field, timestamp,
                              decay)

    def compute_metric_val(self, ev_minor_list, ev_major_list, metric, goalcpa, decay=1.0):

        event_dict = self.event_dict
        cev_major_list = [event_dict.get(e, CEvent('')) for e in ev_major_list]
        cev_minor_list = [event_dict.get(e, CEvent('')) for e in ev_minor_list]

        if metric.startswith('roas'):
            ratio, numerator, denominator = getattr(CEventList, metric)(cev_minor_list, cev_major_list, goalcpa=goalcpa,
                                                                        decay=decay)
        elif metric in ['cpa', 'cpm', 'cvr']:
            ratio, numerator, denominator = getattr(CEventList, metric)(cev_minor_list, self, decay=decay)
        else:
            ratio, numerator, denominator = getattr(CEventList, metric)(cev_minor_list, cev_major_list, decay=decay)

        return ratio, numerator, denominator

    '''
    compute full metric for each minor event in this inventory
    return each metric ratio, numerator, denominator
    '''

    def compute_full_metric(self, ev_minor_list, ev_major_list, metric_list, goalcpa, decay=1.0):

        res_dict = {}
        for event_id in ev_minor_list:
            res_dict[event_id] = {}
            for metric in metric_list:
                metric_key = CEvent.conv_metric_format(metric)
                ratio, numerator, denominator = self.compute_metric_val([event_id], ev_major_list, metric_key, goalcpa,
                                                                        decay)
                res_dict[event_id][metric] = (ratio, numerator, denominator)

            res_dict[event_id]['count'] = self.event_sum(event_id)
            res_dict[event_id]['p_amount'] = self.event_sum(event_id, 'p_amount')

        return res_dict

    @staticmethod
    def merge_cinv_list(cinv_list):

        new_cinv = CInventory({'app_id': 'merged'})
        for cinv in cinv_list:
            new_cinv.update_inventory(cinv)

        return new_cinv

    @staticmethod
    def parse_perday_data(data, is_rtb):

        inv_dict = {}
        for c in data:

            # Note: hot fix of deal id problem
            parsed_inv = [CInventory.gen_inv_key(c)]
            if is_rtb:
                trim_app_id = c['app_id'].split(':')[0]
                parsed_inv.append(CInventory.gen_inv_key({'app_id': trim_app_id}))

            for inv_key in parsed_inv:
                if inv_key not in inv_dict:
                    inv_dict[inv_key] = CInventory(c)

                ts = c.get('time', 0.0)

                for f in ['cost', 'shows', 'clicks', 'actions']:
                    # val_dict = copy.deepcopy(raw_cost)
                    val_dict = {}
                    count = float(c.get(f, 0.0))
                    bkey = CInventory.BASE_DICT.get(f, '')
                    bval = float(c.get(bkey, 0.0))
                    if is_rtb and f in ['cost']:
                        count /= CONST_DOLLAR

                    val_dict.update({'count': [{'time': ts, 'val': count}]})
                    val_dict.update({'base': [{'time': ts, 'val': bval}]})

                    if len(val_dict) > 0:
                        event_id = f
                        event = CEvent(event_id)
                        event.update_value_dict(val_dict)
                        inv_dict[inv_key].update_event_dict({event_id: event})

                        # if inv_key[0] == '265120_9205_707':
                        #    print inv_key, event_id, val_dict

        return inv_dict

    @staticmethod
    def parse_event_data(data):

        inv_dict = {}
        for c in data:
            inv_key = CInventory.gen_inv_key(c)
            if inv_key not in inv_dict:
                inv_dict[inv_key] = CInventory(c)

            metric = c.get('metric', '')
            event_id = 'raw_cost' if metric in ['raw_cost'] else c.get('event_id', '')
            ts = c.get('time', 0.0)
            val = c.get('value', 0.0)

            event = CEvent(event_id)
            event.update_value_dict({metric: [{'time': ts, 'val': val}]})
            inv_dict[inv_key].update_event_dict({event_id: event})

        return inv_dict

    @staticmethod
    def assign_event_ptr(event_dict, ptr_list=('cost', 'raw_cost', 'shows', 'clicks')):

        for eid in event_dict:
            for ptr in ptr_list:
                event_dict[eid].event_ptr[ptr] = event_dict.get(ptr, CEvent(ptr))

    @staticmethod
    def format_inv_key(key_dict, algorithm):

        fields = CInventory.gen_inv_fields(algorithm)
        inv_key = tuple([key_dict.get(f, '') for f in fields])

        return inv_key

    # key for CInventory
    @staticmethod
    def gen_inv_key(key_dict):

        inv_key = tuple([key_dict.get(f, '') for f in CInventory.KEY_FIELDS])

        return inv_key

    # key dict by algorithm
    @staticmethod
    def gen_key_dict(inv_key, algorithm):

        key_dict = CInventory.decompose_inv_key(inv_key)
        algo_key_dict = CInventory.filter_key_dict(key_dict, algorithm)

        return algo_key_dict

    @staticmethod
    def decompose_inv_key(inv_key):

        key_dict = {v: inv_key[i] for i, v in enumerate(CInventory.KEY_FIELDS)}

        return key_dict

    @staticmethod
    def filter_key_dict(key_dict, algorithm):

        fields = CInventory.gen_inv_fields(algorithm)

        algo_key_dict = {f: key_dict[f] for f in fields}

        return algo_key_dict

    @staticmethod
    def gen_inv_fields(algorithm):

        return APP_KEY_DICT.get(algorithm, ['app_id'])
