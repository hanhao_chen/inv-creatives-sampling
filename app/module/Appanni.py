from __future__ import absolute_import, division
import argparse
import csv
import json
import logging
import os
import random
import re
import requests
import time
import arrow

from app.config import V1Config
from app.utils import get_env
from app.module.CEndpointLogService import CEndpointLogService
from app.utils.init_logger import init_logger

logger = init_logger(__name__)

APP_ANNIE_HEADERS = {
    'Authorization': 'Bearer 9b85e515513ce1f30833a763a7288ca81875a214',
    # 'Authorization': 'Bearer 6c493a72479849172eee0ee26d789d89fa299e69',
    'Accept': 'application/json',
}
APP_ANNIE_PREFIX = 'https://api.appannie.com'

def get_product_info(os, product_id_list, interval=2):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'google-play'

    query = '/v1.3/apps/{market}/app/bulk_details?product_ids={product_ids}'.format(
        market=market,
        product_ids='+'.join(product_id_list)
    )
    reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
    if reply['code'] == 200:
        results = reply['products']
    else:
        results = {}
        logger.info('AppAnnie Query: %s fail!!!!!!', query)
    logger.info('AppAnnie Query: %s', query)
    time.sleep(interval + random.random())

    return results


def get_product_id(os, bundles, interval=2):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'google-play'

    bundle2pid = {}
    for bundle in bundles:
        query = '/v1.3/apps/{market}/package-codes2ids?package_codes={package_codes}'.format(
            market=market,
            package_codes=bundle
        )
        reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
        if reply['code'] == 200:
            for x in reply['items']:
                if x['product_id']:
                    bundle2pid[bundle] = str(x['product_id'])
        else:
            continue
        time.sleep(interval + random.random())
    return bundle2pid

def get_top_rank(os, countries, category_path):
    if os == 'ios':
        market = 'ios'
        device = 'iphone'
    else:
        market = 'all-android'
        device = 'android_phone'
    query = '/v1.3/intelligence/apps/{market}/usage-ranking?countries={country}&categories={category}&device={device}&ranks={ranks}'.format(
        market=market,
        country=countries,
        category=category_path,
        ranks=200,
        device=device
    )
    logger.info(query)
    reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
    return reply


def get_review(os, product_id_list, countries, interval=2):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'google-play'
    end_date = arrow.utcnow().shift().format('YYYY-MM-DD')
    start_date = arrow.utcnow().shift(days=-30).format('YYYY-MM-DD')

    logger.info('AppAnnie Query: %d product_id2review.', len(product_id_list))
    result = {}
    for product_id in product_id_list:
        query = '/v1.3/apps/{market}/app/{product_id}/reviews?start_date={start_date}&end_date={end_date}&countries={countries}'.format(
            market=market,
            product_id=product_id,
            start_date=start_date,
            end_date=end_date,
            countries=countries.upper()
        )
        while query:
            logger.info(query)
            reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
            if reply['code'] == 200:
                result.setdefault(product_id, []).extend(reply['reviews'])
                query = reply['next_page']
            else:
                logger.error('Query: %s', query)
                logger.error('Reply: %s', reply)
                query = None
            time.sleep(interval + random.random())
    return result


def get_rank_history(os, product_id_list, countries, interval=2):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'google-play'
    end_date = arrow.utcnow().shift().format('YYYY-MM-DD')
    start_date = arrow.utcnow().shift(days=-7).format('YYYY-MM-DD')

    logger.info('AppAnnie Query: %d product_id2rank_history.', len(product_id_list))
    result = {}
    for product_id in product_id_list:
        query = '/v1.3/apps/{market}/app/{product_id}/ranks?start_date={start_date}&end_date={end_date}&countries={countries}'.format(
            market=market,
            product_id=product_id,
            start_date=start_date,
            end_date=end_date,
            countries=countries.upper()
        )
        while query:
            logger.info(query)
            reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
            if reply['code'] == 200:
                result.setdefault(product_id, []).extend(reply['product_ranks'])
                query = reply['next_page']
            else:
                logger.error('Query: %s', query)
                logger.error('Reply: %s', reply)
                query = None
            time.sleep(interval + random.random())
    return result

def get_usage_app_history(os, product_id_list, country, granularity='monthly', interval=2):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'all-android'

    logger.info('AppAnnie Query: %d product_id2usage_app_history.', len(product_id_list))
    result = {}
    for product_id in product_id_list:
        query = '/v1.3/intelligence/apps/{market}/app/{product_id}/usage-history?countries={country}&device=all&granularity={granularity}'.format(
            market=market,
            product_id=product_id,
            country=country.upper(),
            granularity=granularity
        )
        logger.info(query)
        reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
        if reply['code'] == 200:
            result[product_id] = reply['list']
        else:
            logger.error('Query: %s', query)
            logger.error('Reply: %s', reply)

        time.sleep(interval + random.random())
    return result

def get_app_detail(os, product_id):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'google-play'
    query = '/v1.3/apps/{market}/app/{product_id}/details'.format(
        market=market,
        product_id=product_id
    )
    logger.info(query)
    reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
    return reply

def get_app_details(os, product_id_list, col='bundle_id', interval=2):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'google-play'
    bundles_mp = {}
    for product_id in product_id_list:
        query = '/v1.3/apps/{market}/app/{product_id}/details'.format(
            market=market,
            product_id=product_id
        )
        logger.info(query)
        reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
        if reply['code'] == 200:
            bundles_mp[product_id] = reply['product'][col]
        else:
            logger.error('Query: %s', query)
            logger.error('Reply: %s', reply)
        time.sleep(interval + random.random())
    return bundles_mp

def get_appannie(os, bundle_ids, interval=2):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'google-play'

    query = '/v1.3/apps/{}/package-codes2ids?package_codes={}'.format(
        market,
        ','.join(bundle_ids))
    reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
    return reply

def get_rating(os, product_id_list, interval=2):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'google-play'

    logger.info('AppAnnie Query: %d product_id2rating.', len(product_id_list))
    result = {}
    for product_id in product_id_list:
        query = '/v1.3/apps/{}/app/{}/ratings'.format(market, product_id)
        while query:
            logger.info(query)
            reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
            if reply['code'] == 200:
                result.setdefault(product_id, []).extend(reply['ratings'])
                query = reply['next_page']
            else:
                logger.error('Query: %s', query)
                logger.error('Reply: %s', reply)
                query = None
            time.sleep(interval + random.random())

    return result

def get_categorise(os):
    if os == 'ios':
        market = 'ios'
    else:
        market = 'google-play'

    query = '/v1.3/meta/apps/{market}/categories'.format(market=market)
    reply = requests.get(APP_ANNIE_PREFIX + query, headers=APP_ANNIE_HEADERS).json()
    return reply

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--config', default='app_config.json', type=str,
                        help='You may need to specify this argument to conduct list_control experiments.')
    parser.add_argument('--credential_file', type=str, required=False, default='credential.json')
    args = parser.parse_args()

    cfg = V1Config(get_env(), config_file=args.config, credential_file=args.credential_file)

    logservice_endpoint = CEndpointLogService(cfg.logservice_cfg)
    # prd_ids = logservice_endpoint.get_prd_id("android", ["me.timecash", "io.comup.chickenangle"])
    # rating = get_rating("android", prd_ids.values())
    game_prdid = [20600003045821,
                20600006273759,
                20600009069560,
                20600001526240,
                20600003973014,
                20600007103920,
                20600006367630,
                20600001167325,
                20600009144131,
                20600003607489,
                20600009268870,
                20600007446410,
                20600006417894,
                20600005873203,
                20600009305838,
                20600011130825,
                20600010468485,
                20600010521890]
    app_prdid = [20600004469898,
                20600006608695,
                20600000337565,
                20600000060490,
                20600003409228,
                20600000003054,
                20600002975206,
                20600000181152,
                20600000054647,
                20600000107599,
                20600003216940,
                20600000054372,
                20600000697106,
                20600003216512,
                20600005049565,
                20600000004195,
                20600000006447,
                20600005457241,
                20600004586904,
                20600006184807,
                20600005883880,
                20600007271090,
                20600000307626,
                20600000105500,
                20600000230267,
                20600003127177]
    # game_bundle = get_app_details("android", game_prdid)
    # app_bundle = get_app_details("android", app_prdid)
    # test = get_app_detail("android", 20600009096340)

    # pid = get_product_id('ios', ['895761422', '1507132028'])
    # get_product_id('google-play', ['kr.wedrive.cash'])
    # get_product_id('ios', ['949344041'])

    test = get_product_info('ios', ['302324249'])
    # rank = get_top_rank("android", "KR", "Overall")
    # usage = get_usage_app_history("android", prd_ids.values(), 'KR')
    # rank = get_rank_history("android", prd_ids.values(), 'KR')
    # review = get_review("android", prd_ids.values(), 'KR')
    import pdb; pdb.set_trace()

if __name__ == "__main__":
    main()