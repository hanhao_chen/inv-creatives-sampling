from app.utils.db import MySQL_DB_AIBE_DEV, MySQL_DB_CAMPAIGN, MySQL_DB_RTB_OPT, MySQL_RTB_DB_PRD

class CQueryDevDB:
    def __init__(self, query_db, listctrl_cfg):
        self.query_db = query_db
        self.df_rtb_opt_client = query_db.db_clients[MySQL_DB_RTB_OPT]
        # self.df_db_prd_client = query_db.db_clients[MySQL_RTB_DB_PRD]
        self.df_db_client = query_db.db_clients[MySQL_DB_AIBE_DEV]
        self.campaign_db_client = query_db.db_clients[MySQL_DB_CAMPAIGN]
        self.inv_table = listctrl_cfg['inv_sampling_table']
        self.inv_complete_table = listctrl_cfg['inv_complete_table']
        self.country_oid_rt_uu_table = listctrl_cfg['country_oid_rt_uu_table']
        self.order_info = listctrl_cfg['order_info_table']
        self.campaign = listctrl_cfg['campaign_table']
        self.creative_table = listctrl_cfg['creative_table']
        self.creative_spec_table = listctrl_cfg['creative_spec_table']
        self.goal_value_table = listctrl_cfg['goal_adjust_table']
        self.beethoven_perday_table = listctrl_cfg['beethoven_perday_table']
        self.beethoven_adjust_table = listctrl_cfg['beethoven_adjust_table']
        self.agg_oid_perf = listctrl_cfg['oid_cum_performance_table']
        self.cid_sample_perf = listctrl_cfg['cid_daily_performance_table']
        self.impbid_ctcv_table = listctrl_cfg['impbid_ctcv_table']
        self.oid_cost_hf_table = listctrl_cfg['oid_cost_hf_table']