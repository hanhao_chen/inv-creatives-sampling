from __future__ import print_function
import datetime

import enum
import pytz
from pytz import country_timezones, timezone

# config_file = '/srv/external-optimization/production.ini'
# fraud_config_file = '/srv/external-optimization/fraud_production.ini'

# for auto rule, iDash field value will store one of the following values
RULE_WHITELIST = 'whitelist'
RULE_BLACKLIST = 'blacklist'
RULE_DEEPFUNNEL = 'deepfunnel'

# rule id for ai black white list
RULE_ID_AUTORULE = '4000'
RULE_ID_DEEPFUNNEL = '5000'


class BW_ListType(enum.IntEnum):
    NO_DATA = 0
    NEW_CONFIRM = 1
    PAROLE = 2


APPID_LABEL_CREATIVE = 'CREATIVE'
APPID_LABEL_NATIVE = 'NATIVE'
APPID_LABEL_INTERSTITIAL = 'INTERSTITIAL'
APPID_LABEL_VIDEO = 'VIDEO'
APPID_LABEL_ALL = 'ALL'
CREATIVE_TYPE_SRC2DST = {'banner': 'CREATIVE',
                         'rich text': 'NATIVE',
                         'interstitial': 'INTERSTITIAL',
                         'startapp_video': 'VIDEO'}
NATIVE_VAL_MAP = {APPID_LABEL_CREATIVE: 0,
                  APPID_LABEL_NATIVE: 1,
                  APPID_LABEL_INTERSTITIAL: 2,
                  APPID_LABEL_VIDEO: 3,
                  APPID_LABEL_ALL: 4}
DUMMY_APPID = {'revmob':     '123456789012345678901234',
               'startapp':   'dummy_app_id',
               'appnext':    '999999',
               'rtb':        'dummy_app_id'}
PARM_APPID_NUM_CAP = {'startapp': {'WT': 1000, 'BK': 3000}}

PC_ALGO = ['beethoven', 'fix_price', 'bello', 'zebra', 'alpaca', 'cell']

# The following partners are found in CPI campaigns but no '_' in app_id:
# startapp, heyzap_cpi, sponsorpay, mobair, mobrand, revmob, airpush_dash
AD_NETWORK_CPI = ['appnext',   'minimob',  'supersonic',   'ironsource',   'revmob_cpi',
                  'offerseven', 'personaly', 'tapgerine',    'cygobel',      'allitapp',
                  'appthis',   'datafirst', 'papayamobile', 'startapp_cpi', 'mobrand']
AD_NETWORK_CPC = ['revmob', 'startapp']
AD_NETWORK = ['revmob', 'startapp']
AD_NETWORK.extend(AD_NETWORK_CPI)

ALGO_INV_DIM = {'prefer_deal': ['app_id', 'imp_size_group', 'exchange', 'app_type', 'device_type', 'tagid'],
                'raccoon':     ['app_id', 'imp_size_group', 'exchange', 'app_type', 'device_type', 'tagid'],
                'charlie':     ['app_id', 'imp_size_group', 'exchange', 'app_type', 'tagid'],
                'beethoven':   ['app_id', 'imp_size_group', 'exchange', 'app_type', 'device_type', 'tagid']}

HOST_AGGREGATOR = 'http://bidder-srv.rtb.appier.net:3457/pipeline_depot/optimizer'

TABLE_APPID_PERDAY = {'charlie': 'perday',
                      'prefer_deal': 'prefer_deal_perday',
                      'raccoon': 'prefer_deal_perday'}
TABLE_APPID_PERDAY.update({f: 'beethoven_perday' for f in PC_ALGO})
TABLE_APPID_PERDAY.update({'%s_optimizer' % f: 'ext_perday' for f in AD_NETWORK})

TABLE_APPID_ADJUST = {'charlie': 'adjust',
                      'prefer_deal': 'prefer_deal_adjust',
                      'raccoon': 'prefer_deal_adjust'}
TABLE_APPID_ADJUST.update({f: 'beethoven_adjust' for f in PC_ALGO})

APP_KEY_DICT = {'charlie': ['app_id', 'imp_size_group', 'exchange', 'app_type', 'tagid'],
                'prefer_deal': ['app_id', 'app_type', 'deal_id', 'device_type', 'partner_id', 'imp_type', 'imp_is_instl', 'imp_height', 'imp_width'],
                'raccoon': ['app_id', 'app_type', 'deal_id', 'device_type', 'partner_id', 'imp_type', 'imp_is_instl', 'imp_height', 'imp_width']}
APP_KEY_DICT.update({f: ['app_id', 'imp_size_group', 'exchange', 'app_type', 'device_type', 'tagid'] for f in PC_ALGO})

CONST_BIDPRICE_MODELS = {'CM manual': 'manual',
                         'Fox':       'Fox',
                         'Manatee':   'Manatee',
                         'Fox CPC':   'Fox_CPC',
                         'CPI':       'cpi'}
PARTNER_BIDPRICE_MODELS = {'revmob':   {model: CONST_BIDPRICE_MODELS[model] for model in ['CM manual', 'Fox', 'Manatee', 'Fox CPC']},
                           'startapp': {model: CONST_BIDPRICE_MODELS[model] for model in ['CM manual', 'Fox', 'Manatee', 'Fox CPC']},
                           'default':  {model: CONST_BIDPRICE_MODELS[model] for model in ['CM manual']}}
PARTNER_BIDPRICE_MODELS.update({partner: {model: CONST_BIDPRICE_MODELS[model] for model in [
                               'CM manual', 'CPI']} for partner in AD_NETWORK_CPI})

CONST_DOLLAR = 10000000.0
CONST_CPA_MAX = 1000 * CONST_DOLLAR
CONST_INF = 10000000.0
CONST_UNDEF = -1.0

SERVER_HOST = 'https://idash.appier.net:9935'   # production

ISP_FILTER_COUNTRY = ['jp']

PARM_RR_DAYS = [1, 3, 7, 14, 21, 30]
PARM_DATA_OFFSET = max(PARM_RR_DAYS) + 1

FORMAT_RETENTION = {'event': 'rr{days}_event', 'major': 'rr_major', 'metric': 'RR-{days}D'}
FORMAT_RANGE_RETENTION = {'roas': 'rng{days}_event_p_amount',
                          'prat': 'rng{days}_event_count'}

FORMAT_PURCHASE = {key: key for key in ['p_amount', 'm_count']}
FORMAT_COUNT = {key: key for key in ['count', 'count_UU', 'm_count']}

FORMAT_METRIC = {'RANGE_ROAS': 'ROAS-{days}D', 'RANGE_PRAT': 'PRAT-{days}D'}

EVENT_ID_MAJOR = 'event_major'


def my_enum(*sequential, **named):
    enums = dict(list(zip(sequential, list(range(len(sequential))))), **named)
    keys = list(enums.keys())
    vals = list(enums.values())
    reverse = dict((value, key) for key, value in enums.items())

    enums['mapping'] = enums
    enums['reverse_mapping'] = reverse
    enums['keys'] = keys
    enums['values'] = vals
    return type('Enum', (), enums)


ENUM_AI_ADGROUP_TYPE = my_enum(SM=0, WT=1, EX=2, WTD=3, LW=4, FIX=5)
ENUM_AI_CREATIVE_TYPE = my_enum(CREATIVE=NATIVE_VAL_MAP[APPID_LABEL_CREATIVE],
                             NATIVE=NATIVE_VAL_MAP[APPID_LABEL_NATIVE],
                             INTERSTITIAL=NATIVE_VAL_MAP[APPID_LABEL_INTERSTITIAL],
                             VIDEO=NATIVE_VAL_MAP[APPID_LABEL_VIDEO],
                             ALL=NATIVE_VAL_MAP[APPID_LABEL_ALL])
ENUM_AI_LIST_TYPE = my_enum(EX=0.25, WT=0.5, LW=-0.25, BK=-1.0, SM=0.0, AL=1.0)
ENUM_DATETIME_WEEKDAY = my_enum(WEEKDAY=0, WEEKEND=1, ALL=2, TODAY=3)
AI_AUTO_BIDPRICE_OPTION = my_enum(FOX='Fox', MDPS='Manatee', CM='manual', FOX_CPC='Fox_CPC')
ENUM_AI_RULE_OPTION = my_enum(FOX='Fox', BLACK='black', WHITE='white', AUTO='auto')
ENUM_OID_STATUS = my_enum(RUN='Running', STOP='Stopped', PAUSE='Paused')
ENUM_PERIOD_DICT = my_enum(DAYS_30=30, DAYS_7=7, DAYS_3=3, DAYS_1=1)
ENUM_RULE_ACTION_TYPE = my_enum(ACTION_SET_BLACKLIST=3, ACTION_SET_WHITELIST=5)


def usd(appier_dollar):
    return appier_dollar / CONST_DOLLAR


def get_appid_label(native):
    return APPID_LABEL_NATIVE if native == 1 else APPID_LABEL_CREATIVE


def get_local_datetime(ts, country):
    utc = pytz.utc
    dt_utc = utc.localize(datetime.datetime.fromtimestamp(ts))
    res_tz_list = country_timezones(country)
    res_tz = timezone(res_tz_list[0])
    res_dt = dt_utc.astimezone(res_tz)

    return res_dt


def get_local_weekend(local_dt):
    if local_dt.weekday() in [5, 6]:
        return ENUM_DATETIME_WEEKDAY.WEEKEND
    return ENUM_DATETIME_WEEKDAY.WEEKDAY


class BWInvDest(enum.Enum):
    DB_AND_BIDDER_HOST = 0
    DB_ONLY = 1


class AutoRuleDest(enum.Enum):
    DB_AND_IDASH_AND_BIDDER_HOST = 0
    DB_ONLY = 1
