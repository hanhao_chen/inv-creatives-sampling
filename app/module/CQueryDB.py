import sys
import time
import traceback

from builtins import range

from app.utils.db import MySQL_DB, MySQL_DB_W, MySQL_DB_RULE, MySQL_DB_RULE_W, \
    MySQL_DB_CAMPAIGN, MySQL_DB_CAMPAIGN_W, MySQL_DB_EXTERNAL, MySQL_DB_EXTERNAL_W, MySQL_DB_FRAUD, MySQL_DB_FRAUD_W, \
    MySQL_DB_EVENT_METRIC_TRANSIENT, MySQL_DB_EVENT_METRIC_TRANSIENT_W, \
    MySQL_DB_AI_RR_PERFORMANCE, MySQL_DB_AI_RR_PERFORMANCE_W, \
    MySQL_DB_AI_RR_FRAM_RAW_ACTION, MySQL_DB_AI_RR_FRAM_RAW_ACTION_W, \
    MySQL_DB_AI_DEEP_FUNNEL, MySQL_DB_AI_DEEP_FUNNEL_W, MySQL_DB_AI_OPTIMIZER, MySQL_DB_AI_OPTIMIZER_W, \
    MySQL_DB_RTB_OPT_W, MySQL_DB_RTB_OPT, MySQL_DB_AI_HYDRA, MySQL_DB_AI_HYDRA_W
from app.utils.db import get_query_result, execute_query, execute_query_many, \
    get_query_result_secure, execute_query_secure
from app.module.Const import TABLE_APPID_ADJUST
from app.utils.init_logger import init_logger

logger = init_logger(__name__)


class CQueryDB:
    # TODO: use ORM
    TABLE_AUTO_CAMPAIGN = 'auto_campaign'
    TABLE_CAMPAIGN = 'campaign'
    TABLE_PERDAY = 'ext_perday'
    TABLE_PREFER_DEAL_PERDAY = 'prefer_deal_perday'
    TABLE_CHARLIE_PERDAY = 'perday'
    TABLE_PREFER_DEAL_ADJUST = 'prefer_deal_adjust'
    TABLE_BEETHOVEN_PERDAY = 'beethoven_perday'
    TABLE_BEETHOVEN_ADJUST = 'beethoven_adjust'
    TABLE_EVENT_ID = 'event_id'
    TABLE_SIM_BID_HISTORY = 'ext_sim_bid_history'
    TABLE_EXT_AI_PERDAY_STREAM = 'ext_ai_perday_stream_v2'
    TABLE_EXT_AI_PERDAY_STREAM_HOUR = 'ext_ai_perday_stream_hour'
    TABLE_EXT_AI_BID = 'ext_ai_bid'
    TABLE_ACTION_FLATTEN = 'action_flatten'
    TABLE_ACTION_FLATTEN_NEW = 'action_flatten_new'
    TABLE_ACTION_FLATTEN_TTI = 'action_flatten_tti'
    TABLE_RETENTION = 'retention'
    TABLE_CURRENCY_RATE = 'currency_exchange_usd'
    TABLE_MANATEE_STEPSIZE = 'mdp_stepsize'
    TABLE_AI_RULE_LOG = 'ai_auto_rule_log'
    TABLE_EVENT_METRIC = 'app_id_event_metric'
    TABLE_EVENT_METRIC_HOURLY = 'event_metric_hourly'
    TABLE_AI_RULE_RTB = 'ai_rule_rtb_with_index'
    TABLE_UPDATE_LOG = 'table_update_log'
    TABLE_EXT_FRAUD_APPID = 'ext_fraud_appid'
    TABLE_EXT_FRAUD_APPID_TEST = 'ext_fraud_appid_test'
    TABLE_OPT_GOAL = 'optimization_goal'
    TABLE_OPT_GOAL_EID = 'optimization_goal_eids_hash'
    TABLE_EXT_FRAUD_APPID_RECOVER = 'ext_fraud_appid_recover'
    TABLE_EXT_FRAUD_APPID_RAW = 'ext_fraud_appid_raw'
    TABLE_EXT_FRAUD_APPID_RAW_TEST = 'ext_fraud_appid_raw_test'
    TABLE_EXT_FRAUD_UPDATETIME = 'ext_fraud_updatetime'
    TABLE_EXT_AI_PERDAY_ACTION_TIME = 'ext_ai_perday_action_time'
    TABLE_PUB2BLOCK = 'publisherid_toblock_list'
    TABLE_EXT_FRAUD_FEATURE = 'ext_fraud_feature'
    TABLE_EXT_FRAUD_FEATURE_TEST = 'ext_fraud_feature_test'
    TABLE_EXT_CLICK_SPAM_APPID = 'api_click_spam_appid'
    TABLE_ORDER_INFO = 'order_info'
    TABLE_DF_CATEGORY_WHITELIST = 'asap_initial_prediction_whitelist'
    TABLE_DF_ADGROUP_STATUS = 'deep_funnel_cid_status'
    TABLE_DF_ADGROUP_STATUS_TEST = 'deep_funnel_cid_status_test'
    TABLE_DF_PREDICTION_EXTENTION = 'deep_funnel_prediction_extention'
    TABLE_DF_PREDICTION_EXTENTION_TEST = 'deep_funnel_prediction_extention_test'
    TABLE_DF_PREDICTION_EXTENTION_STAGING = 'deep_funnel_prediction_extention_staging'
    TABLE_DF_VARIABLES_EXTENTION = 'deep_funnel_variables_extention'
    TABLE_DF_VARIABLES_EXTENTION_TEST = 'deep_funnel_variables_extention_test'

    TABLE_ASAP_EVENT_METRIC = 'asap_metric_distribution'
    TABLE_EXT_FRAUD_ACCOUNT_BLACKLIST = 'ext_fraud_account_blacklist'

    TABLE_AI_RULE_RTB_CID_STATUS = 'ai_rule_rtb_cid_status'
    TABLE_BW_INV_BWLIST = 'bw_inv_bwlist'
    TABLE_AUTO_RULE_ADN_BWLIST = 'auto_rule_adn_bwlist'
    TABLE_AUTO_RULE_RTB_WLIST = 'auto_rule_RTB_whitelist'
    TABLE_AUTO_RULE_RTB_BLIST = 'auto_rule_RTB_blacklist'

    TABLE_AI_OPTIMIZER_ACTION_BREAKDOWN = 'action_breakdown'
    TABLE_AI_OPTIMIZER_CID_PERFORMANCE = 'cid_hourly_performance'

    TABLE_HYDRA_CAMPAIGN_QUALITY_PREDICTION = 'campaign_quality_prediction'
    TABLE_HYDRA_EXT_CAPABILITY = 'external_capability'

    MySQL_DB_MAP_WRITE = {MySQL_DB_CAMPAIGN_W: [TABLE_CAMPAIGN,
                                                TABLE_OPT_GOAL,
                                                TABLE_OPT_GOAL_EID,
                                                TABLE_EVENT_ID,
                                                TABLE_ORDER_INFO],
                          MySQL_DB_FRAUD_W: [TABLE_EXT_AI_PERDAY_STREAM,
                                             TABLE_EXT_AI_PERDAY_STREAM_HOUR,
                                             TABLE_EXT_FRAUD_APPID,
                                             TABLE_EXT_FRAUD_APPID_RECOVER,
                                             TABLE_EXT_AI_PERDAY_ACTION_TIME,
                                             TABLE_EXT_FRAUD_APPID_RAW,
                                             TABLE_EXT_FRAUD_UPDATETIME,
                                             TABLE_PUB2BLOCK,
                                             TABLE_EXT_FRAUD_FEATURE,
                                             TABLE_EXT_FRAUD_FEATURE_TEST,
                                             TABLE_EXT_FRAUD_APPID_RAW_TEST,
                                             TABLE_EXT_FRAUD_APPID_TEST,
                                             TABLE_EXT_FRAUD_ACCOUNT_BLACKLIST,
                                             TABLE_AI_RULE_LOG,
                                             'invalid_action_category_app_id',
                                             'invalid_action_category_publisher',
                                             'invalid_click_category_app_id',
                                             'invalid_click_category_publisher',
                                             ],
                          MySQL_DB_EXTERNAL_W: [TABLE_EXT_CLICK_SPAM_APPID],
                          MySQL_DB_EVENT_METRIC_TRANSIENT_W: [],
                          MySQL_DB_AI_RR_PERFORMANCE_W: [TABLE_EVENT_METRIC],
                          MySQL_DB_AI_RR_FRAM_RAW_ACTION_W: [TABLE_EVENT_METRIC],
                          MySQL_DB_AI_DEEP_FUNNEL_W: [TABLE_DF_CATEGORY_WHITELIST,
                                                      TABLE_DF_ADGROUP_STATUS,
                                                      TABLE_DF_ADGROUP_STATUS_TEST,
                                                      TABLE_DF_PREDICTION_EXTENTION,
                                                      TABLE_DF_PREDICTION_EXTENTION_TEST,
                                                      TABLE_DF_PREDICTION_EXTENTION_STAGING,
                                                      TABLE_DF_VARIABLES_EXTENTION,
                                                      TABLE_DF_VARIABLES_EXTENTION_TEST
                                                      ],
                          MySQL_DB_RULE_W: [TABLE_AI_RULE_RTB_CID_STATUS,
                                            TABLE_AI_RULE_RTB,
                                            TABLE_BW_INV_BWLIST,
                                            TABLE_AUTO_RULE_ADN_BWLIST,
                                            TABLE_AUTO_RULE_RTB_WLIST,
                                            TABLE_AUTO_RULE_RTB_BLIST
                                            ],
                          MySQL_DB_AI_OPTIMIZER_W: [TABLE_AI_OPTIMIZER_ACTION_BREAKDOWN,
                                                    TABLE_AI_OPTIMIZER_CID_PERFORMANCE],
                          MySQL_DB_RTB_OPT_W: [TABLE_PREFER_DEAL_ADJUST,
                                               TABLE_BEETHOVEN_ADJUST],
                          MySQL_DB_AI_HYDRA_W: [TABLE_HYDRA_CAMPAIGN_QUALITY_PREDICTION,
                                                TABLE_HYDRA_EXT_CAPABILITY]
                          }

    MySQL_DB_MAP = {MySQL_DB_CAMPAIGN: [TABLE_CAMPAIGN,
                                        TABLE_OPT_GOAL,
                                        TABLE_OPT_GOAL_EID,
                                        TABLE_EVENT_ID,
                                        TABLE_ORDER_INFO],
                    MySQL_DB_FRAUD: [TABLE_EXT_AI_PERDAY_STREAM,
                                     TABLE_EXT_AI_PERDAY_STREAM_HOUR,
                                     TABLE_EXT_FRAUD_APPID,
                                     TABLE_EXT_FRAUD_APPID_RECOVER,
                                     TABLE_EXT_AI_PERDAY_ACTION_TIME,
                                     TABLE_EXT_FRAUD_APPID_RAW,
                                     TABLE_EXT_FRAUD_UPDATETIME,
                                     TABLE_PUB2BLOCK,
                                     TABLE_EXT_FRAUD_FEATURE,
                                     TABLE_EXT_FRAUD_FEATURE_TEST,
                                     TABLE_EXT_FRAUD_APPID_RAW_TEST,
                                     TABLE_EXT_FRAUD_APPID_TEST,
                                     TABLE_EXT_FRAUD_ACCOUNT_BLACKLIST,
                                     TABLE_AI_RULE_LOG,
                                     'invalid_action_category_app_id',
                                     'invalid_action_category_publisher',
                                     'invalid_click_category_app_id',
                                     'invalid_click_category_publisher',
                                     ],
                    MySQL_DB_EXTERNAL: [TABLE_EXT_CLICK_SPAM_APPID],
                    MySQL_DB_EVENT_METRIC_TRANSIENT: [],
                    MySQL_DB_AI_RR_PERFORMANCE: [TABLE_EVENT_METRIC],
                    MySQL_DB_AI_RR_FRAM_RAW_ACTION: [TABLE_EVENT_METRIC],
                    MySQL_DB_AI_DEEP_FUNNEL: [TABLE_DF_CATEGORY_WHITELIST,
                                              TABLE_DF_ADGROUP_STATUS,
                                              TABLE_DF_ADGROUP_STATUS_TEST,
                                              TABLE_DF_PREDICTION_EXTENTION,
                                              TABLE_DF_PREDICTION_EXTENTION_TEST,
                                              TABLE_DF_PREDICTION_EXTENTION_STAGING,
                                              TABLE_DF_VARIABLES_EXTENTION,
                                              TABLE_DF_VARIABLES_EXTENTION_TEST],
                    MySQL_DB_RULE: [TABLE_AI_RULE_RTB_CID_STATUS,
                                    TABLE_AI_RULE_RTB
                                    ],
                    MySQL_DB_AI_OPTIMIZER: [TABLE_AI_OPTIMIZER_ACTION_BREAKDOWN,
                                            TABLE_AI_OPTIMIZER_CID_PERFORMANCE],
                    MySQL_DB_RTB_OPT: [TABLE_PREFER_DEAL_ADJUST,
                                       TABLE_BEETHOVEN_ADJUST,
                                       TABLE_BEETHOVEN_PERDAY,
                                       TABLE_PREFER_DEAL_PERDAY],
                    MySQL_DB_AI_HYDRA: [TABLE_HYDRA_CAMPAIGN_QUALITY_PREDICTION,
                                        TABLE_HYDRA_EXT_CAPABILITY]
                    }

    def __init__(self, db_clients):
        self.db_clients = db_clients

    def _get_default_db_client(self, is_write=True):
        if is_write:
            return self.db_clients[MySQL_DB_W]
        else:
            return self.db_clients[MySQL_DB]

    def execute_query(self, query, db_client=None, args=None):
        if not db_client:
            db_client = self._get_default_db_client()

        execute_query(db_client, query, args=args)

    def execute_query_many(self, query, data, db_client=None):
        if not db_client:
            db_client = self._get_default_db_client()

        execute_query_many(db_client, query, data)

    def get_query_result(self, query, db_client=None):
        if not db_client:
            db_client = self._get_default_db_client()

        res = get_query_result(db_client, query)
        return res

    # TODO: may have better way to maintain table names
    @staticmethod
    def algorithm_perday_table(is_rtb, algorithm):
        if is_rtb == False:
            table = CQueryDB.TABLE_PERDAY
        else:
            if algorithm == 'prefer_deal':
                table = CQueryDB.TABLE_PREFER_DEAL_PERDAY
            elif algorithm == 'charlie':
                table = CQueryDB.TABLE_CHARLIE_PERDAY
            else:
                table = CQueryDB.TABLE_BEETHOVEN_PERDAY
        return table

    # TODO: may have better way to maintain table names
    @staticmethod
    def algorithm_adjust_table(is_rtb, algorithm):

        if not is_rtb:
            raise RuntimeError("Adjust only supports RTB")
        else:
            table = TABLE_APPID_ADJUST.get(algorithm)

        return table

    def query_field_unique_values(self, table, field, list_dict, range_dict={}):

        query_rule = []
        for field in list_dict:
            val = list_dict[field]
            rule = ','.join(["'%s'" % s for s in val])
            query_rule.append('%s in (%s)' % (field, rule))

        for field in range_dict:
            begin = range_dict[field].get('begin', 0)
            end = range_dict[field].get('end', 0)
            query_rule.append('%s BETWEEN %s AND %s' % (field, begin, end))

        query_rule_str = ' AND '.join(query_rule)

        if len(query_rule_str) > 0:
            query_rule_str = 'WHERE ' + query_rule_str
        query = "SELECT DISTINCT %s FROM %s %s" % (field, table, query_rule_str)
        logger.debug('[QURY] %s', query)
        res = []
        try:
            db_client = self.get_db_client_by_table(table, is_write=False)
            res = get_query_result(db_client, query)
        except:
            logger.critical('[ERROR] Query Fail: %s', query)

        unique_values = []
        for c in res:
            unique_values.append(c[field])

        return unique_values

    def get_db_client_by_table(self, table, from_raw_action=False, is_write=True):
        # FIXME: walk around for read event_metric from raw action
        # since ai_rr_performance.app_id_event_metric and ai_rr_from_raw_action shares the same table name
        dbs = []

        if is_write:
            for db_i in CQueryDB.MySQL_DB_MAP_WRITE:
                if table in CQueryDB.MySQL_DB_MAP_WRITE[db_i]:
                    dbs.append(db_i)
        else:
            for db_i in CQueryDB.MySQL_DB_MAP:
                if table in CQueryDB.MySQL_DB_MAP[db_i]:
                    dbs.append(db_i)

        if not dbs:
            return self._get_default_db_client(is_write=is_write)
        elif table == CQueryDB.TABLE_EVENT_METRIC and len(dbs) > 1:
            return self.db_clients[MySQL_DB_AI_RR_FRAM_RAW_ACTION] if from_raw_action else self.db_clients[
                MySQL_DB_AI_RR_PERFORMANCE]
        else:
            return self.db_clients[dbs[0]]

    def query_performance_db(self, table, list_dict, range_dict={}):

        if table == CQueryDB.TABLE_EVENT_METRIC:
            res = self.query_event_metric_with_C_connector(table, list_dict, range_dict=range_dict)
        else:
            res = self.query_performance_db_safe(table, list_dict, range_dict=range_dict)

        return res

    def query_event_metric_with_C_connector(self, table, list_dict, range_dict={}):

        db_client = self.get_db_client_by_table(table, is_write=False)

        query_rule = []
        for field in list_dict:
            val = list_dict[field]
            rule = ','.join(["'%s'" % s for s in val])
            query_rule.append('%s in (%s)' % (field, rule))

        for field in range_dict:
            begin = range_dict[field].get('begin', 0)
            end = range_dict[field].get('end', 0)
            query_rule.append('%s BETWEEN %s AND %s' % (field, begin, end))

        query_rule_str = ' AND '.join(query_rule)
        if len(query_rule_str) > 0:
            query_rule_str = 'WHERE ' + query_rule_str

        query = "SELECT cid, app_id, time, event_id, metric, value FROM %s %s" % (table, query_rule_str)
        logger.debug('query: ' + query)

        res = []
        try:
            res = get_query_result(db_client, query)
        except:
            logger.critical('[ERROR] Query Fail: %s', query)
            traceback.print_exc(file=sys.stdout)

        return res

    def query_performance_db_safe(self, table, list_dict, range_dict={}):

        db_client = self.get_db_client_by_table(table, is_write=False)

        query_rule = []
        query_parm = []
        for field in list_dict:
            val = list_dict[field]
            rule = ','.join(['%s' for _ in val])
            query_rule.append('{field} in ({list_format})'.format(field=field, list_format=rule))
            query_parm.extend(val)

        for field in range_dict:
            begin = range_dict[field].get('begin', 0)
            end = range_dict[field].get('end', 0)
            query_rule.append('{field} BETWEEN %s AND %s'.format(field=field))
            query_parm.extend([begin, end])
        query_rule_str = ' AND '.join(query_rule)
        if len(query_rule_str) > 0:
            query_rule_str = 'WHERE ' + query_rule_str

        query = "SELECT * FROM {table} {query_str}".format(table=table, query_str=query_rule_str)
        logger.debug('query: ' + query % tuple(query_parm))

        res = []
        try:
            res = get_query_result_secure(db_client, query, tuple(query_parm))
        except:
            logger.critical('[ERROR] Query Fail: %s', query)
            traceback.print_exc(file=sys.stdout)

        return res

    def query_appid_stat_perday(self, table, list_dict, range_dict={}):

        query_rule = []
        for field in list_dict:
            val = list_dict[field]
            rule = ','.join(["'%s'" % s for s in val])
            query_rule.append('%s in (%s)' % (field, rule))

        for field in range_dict:
            begin = range_dict[field].get('begin', 0)
            end = range_dict[field].get('end', 0)
            query_rule.append('%s BETWEEN %s AND %s' % (field, begin, end))

        query_rule_str = ' AND '.join(query_rule)

        if len(query_rule_str) > 0:
            query_rule_str = 'WHERE ' + query_rule_str
        query = "SELECT app_id, sum(bids) as bids, sum(shows) as shows, sum(clicks) as clicks, sum(actions) as actions, sum(cost) as cost FROM %s %s GROUP BY app_id" % (
            table, query_rule_str)
        logger.debug('[QURY] %s', query)
        res = []
        try:
            db_client = self.get_db_client_by_table(table, is_write=False)
            res = get_query_result(db_client, query)
        except:
            logger.critical('[ERROR] Query Fail: %s', query)

        return res

    def query_sim_bid_history(self, cid):

        res = self.query_performance_db(CQueryDB.TABLE_SIM_BID_HISTORY, {'cid': [cid]})
        return res

    def insert_sim_bid_history(self, field_data_dict):

        query = "REPLACE INTO %s (cid, time, bidprice, budget, hour_budget, coefficient, clicks_threshold, clicks_obtained, clicks_error, " \
                "clicks_coefficient, budget_cap, hour_cpc_plan) " \
                "VALUES ('%s', %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '%s')" % (CQueryDB.TABLE_SIM_BID_HISTORY,
                                                                                 field_data_dict['cid'],
                                                                                 field_data_dict['time'],
                                                                                 field_data_dict['bidprice'],
                                                                                 field_data_dict['budget'],
                                                                                 field_data_dict['hour_budget'],
                                                                                 field_data_dict['coefficient'],
                                                                                 field_data_dict['clicks_threshold'],
                                                                                 field_data_dict['clicks_obtained'],
                                                                                 field_data_dict['clicks_error'],
                                                                                 field_data_dict['clicks_coefficient'],
                                                                                 field_data_dict['budget_cap'],
                                                                                 field_data_dict['hour_cpc_plan'])

        db_client = self._get_default_db_client()
        execute_query(db_client, query)

    def get_major_event_id(self, cid):

        res = self.query_performance_db(CQueryDB.TABLE_EVENT_ID, {'cid': [cid]})
        eid = []
        for c in res:
            major = int(c['major'])
            if major != 1:
                continue
            eid.append(c['event_id'].strip(' '))

        return eid

    def query_campaign_distinct_field(self, field):

        query = "SELECT DISTINCT(%s) FROM %s WHERE running=1" % (field, CQueryDB.TABLE_CAMPAIGN)
        db_client = self._get_default_db_client()
        res = get_query_result(db_client, query)
        return res

    def insert_ai_bid(self, entries):

        val_str = ','.join(["('%s', '%s', '%s', %s, %s, %s, %s, %s)" %
                            (x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7]) for x in entries])
        query = "REPLACE INTO %s (country, ext_channel, require_device_os, time, native, clicks, actions, cost) VALUES %s" % (
            CQueryDB.TABLE_EXT_AI_BID, val_str)
        db_client = self._get_default_db_client()
        execute_query(db_client, query)

    def query_ai_bid(self, timestamp_dict={}):

        ts_now = int(time.time())
        ts_bgn = timestamp_dict.get('begin', 0.0)
        ts_end = timestamp_dict.get('end', ts_now)

        query = "SELECT * FROM %s WHERE time BETWEEN %s AND %s" % (CQueryDB.TABLE_EXT_AI_BID, ts_bgn, ts_end)
        db_client = self._get_default_db_client()
        res = get_query_result(db_client, query)
        return res

    def insert_retention(self, entries):

        val_str = ','.join(["('%s', '%s', %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '%s')" % (
            x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8], x[9], x[10], x[11], x[12], x[13]) for x in entries])
        query = "REPLACE INTO %s (cid, app_id, time, rr1_major, rr1_open, rr3_major, rr3_open, rr7_major, rr7_open, major, p_count, p_amount, raw_cost, currency) VALUES %s" % (
            CQueryDB.TABLE_RETENTION, val_str)
        try:
            db_client = self._get_default_db_client()
            execute_query(db_client, query)
        except:
            logger.critical('[ERROR] Query Fail: %s', query)
            raise

    def query_event_active_cid(self, start_ts):

        query = "SELECT DISTINCT(cid) as cid FROM %s WHERE time > %s" % (CQueryDB.TABLE_ACTION_FLATTEN, start_ts)
        try:
            db_client = self._get_default_db_client()
            res = get_query_result(db_client, query)
        except:
            logger.critical('[ERROR] Query Fail: %s', query)

        return res

    def insert_action_flatten(self, entry):

        query = "REPLACE INTO %s (cid, time, adid, event_id, partner_id, app_id, p_amount, p_currency) VALUES ('%s', %s, '%s', '%s', '%s', '%s', %s, '%s')" % (
            CQueryDB.TABLE_ACTION_FLATTEN,
            entry.get('cid', ''),
            entry.get('time', 0),
            entry.get('adid', ''),
            entry.get('event_id', ''),
            entry.get('partner_id', ''),
            entry.get('app_id', ''),
            entry.get('p_amount', 0),
            entry.get('p_currency', ''))

        db_client = self._get_default_db_client()
        execute_query(db_client, query)

    def query_manatee_stepsize(self, cid, ext_bid_price):

        query = "SELECT cid, mean_bidprice, avg_stepsize, stddev FROM %s WHERE time = 'latest' and cid = '%s'" % (
            CQueryDB.TABLE_MANATEE_STEPSIZE, cid)
        res = []
        min_step_ratio, max_step_ratio = 0.05, 0.20
        try:
            db_client = self._get_default_db_client()
            res = get_query_result(db_client, query)[0]
            cid, mean, avg_stepsize, stddev = res['cid'], res['mean_bidprice'], res['avg_stepsize'], res['stddev']
            # compute decay_ratio
            decay_ratio = 0.
            if stddev != 0.:
                if ext_bid_price >= mean and ext_bid_price <= mean + 4 * stddev:
                    decay_ratio = ((mean + 4 * stddev) - ext_bid_price) / (4 * stddev)
                elif ext_bid_price <= mean and ext_bid_price >= mean - 4 * stddev:
                    decay_ratio = (ext_bid_price - (mean - 4 * stddev)) / (4 * stddev)
            manatee_bid_step = round(
                min(max(avg_stepsize * decay_ratio, min_step_ratio * mean), max_step_ratio * mean), 4)
            res = [{'cid': cid, 'manatee_bid_step': manatee_bid_step}]
        except IndexError:
            logger.critical('[ERROR] IndexError: Not Manatee Adgroup or Initial Adgroup, %s', cid)
            res = [{'cid': cid, 'manatee_bid_step': round(min_step_ratio * ext_bid_price, 4)}]
        except:
            logger.critical('[ERROR] Query Fail: %s', query)
        return res

    def query_pmp_recommendation(self, action_type, action_threshold, oid_dimension, week_size):

        oid_command = 'oid, ' if oid_dimension == '1' else ''
        time_command = ''
        res = []

        query_time = 'select time from pmp_recommendation group by time DESC limit {}'.format(week_size)
        try:
            db_client = self._get_default_db_client()
            res_time = get_query_result(db_client, query_time)
            times = []
            for r in res_time:
                times.append(r['time'])
            time_command = " and time in ({})".format(','.join(["'{}'".format(t) for t in times]))

        except:
            logger.critical('[ERROR] Query Fail: %s', query_time)

        query = "select time, {oid_command}partner_id as exchange, country, if(app_type='1', os, '') as os, app_id, app_name, bundle_id, \
                case when app_type = '1' then 'app' when app_type = '2' then 'web' else 'unkonwn' end as app_type, \
                case when device_type = '1' then 'mobile' when device_type = '2' then 'pc' when device_type = '3' then 'tv' when device_type = '4' \
                then 'phone' when device_type = '5' then 'tablet' when device_type = '6' then 'connected_device' when device_type = '7' then 'set_top_box' else 'unkonwn' end as device_type, \
                if(imp_width = '-1' and imp_height = '-1', 'native', concat(imp_width, ' x ', imp_height)) as size, \
                imp_is_instl, round(sum(cost) / sum(shows) * 1000, 4) as cpm, round(sum(cost) / sum(clicks), 4) as cpc, round(sum(cost) / sum(major_action), 4) as cpa, \
                round(sum(cost), 4) as cost, sum(shows) as shows, sum(clicks) as clicks, sum(major_action) as {action_type} \
                from pmp_recommendation where {action_type} > 0 and app_id not in ('', 'anonymous', 'Anonymous'){time_command} group by time, \
                {oid_command}partner_id, country, if(app_type='1', os, ''), app_id, app_name, bundle_id, \
                case when app_type = '1' then 'app' when app_type = '2' then 'web' else 'unkonwn' end, \
                case when device_type = '1' then 'mobile' when device_type = '2' then 'pc' when device_type = '3' then 'tv' when device_type = '4' \
                then 'phone' when device_type = '5' then 'tablet' when device_type = '6' then 'connected_device' when device_type = '7' then 'set_top_box' else 'unkonwn' end, \
                if(imp_width = '-1' and imp_height = '-1', 'native', concat(imp_width, ' x ', imp_height)), \
                imp_is_instl having {action_type} >= {action_threshold} ORDER BY time DESC, {action_type} DESC".format(
            oid_command=oid_command,
            action_type=action_type,
            action_threshold=action_threshold,
            time_command=time_command,
        )
        try:
            db_client = self._get_default_db_client()
            res = get_query_result(db_client, query)
        except:
            logger.critical('[ERROR] Query Fail: %s', query)
        return res

    def insert_ai_rule_log(self, entry):

        fields = sorted(entry.keys())
        vals = ['\'%s\'' % entry[x] for x in fields]

        query = "INSERT INTO %s (%s) VALUES (%s)" % (
            CQueryDB.TABLE_AI_RULE_LOG,
            ','.join(fields),
            ','.join(vals))

        execute_query(self.get_db_client_by_table(CQueryDB.TABLE_AI_RULE_LOG), query)

    def update_db_table_for_testing(self, table, cid, entries, limit=10):
        for c in entries:
            for k in c:
                c[k] = str(c[k])
            c.update({'cid': str(cid)})
        self.update_db_table(table, entries, limit=limit)

    def update_db_table(self, table, entries, limit=10):

        if len(entries) <= 0:
            return

        fields = sorted(entries[0].keys())
        self.update_db_table_field(table, entries, fields, limit)

    def update_db_table_field(self, table, entries, update_fields, limit=10, max_fields=[]):

        self.update_db_table_field_safe(table, entries, update_fields, limit=limit, max_fields=max_fields)

    def update_db_table_field_safe(self, table, entries, update_fields, limit=10, max_fields=[]):

        if len(entries) <= 0:
            return

        db_client = self.get_db_client_by_table(table)

        fields = sorted(entries[0].keys())
        parm_size = limit
        for i in range(0, len(entries), parm_size):
            val_list = []
            for c in entries[i:i + parm_size]:
                val_list.extend([c[x].strip('\"\n\t') if isinstance(c[x], str) else c[x] for x in fields])

            try:
                field_str = ','.join(['%s' % f for f in fields])
                format_str = ','.join(['%s' for _ in fields])
                format_str_list = ','.join(['(%s)' % format_str for _ in range(0, len(entries[i:i + parm_size]), 1)])
                max_str = ',' + ','.join(["{f}=GREATEST({f}, VALUES({f}))".format(f=f)
                                          for f in max_fields]) if len(max_fields) > 0 else ''
                query = "INSERT INTO {table} ({field_str}) VALUES {format_str_list} ON DUPLICATE KEY UPDATE {update_str} {max_str}".format(
                    table=table,
                    field_str=field_str,
                    format_str_list=format_str_list,
                    update_str=','.join(["%s=VALUES(%s)" % (f, f) for f in update_fields]),
                    max_str=max_str)

                execute_query_secure(db_client, query, tuple(val_list))
            except:
                traceback.print_exc()


def get_active_oid_list(idash_endpoint):
    oid_list = []
    for status in ['Running', 'Paused']:
        olist = idash_endpoint.read_order_by_status(status)
        oid_list.extend(olist)

    return oid_list
