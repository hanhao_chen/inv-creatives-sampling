import json
import logging
import os
from pathlib2 import Path
from logging.handlers import RotatingFileHandler

from app.utils.base_db import BaseDB
from app.utils.db import MySQL_DB, MySQL_DB_W, MySQL_DB_RULE, MySQL_DB_RULE_W, \
    MySQL_DB_CAMPAIGN, MySQL_DB_CAMPAIGN_W, MySQL_DB_EXTERNAL, MySQL_DB_EXTERNAL_W, MySQL_DB_FRAUD, MySQL_DB_FRAUD_W, \
    MySQL_DB_EVENT_METRIC_TRANSIENT, MySQL_DB_EVENT_METRIC_TRANSIENT_W, \
    MySQL_DB_AI_RR_PERFORMANCE, MySQL_DB_AI_RR_PERFORMANCE_W, \
    MySQL_DB_AI_RR_FRAM_RAW_ACTION, MySQL_DB_AI_RR_FRAM_RAW_ACTION_W, \
    MySQL_DB_AI_DEEP_FUNNEL, MySQL_DB_AI_DEEP_FUNNEL_W, MySQL_DB_AI_OPTIMIZER, MySQL_DB_AI_OPTIMIZER_W, \
    MySQL_DB_RTB_OPT_W, MySQL_DB_RTB_OPT, MySQL_DB_AI_HYDRA, MySQL_DB_AI_HYDRA_W, MySQL_DB_AIBE_DEV


def init_db_clients(cfg):
    def _get_db_client(db_cfg, charset='utf8'):
        return BaseDB(
            db_cfg["host"],
            db_cfg["port"],
            db_cfg["username"],
            db_cfg["password"],
            db_cfg["db_name"],
            charset=charset,
        )

    db_clients = {}
    db_clients[MySQL_DB_CAMPAIGN] = _get_db_client(cfg.campaign_db)
    db_clients[MySQL_DB_FRAUD] = _get_db_client(cfg.fraud_db)
    db_clients[MySQL_DB] = _get_db_client(cfg.performance_db)
    db_clients[MySQL_DB_EXTERNAL] = _get_db_client(cfg.external_db)
    db_clients[MySQL_DB_EVENT_METRIC_TRANSIENT] = _get_db_client(cfg.event_metric_transient_db)
    db_clients[MySQL_DB_AI_RR_PERFORMANCE] = _get_db_client(cfg.ai_rr_performance_db)
    db_clients[MySQL_DB_AI_RR_FRAM_RAW_ACTION] = _get_db_client(cfg.ai_rr_from_raw_action_db)
    db_clients[MySQL_DB_AI_DEEP_FUNNEL] = _get_db_client(cfg.ai_deep_funnel_db)
    db_clients[MySQL_DB_RULE] = _get_db_client(cfg.rule_db)
    db_clients[MySQL_DB_AI_OPTIMIZER] = _get_db_client(cfg.ai_optimizer)
    db_clients[MySQL_DB_RTB_OPT] = _get_db_client(cfg.rtb_opt, charset='utf8mb4')
    db_clients[MySQL_DB_AI_HYDRA] = _get_db_client(cfg.ai_hydra)
    db_clients[MySQL_DB_AIBE_DEV] = _get_db_client(cfg.aibe_db)
    # db_clients[MySQL_RTB_DB_PRD] = _get_db_client(cfg.rtb_db_prd)

    db_clients[MySQL_DB_CAMPAIGN_W] = _get_db_client(cfg.campaign_db_w)
    db_clients[MySQL_DB_FRAUD_W] = _get_db_client(cfg.fraud_db_w)
    db_clients[MySQL_DB_W] = _get_db_client(cfg.performance_db_w)
    db_clients[MySQL_DB_EXTERNAL_W] = _get_db_client(cfg.external_db_w)
    db_clients[MySQL_DB_EVENT_METRIC_TRANSIENT_W] = _get_db_client(cfg.event_metric_transient_db_w)
    db_clients[MySQL_DB_AI_RR_PERFORMANCE_W] = _get_db_client(cfg.ai_rr_performance_db_w)
    db_clients[MySQL_DB_AI_RR_FRAM_RAW_ACTION_W] = _get_db_client(cfg.ai_rr_from_raw_action_db_w)
    db_clients[MySQL_DB_AI_DEEP_FUNNEL_W] = _get_db_client(cfg.ai_deep_funnel_db_w)
    db_clients[MySQL_DB_RULE_W] = _get_db_client(cfg.rule_db_w)
    db_clients[MySQL_DB_AI_OPTIMIZER_W] = _get_db_client(cfg.ai_optimizer_w)
    db_clients[MySQL_DB_RTB_OPT_W] = _get_db_client(cfg.rtb_opt_w, charset='utf8mb4')
    db_clients[MySQL_DB_AI_HYDRA_W] = _get_db_client(cfg.ai_hydra_w)
    return db_clients


def close_db_clients(db_clients):
    for client in db_clients.values():
        client.close()


class V1Config(object):
    debug = False

    ts_fmt = "%Y-%m-%d %H:%M:%S"
    idash = {}

    formatter = logging.Formatter("%(asctime)s - %(pathname)s:%(lineno)d - %(levelname)s - %(message)s")
    formatter.default_msec_format = "%s.%03d"

    proj_folder = os.path.dirname(os.path.realpath(__file__))
    log_folder = os.path.join(proj_folder, "log")
    Path(log_folder).mkdir(exist_ok=True)
    handler = RotatingFileHandler(os.path.join(log_folder, "api.log"), maxBytes=200 * (1024**2), backupCount=5)
    handler.setFormatter(formatter)

    def __init__(self, env, config_file="app_config.json", credential_file="credential.json"):
        if env not in ("production", "staging", "dev"):
            raise ValueError("Invalid ENV setting")
        if os.environ.get("DEBUG"):
            self.debug = True

        if not os.path.exists(config_file):
            raise IOError("configuration file {} not found".format(config_file))

        if not os.path.exists(credential_file):
            raise IOError("credential file {} not found".format(credential_file))

        config = self.read_settings(config_file, env)
        credentials = self.read_settings(credential_file, env)

        self.merge_settings(config, credentials)

        self.handler.setLevel(logging.DEBUG if self.debug else logging.INFO)
        self.env = env

    def read_settings(self, setting_file, env):
        with open(setting_file, "r") as f:
            setting_json = json.loads(f.read())

        default_setting = setting_json["default"]
        env_specific_setting = setting_json[env]
        for key in env_specific_setting:
            if key in default_setting:
                default_setting[key].update(env_specific_setting[key])
            else:
                default_setting[key] = env_specific_setting[key]

        default_setting = self.byteify(default_setting)

        return default_setting

    def byteify(self, input_dict):
        if isinstance(input_dict, dict):
            return {self.byteify(key): self.byteify(value)
                    for key, value in input_dict.items()}
        elif isinstance(input_dict, list):
            return [self.byteify(element) for element in input_dict]
        else:
            return input_dict
        # elif isinstance(input_dict, unicode):
        #     return input_dict.encode('utf-8')
        # else:
        #     return input_dict

    def merge_settings(self, config, credentials):
        self.logservice_cfg = self.merge_dict(config.get("logservice", {}), credentials.get("logservice", {}))
        self.idash_cfg = self.merge_dict(config.get("idash", {}), credentials.get("idash", {}))
        self.app_cfg = self.merge_dict(config.get("app", {}), credentials.get("app", {}))
        self.spark_cfg = self.merge_dict(config.get("spark", {}), credentials.get("spark", {}))
        self.abnormal_result_server = self.merge_dict(config.get(
            "abnormal_result_server", {}), credentials.get("abnormal_result_server", {}))
        self.all_action_cfg = self.merge_dict(config.get("all_action_api", {}), credentials.get("all_action_api", {}))
        self.fraud_rule_cfg = self.merge_dict(config.get("fraud_rule_api", {}), credentials.get("fraud_rule_api", {}))

        self.performance_db = self.merge_dict(config.get("performance_db", {}), credentials.get("performance_db", {}))
        self.performance_db_w = self.merge_dict(
            config.get("performance_db_w", {}), credentials.get("performance_db_w", {}))
        self.campaign_db = self.merge_dict(config.get("campaign_db", {}), credentials.get("campaign_db", {}))
        self.campaign_db_w = self.merge_dict(config.get("campaign_db_w", {}), credentials.get("campaign_db_w", {}))
        self.external_db = self.merge_dict(config.get("external_db", {}), credentials.get("external_db", {}))
        self.external_db_w = self.merge_dict(config.get("external_db_w", {}), credentials.get("external_db_w", {}))
        self.event_metric_transient_db = self.merge_dict(
            config.get("event_metric_transient_db", {}), credentials.get("event_metric_transient_db", {}))
        self.event_metric_transient_db_w = self.merge_dict(
            config.get("event_metric_transient_db_w", {}), credentials.get("event_metric_transient_db_w", {}))
        self.fraud_db = self.merge_dict(config.get("fraud_db", {}), credentials.get("fraud_db", {}))
        self.fraud_db_w = self.merge_dict(config.get("fraud_db_w", {}), credentials.get("fraud_db_w", {}))
        self.ai_rr_performance_db = self.merge_dict(config.get(
            "ai_rr_performance_db", {}), credentials.get("ai_rr_performance_db", {}))
        self.ai_rr_performance_db_w = self.merge_dict(config.get(
            "ai_rr_performance_db_w", {}), credentials.get("ai_rr_performance_db_w", {}))
        self.ai_rr_from_raw_action_db = self.merge_dict(config.get(
            "ai_rr_from_raw_action_db", {}), credentials.get("ai_rr_from_raw_action_db", {}))
        self.ai_rr_from_raw_action_db_w = self.merge_dict(config.get(
            "ai_rr_from_raw_action_db_w", {}), credentials.get("ai_rr_from_raw_action_db_w", {}))
        self.ai_deep_funnel_db = self.merge_dict(config.get(
            "ai_deep_funnel_db", {}), credentials.get("ai_deep_funnel_db", {}))
        self.ai_deep_funnel_db_w = self.merge_dict(config.get(
            "ai_deep_funnel_db_w", {}), credentials.get("ai_deep_funnel_db_w", {}))
        self.rule_db = self.merge_dict(config.get("rule_db", {}), credentials.get("rule_db", {}))
        self.rule_db_w = self.merge_dict(config.get("rule_db_w", {}), credentials.get("rule_db_w", {}))
        self.ai_optimizer = self.merge_dict(config.get("ai_optimizer", {}), credentials.get("ai_optimizer", {}))
        self.ai_optimizer_w = self.merge_dict(config.get("ai_optimizer_w", {}), credentials.get("ai_optimizer_w", {}))
        self.gs_credential_file = credentials.get("app", {}).get("google_spreadsheet_credential_file")
        self.rtb_opt_w = self.merge_dict(config.get("rtb_opt_db_w", {}), credentials.get("rtb_opt_db_w", {}))
        self.rtb_opt = self.merge_dict(config.get("rtb_opt_db", {}), credentials.get("rtb_opt_db", {}))
        self.ai_hydra = self.merge_dict(config.get("ai_hydra_db", {}), credentials.get("ai_hydra_db", {}))
        self.ai_hydra_w = self.merge_dict(config.get("ai_hydra_db_w", {}), credentials.get("ai_hydra_db_w", {}))
        self.aibe_db = self.merge_dict(config.get("aibe_db_dev", {}), credentials.get("aibe_db_dev", {}))
        # self.rtb_db_prd = self.merge_dict(config.get("rtb_db_prd", {}), credentials.get("rtb_db_prd", {}))

        self.hydra_sampling = config.get("hydra_sampling", {})
        # self.hydra_list_control = config.get("hydra_list_control", {})
        self.inv_list_control = config.get("inv_list_control", {})
        self.slack_app = {}
        for app_name in set(list(config.get('slack_app', {}).keys()) + list(credentials.get('slack_app', {}).keys())):
            self.slack_app[app_name] = self.merge_dict(
                config.get('slack_app', {}).get(app_name, {}),
                credentials.get('slack_app', {}).get(app_name, {}))

    def merge_dict(self, dict1, dict2):
        dict_out = dict1.copy()
        dict_out.update(dict2)
        return dict_out
