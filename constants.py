EXP_OIDS = ['LLpf015wTpmSKBQ6EGtNbA',
            'CqcsvVYlS9mdYVNkzxEf6w',
            'zvJOSyg4QvmptysPkPuTDA',
            '775q5ULHR1inTJExEO65Ag',
            'Ri9wGHO4QGeGttYeNBfLpw',
            'hXoaTv7HRBWFxKY1jJPbJQ',
            'LFo4PSeZR1SPzJvvfIXY4w',
            'qn30sB95TxGIPtXUoHTPTw',
            'TwBO4vGqQI29z-2BshvrQA']

EXP_OIDS_BID = ['PRvhXQESReGN1wDrVNq48w',
                'ogyTaxMmQAm_54BgnPeOFg',
                'gUDTYvgeR6-p6Wif7kODkg',
                'mh7CZ6ITSQedJWpAJJZ-rA',
                'yzdhS4mFQXWtBay_vCfPIw',
                'ksCb9oc9Tt-PKakQfz3uPg',
                'gFcX8k9bT-6x9YYiusEFNw',
                'uiPYBpYVQjyVJfTWNIWibw',
                'EOAsshHKRTSWQULTw4QO2w',
                'TwBO4vGqQI29z-2BshvrQA']

# EXP_CIDS = {
#     'bxR6euOQRw6XWcZ-yOU42A': {'cid': 'l-6Q9huOSti0zzV97Kyhnw', 'imp_type':4, 'is_rewarded': 0},
#     'TvJQZFq_TnurqIncrJ6Gqw': {'cid': 'NVTw3uISSMK8_Q7uVXU0IQ', 'imp_type':4, 'is_rewarded': 0},
#     'X3Tq_EzLQTW5raIaU2tX1g': {'cid': 'pbK1ZjZWSHGxdvGQtst2FQ', 'imp_type':4, 'is_rewarded': 0},
#     'lfJelrMGTIKhUutRCZNEVw': {'cid': 'PvaKOdPNTV-4FgHk6HuFRQ', 'imp_type':4, 'is_rewarded': 0},
# }

EXP_CIDS = [
    {'cid': 'nChqHXWhTTCLLbR2UI-cFg', 'imp_type': 2, 'is_rewarded': 0, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'zSWB9iT-R-aI051uj30FbQ', 'imp_type': 2, 'is_rewarded': 1, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'zxO43fGiTuumTtzR2lX3xg', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'DnA-5hljT4q9J6ChrDwmSQ', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'eGwkpkbWQDS6NFFG0AXnFA', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'v4hF5ycyQR2IY2UXMVDGPg', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'fFpyvMOiQQOoSVpzvPDQRQ', 'imp_type': 2, 'is_rewarded': 0, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'uSsQpG8nSImly_3fkaqylw', 'imp_type': 2, 'is_rewarded': 1, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'YM2VbLj3TXGfqsJhiskYIA', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'jFBVY5_fTwSQFTb3vi5oiA', 'imp_type': 2, 'is_rewarded': 1, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'p9_D0sxOSeKkslGJpEp30A', 'imp_type': 2, 'is_rewarded': 0, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'PGiJ2BWNTpW9uSg7X2z-jw', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'NigX0rbMR7y_xylGN3U_yg', 'imp_type': 2, 'is_rewarded': 1, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'umb8TbYmQPirPe00rON_ZQ', 'imp_type': 2, 'is_rewarded': 0, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': '2EbCArlKQuCpn11KmEe2YA', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'Rhvs0HceTiieMMGMEjgxhA', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'OMSDNutgRduPIMkrwofTHQ', 'imp_type': 2, 'is_rewarded': 1, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'wIHpnlvOQBijBTO6Ssumhw', 'imp_type': 2, 'is_rewarded': 0, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'OjMDdtD4TIS718aJ1XC_Lg', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'sk3scuGpS5OLW942putaXg', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'McLDlKlTSKin-uqm0akdXg', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'shrk9SrQQ-i_XEtmeFT8Zg', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'lPqXHKqWTzmhZ_zyrgZi8w', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'WvpGCxaWS1iwAvFkpWzovQ', 'imp_type': 2, 'is_rewarded': 1, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'qhHALQmTR9KGrauC0nceGg', 'imp_type': 2, 'is_rewarded': 0, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'HWFNvsGdSSegdcmEEz3dxA', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'BYG5bei0T3uxlh1j2C6D6A', 'imp_type': 2, 'is_rewarded': 1, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'jsnAGSWOTiahGVEOY_mXIA', 'imp_type': 2, 'is_rewarded': 0, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'Bfzdrc2aTbWxbpZoj_N7cw', 'imp_type': 2, 'is_rewarded': 1, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'aMQayaENRV-zrRQuG9urpw', 'imp_type': 2, 'is_rewarded': 0, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'zKZH7hSRTY2XH9PiqY6r2Q', 'imp_type': 2, 'is_rewarded': 1, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    {'cid': 'LwbQhMTzRrurNESnnOtSiA', 'imp_type': 2, 'is_rewarded': 0, 'ssp': ['mopub', 'google', 'adcolony', 'vungle']},
    # {'cid': 'DNQ-gZODTounOwClA-Dt3g', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': '7bb5Xe-HT-SpfO5DEY_t2w', 'imp_type': 4, 'is_rewarded': 0},
    {'cid': 'ElnvYNZMQeeQu_ORxYxqJg', 'imp_type':4, 'is_rewarded': 0},
    {'cid': 'jG_MepCUQfOxuUVt2kyA-w', 'imp_type':4, 'is_rewarded': 0},
    # {'cid': 'l-6Q9huOSti0zzV97Kyhnw', 'imp_type':4, 'is_rewarded': 0},
    # {'cid': 'NVTw3uISSMK8_Q7uVXU0IQ', 'imp_type':4, 'is_rewarded': 0},
    # {'cid': 'pbK1ZjZWSHGxdvGQtst2FQ', 'imp_type':4, 'is_rewarded': 0},
    # {'cid': 'PvaKOdPNTV-4FgHk6HuFRQ', 'imp_type':4, 'is_rewarded': 0},
    # {'cid': 'mQc58fckTFejRg3zgFg59Q', 'imp_type':2, 'is_rewarded': 0, 'ssp': ['mopub']},
    # {'cid': 'OKUHka6NQsWuMjGvaxCTyg', 'imp_type':2, 'is_rewarded': 1, 'ssp': ['mopub']},
    # {'cid': '925kL7iDQduTWF7G2Wkgcg', 'imp_type':2, 'is_rewarded': 0, 'ssp': ['mopub']},
    # {'cid': 'scd_8R56QOeL1AhNU9FziQ', 'imp_type':2, 'is_rewarded': 1, 'ssp': ['mopub']},
    # {'cid': 'tNeXg4U3TWymrh14YuHLvg', 'imp_type':2, 'is_rewarded': 1, 'ssp': []},
    # {'cid': '2JdGtWUnTbyGLxLQR93IKQ', 'imp_type':2, 'is_rewarded': 0, 'ssp': []},
    # {'cid': 'e_8aEdoBSNSrTptVO8oBAQ', 'imp_type':2, 'is_rewarded': 1, 'ssp': []},
    # {'cid': '1RTqXcqgQcKS_9MGXm7daw', 'imp_type':2, 'is_rewarded': 0, 'ssp': []},
    {'cid': 'xLKbfEkrS7eHJvBetmRHzQ', 'imp_type':2, 'is_rewarded': 0, 'ssp': []},
    {'cid': 'FZUrugDDRP-fbYMfzrveNg', 'imp_type':2, 'is_rewarded': 1, 'ssp': []},
    # {'cid': 'iRSYr10VT5qDFbTgREF-NQ', 'imp_type':2, 'is_rewarded': 0, 'ssp': []},
    # {'cid': 'MxxMehh1SWKFh2Jp7YbRfw', 'imp_type':2, 'is_rewarded': 1, 'ssp': []},
    {'cid': 'U5DLzKuVSlCMaro96HsFRg', 'imp_type':2, 'is_rewarded': 0, 'ssp': []},
    {'cid': 'qp2V5xQ2Rha4b_VEmJUjIA', 'imp_type':2, 'is_rewarded': 1, 'ssp': []},
]

ssp_enhance_map = {
    'google': ["gadex_hk", "gadex2_hk", "gadex2_use", "gadex_usw", "gadex_use"],
    'mopub': ["mopub"],
    'adcolony': ["adcolony_sg", "adcolony_use"],
    'mediba': ["mediba_jp"],
    'vungle': ["vungle_use"],
    'unity': ["unity_use"]
}

IMP_SIZE_SET = {'video', 'native', 'large', 'medium', 'small', 'full_large', 'full_medium'}
adjust_weight_template = """{{
    'type': 'ai_optimizer',
    'command':
    {{
        "cid": "{cid}",
        "adjust_rules": [
        {{
            "exchange": "{exchange}",
            "device_type": "{device_type}",
            "adjust_val": {weight},
            "adjust_by_robot": False,
            "imp_size_group": "{imp_size_group}",
            "tagid": "{tagid}",
            "app_type": "{app_type}",
            "app_id": "{app_id}",
            "cid": "{cid}"
        }}]
    }},
    'cid': '{cid}'
}}"""

WHITELIST_UPDATE_TEMPLATE = {
        "_ui": {},
        "enhancement_type": "white",
        "operator": "in",
        "price_multiplier": 1,
        "source": "publisher_id_or_deal_id",
        "value": []
}

TAGID_UPDATE_TEMPLATE = {
        "_ui": {},
        "enhancement_type": "white",
        "operator": "in",
        "price_multiplier": 1,
        "source": "tag_id",
        "value": []
}


app_type_mp = {
    'api buying': 0,
    'app': 1,
    'web': 2,
    'pushad': 3
}

device_type_mp = {
    'mobile/tablet': 1,
    'pc': 2,
    'connected tv': 3,
    'phone': 4,
    'tablet': 5,
    'connected device': 6,
    'set top box': 7
}

score_type_mp = {
    '3Dbh8E12QHW2fEnetj3P9A': 'vp_bundle',
    'Hqt5wqh6R2iNXR13e-RitQ': 'country_whitelist',
    'iaVBIZ-FQoOLBwVAPQ984Q': 'vp_bundle',
    'lYmKXlMdQjeCt8Iglc8Kkg': 'ctcv',
    'wibmRCDHRxyIJLIh21D9kA': 'vp_bundle',
    'NQK71_vlRfW83HGZskOVxw': 'ctcv',
    'pI8ujKGqR7a-Fm7v-xqs5g': 'ctcv',
    'f2qmXpW9QvSlJmdthXaiEg': 'vp_bundle',
    '_ZgdP39wRyqtr-kEqhMG_A': 'vp_bundle',
    'dacwUmowQ8KcLgq8ID4seQ': 'vp_bundle',
    'lliahuiZRheenw_QtOXIYw': 'vp_bundle',
    'jq37inBqT8C-Z4W7HKNiXw': 'ctcv',
    'ZUgS-hMLSJ6Yy04vBTXqCQ': 'cpi',
    'PzQQASIQR2eCMrb0mbQ5gQ': 'ctcv:gadex',
    'Xb98g_8wRci17y9IQbj4bQ': 'ctcv_impbid',
    'LiMMH56wRJWN9QYfnr8Pzw': 'ctcv_impbid',
    '6n0PJjj6QsS31mmZ66WkPw': 'ctcv_impbid',
    'Miv1tp-lQAux5sQUkm8rtQ': 'ctcv_impbid'
}

RT_THRESHOLD = {
    "8UqNB8jgRyWTGRPrXDHGlA": 1,
    "WqvPQ27vQaK3LHzeBLNZeA": 1,
    "v0S2yCmvQfSWzB_eRySEJg": 2,
    "F0KG-4YOS_OS1hT4HTkwXQ": 2

}

spark_sql_info = {
        # "master": "master-general.spark.appier.info",
        "master": "master-ai-general.spark.appier.info",
        "user": "external_optimization",
        "password": "iLTMP3PPs0s902Gv"
}

S3_KEY_ID = 'AKIAIQKWUKXPHVG6TO7A'
S3_CREDENTIAL = 'OdSuZDaLu1TKSoJRSguetd8nLuegN9A76WksRSqI'

INV_FEATURE_KEYS = {
    'fix_price': ['app_id', 'app_type', 'device_type', 'exchange', 'imp_size_group', 'tagid']
}